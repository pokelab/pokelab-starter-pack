        #==============================================================================
        # ■ Window_PasswordEdit
        # Pokemon Script Project - Krosk 
        # 19/08/07
        #-----------------------------------------------------------------------------
        # Scène à ne pas modifier
        #-----------------------------------------------------------------------------

        class Window_PasswordEdit < Window_Base
          attr_accessor :name                     # 名前
          attr_accessor :index                    # カーソル位置
          #--------------------------------------------------------------------------
          # ● オブジェクト初期化
          #--------------------------------------------------------------------------
          def initialize(max_char,texte)
            super(99, 27, 442, 114)
            self.contents = Bitmap.new(width - 32, height - 32)
            self.contents.font.name = $fontface
            self.contents.font.size = $fontsizebig
            self.contents.font.color = normal_color
            self.opacity = 0
            $password = ""
            @name = ""
            @max_char = max_char
            @texte = texte
            name_array = @name.split(//)[0... @max_char]
            @name = ""
            for i in 0...name_array.size
              @name += name_array[i]
            end
            @default_name = @name
            @index = name_array.size
            refresh
            update_cursor_rect
          end
          #--------------------------------------------------------------------------
          # ● リフレッシュ
          #--------------------------------------------------------------------------
          def refresh
            self.contents.clear
            
            name_array = @name.split(//)
            for i in 0... @max_char
              c = name_array[i]
              if c == nil
                c = "_"
              end
              x = 203.5 - (@max_char * 10.5) + (i * 21)
              draw_text(x, 33, 24, 44, c, 1, normal_color)
            end
            
            self.contents.font.size = $fontsize
            
            self.contents.draw_text(0, 0, 410, $fontsizebig, @texte, 1)
            self.contents.font.size = $fontsizebig
          end
          #--------------------------------------------------------------------------
          # ● カーソルの矩形更新
          #--------------------------------------------------------------------------
          def update_cursor_rect
            x = 196.5 - (@max_char * 10.5) + (@index * 21)
            if @index == @max_char
              x -= 21
            end
            self.cursor_rect.set(x, 40, 32, 32)
          end
          def back
            if @index > 0
              # 一字削除
              name_array = @name.split(//)
              @name = ""
              for i in 0...name_array.size-1
                @name += name_array[i]
              end
              @index -= 1
              refresh
              update_cursor_rect
            end
          end
          def add(character)
            if @index < @max_char and character != ""
              @name += character
              @index += 1
              refresh
              update_cursor_rect
            end
          end
          def restore_default
            @name = @default_name
            @index = @name.split(//).size
            refresh
            update_cursor_rect
          end
        end