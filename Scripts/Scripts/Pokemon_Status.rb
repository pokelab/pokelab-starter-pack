#==============================================================================
# ■ Pokemon_Status
# Pokemon Script Project - Krosk 
# 20/07/07
#-----------------------------------------------------------------------------
# Scène modifiable
#-----------------------------------------------------------------------------
# Menu statistique individuelle
#-----------------------------------------------------------------------------
# data_ext from Box: [@mode, @box_party.index]
#-----------------------------------------------------------------------------

module POKEMON_S
  class Pokemon_Status
    def initialize(pokemon, index = -1, z_level = 150, data_ext = nil, current_index = 0)
      # Index du Pokémon dans l'équipe
      # -1 signifie on ne peut naviguer dans l'index d'équipe
      @party_index = index
      @data_ext = data_ext # Elements de sauvegarde
      @pokemon = pokemon
      @index = current_index # Position du menu
      @party_order = $battle_var.in_battle ? $battle_var.battle_order : [0,1,2,3,4,5]
      @z_level = z_level
    end
    
    def main
      Graphics.freeze
      # Background
      @background = Sprite.new
      @background.z = @z_level
      
      case @index
      when 0 #Info générales
        @background.bitmap = RPG::Cache.picture("MenuAfond.png")
      when 1 #Info statistiques
        @background.bitmap = RPG::Cache.picture("MenuBfond.png")
      when 2 #Skills
        @background.bitmap = RPG::Cache.picture("MenuCfond.png")
      when 3 #Skills details
        @background.bitmap = RPG::Cache.picture("MenuDfond.png")
      end
      
      # Fenetre gauche
      @status = Window_Base.new(0 - 16, 54 - 16, 640 + 32, 426 + 32)
      @status.opacity = 0
      @status.contents = Bitmap.new(640, 426)
      # @status.contents.font.name = $fontfacebis
      # @status.contents.font.size = $fontsizebis + 5
      @status.contents.font.name = $fontface
      @status.contents.font.size = $fontsizebig
      @status.z = @z_level + 2
      @skill_index = 0
      @skill_selected = -1
      @pokemon_sprite = Sprite.new
      @pokemon_sprite.mirror = true
      @pokemon_sprite.z = @z_level + 4
      @ball_sprite = Sprite.new
      @ball_sprite.bitmap = RPG::Cache.picture(@pokemon.ball_sprite)
      @ball_sprite.x = 270
      @ball_sprite.y = 249
      @ball_sprite.z = @z_level + 4
      reset_pokemon_sprite
      
      pokemon_window_draw
      
      refresh
      # Cri du Pokémon
      if FileTest.exist?(@pokemon.cry)
        Audio.se_play(@pokemon.cry)
      end
      
      Graphics.transition
      loop do
        Graphics.update
        Input.update
        if @index == 3
          update_skill
        else
          update
        end
        if @done
          break
        end
      end
      Graphics.freeze
      @ball_sprite.dispose
      @background.dispose
      @pokemon_sprite.dispose
      @pokemon_window.dispose
      @status.dispose
    end
    
    def return_data
      return @return_data
    end
    
    def update
      # Annulation
      if Input.trigger?(Input::B)
        $game_system.se_play($data_system.cancel_se)
        if $temp_computer # PC
          # data_ext[0] = mode, @data_ext[1] = index
          @done = true
        else # Autre
          @return_data = @party_index
          @done = true
        end
        return
      end
      
      if Input.trigger?(Input::C) and @index == 2
        $game_system.se_play($data_system.decision_se)
        @index = 3
        refresh
        return
      end
      
      if Input.trigger?(Input::RIGHT)
        if @pokemon.egg
          @index = 0
          refresh
          return
        end
        $game_system.se_play($data_system.decision_se)
        @index += (@index == 2) ? 0 : 1
        refresh
        return
      end
      
      if Input.trigger?(Input::LEFT)
        if @pokemon.egg
          @index = 0
          refresh
          return
        end
        $game_system.se_play($data_system.decision_se)
        @index -= (@index == 0) ? 0 : 1
        refresh
        return
      end
      
      if Input.trigger?(Input::UP) and @party_index != -1
        Graphics.freeze
        $game_system.se_play($data_system.decision_se)
        @party_index = @party_index == 0 ? $pokemon_party.size - 1 : @party_index - 1
        change_pokemon
        Graphics.transition
        return
      end
      
      if Input.trigger?(Input::DOWN) and @party_index != -1
        Graphics.freeze
        $game_system.se_play($data_system.decision_se)
        @party_index = @party_index < $pokemon_party.size - 1 ? @party_index + 1 : 0
        change_pokemon
        Graphics.transition
        return
      end
    end
    
    def change_pokemon
      @pokemon = $pokemon_party.actors[@party_order[@party_index]]
      refresh_pokemon_window
      if @pokemon.egg
        @index = 0
      end
      refresh
      # Cri du Pokémon
      if FileTest.exist?(@pokemon.cry)
        Audio.se_play(@pokemon.cry)
      end
    end
    
    def update_skill
      if Input.trigger?(Input::B) and @skill_selected == -1
        $game_system.se_play($data_system.cancel_se)
        @index = 2
        @ball_sprite.visible = true
        @pokemon_sprite.visible = true
        @pokemon_window.visible = true
        reset_pokemon_sprite
        refresh
        return
      end
      
      if Input.trigger?(Input::DOWN)
        max = @pokemon.skills_set.length-1
        if @skill_index != max
          $game_system.se_play($data_system.cursor_se)
        end
        @skill_index += @skill_index == max ? 0 : 1
        refresh
        return
      end
      
      if Input.trigger?(Input::UP)
        if @skill_index != 0
          $game_system.se_play($data_system.cursor_se)
        end
        @skill_index -= @skill_index == 0 ? 0 : 1
        refresh
        return
      end
      
      if Input.trigger?(Input::C) and @skill_selected == -1
        $game_system.se_play($data_system.decision_se)
        @skill_selected = @skill_index
        refresh
        return
      end
      
      if Input.trigger?(Input::C) and @skill_selected != -1
        $game_system.se_play($data_system.decision_se)
        @pokemon.skills_set.switch({ @skill_selected=>@skill_index , @skill_index=>@skill_selected })
        @skill_selected = -1
        refresh
        return
      end
      
      if Input.trigger?(Input::B) and @skill_selected != -1
        $game_system.se_play($data_system.decision_se)
        @skill_selected = -1
        refresh
        return
      end
      
    end
    
    
    def refresh
      normal_color = @status.normal_color
      
      case @index
      when 0 # Infos générales
        @background.bitmap = RPG::Cache.picture("MenuAfond.png")
        @status.contents.clear
        
        @status.draw_text(440, 48, 194, $fhb, @pokemon.name, 0, normal_color)
        
        if @pokemon.egg
          @status.draw_text(440, 138, 194, $fhb, "?????", 0, normal_color)
          @status.draw_text(440, 183, 194, $fhb, "?????", 0, normal_color)
          if @pokemon.step_remaining >= 10241
            string = ["Cet œuf va sûrement", "mettre du temps à éclore."]
          elsif @pokemon.step_remaining >= 2561
            string = ["Qu'est-ce qui va en sortir ? ", "Ça va mettre du temps."]
          elsif @pokemon.step_remaining >= 1281 
            string = ["Il bouge de temps en temps.", "Il devrait bientôt éclore."]
          elsif @pokemon.step_remaining >= 1
            string = ["Il fait du bruit.", "Il va éclore !"]
          end
          @status.draw_text(26, 291, 452, $fhb, string[0], 0, normal_color)
          @status.draw_text(26, 336, 452, $fhb, string[1], 0, normal_color)
          return
        end
        
        @status.draw_text(440, 3, 194, $fhb, sprintf("%03d", @pokemon.id), 0, normal_color)
        @status.draw_text(440, 138, 194, $fhb, @pokemon.trainer_name, 0, normal_color)
        @status.draw_text(440, 183, 194, $fhb, @pokemon.trainer_id, 0, normal_color)
        @status.draw_text(440, 228, 194, $fhb, @pokemon.item_name, 0, normal_color)
        
        draw_type(437, 96, @pokemon.type1)
        draw_type(437 + 96 + 3, 96, @pokemon.type2)
        
        string = @pokemon.nature[0] + " de nature."
        @status.draw_text(26, 291, 382, $fhb, string, 0, normal_color)
      
      when 1 # Statistiques
        @background.bitmap = RPG::Cache.picture("MenuBfond.png")
        @status.contents.clear
        
        hp = @pokemon.hp > 0 ? @pokemon.hp : 0
        string = hp.to_s + "/ "+ @pokemon.maxhp_basis.to_s
        @status.draw_text(455, 3, 173, $fhb, string, 2, normal_color)
        @status.draw_text(532, 57, 96, $fhb, @pokemon.atk.to_s, 2, normal_color)
        @status.draw_text(532, 96, 96, $fhb, @pokemon.dfe.to_s, 2, normal_color)
        @status.draw_text(532, 135, 96, $fhb, @pokemon.ats.to_s, 2, normal_color)
        @status.draw_text(532, 174, 96, $fhb, @pokemon.dfs.to_s, 2, normal_color)
        @status.draw_text(532, 213, 96, $fhb, @pokemon.spd.to_s, 2, normal_color)
        @status.draw_text(212, 252, 201, $fhb, "POINTS", 0, normal_color)
        @status.draw_text(212, 291, 201, $fhb, "N. SUIVANT", 0, normal_color)
        @status.draw_text(418, 252, 213, $fhb, @pokemon.exp.to_s, 2, normal_color)
        @status.draw_text(418, 291, 213, $fhb, @pokemon.next_exp.to_s, 2, normal_color)
        @status.draw_text(209, 330, 231, $fhb, @pokemon.ability_name, 0, normal_color)
        @status.draw_text(20, 369, 603, $fhb, @pokemon.ability_descr, 0, normal_color)
        
        if @pokemon.level < 100
          level = @pokemon.next_exp.to_f / 
            (@pokemon.exp_list[@pokemon.level+1] - @pokemon.exp_list[@pokemon.level]).to_f
        else
          level = 0
        end
        draw_exp_bar(475, 336, 1.0 - level, 150)
      
      when 2 # Skills
        @background.bitmap = RPG::Cache.picture("MenuCfond.png")
        @status.contents.clear
        i = 0
        for skill in @pokemon.skills_set
          draw_type(323, 6 + 84*i, skill.type)
          @status.draw_text(422, 3 + 84 * i, 213, $fhb, skill.name, 0, normal_color)
          @status.draw_text(422, 3 + 84 * i + 36, 213, $fhb, "PP " + skill.pp.to_s + "/" + skill.ppmax.to_s, 2, normal_color)
          i += 1
        end
      when 3 # Skills détaillé
        @background.bitmap = RPG::Cache.picture("MenuDfond.png")
        @ball_sprite.visible = false
        @status.contents.clear
        @pokemon_sprite.bitmap = RPG::Cache.battler(@pokemon.icon, 0)
        @pokemon_sprite.x = 12
        @pokemon_sprite.y = 80
        draw_type(88, 45, @pokemon.type1)
        draw_type(187, 45, @pokemon.type2)
        i = 0
        for skill in @pokemon.skills_set
          if @skill_index == i
            if @skill_selected != @skill_index
              rect = Rect.new(0, 0, 303, 72)
              bitmap = RPG::Cache.picture("skillselection.png")
              @status.contents.blt(335 ,9+i*84, bitmap, rect)
            end
            if skill.power == 0 or skill.power == 1
              string = "---"
            else
              string = skill.power.to_s
            end
            @status.draw_text(159, 114, 74, $fhb, string, 1, normal_color)
            if skill.accuracy == 0
              string = "---"
            else
              string = skill.accuracy.to_s
            end
            @status.draw_text(159, 156, 74, $fhb, string, 1, normal_color)
            list = string_builder(skill.description, 16)
            for k in 0..3
              @status.draw_text(18, 240 + 42*k, 276, $fhb, list[k], 0, normal_color)
            end
          end
          if @skill_selected == i
            rect = Rect.new(0, 0, 303, 72)
            bitmap = RPG::Cache.picture("skillselected.png")
            @status.contents.blt(335 ,9+i*84, bitmap, rect)
          end
          draw_type(323, 6 + 84*i, skill.type)
          @status.draw_text(422, 3 + 84 * i, 216, $fhb, skill.name, 0, normal_color)
          @status.draw_text(422, 3 + 84 * i + 36, 213, $fhb, "PP " + skill.pp.to_s + "/" + skill.ppmax.to_s, 2, normal_color)
          i += 1
        end
      end
    end
    
    
    def pokemon_window_draw
      @pokemon_window = Window_Base.new(11-16, 60-16, 295+32, 39+32)
      @pokemon_window.opacity = 0
      @pokemon_window.z = @z_level + 6
      @pokemon_window.contents = Bitmap.new(295, 39)
      @pokemon_window.contents.font.name = $fontface
      @pokemon_window.contents.font.size = $fontsizebig
      @pokemon_window.draw_text(90, -6, 186, 39, @pokemon.given_name)
      
      if @pokemon.egg
        return
      end
      
      @pokemon_window.draw_text(9, -6, 87, 39, "N."+ @pokemon.level.to_s)
      draw_gender(276, 0, @pokemon.gender)
    end
    
    def refresh_pokemon_window
      @pokemon_window.contents.clear
      @ball_sprite.bitmap = RPG::Cache.picture(@pokemon.ball_sprite)
      @pokemon_sprite.bitmap = RPG::Cache.battler(@pokemon.battler_face, 0)
      @pokemon_window.draw_text(90, -6, 186, 39, @pokemon.given_name)
      if @pokemon.egg
        return
      end
      @pokemon_window.draw_text(9, -6, 87, 39, "N."+ @pokemon.level.to_s)
      draw_gender(276, 0, @pokemon.gender)
    end
    
    def draw_type(x, y, type)
      src_rect = Rect.new(0, 0, 96, 42)
      bitmap = RPG::Cache.picture("T" + type.to_s + ".png")
      @status.contents.blt(x, y, bitmap, src_rect, 255)
    end
    
    def draw_exp_bar(x, y, level, width)
      rect1 = Rect.new(x, y, level*width.to_i, 9)
      @status.contents.fill_rect(rect1, Color.new(255, 255, 160, 160))
    end
    
    def draw_gender(x, y, gender)
      if gender == 1
        rect = Rect.new(0, 0, 18, 33)
        bitmap = RPG::Cache.picture("Male.png")
        @pokemon_window.contents.blt(x, y, bitmap, rect, 255)
      end
      if gender == 2
        rect = Rect.new(0, 0, 18, 33)
        bitmap = RPG::Cache.picture("Female.png")
        @pokemon_window.contents.blt(x, y, bitmap, rect, 255)        
      end
    end
    
    def reset_pokemon_sprite
      @pokemon_sprite.visible = true
      @pokemon_sprite.bitmap = RPG::Cache.battler(@pokemon.battler_face, 0)
      @pokemon_sprite.x = 75
      @pokemon_sprite.y = 114
    end
    
    def string_builder(text, limit)
      length = text.length
      full1 = false
      full2 = false
      full3 = false
      full4 = false
      string1 = ""
      string2 = ""
      string3 = ""
      string4 = ""
      word = ""
      for i in 0..length
        letter = text[i..i]
        if letter != " " and i != length
          word += letter.to_s
        else
          word = word + " "
          if (string1 + word).length < limit and not(full1)
            string1 += word
            word = ""
          else
            full1 = true
          end
          
          if (string2 + word).length < limit and not(full2)
            string2 += word
            word = ""
          else
            full2 = true
          end
          
          if (string3 + word).length < limit and not(full3)
            string3 += word
            word = ""
          else
            full3 = true
          end
          
          #if (string4 + word).length < limit and not(full4)
            string4 += word
            word = ""
          #else
          #  full4 = true
          #end
        end
      end
      if string4.length > 1
        string4 = string4[0..string4.length-2]
      end
      return [string1, string2, string3, string4]
    end

  end
end