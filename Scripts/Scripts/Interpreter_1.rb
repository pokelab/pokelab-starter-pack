class Interpreter # By Tok', Arc-Arceus & Sphinx
  def changer_do(id_equipe, new_do)
    $pokemon_party.actors[id_equipe].set_do(new_do)
  end
    def changer_id(id_equipe, new_id)
    $pokemon_party.actors[id_equipe].set_id(new_id)
  end
  
  #----------------------------------------------------------------------------
  # Compléter le pokédex manuellement par ID du pkm
  #----------------------------------------------------------------------------
   def pokedex_completer_page(id)
       $data_pokedex[id] = [true, true]
    end
  #----------------------------------------------------------------------------
  # Voir un pkm manuellement par son ID
  #----------------------------------------------------------------------------
   def pokedex_vu_page(id)
       $data_pokedex[id] = [true, false]
     end
    
    #---------------------------------------------------------------------------
    # afficher_pokedex
    #   Affiche la page du pokedex
    #---------------------------------------------------------------------------
    def afficher_pokedex(id)
      $scene = POKEMON_S::Pokemon_Detail.new(id, false, 0, "map", 0)
    end    
  end
  
  