#==============================================================================
# ■ Pokemon_Battle_Core
# Pokemon Script Project - Krosk 
# 26/07/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------
# Système de Combat - Pokémon Sauvage
#   Composantes de programmation des attaques
#   Attaque
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# 0: Normal, 1: Poison, 2: Paralysé, 3: Brulé, 4:Sommeil, 5:Gelé, 8: Toxic
# @confuse (6), @flinch (7)
#-----------------------------------------------------------------------------
# 1 Normal  2 Feu  3 Eau 4 Electrique 5 Plante 6 Glace 7 Combat 8 Poison 9 Sol
# 10 Vol 11 Psy 12 Insecte 13 Roche 14 Spectre 15 Dragon 16 Acier 17 Tenebres
#-----------------------------------------------------------------------------

module POKEMON_S
  #------------------------------------------------------------  
  # Pokemon_Battle_Core
  # scene
  #------------------------------------------------------------  
  class Pokemon_Battle_Core
    #------------------------------------------------------------  
    # Détail du déroulement - Actions pré-round
    #------------------------------------------------------------    
    # Vérification saut de phase
    #     Force l'utilisateur à continuer à attaquer
    #     0x27 # Two_turn attack
    #     0x2A # Multi-turn attack
    #     0x4B # Two_turn attack
    #     0x50 # Two_turn reverse
    #     0x75 # Roulade
    #     0x9F # Brouhaha / Uproar
    #     0x1A # Patience / Bide
    #     0x1B # Mania / Thrash
    #------------------------------------------------------------            
    def phase_jump(enemy = false)
      list = [0x27, 0x4B, 0x50, 0x75, 0x9F, 0x1A, 0x1B, 0x91, 0x97, 0x9B0, 0x9B1, 0x9B2, 0x9B3]
      if not(enemy) # Actor
        for effect in @actor.effect_list
          if list.include?(effect)
            @actor_skill = $battle_var.actor_last_used
            @phase = 2
            return true
          end
        end
        return false
      elsif enemy
        for effect in @enemy.effect_list
          if list.include?(effect)
            @enemy_skill = $battle_var.enemy_last_used
            return false
          end
        end
        return true
      end
    end
    
    #------------------------------------------------------------    
    # Interdictions de switcher
    #------------------------------------------------------------        
    def switch_able(actor, enemy)
      # Arena Trap / Piege (ab)
      if enemy.ability == 71
        return false
      end
      # Effets appliqués au user empêchant la fuite
      list = [0x1A, 0x1B, 0x6A, 0x9F, 0xB5]
      for effect in actor.effect_list
        if list.include?(effect)
          return false
        end
      end
      # Effets appliqués à l'ennemi empêchant la fuite
      list = [0x2A]
      for effect in enemy.effect_list
        if list.include?(effect)
          return false
        end
      end
      # Shadow Tag / Marque Ombre (ab) // Magnepiece / Magnet Pull (ab)
      if enemy.ability == 23 or (enemy.ability == 42 and actor.type_steel?)
        draw_text("#{enemy.ability_name} de #{enemy.given_name}", "empêche le changement!")
        wait(40)
        return false
      end
      return true
    end
    
    #------------------------------------------------------------  
    # Capacité de fuite
    #------------------------------------------------------------           
    def run_able?(runner, opponent)
      x = (Integer(opponent.spd/4) / 255.0)
      rate = Integer(runner.spd*32/x)+(30*($battle_var.run_count))
      if not(flee_able(runner, opponent))
        return false
      end
      if opponent.spd <= runner.spd
        return true
      elsif x == 0
        return true
      elsif rate > 255
        return true
      elsif rand(256) <= rate
        return true
      else
        return false
      end
    end 
    
    #------------------------------------------------------------    
    # Interdictions de fuire
    #------------------------------------------------------------    
    def flee_able(actor, enemy)
      # Run away / Fuite (ab)
      if actor.ability == 50
        return true
      end
      # Arena Trap / Piege (ab)
      if enemy.ability == 71
        return false
      end
      list = [0x6A, 0xB5]
      for effect in actor.effect_list
        if list.include?(effect)
          return false
        end
      end
      # Shadow Tag / Marque Ombre (ab) // Magnepiece / Magnet Pull (ab)
      if enemy.ability == 23 or (enemy.ability == 42 and actor.type_steel?)
        draw_text("#{enemy.ability_name} de #{enemy.given_name}", "empêche la fuite!")
        wait(40)
        return false
      end
      return true
    end

    #------------------------------------------------------------    
    # Vérification saut de phase de sélection d'attaque
    #     Force l'utilisateur à utiliser la même attaque
    #------------------------------------------------------------        
    def attack_selection_jump
      for effect in @actor.effect_list
        case effect
        when 0x5A # Encore
          return true
        when 0x1A # Patience / Bide
          return true
        when 0x1B # Mania / Thrash
          return true
        end
      end
      return false
    end
    
    #------------------------------------------------------------  
    # Détail du déroulement - Round
    #------------------------------------------------------------    
    # Pré-actions , Actions
    #------------------------------------------------------------ 
    def attack_action(user, user_skill, enemy)
      #------------------------------------------------------------ 
      # Target , Sprite definition
      #------------------------------------------------------------
      # Target
      target_id = user_skill.target
      if target_id == 0 or target_id == 4 or target_id == 8 or target_id == 20 #Opposants
        target = enemy
      elsif target_id == 10
        target = user # Défini la cible comme l'acteur
      end
      
      # Assignation sprite et informations
      if user == @actor
        user_sprite = @actor_sprite
        user_status = @actor_status
        user_last_skill = $battle_var.actor_last_used
        user_last_taken_damage = $battle_var.actor_last_taken_damage
      elsif user == @enemy
        user_sprite = @enemy_sprite
        user_status = @enemy_status
        user_last_skill = $battle_var.enemy_last_used
        user_last_taken_damage = $battle_var.enemy_last_taken_damage
      end
      if target == @actor
        target_sprite = @actor_sprite
        target_status = @actor_status
        target_last_skill = $battle_var.actor_last_used
        target_last_taken_damage = $battle_var.actor_last_taken_damage
      elsif target == @enemy
        target_sprite = @enemy_sprite
        target_status = @enemy_status
        target_last_skill = $battle_var.enemy_last_used
        target_last_taken_damage = $battle_var.enemy_last_taken_damage
      end
      
      #------------------------------------------------------------ 
      # Status Check
      #------------------------------------------------------------
      # Sommeil
      if user.asleep?
        if user.sleep_check and not(user.effect_list.include?(0x9F)) # Uproar
          draw_text("#{user.given_name}", "dort!")
          status_animation(user_sprite, user.status)
          wait(40)
          # Attaques actives au sommeil
          # Sleep_talk/Snore non affecté par le sommeil
          if not([0x61, 0x5C].include?(user_skill.effect)) 
            return
          end
        else
          user_status.refresh
          draw_text("#{user.given_name}", "se réveille!")
          wait(40)
        end
      end
      
      # Frozen
      if user.frozen?
        if user.froze_check
          if user_skill.effect == 0x7D # Defrost move
            user.cure
            user_status.refresh
            draw_text("#{user.given_name}", "dégèle!")
            wait(40)
          else
            draw_text("#{user.given_name}", "est gelé!")
            status_animation(user_sprite, user.status)
            wait(40)
            return
          end
        else
          user_status.refresh
          draw_text("#{user.given_name}", "dégèle!")
          wait(40)
        end
      end
      
      # Two turn reverse
      case user_skill.effect
      when 0x50 # Two turn reverse
        if user.effect_list.include?(0x50) # Déjà attaqué
          draw_text("#{user.given_name}", "doit recharger!")
          wait(40)
          return
        end
      end
      
      # Truant / Absentéisme (ab)
      if user.ability == 54
        if user.ability_token == nil
          user.ability_token = false
        end
        if user.ability_token == true
          draw_text("#{user.given_name}", "paresse!")
          wait(40)
          return
        end
      end
      
      # Flinch
      if user.flinch?
        draw_text("#{user.given_name}", "est appeuré!")
        status_animation(user_sprite, 7)
        user.flinch_check
        wait(40)
        return
      end
      
      # Confuse
      if user.confused?
        if user.state_count > 0
          draw_text("#{user.given_name}", "est confus!")
          status_animation(user_sprite, 6)
          condition = user.confuse_check
          if condition == true
            draw_text("#{user.given_name}" +  " se blesse", "dans sa confusion.")
            damage = user.confuse_damage
            self_damage(user, user_sprite, user_status, damage)
            wait(40)
            return
          end
        elsif user.state_count == 0
          draw_text("#{user.given_name}", "n'est plus confus!")
          user.cure_state
          wait(40)
        end
      end
      
      # Paralyze
      if user.paralyzed?
        if user.paralysis_check
          draw_text("#{user.given_name} est paralysé!", "Il ne peut pas attaquer!")
          status_animation(user_sprite, user.status)
          wait(40)
          return
        end
      end
      
      #------------------------------------------------------------ 
      # Effets pré-accuracy_check et programmation des attaques
      #------------------------------------------------------------  
      pp_use = true
      jumper_end = false
      case user_skill.effect
      when 0x1A # Bide / Patience
        index = user.effect_list.index(0x1A)
        if user.effect_list.include?(0x1A) and [4,5].include?(user.status)
          user.effect.delete_at(index) # Supprime si endormi ou gelé
        end
        if not(user.effect_list.include?(0x1A))
          draw_text("#{user.given_name}", "patiente...")
          wait(40)
          turn = rand(2) + 3
          user.skill_effect(0x1A, turn, 0)
          jumper_end = true
        elsif user.effect[index][1] >= 2 # Encore en charge
          draw_text("#{user.given_name} patiente...")
          wait(40)
          pp_use = false
          jumper_end = true
        else
          pp_use = false
          # Changement de cible!
          if user == @actor
            target = @enemy
            target_sprite = @enemy_sprite
            target_status = @enemy_status
          elsif user == @enemy
            target = @actor
            target_sprite = @actor_sprite
            target_status = @actor_status
          end
        end
      when 0x1B # Thrash / Mania
        index = user.effect_list.index(0x1B)
        if user.effect_list.include?(0x1B) and [4,5].include?(user.status)
          user.effect.delete_at(index) # Supprime si endormi ou gelé
        end
        if not(user.effect_list.include?(0x1B))
          turn = rand(2) + 2
          user.skill_effect(0x1B, turn)
          pp_use = true
        else
          pp_use = false
        end
      when 0x27 # Razor wind / Coupe-Vent / Two turn atk
        if not(user.effect_list.include?(0x27)) # not(Déjà préparé)
          user.skill_effect(0x27, 2)
          draw_text("#{user.given_name} se prépare!")
          jumper_end = true
          wait(40)
        elsif user.effect_list.include?(0x27)
          pp_use = false
          index = user.effect_list.index(0x27)
          user.effect.delete_at(index)
        end
      when 0x4B # Pique/Sky attack / TTA
        if not(user.effect_list.include?(0x4B)) # not(Déjà préparé)
          user.skill_effect(0x4B, 2)
          draw_text("#{user.given_name} se concentre!")
          wait(40)
          jumper_end = true
        elsif user.effect_list.include?(0x4B)
          index = user.effect_list.index(0x4B)
          user.effect.delete_at(index)
          pp_use = false
        end
      when 0x91 # Skull Bash / Coud krane / TTA
        if not(user.effect_list.include?(0x91))
          user.skill_effect(0x91, 2)
          draw_text("#{user.given_name} se prépare!")
          wait(40)
          n = user.change_dfe(+1)
          raise_stat("DFE", user, n)
          jumper_end = true
        elsif user.effect_list.include?(0x91)
          index = user.effect_list.index(0x91)
          user.effect.delete_at(index)
          pp_use = false
        end
      when 0x97 # Solarbeam / Lance-Soleil
        if $battle_var.sunny? and not(user.effect_list.include?(0x97)) # Sunny
          # Continue et lance l'attaque
        elsif not(user.effect_list.include?(0x97))
          user.skill_effect(0x97, 2)
          draw_text("#{user.given_name} se charge!")
          wait(40)
          jumper_end = true
        elsif user.effect_list.include?(0x97)
          pp_use = false
          index = user.effect_list.index(0x97)
          user.effect.delete_at(index)
        end
      when 0x9B0 # Tunnel / Dig
        if not(user.effect_list.include?(0x9B0)) # not(Déjà préparé)
          user.skill_effect(0x9B0, 2)
          draw_text("#{user.given_name} creuse!")
          wait(40)
          jumper_end = true
        else
          index = user.effect_list.index(0x9B0)
          user.effect.delete_at(index)
          pp_use = false
        end
      when 0x9B1 # Fly / Vol
        if not(user.effect_list.include?(0x9B1)) # not(Déjà préparé)
          user.skill_effect(0x9B1, 2)
          draw_text("#{user.given_name} s'envole!")
          wait(40)
          jumper_end = true
        else
          index = user.effect_list.index(0x9B1)
          user.effect.delete_at(index)
          pp_use = false
        end
      when 0x9B2 # Rebond / Bounce
        if not(user.effect_list.include?(0x9B2)) # not(Déjà préparé)
          user.skill_effect(0x9B2, 2)
          draw_text("#{user.given_name} saute!")
          wait(40)
          jumper_end = true
        else
          index = user.effect_list.index(0x9B2)
          user.effect.delete_at(index)
          pp_use = false
        end
      when 0x9B3 # Dive / Plongée
        if not(user.effect_list.include?(0x9B3)) # not(Déjà préparé)
          user.skill_effect(0x9B3, 2)
          draw_text("#{user.given_name} plonge!")
          wait(40)
          jumper_end = true
        else
          pp_use = false 
          index = user.effect_list.index(0x9B3)
          user.effect.delete_at(index)
        end
      end
      
      # Spécial dernier skill utilisé
      if user_last_skill != nil
        case user_last_skill.effect
        when 0xB3 # Wish / Souhait
          user.skill_effect(0xB3, 1)
        end
      end
      
      for effect in user.effect
        case effect[0]
        when 0x51 # Rage / Frénésie # Fin du bonus
          if user_skill.effect != 0x51
            user.change_atk(-effect[2])
            user.effect.delete(effect)
          end
        when 0x5A # Encore
          user_skill = user.skills_set[effect[2]]
        when 0x62 # Prélevement destin / Destiny Bond # Suppression état
          index = user.effect.index(effect)
          user.effect.delete_at(index)
        when 0x75 # Rollout/roulade
          pp_use = false
        when 0x9F # Uproar / Brouhaha
          pp_use = false
        when 0x78 # Attract
          if rand(2) == 1
            draw_text("#{user.given_name}est amoureux!")
            wait(40)
            return
          end
        when 0xC2 # Grudge
          index = user.effect.index(effect)
          user.effect.delete_at(index)
        end
      end

      #------------------------------------------------------------ 
      # Utilisation de l'attaque
      #------------------------------------------------------------ 
      # PP spent
      if pp_use
        user_skill.use
        if user == @actor
          $battle_var.actor_last_used = user_skill
          user_last_skill = $battle_var.actor_last_used
        elsif user == @enemy
          $battle_var.enemy_last_used = user_skill
          user_last_skill = $battle_var.enemy_last_used
        end
      end
      
      # Pressure / Pression
      if pp_use and target.ability == 46 and target == enemy and user_skill.pp > 0
        user_skill.use
      end
      
      if jumper_end
        return
      end
      
      # Text
      draw_text("#{user.given_name} utilise", "#{user_skill.name}!")
      wait(10)
      
      # Redéfinition des derniers skills utilisés
      user_last_skill = user_skill
      
      #------------------------------------------------------------ 
      # Décision Hit Miss
      #------------------------------------------------------------       
      # Accuracy check
      n = accuracy_check(user_skill, user, target) #weather, status...
      # Accuracy stage 
      # si la cible n'est pas soi meme (10), ou OHKO (0x26), pas de vérif
      if user_skill.target != 10 or user.effect == 0x26
        n = Integer(n*accuracy_stage(user, target))
      end
      # Hit or miss
      if rand(100) < n
        hit = true
      else
        hit = false
      end
      
      multi_hit = 1 # Nombre de coups
      total_hit = 1
      
      # miss: indicateur que l'attaque touche l'adversaire mais inefficace
      #       "Mais cela échoue!"
      miss = false 
      
      #------------------------------------------------------------ 
      # Effets pré-attaque et programmation des attaques
      #------------------------------------------------------------
      #    ( zone détermination de la précision / touche ou pas )
      #------------------------------------------------------------
      # Comparaison préliminaire pour les skills "copieurs"
      
      case user_skill.effect
      when 0x09 # Mirror move / Mimique / Condition de validité
        unlist = [0x52,0x09,0x39,0x53,0x5F,0x61,0x6D,0x70,0xC7,0xB4,0x8F,0x94,0xAA,0xAD,0xB2]
        if target_last_skill == nil or user_last_taken_damage == 0
          miss = true
        elsif target_last_skill.target == 10 or unlist.include?(target_last_skill.effect)
          miss = true
        else
          user_skill = target_last_skill.clone
          user_skill.enable
          user_skill.refill
        end
      when 0x52 # Copie / Mimic
        list = [102, 166, 118, 165]
        for skill in user.skills_set
          list.push(skill.id)
        end
        # Conditions de validité: ne copie pas la luste ni lorsqu'il y a Morphing
        if target_last_skill == nil
          miss = true
        elsif list.include?(target_last_skill.id) or user.effect_list.include?(0x39)
          miss = true
        end
      when 0x53 # Metronome / Condition de validité
        list = [0x53,0x52,0x59,0x90,0x6F,0x74,0x62,0x61,0x69,0xAC,0xC3,0xB0,0xB1,0xAA]
        until not(list.include?(user_skill.effect) or user_skill.id == 165) # Tant que le skill copié est interdit
          user_skill = POKEMON_S::Skill.new(rand($data_skills_pokemon.length)+1)
        end
      when 0x61 # Sleep talk
        list = []
        for i in 0..@target.skills_set.size - 1
          list.push(i)
        end
        list.shuffle!
        
        copied_skill = target.skills_set[list.shift]
        until copied_skill.usable? and not([0x1A, 0x61, 0xB4, 0x09, 0x52, 0xAA, 0x9F, 0x27, 0x4B, 0x91, 0x97, 0x9B].include?(copied_skill.effect))
          copied_skill = target.skills_set[list.shift]
        end
        user_skill = copied_skill
      when 0xB4 # Assist / Assitance
        no = [0x53,0x52,0x59,0x90,0x6F,0x74,0x62,0x61,0x69,0xAC,0xC3,0xB0,0xB1,0xAA,0xB4]
        list = []
        copied_skill = user_skill
        if user == @actor
          for pokemon in @party.actors
            for skill in pokemon.skills_set
              list.push(skill.clone)
            end
          end
          list.shuffle!
          copied_skill = list.shift
          until not(no.include?(copied_skill)) # Jusqu'a que le skill copié est interdit
            copied_skill = list.shift
          end
        elsif user == @enemy
          for skill in user.skills_set
            list.push(skill.clone)
          end
          list.shuffle!
          copied_skill = list.shift
          until not(no.include?(copied_skill)) # Jusqu'à que le skill copié est interdit
            copied_skill = list.shift
          end
        end
        user_skill = copied_skill
      end

      case user_skill.effect
      # --------- ---------- --------
      #    Conditions de touche
      # --------- ---------- --------
      when 0x11 # Hit without fail
        hit = true
      when 0x4D # Fatal Throw / Corps perdu #Hit without fail
        hit = true
      when 0x98 # Thunder
        if $battle_var.rain?
          hit = true
        end
      when 0x1D # Random Multi_hit
        number = rand(8)
        case number
        when 0
          multi_hit = 5
        when 1
          multi_hit = 4
        end
        if [2,3,4].include?(number)
          multi_hit = 3
        elsif [5,6,7].include?(number)
          multi_hit = 2
        end
        total_hit = multi_hit
      when 0x2C # Double Strike
        multi_hit = 2
        total_hit = 2
      when 0x4D # Twineedle / Double Dard
        multi_hit = 2
        total_hit = 2
      when 0x68 # Triple pied / Triple Kick
        multi_hit = 3
        total_hit = 3
      # --------- ---------- --------
      #    Conditions de foirage
      # --------- ---------- -------- 
      when 0x07 # Self destruct et Damp / Moiteur (ab)
        if target.ability == 6
          draw_text("#{target.ability_name} de #{target.given_name}", "empêche l'auto-destruction.")
          wait(40)
          return
        end
        if user.ability == 6
          draw_text("#{user.ability_name} de #{user.given_name}", "empêche l'auto-destruction.")
          wait(40)
          return
        end
      when 0x20 # Heal 1/2
        if user.hp == user.max_hp
          miss = true
        end
      when 0x23 # Light screen / Mur Lumière
        if user.effect_list.include?(0x23)
          miss = true
        end
      when 0x25
        if user.hp == user.max_hp or user.asleep?
          miss = true
        end
      when 0x2A
        if user.effect_list.include?(0x2A)
          wait(40)
          miss = true
          draw_text("#{target.given_name} est déjà pris", "au piège !")
     wait(40)
    end  
      when 0x2E # Mist
        if user.effect_list.include?(0x2E)
          miss = true
        end
      when 0x41 # Reflect/ Reflet
        if user.effect_list.include?(0x41)
          miss = true
        end
      when 0x5C # Ronflement / Snore
        if not(user.asleep?)
          miss = true
        end
      when 0x7C # Rune Protect / Safeguard
        if user.effect_list.include?(0x7C)
          miss = true
        end
      when 0x39 # Transform / Morphing
        if target.effect_list.include?(0x39)
          miss = true
        end
      when 0x54 # Vampigraine / Leech seed # Fail if grass or already infected
        if [target.type1, target.type2].include?(5)
          miss = true
        end
        if user.effect_list.include?(0x54)
          miss = true
        end
      when 0x56 # Entrave / Disable # Conditions de miss
        # Skill invalide ou non possédé
        if target_last_skill == nil or target.skills_set.index(target_last_skill) == nil
          miss = true
        elsif target.effect_list.include?(0x56) or target_last_skill.pp == 0
          miss = true
        end
      when 0x59 # Riposte / Counter # Conditions de miss
        if target_last_skill == nil or user_last_taken_damage == 0
          miss = true
        elsif not(target_last_skill.physical)
          miss = true
        end
      when 0x64 # Depit / Spite
        if target_last_skill != nil
          if target_last_skill.pp <= 1 or target.skills_set.include?(target_last_skill)
            miss = true
          end
        else
          miss = true
        end
      when 0x6A # Regard noir / Mean Look
        if target.effect_list.include?(0x6A)
          miss = true
        end
      when 0x6B # Nightmare
        if not(target.asleep?) or target.effect_list.include?(0x6B)
          miss = true
        end
      when 0x6D # Curse / Malédiction
        if target.effect_list.include?(0x6D)
          miss = true
        end
      when 0x6F # Abri / Detection
        if user.effect_list.include?(0x6F)
          index = user.effect_list.index(0x6F)
          used = user.effect[index][2] + 1
          rate = rand(2**16)
          if rate > (2**16) / used
            miss = true
          end
        end
      when 0x74 # Endure / Tenacité
        if user.effect_list.include?(0x74)
          index = user.effect_list.index(0x74)
          used = user.effect[index][2] + 1
          rate = rand(2**16)
          if rate > (2**16) / used
            miss = true
          end
        end
      when 0x78 # Attract
        if target.effect_list.include?(0x78) or (target.gender + user.gender) != 3
          miss = true
        end
        if target.ability == 12 # Benet / Oblivious (ab)
          draw_text("#{target.ability_name} de #{target.given_name}", "l'empêche d'être amoureux.")
          wait(40)
          return
        end
      when 0x90 # Voile miroir / Mirror Coat # Conditions de miss
        if not(target_last_skill.special) or user_last_taken_damage == 0
          miss = true
        end  
      when 0x5A # Encore
        if target_last_skill == nil
          miss = true
        elsif target_last_skill.pp == 0 or 
            not(target.skills_set.include?(target_last_skill)) or
            [165, 227, 119].include?(target_last_skill.id)
          miss = true
        elsif target.effect_list.include?(0x5A) or user.effect_list.include?(0x5A)
          miss = true
        end
      when 0x5F # Gribouille / Sketch
        if [166, 165].include?(target_last_skill.id) or
            user.skills.include?(target_last_skill.id)
          miss = true
        end
      when 0x8E # Cognobidon / Belly drum
        if user.hp <= (user.max_hp / 2) or user.atk_stage >= 6
          miss = true
        end
      when 0xA5 # Torment / Tourmente
        index = target.skills_set.index(target_last_skill)
        if index == nil
          miss = true
        end
      when 0xBA # Brick Break
        if hit
          target.effect.delete(target.effect.rassoc("0x41"))
          target.effect.delete(target.effect.rassoc("0x23"))
        end
      when 0xBB # Yawn
        if target.status != 0 or target.effect_list.include?(0x7C)
          miss = true
        end
      when 0xB5
        if user.effect_list.include?(0xB5)
          miss = true
        end
      when 0xB1 # Trick
        if user.item_hold == 0 and target.item_hold == 0
          miss = true
        end
      when 0x1E # Conversion / Adaptation
        # Si il existe un skill qui est valide
        list = []
        for skill in user.skills_set
          list.push(skill.type)
        end
        list.delete(user.type1)
        list.delete(user.type2)
        list.delete(0) # Curse / Malédiction
        if list.length == 0
          miss = true
        end
      when 0xB3 # Wish / Souhait
        if user.effect_list.include?(0xB3)
          miss = true
        end
      when 0xC9 # Mud Sport
        if user.effect_list.include?(0xC9)
          miss = true
        end
      when 0xD2 # Water Sport
        if user.effect_list.include?(0xD2)
          miss = true
        end
      when 0x99 # Teleport
        # Arena Trap / Piege (ab) // Run away / Fuite (ab)
        if enemy.ability == 71 and user.ability != 50
          draw_text("#{target.ability_name} de #{target.given_name}", "empêche la fuite.")
          wait(40)
          return
        end
        # Magnet Pull / Magnepiege (ab) // Run away / Fuite (ab)
        if enemy.ability == 42 and user.type_steel? and user.ability != 50
          draw_text("#{enemy.ability_name} de #{enemy.given_name}", "empêche la fuite.")
          wait(40)
          return
        end
        # Shadow Tag / Marque ombre (ab) // Run away / Fuite (ab)
        if enemy.ability == 23 and user.ability != 50
          draw_text("#{enemy.ability_name} de #{enemy.given_name}", "empêche la fuite.")
          wait(40)
          return
        end
      end
      
      #------------------------------------------------------------ 
      # Attaque
      #------------------------------------------------------------ 
      # 
      #------------------------------------------------------------ 
      # Multi_hit
      until multi_hit == 0
        multi_hit -= 1

        #------------------------------------------------------------ 
        # Programmation des attaques et effets de statut
        #------------------------------------------------------------ 
        critical_special = 0
        # Skill Effect
        case user_skill.effect
        when 0x2B # Good chance of critical_hit
          critical_special = 1
        when 0x4B # Pique/Sky attack / TTA
          critical_special = 1
        when 0x68 # Triple Kick / Triple Pied
          if not(user.effect_list.include?(0x68))
            user.skill_effect(0x68, 1, 0)
          else
            index = user.effect_list.index(0x68)
            user.effect[index][2] += 1
            # Accuracy check à chaque coup
            n = accuracy_check(user_skill, user, target)
            n = Integer(n*accuracy_stage(user, target))
            if rand(100) <= n
              hit = true
            else
              hit = false
            end
          end
        when 0xC8 # Blaze Kick
          critical_special = 1
        when 0xD1 # Poison Tail
          critical_special = 1
        end
        
        # User effect
        for effect in user.effect
          case effect[0]
          when 0x2F # Focus energy
            critical_special = 2
          when 0xA5 # Torment
            index = user.skills_set.index(user_last_skill)
            user.skills_set[index].disable
            
            skill_index = effect[2]
            user.skills_set[skill_index].enable
            
            # new index = index du skill bloqué
            new_index = user.skills_set.index(user_last_skill)
            effect[2] = new_index
          end
        end
        
        #------------------------------------------------------------ 
        # Détermination des dommages
        #------------------------------------------------------------
        info = damage_calculation(user_skill, user, target, critical_special)
        
        # Animation d'attaque
        attack_animation(info, hit, miss, user, user_skill, user_sprite, target_sprite)
        
        # Invincibilité
        if $game_temp.god_mode
          hit = false
        end
        
        if hit and not(miss)
          
          # Damage deal to target
          damage = info[0]
          efficiency = info[2]
          
          #------------------------------------------------------------ 
          # Effets attaque et programmation des attaques
          #------------------------------------------------------------ 
          case user_skill.effect
          # ----------------- -------------- ---------------
          #     Dommages Pré-déterminés / Utilisateur
          # ----------------- -------------- ---------------
          when 0x26 # OHKO
            if damage > 0 # Affecté par les immunités
              damage = target.hp
            end
          when 0x28 # Super Fang
            if damage > 0 # Affecté par les immunités
              damage = target.hp / 2
            end
          when 0x29 # Draco-rage
            if damage > 0
              damage = 40
            end
          when 0x57 # Frappe Atlas
            if damage > 0
              damage = user.level
            end
          when 0x58 # Vague Psy
            if damage > 0
              damage = Integer( user.level * (rand(11) * 10 + 50) / 100 )
            end
          when 0x59 # Counter / Riposte
            if damage > 0
              damage = user_last_taken_damage * 2
            end
          when 0x90 # Voile Miroir / Mirror Coat
            if damage > 0
              damage = user_last_taken_damage * 2
            end  
          when 0x65 # False Swipe
            if target.hp - damage <= 0
              damage = target.hp - 1
            end
          when 0x7A # Cadeau . gift
            number = rand(256)
            gift = false
            if number < 26
              damage = 120
            elsif number < 102
              damage = 80
            elsif number < 204
              damage = 40
            else
              damage = 0
              gift = true
            end
          when 0x82 # Sonic boom
            if damage > 0
              damage = 20
            end
          when 0xBD # Endeavor
            if damage > 0
              damage = target.hp - user.hp
            end
          when 0x1A # Bide / Patience
            if damage > 0
              index = user.effect_list.index(0x1A)
              damage = user.effect[index][2] * 2
            end
          end
          
          # ----------------- -------------- ---------------
          #      Dommages Pré-déterminés / Adversaire
          # ----------------- -------------- ---------------          
          for effect in target.effect_list
            case effect
            when 0x6F # Abri / Detection
              index = target.effect_list.index(0x6F)
              ind = target.effect[index][1]
              if damage > 0 and ind == 2
                damage = 0
                draw_text("#{target.given_name}", "est protégé!")
                wait(40)
                return
              end
            when 0x74 # Endure/Tenacité
              index = target.effect_list.index(0x74)
              ind = target.effect[index][1]
              if target.hp - damage <= 0 and ind == 2
                damage = target.hp - 1
                draw_text("#{target.given_name}tient le coup!")
                wait(40)
              end
            end
          end
          
          # ----------------- -------------- ---------------
          #                 Capacités spéciales
          # ----------------- -------------- ---------------          
          case target.ability
          when 10 # Absorb Volt / Volt Absorb (ab)
            if user_skill.type_electric? and damage > 0
              damage = 0
              bonus = target.max_hp / 4
              heal(target, target_sprite, target_status, bonus)
              draw_text("#{target.ability_name} de #{target.given_name}", "restaure les PV.")
              wait(40)
            end
          when 11 # Absorb Eau / Water Absorb (ab)
            if user_skill.type_water? and damage > 0
              damage = 0
              bonus = target.max_hp / 4
              heal(target, target_sprite, target_status, bonus)
              draw_text("#{target.ability_name} de #{target.given_name}", "restaure les PV.")
              wait(40)
            end
          when 18 # Torche / Flash Fire (ab)
            if user_skill.type_fire? and not(target.ability_active)
              damage = 0
              target.ability_active = true
              draw_text("#{target.ability_name} de #{target.given_name}", "s'active!")
              wait(40)
            end
          when 25 # Garde Mystik / Wonder Guard (ab)
            if efficiency == 1
              damage = 0
              draw_text("#{target.ability_name} de #{target.given_name}", "le protège!")
              wait(40)
            end
          when 26 # Levitate (ab)
            if user_skill.type_ground?
              damage = 0
              draw_text("#{target.ability_name} de #{target.given_name}", "le protège!")
              wait(40)
            end
          end
          
          effective_damage = 0
          # Dommages
          for i in 1..damage
            effective_damage += 1
            target.remove_hp(1)
            target_status.refresh
            if target.max_hp >= 144 and effective_damage % (target.max_hp / 144 + 1) != 0
              next
            end
            Graphics.update
            Graphics.update
            if target.dead?
              # Loyauté
              target.drop_loyalty
              break
            end
          end
          damage = effective_damage
          
          # Redéfinition des derniers points de dommage infligés
          if user == @actor #and damage != 0
            $battle_var.enemy_last_taken_damage = damage
          elsif user == @enemy #and damage != 0
            $battle_var.actor_last_taken_damage = damage
          end
          
          # Multi_hit
          if target.dead?
            total_hit -= multi_hit
            multi_hit = 0
          end
          
          #------------------------------------------------------------ 
          # Effets post-attaque et programmation des attaques
          #------------------------------------------------------------ 
          # Effectiveness
          
          if multi_hit == 0
            post_attack(info, damage, user_skill.power)
          end
          
          # --------------------------- ---------------------------------
          # Détermination de l'application de l'effet supplémentaire
          # --------------------------- ---------------------------------
          ec_rate = rand(101)
          effect_applied = ec_rate <= user_skill.effect_chance
          
          # Serene Grace / Sérenité (ab)
          if user.ability == 32
            effect_applied = ec_rate <= (user_skill.effect_chance * 2)
          end
          
          # --------------------------- ---------------------------------
          # Prise en compte du pourcentage de chance que l'effet s'active
          #                Si l'ennemi est mort ou pas mort
          # --------------------------- ---------------------------------
          if ((user_skill.effect_chance != 0 and effect_applied) or 
              user_skill.effect_chance == 0)
            case user_skill.effect
            # ----------------- -------------- -------------------
            #  Dommages/Récupération Post-attaques / Utilisateur
            # ----------------- -------------- ------------------
            when 0x07 # Self_destruct
              rec_damage = user.hp
              self_damage(user, user_sprite, user_status, rec_damage)
              draw_text("#{user.given_name}", "se sacrifie.")
              wait(40)
            when 0x08 # Devoreve / Dream eater
              bonus = damage / 2
              if damage > 0 and bonus == 0
                bonus = 1
              end
              if target.ability != 64 # Liquid ooze / Suintement (ab)
                heal(user, user_sprite, user_status, bonus)
                draw_text("L'énergie du #{target.given_name}", "est draînée.")
                wait(40)
              else
                self_damage(user, user_sprite, user_status, bonus)
                draw_text("#{target.ability_name} de #{target.given_name}", "réduit les PV.")
                wait(40)
              end
            when 0x30 # Recoil
              recoil_damage = damage / 4
              if damage > 0 and recoil_damage == 0
                recoil_damage = 1
              end
              # Head Rock / Tete de rock (ab) // Struggle
              if user.ability != 69 or user_skill.id == 165 
                self_damage(user, user_sprite, user_status, recoil_damage)
                draw_text("#{user.given_name} se blesse", "en frappant.")
                wait(40)
              end  
            when 0xC6 # Recoil
              recoil_damage = damage / 3
              if damage > 0 and recoil_damage == 0
                recoil_damage = 1
              end
              # Head Rock / Tete de rock (ab) // Struggle
              if user.ability != 69 or user_skill.id == 165
                self_damage(user, user_sprite, user_status, recoil_damage)
                draw_text("#{user.given_name} se blesse", "en frappant.")
                wait(40)
              end
            when 0x03 # Leech
              bonus = damage / 2
              if damage > 0 and bonus == 0
                bonus = 1
              end
              heal(user, user_sprite, user_status, bonus)
              draw_text("L'énergie du #{target.given_name}", "est draînée.")
              wait(40)
            when 0x20 # Heal
              bonus = user.max_hp / 2
              heal(user, user_sprite, user_status, bonus)
              draw_text("#{"#{user.given_name}"}", "se soigne!")
              wait(40)
            when 0x25 # Rest / Repos
              status_check(target, 4, true)
              target_status.refresh
              target.status_count = 3 # Dort pour 2 tours seulement
              bonus = user.max_hp
              heal(user, user_sprite, user_status, bonus)
              draw_text("#{user.given_name} a regagné", "son énergie.")
              wait(40)
            when 0x5B # Balance / Pain Split
              middle = (user.hp + target.hp)/2
              user_diff = middle - user.hp
              heal(user, user_sprite, user_status, user_diff)
              target_diff = middle - target.hp
              heal(target, target_sprite, target_status, target_diff)
            when 0x7A # Cadeau
              if gift
                bonus = target.hp / 4
                heal(target, target_sprite, target_status, bonus)
              end
              draw_text("Surprise!")
              wait(40)
            when 0x84 # Morning sun / Aurore
              weather = $battle_var.weather[0]
              case weather
              when 1 # Rainy
                bonus = user.max_hp / 4
              when 2 # Sunny
                bonus = user.max_hp * 2 / 3
              when 3 #Sandstorm
                bonus = user.max_hp / 4
              when 4 # Hail
                bonus = user.max_hp / 4
              when 0
                bonus = user.max_hp / 2
              end
              heal(user, user_sprite, user_status, bonus)
            when 0x85 # Synthesis / Synthese
              weather = $battle_var.weather[0]
              case weather
              when 1 # Rainy
                bonus = user.max_hp / 4
              when 2 # Sunny
                bonus = user.max_hp * 2 / 3
              when 3 #Sandstorm
                bonus = user.max_hp / 4
              when 4 # Hail
                bonus = user.max_hp / 4
              when 0
                bonus = user.max_hp / 2
              end
              heal(user, user_sprite, user_status, bonus)
            when 0x86 # Rayon Lune
              weather = $battle_var.weather[0]
              case weather
              when 1 # Rainy
                bonus = user.max_hp / 4
              when 2 # Sunny
                bonus = user.max_hp * 2 / 3
              when 3 #Sandstorm
                bonus = user.max_hp / 4
              when 4 # Hail
                bonus = user.max_hp / 4
              when 0
                bonus = user.max_hp / 2
              end
              heal(user, user_sprite, user_status, bonus)
            when 0x8E # Belly drum / Cognobidon
              damage = user.max_hp / 2
              self_damage(user, user_sprite, user_status, damage)
            when 0xA2 # Avale
              bonus = 0
              if user.effect_list.include?(0xA0)
                index = user.effect_list.index(0xA0)
                case user.effect[index][2]
                when 1
                  bonus = user.max_hp / 4
                when 2
                  bonus = user.max_hp / 2
                when 3
                  bonus = user.max_hp
                end
              end
              heal(user, user_sprite, user_status, bonus)
            # ----------------- -------------- -------------------
            #                  Autres
            # ----------------- -------------- ------------------  
            when 0x26 # OHKO
              draw_text("K.O. en un coup!")
              wait(40)
            when 0x50 # Two_turn reverse
              user.skill_effect(0x50, 2)
            when 0x1D # Multihit
              if multi_hit == 0
                draw_text("#{target.given_name}", "est touché #{total_hit.to_s} fois!")
                wait(40)
              else
                draw_text("... #{(total_hit - multi_hit).to_s} fois...") 
              end
            end
          end
          
          faint_check(user)
          
          # --------------------------- ---------------------------------
          # Prise en compte du pourcentage de chance que l'effet s'active
          #                     Si l'ennemi n'est pas mort
          # --------------------------- ---------------------------------
          if ((user_skill.effect_chance != 0 and effect_applied) or 
              user_skill.effect_chance == 0) and not(target.dead?) and 
              (user_skill.power == 0 or (user_skill.power > 0 and efficiency != -2)) and # Cas d'une attaque dégat sans être efficace
              not(target.ability == 19 and user_skill.effect_chance > 0) # Ecran Poudre / Shield Dust (ab)
            # ----------------- -------------- ---------------
            #                     Utilisateur
            # ----------------- -------------- ---------------              
            case user_skill.effect
            # ----------------- -------------- ---------------
            #               Stage Effect
            # ----------------- -------------- ---------------
            when 0x0A
              n = target.change_atk(+1)
              raise_stat("ATK", target, n)
            when 0x0B
              n = target.change_dfe(+1)
              raise_stat("DFE", target, n)
            when 0x0D
              n = target.change_ats(+1)
              raise_stat("ATS", target, n)
            when 0x10
              n = target.change_eva(+1)
              raise_stat("EVA", target, n)
            when 0x12
              n = target.change_atk(-1)
              reduce_stat("ATK", target, n)
            when 0x13
              n = target.change_dfe(-1)
              reduce_stat("DFE", target, n)
            when 0x14
              n = target.change_spd(-1)
              reduce_stat("SPD", target, n)
            when 0x17
              n = target.change_acc(-1)
              reduce_stat("ACC", target, n)
            when 0x18
              n = target.change_eva(-1)
              reduce_stat("EVA", target, n)
            when 0x32
              n = target.change_atk(+2)
              raise_stat("ATK", target, n)
            when 0x33
              n = target.change_dfe(+2)
              raise_stat("DFE", target, n)
            when 0x34
              n = target.change_spd(+2)
              raise_stat("SPD", target, n)
            when 0x35
              n = target.change_ats(+2)
              raise_stat("ATS", target, n)
            when 0x36
              n = target.change_dfs(+2)
              raise_stat("DFS", target, n)
            when 0x3A
              n = target.change_atk(-2)
              reduce_stat("ATK", target, n)
            when 0x3B
              n = target.change_dfe(-2)
              reduce_stat("DFE", target, n)
            when 0x3C
              n = target.change_spd(-2)
              reduce_stat("SPD", target, n)
            when 0x3E
              n = target.change_dfs(-2)
              reduce_stat("DFS", target, n)
            when 0x44
              n = target.change_atk(-1)
              reduce_stat("ATK", target, n)
            when 0x45
              n = target.change_dfe(-1)
              reduce_stat("DFE", target, n)
            when 0x46
              n = target.change_spd(-1)
              reduce_stat("SPD", target, n)
            when 0x47
              n = target.change_ats(-1)
              reduce_stat("ATS", target, n)
            when 0x48
              n = target.change_dfs(-1)
              reduce_stat("DFS", target, n)
            when 0x49
              n = target.change_acc(-1)
              reduce_stat("ACC", target, n)  
            when 0x6C
              target.skill_effect(0x6C)
              draw_text("#{target.given_name} est tout", "petit!")
              wait(40)
              n = target.change_eva(+1)
              raise_stat("EVA", target, n)
            when 0x76 # Swagger
              n = target.change_atk(+2)
              raise_stat("ATK", target, n)
              status_check(target, 6)
              target_status.refresh
            when 0xA6 # Flatter
              n = target.change_ats(+1)
              raise_stat("ATS", target, n)
              status_check(target, 6)
              target_status.refresh
            when 0x8A 
              n = user.change_dfe(+1)
              raise_stat("DFE", user, n)
            when 0x8B
              n = user.change_atk(+1)
              raise_stat("ATK", user, n)
            when 0x8C # Ancient power
              n = user.change_atk(+1)
              raise_stat("ATK", user, n)
              n = user.change_dfe(+1)
              raise_stat("DFE", user, n)
              n = user.change_spd(+1)
              raise_stat("SPD", user, n)
              n = user.change_ats(+1)
              raise_stat("ATS", user, n)
              n = user.change_dfs(+1)
              raise_stat("DFS", user, n)
            when 0x8E # Belly / Drum Cognobion
              n = user.change_atk(+12)
              raise_stat("ATK", user, n)
            when 0xA8 # Memento
              n = target.change_atk(-2)
              reduce_stat("ATK", target, n)
              n = target.change_ats(-2)
              reduce_stat("ATS", target, n)
              damage = user.hp
              self_damage(user, user_sprite, user_status, damage)
            when 0xB6 # Superpower
              n = user.change_atk(-1)
              reduce_stat("ATK", user, n, true)
              n = user.change_dfe(-1)
              reduce_stat("DFE", user, n, true)
            when 0xCC # Overheat / Psycho Boost
              n = user.change_ats(-2)
              reduce_stat("ATS", user, n, true)
            when 0xCD # Tickle
              n = target.change_atk(-1)
              reduce_stat("ATK", target, n)
              n = target.change_dfe(-1)
              reduce_stat("DFE", target, n)
            when 0xCE # Cosmic Power
              n = user.change_dfe(+1)
              raise_stat("DFE", user, n)
              n = user.change_dfs(+1)
              raise_stat("DFS", user, n)
            when 0xD0 # Bulk Up
              n = user.change_atk(+1)
              raise_stat("ATK", user, n)
              n = user.change_dfe(+1)
              raise_stat("DFE", user, n)
            when 0xD3 # Calm Mind
              n = user.change_ats(+1)
              raise_stat("ATS", user, n)
              n = user.change_dfs(+1)
              raise_stat("DFS", user, n)
            when 0xD4 # Dragon Dance
              n = user.change_atk(+1)
              raise_stat("ATK", user, n)
              n = user.change_spd(+1)
              raise_stat("SPD", user, n)
            when 0x8F # Psych Up / Boost
              user.atk_stage = target.atk_stage
              user.dfe_stage = target.dfe_stage
              user.spd_stage = target.spd_stage
              user.ats_stage = target.ats_stage
              user.dfs_stage = target.dfs_stage
              user.acc_stage = target.acc_stage
              user.eva_stage = target.eva_stage
              draw_text("#{user.given_name} copie les stats", "de "+ "#{target.given_name}"+"!")
              wait(40)
            when 0x6D # Malédiction  / Curse
              if not(user.type_ghost?) # Non-ghost
                n = user.change_spd(-1)
                reduce_stat("SPD", user, n, true)
                n = user.change_atk(+1)
                raise_stat("ATK", user, n)
                n = user.change_dfe(+1)
                raise_stat("DFE", user, n)
              elsif user.type_ghost?
                draw_text("#{user.given_name} maudit", "#{target.given_name}!")
                wait(40)
                damage = user.max_hp / 2
                self_damage(user, user_sprite, user_status, damage)
                target.skill_effect(0x6D)
              end
            when 0x19 # Haze / Buée Noire
              user.reset_stat_stage
              enemy.reset_stat_stage
              raise_stat(0, target)
            # ----------------- -------------- ---------------
            #               Déroulement du combat
            # ----------------- -------------- ---------------                
            when 0x1C # Expulsion du combat
              if target.ability == 21 # Ventouse / Suction cups (ab)
                draw_text("#{target.ability_name} de #{target.given_name}", "l'empêche d'être expulsé.")
                wait(40)
              else
                if target == @enemy
                  end_battle_flee_enemy(true)
                elsif target == @actor
                  end_battle_flee(true)
                end
              end
            when 0x73 # Weather / sandstorm
              $battle_var.set_sandstorm(5)
              draw_text("Une tempête de sable", "se lève...")
              animation = $data_animations[494]
              @actor_sprite.animation(animation, true)
              loop do
                @actor_sprite.update
                Graphics.update
                Input.update
                if not(@actor_sprite.effect?)
                  break
                end
              end
              wait(20)
            when 0x88 # Weather / Rain dance
              $battle_var.set_rain(5)
              draw_text("Il commence à pleuvoir.")
              animation = $data_animations[493]
              @actor_sprite.animation(animation, true)
              loop do
                @actor_sprite.update
                Graphics.update
                Input.update
                if not(@actor_sprite.effect?)
                  break
                end
              end
              wait(20)
            when 0x89 # weather / sunny
              $battle_var.set_sunny(5)
              draw_text("Le soleil brille.")
              animation = $data_animations[492]
              @actor_sprite.animation(animation, true)
              loop do
                @actor_sprite.update
                Graphics.update
                Input.update
                if not(@actor_sprite.effect?)
                  break
                end
              end
              wait(20)
            when 0xA4 # Hail / Grele
              $battle_var.set_hail(5)
              draw_text("Il commence à grêler.")
              animation = $data_animations[495]
              @actor_sprite.animation(animation, true)
              loop do
                @actor_sprite.update
                Graphics.update
                Input.update
                if not(@actor_sprite.effect?)
                  break
                end
              end
              wait(20)
            when 0x99 # Teleport
              if user == @actor
                end_battle_flee
              else
                end_battle_flee_enemy
              end
            # ----------------- -------------- ---------------
            #             Attaques particulières
            # ----------------- -------------- --------------- 
            
            when 0x55 # Trempette
              draw_text("Mais rien ne se passe...")
              wait(40)
            when 0x64 # Dépit/Spite
              amount = rand(4)+2
              string1 = " a perdu #{amount.to_s} PP!"
              draw_text(target_last_used.name, string1)
              wait(40)
              target_last_used.pp -= amount
            when 0x7E # Magnitude / Ampleur
              draw_text("Séisme de magnitude...", "#{info[3].to_s}!")
              wait(40)
            when 0x69 # Thief / Larcin
              if target.ability == 60
                draw_text("#{target.ability_name} de #{target.given_name}", "empêche le vol d'objet!")
                wait(40)
              elsif user.item_hold == 0 and target.item_hold != 0
                user.item_hold = target.item_hold
                target.item_hold = 0
                draw_text("#{user.given_name} vole ", "#{user.item_name}!")
                wait(40)
              end
            when 0xB1 # Trick
              if target.ability == 60
                draw_text("#{target.ability_name} de #{target.given_name}", "empêche le vol d'objet!")
                wait(40)
              elsif not(user.item_hold == 0 and target.item_hold == 0)
                hold = user.item_hold
                user.item_hold = target.item_hold
                target.item_hold = hold
                if user.item_hold == 0
                  string = "et ne reçoit rien!"
                else
                  string = "et reçoit #{user.item_name}!"
                end
                draw_text("#{user.given_name} échange les objets", string)
                wait(40)
              end
            when 0xBC # Knock Off
              if target.hp > 0 and target.item_hold != 0
                draw_text("#{target.given_name} lâche ", Item.name(target.item_hold) + "!")
                target.item_hold = 0
                wait(40)
              end
            when 0x22 # Pay Day / Jackpot
              draw_text("#{target.given_name} lâche de l'argent!")
              $battle_var.add_money(5*target.level)
            # ----------------- -------------- ---------------
            #             Modificateur de status
            # ----------------- -------------- ---------------
            # 0: Normal, 1: Poison, 2: Paralysé, 3: Brulé, 
            # 4:Sommeil, 5:Gelé;    6: Confus,   7: Flinch, 8:Toxic
            when 0x01 # Sleep
              status_check(target, 4)
              target_status.refresh
            when 0x02 # Poison
              status_check(target, 1)
              target_status.refresh
            when 0x04 # Burn
              status_check(target, 3)
              target_status.refresh
            when 0x05 # Freeze
              status_check(target, 5)
              target_status.refresh
            when 0x06 # Paralyze
              status_check(target, 2)
              target_status.refresh
            when 0x1F # Flinch
              status_check(target, 7)
              target_status.refresh
            when 0x21 # Toxic
              status_check(target, 8)
              target_status.refresh
            when 0xCA # Toxic
              status_check(target, 8)
              target_status.refresh
            when 0x31 # Confuse
              status_check(target, 6)
              target_status.refresh
            when 0xC7 # Confuse
              status_check(target, 6)
              target_status.refresh
            when 0x24 # Triplattaque
              status_check(target, [2,3,5][rand(3)])              
              target_status.refresh
            when 0x42 # Poison (sur)
              status_check(target, 1)
              target_status.refresh
            when 0x43 # Paralyze (affected by immunity)
              status_check(target, 2)
              target_status.refresh
            when 0x4C # Confuse
              status_check(target, 6)
              target_status.refresh
            when 0x4D # Poison/Double dar
              status_check(target, 1)
              target_status.refresh
            when 0x5C # Flinch for Snore/Ronflement
              status_check(target, 7)
              target_status.refresh
            when 0x7D # Defrost move
              status_check(target, 3)
              target_status.refresh
            when 0x92 # Twister / Ouragan
              status_check(target, 7)
              target_status.refresh
            when 0x96 # Flinch
              status_check(target, 7)
              target_status.refresh
            when 0x98 # Thunder / paralyze
              status_check(target, 2)
              target_status.refresh
            when 0x9B2 # Bounce / Rebond
              status_check(target, 2)
              target_status.refresh
              #index = user.effect_list.index(0x9B2)
              #user.effect.delete_at(index)
            when 0xA7 # Burn
              status_check(target, 3)
              target_status.refresh
            when 0xC8 # Burn
              status_check(target, 3)
              target_status.refresh
            when 0xD1 # Poison Tail
              status_check(target, 1)
              target_status.refresh
            when 0x66 # Glas de soin / Heal Bell / Aromatherapy
              user.cure
              if user == @actor
                draw_text("L'équipe est soignée!")
                wait(40)
                @party.cure_party
              end
            when 0xAB # smelling salt
              if user.paralyzed?
                user.cure
                user_status.refresh
              end
            when 0xC1 # Refresh
              if user.paralyzed? or user.burn? or user.poisoned?
                user.cure
                user_status.refresh
              end
            when 0x1B # Confuse - Mania / Thrash
              index = user.effect_list.index(0x1B)
              if not(user.confused?) and user.effect[index][1] == 1
                status_check(user, 6)
                user_status.refresh
              end
            # ----------------- -------------- ---------------
            #        Modificateur d'effets sur la durée
            # ----------------- -------------- ---------------
            when 0x1E # Conversion / Adaptation
              list = []
              for i in 0..user.skills_set.length-1
                list.push(i)
              end
              list.shuffle!
              
              chosen_type = user.skills_set[list.pop].type
              until chosen_type != 0 and 
                  not([user.type1, user.type2].include?(chosen_type))
                chosen_type = user.skills_set[list.pop].type
              end
              user.skill_effect(0x1E, -1, chosen_type)
              string = type_string(chosen_type) + "!"
              draw_text("#{user.given_name} est", "maintenant de type " + string)
              wait(40)
            when 0x23 # Light screen / Mur Lumière
              target.skill_effect(0x23, 5)
              draw_text("#{user.given_name}", "est protégé!")
              wait(40)
            when 0x2A # Multi Turn Attack
              if not(user.effect_list.include?(0x2A))
                turn = rand(4) + 3
                user.skill_effect(0x2A, turn)
                draw_text("#{target.given_name} est","piégé par " + user_skill.name+"!")
                wait(40)
              end
            when 0x54 # Leech Seed / Vampigraine
              user.skill_effect(0x54)
              draw_text("#{target.given_name}", "est infecté!")
              wait(40)
            when 0x70 # Picots / Spikes
              
            when 0x2F # Focus energy / Puissance
              user.skill_effect(0x2F)
              draw_text("#{user.given_name}", "se gonfle!")
              wait(40)
            when 0x2E # Brume / Mist
              user.skill_effect(0x2E, 5)
              draw_text("#{user.given_name} s'entoure", "de brume!")
              wait(40)
            when 0x39 # Transform / Morphing
              if user.effect_list.include?(0x56) # Cure Disable / Entrave
                index = user.effect_list.index(0x56)
                skill_index = user.effect[index][2]
                user.skills_set[skill_index].enable
              end
              
              set = []
              for skill in target.skills_set
                adskill = skill.clone
                adskill.define_ppmax(5)
                adskill.refill
                adskill.enable
                set.push(adskill)
              end
              
              data = user.clone
              user.skill_effect(0x39, -1, data)
              draw_text("#{user.given_name}", "se métamorphose!")
              if user == @actor
                @actor.skills_set = set
                @actor.transform_effect(target)
                @actor_sprite.bitmap = RPG::Cache.battler(@enemy.battler_back, 0)
                @actor_sprite.ox = @actor_sprite.bitmap.width / 2
                @actor_sprite.oy = @actor_sprite.bitmap.height
                #@actor_sprite.tone = Tone.new(200,0,200,180)
              else
                @enemy.skills_set = set
                @enemy.transform_effect(target)
                @enemy_sprite.bitmap = RPG::Cache.battler(@actor.battler_face, 0)
                #@enemy_sprite.tone = Tone.new(200,0,200,180)
              end
              wait(40)
            when 0x41 # Reflect / Protection
              target.skill_effect(0x41, 5)
              draw_text("#{user.given_name}", "est protégé!")
              wait(40)
            when 0x4B # Sky attack / TTA
              status_check(target, 7)
              target_status.refresh
            when 0x51 # Frénésie / Rage
              if not(user.effect_list.include?(0x51))
                user.skill_effect(0x51, -1, 0)
              end
            when 0x52 # Copie / Mimic
              cloned_skill = target_last_skill.clone
              cloned_skill.define_ppmax(5)
              cloned_skill.refill
              user.skill_effect(0x52, -1, [user_skill.clone, cloned_skill])
              index = user.skills_set.index(user_skill)
              user.skills_set[index] = cloned_skill
              draw_text("#{user.given_name} copie", cloned_skill.name+"!")
              wait(40)
            when 0x56 # Disable / Entrave
              index = target.skills_set.index(target_last_skill)
              if index != nil
                target.skill_effect(0x56, rand(4) + 2, index)
                target.skills_set[index].disable
                draw_text(target.skills_set[index].name+" est bloqué!")
                wait(40)
              end
            when 0x5A # Encore
              index = target.skills_set.index(target_last_skill)
              target.skill_effect(0x5A, rand(4) + 3, index)
            when 0x5E # Lock-on / Verrouillage
              user.skill_effect(0x5E, 2)
              draw_text("#{user.given_name} cible #{target.given_name}!")
              wait(40)
            when 0x5F # Gribouille // Sketch
              index = user.skills_set.index(user_last_skill)
              user.skills_set[index] = target_last_skill
              draw_text("#{user.given_name} a copié", target_last_skill.name+"!")
              wait(40)
            when 0x62 # Prélèvement destin / Destiny Bond
              user.skill_effect(0x62, 2)
              draw_text("#{user.given_name} lie son" , "destin.")
              wait(40)
            when 0x6A # Regard Noir / Mean Look
              target.skill_effect(0x6A)
              draw_text("#{target.given_name}", "ne peut plus fuir!")
              wait(40)
            when 0x6B # Cauchemar/Nightmare
              target.skill_effect(0x6B)
              draw_text("#{target.given_name} fait des" , "cauchemars!")
              wait(40)
            when 0x6F # Abri / Détection
              if user.effect_list.include?(0x6F)
                index = user.effect_list.index(0x6F)
                effect = user.effect[index]
                effect[1] = 2
                effect[2] += 1
              else
                user.skill_effect(0x6F, 2, 1)
              end
              draw_text("#{user.given_name}", "se protège!")
              wait(40)
            when 0x72 # Requiem / Perish Song
              if not(user.effect_list.include?(0x72))
                user.skill_effect(0x72, 4)
              end
              if not(enemy.effect_list.include?(0x72))
                enemy.skill_effect(0x72, 4)
              end
              draw_text("Une chanson déprimante.")
              wait(40)
            when 0x71 # Clairvoyance / Fore Sight
              target.skill_effect(0x71, -1)
              draw_text("#{user.given_name} identifie ", "#{target.given_name}.")
              wait(40)
            when 0x74 # Tenacité/Endure
              if user.effect_list.include?(0x74)
                index = user.effect_list.index(0x74)
                effect = user.effect[index]
                effect[1] = 2
                effect[2] += 1
              else
                user.skill_effect(0x74, 1)
              end
              draw_text("#{user.given_name} résiste!")
              wait(40)
            when 0x75 # Roulade/Rollout
              if not(user.effect_list.include?(0x75))
                user.skill_effect(0x75, 5, 5)
              elsif damage > 0 
                index = user.effect_list.index(0x75)
                user.effect[index][2] -= 1
              elsif damage == 0
                index = user.effect_list.index(0x75)
                user.effect.delete_at(index)
              end
            when 0x77 # Taillade / Fury Cutter
              if not(user.effect_list.include?(0x77))
                user.skill_effect(0x77, -1, -1)
              elsif damage > 0
                index = user.effect_list.index(0x77)
                user.effect[index][2] -= 1
              end
            when 0x78 # Attract
              target.skill_effect(0x78)
              draw_text("#{target.given_name} tombe amoureux", "de #{user.given_name}"+"!")
              wait(40)
            when 0x7C # Safeguard / Rune protect
              target.skill_effect(0x7C, 5)
              draw_text("#{target.given_name}", "est protégé!")
              wait(40)
            when 0x81 # Rapid Spin / Tour rapide
              list = [0x54, 0x70, 0x2A]
              for effect in list
                if target.effect_list.include?(effect)
                  index = target.effect_list.index(effect)
                  target.effect.delete_at(index)
                  draw_text("#{user.given_name}", "se libère!")
                  wait(40)
                end
              end
            when 0x9C # Defense Curl / Boul'Armure
              n = user.change_dfe(+1)
              raise_stat("DFE", user, n)
              user.skill_effect(0x9C)
            when 0x9F # Uproar / Brouhaha
              turn = rand(3)+3
              user.skill_effect(0x9F, turn)
              draw_text("#{user.given_name}", "fait un BROUHAHA!")
              wait(40)
            when 0xA0 # Stockpile
              if user.effect_list.include?(0xA0)
                index = user.effect_list.index(0xA0)
                case user.effect[index][2]
                when 1
                  user.effect[index][2] = 2
                  draw_text("#{user.given_name}", "se charge!")
                  wait(40)
                when 2
                  user.effect[index][2] = 3
                  draw_text("#{user.given_name}", "se charge!")
                  wait(40)
                end
                if user.effect[index][2] > 3
                  draw_text("#{user.given_name}", "est chargé à bloc!")
                  wait(40)
                end
              else
                user.skill_effect(0xA0, -1, 1)
                draw_text("#{user.given_name}", "se charge!")
                wait(40)
              end
            when 0xA1 # Spit up / Relache
              if user.effect_list.include?(0xA0)
                index = user.effect_list.index(0xA0)
                user.effect.delete_at(index)
              end
            when 0xA2 # Swallow / Avale
              if user.effect_list.include?(0xA0)
                index = user.effect_list.index(0xA0)
                user.effect.delete_at(index)
              end
            when 0xA5 # Tourment
              index = target.skills_set.index(target_last_skill)
              target.skill_effect(0xA5, -1, index)
              target.skills_set[index].disable
              draw_text("#{target.given_name}", "est tourmenté!")
              wait(40)
            when 0xAE # Charge / Chargeur
              if user.effect_list.include?(0xAE)
                index = user.effect_list.index(0xAE)
                user.effect.delete_at(index)
              end
              user.skill_effect(0xAE, 2)
              draw_text("#{user.given_name}", "est chargé!")
              wait(40)
            when 0xAF # Taunt / Provoc
              target.skill_effect(0xAF, 2)
              draw_text("#{user.given_name} cède à la", "provocation!")
              wait(40)
            when 0xB5 # Ingrain / Racines
              user.skill_effect(0xB5)
              draw_text("#{user.given_name}" , "est enraciné!")
              wait(40)
            when 0xBB # Yawn
              target.skill_effect(0xBB, 2)
              draw_text("#{user.given_name}" , "baîlle!")
              wait(40)
            when 0xC2 # Grudge / Rancune
              user.skill_effect(0xC2, 2)
              draw_text("#{user.given_name}" , "est rancunier! (?)")
              wait(40)
            when 0xC9 # Mud Sport
              user.skill_effect(0xC9)
              draw_text("#{user.given_name}" , "est couvert de boue!")
              wait(40)
            when 0xD2 # Water Sport
              user.skill_effect(0xD2)
              draw_text("#{user.given_name}" , "est mouillé! ... ...")
              wait(40)
            end
          end
          
          # ----------------- -------------- -------------------
          #     Contrôle des effets adverses / Adversaire
          # ----------------- -------------- ------------------
          for effect in target.effect_list
            case effect
            when 0x62 # Prélèvement destin / Destiny Bond
              if target.dead?
                damage = user.hp
                draw_text("#{target.given_name}", "est lié par le destin.")
                self_damage(user, user_sprite, user_status, damage)
                wait(40)
                faint_check(user)
              end
            when 0xC2 # Grudge
              if target.dead? and not(user.dead?)
                user_skill.pp = 0
              end
            when 0x1A # Bide / Patience
              index = target.effect_list.index(0x1A)
              target.effect[index][2] += damage
            when 0x51 # Frénésie / Rage
              if damage > 0 and multi_hit == 0 # Compte une seule fois
                index = target.effect_list.index(0x51)
                target.change_atk(+1)
                target.effect[index][2] += 1
              end
            end
          end
          
          faint_check(target)
          
          # ----------- ---------------- --------------
          #            Capacités spéciales
          # ----------- ---------------- --------------
          case target.ability
          when 28 # Synchro (ab)
            if target.ability_token != nil
              draw_text("#{target.ability_name} de #{target.given_name}", "inflige le même statut!")
              wait(40)
              status_check(user, target.ability_token, true)
              user_status.refresh
              target.ability_token = nil
            end
          when 16 # Déguisement / Color change (ab)
            type = user_skill.type
            if [user.type1, user.type2].include?(type) and type != 0 and 
                type != user.ability_token and damage > 0
              user.ability_token = type
              string = type_string(type)
              draw_text("#{target.ability_name} de #{target.given_name}", "change le type en " + string + "!")
              wait(40)
            end
          when 9 # Static (ab)
            # Contact + dommages + 30% chance # force?
            if user_skill.direct? and damage > 0 and rand(100) < 30
              draw_text("#{target.ability_name} de #{target.given_name}", "paralyse #{user.given_name}!")
              wait(40)
              status_check(user, 2, true)
              user_status.refresh
              wait(40)
            end
          when 24 # Peau dure / Rough skin (ab)
            if user_skill.direct? and damage > 0
              draw_text("#{target.ability_name} de #{target.given_name}", "blesse #{user.given_name}!")
              rec_damage = user.max_hp / 16
              self_damage(user, user_sprite, user_status, rec_damage)
              wait(40)
            end
          when 27 # Pose Spore / Effect Spore (ab)
            # Contact + dommages + 10% chance # force?
            if user_skill.direct? and damage > 0 and rand(100) < 10 
              draw_text("#{target.ability_name} de #{target.given_name}", "laisse des spores!")
              wait(40)
              status_check(user, [1, 2, 4][rand(3)], true)
              user_status.refresh
              wait(40)
            end
          when 38 # Point poison / Poison point (ab)
            # Contact + dommages + 30% chance # force?
            if user_skill.direct? and damage > 0 and rand(100) < 30 
              draw_text("#{target.ability_name} de #{target.given_name}", "empoisonne #{user.given_name}!")
              wait(40)
              status_check(user, 1, true)
              user_status.refresh
              wait(40)
            end
          when 49 # Corps ardent / Flame body (ab)
            # Contact + dommages + 30% chance # force?
            if user_skill.direct? and damage > 0 and rand(100) < 30 
              draw_text("#{target.ability_name} de #{target.given_name}", "brûle #{user.given_name}!")
              wait(40)
              status_check(user, 3, true)
              user_status.refresh
              wait(40)
            end
          when 56 # Joli sourire / Cute charm (ab)
            # Contact + dommages + 30% chance # force?
            if user_skill.direct? and damage > 0 and rand(100) < 30 and
                not(target.effect_list.include?(0x78) or (target.gender + user.gender) != 3)
              user.skill_effect(0x78)
              draw_text("#{target.ability_name} de #{target.given_name}", "séduit #{user.given_name}!")
              wait(40)
            end
          end
          
          faint_check(target)
          
        else
          #-------------------------------------------------------
          # Miss
          #--------------------------------------------------------
          case user_skill.effect
          when 0x07 # Self_destruct
            rec_damage = user.hp
            self_damage(user, user_sprite, user_status, rec_damage)
            draw_text("#{user.given_name}","se sacrifie.")
            wait(40)
            faint_check(user)
          when 0x2D # Miss and self_damage
            damage = info[0]
            rec_damage = damage/2
            draw_text("#{user.given_name}","retombe au sol!")
            self_damage(user, user_sprite, user_status, rec_damage)
            wait(30)
            faint_check(user)
          when 0x68 # Triple Kick
            multi_hit = 0 #Arret si échec
          when 0x9B2 # Bounce / Rebond
            if user.effect_list.include?(0x9B2)
              index = user.effect_list.index(0x9B2)
              user.effect.delete_at(index)
            end
          when 0x75 # Roulade/Rollout
            if user.effect_list.include?(0x75)
              index = user.effect_list.index(0x75)
              user.effect.delete_at(index)
            end
          end
        end
        #-------------------------------------------------------
        # Réduction compteur d'effets ?
        #--------------------------------------------------------
        # user.skill_effect_end_turn
        user_status.refresh
        target_status.refresh
      end
    end
    
    #------------------------------------------------------------
    # Gestion des dêgats automatique en combat
    # Skill utilisé sur l'adversaire
    # Dommages Infligés - user_skill = class Skill, enemy = class Pokemon
    # Renvoie: [damage, critical (true/false), efficency (-2, -1,0,1)]
    #------------------------------------------------------------        
    def damage_calculation(user_skill, user, target, critical_special = 0, shortcut = false)      
      critical_hit = critical_decision(critical_special)
      level = user.level
      statistic_refresh
      user.statistic_refresh
      if user_skill.physical
        atk = (critical_hit and user.atk_stage < 0) ? user.atk_basis : user.atk
        dfe = (critical_hit and target.dfe_stage > 0) ? target.dfe_basis : target.dfe
      elsif user_skill.special
        atk = (critical_hit and user.ats_stage < 0) ? user.ats_basis : user.ats
        dfe = (critical_hit and target.dfs_stage > 0) ? target.dfs_basis : target.dfs
      else
        atk = (critical_hit and user.atk_stage < 0) ? user.atk_basis : user.atk
        dfe = (critical_hit and target.dfe_stage > 0) ? target.dfe_basis : target.dfe
      end
      
      base_damage = user_skill.power
      user_type1 = user.type1
      user_type2 = user.type2
      skill_type = user_skill.type
      target_type1 = target.type1
      target_type2 = target.type2
      weather = $battle_var.weather[0]
      
      # ------------- -------------- ---------------
      #        Programmation des objets
      # ------------- -------------- ---------------
      #            Boosts de type
      # ------------- -------------- ---------------
      case user.item_name
      when "MOUCH.SOIE"
        atk *= user_type1 == 1 ? 1.1 : 1
      when "CHARBON"
        atk *= user_type1 == 2 ? 1.1 : 1
      when "EAU MYSTIQUE"
        atk *= user_type1 == 3 ? 1.1 : 1
      when "AIMANT"
        atk *= user_type1 == 4 ? 1.1 : 1
      when "GRAIN MIRACL"
        atk *= user_type1 == 5 ? 1.1 : 1
      when "GLACETERNEL"
        atk *= user_type1 == 6 ? 1.1 : 1
      when "CEINT.NOIRE"
        atk *= user_type1 == 7 ? 1.1 : 1
      when "PIC VENIN"
        atk *= user_type1 == 8 ? 1.1 : 1
      when "SABLE DOUX"
        atk *= user_type1 == 9 ? 1.1 : 1
      when "BEC POINTU"
        atk *= user_type1 == 10 ? 1.1 : 1
      when "CUILLERTORDU"
        atk *= user_type1 == 11 ? 1.1 : 1
      when "POUDRE ARG."
        atk *= user_type1 == 12 ? 1.1 : 1
      when "PIERRE DURE"
        atk *= user_type1 == 13 ? 1.1 : 1
      when "RUNE SORT"
        atk *= user_type1 == 14 ? 1.1 : 1
      when "CROC DRAGON"
        atk *= user_type1 == 15 ? 1.1 : 1
      when "PEAU METAL"
        atk *= user_type1 == 16 ? 1.1 : 1
      when "LUNET.NOIRES"
        atk *= user_type1 == 17 ? 1.1 : 1
      end
      

      # ------------- -------------- ---------------
      #        Programmation des attaques
      # ------------- -------------- ---------------
      #      Redéfinition base_damage (puissance)
      # ------------- -------------- ---------------
      data = nil
      
      case user_skill.effect
      when 0x07 # Self_destruct
        dfe = dfe / 2
      when 0x63 # Base damage variant
        a = user.hp * 48.0 / user.max_hp
        if a >= 33
          base_damage = 20
        elsif a >= 17
          base_damage = 40
        elsif a >= 10
          base_damage = 80
        elsif a >= 5
          base_damage = 100
        elsif a >= 2
          base_damage = 150
        elsif a < 2
          base_damage = 200
        end
      when 0x7E # Magnitude / Ampleur
        a = rand(100)
        if a < 5
          data = 4
          base_damage = 10
        elsif a < 15
          data = 5
          base_damage = 30
        elsif a < 35
          data = 6
          base_damage = 50
        elsif a < 65
          data = 7
          base_damage = 70
        elsif a < 85
          data = 8
          base_damage = 90
        elsif a < 95
          data = 9
          base_damage = 110
        else
          data = 10
          base_damage = 150
        end
      when 0x68 # Triple Kick/Triple Pied
        if user.effect_list.include?(0x68)
          index = user.effect_list.index(0x68)
          base_damage += 10 * user.effect[index][2]
        end
      when 0x75 # Rollout / Roulade
        # Bonus cumulatif
        if user.effect_list.include?(0x75)
          index = user.effect_list.index(0x75)
          base_damage *= (5-user.effect[index][1])**2
        end
        # Defense Curl/Boul'Armure bonus
        if user.effect_list.include?(0x9C)
          base_damage *= 2
        end
      when 0x77  # Fury Cutter / Taillade
        if user.effect_list.include?(0x77)
          index = user.effect_list.index(0x77)
          multiplier = - user.effect[index][1]
          if multiplier < 5
            base_damage *= multiplier**2
          elsif multiplier >= 5
            base_damage *= 5**2
          end
        end
      when 0x79 # Return / Retour
        base_damage = user.loyalty * 10 / 25
      when 0x7B # Frustration
        base_damage = (255 - user.loyalty) * 10 / 25
      when 0xBE # Base damage basé sur hp
        base_damage = [1, user.hp * 150 / user.max_hp].max
      when 0xC4 # Low Kick
        string = Pokemon_Info.weight(target.id)
        weight = string[0..string.length-2].to_f
        if weight < 10
          base_damage = 20
        elsif weight < 25
          base_damage = 40
        elsif weight < 50
          base_damage = 60
        elsif weight < 100
          base_damage = 80
        elsif weight < 200
          base_damage = 100
        else
          base_damage = 120
        end
      end
      
      # --------- --------------- ----------
      #       Protections adverses
      # --------- --------------- ----------
      for effect in target.effect_list
        case effect
        when 0x9B0 # Dig / Tunnel upx2 Magnitude / Earthquake
          if [0x7E, 0x93].include?(user_skill.effect)
            base_damage *= 2
          end
        when 0x9B1 # Vol / Fly upx2
          if [0x95, 0x98, 0x92].include?(user_skill.effect)
            base_damage *= 2
          end
        when 0x9B2 # Bounce upx2
          if [0x95, 0x98, 0x92].include?(user_skill.effect)
            base_damage *= 2
          end
        when 0x9B3 # Dive + Surf
          if user_skill.id == 15
            base_damage *= 2
          end
        when 0x1E # Conversion / Adaptation
          index = target.effect_list.index(0x1E)
          target_type1 = target.effect[index][2]
          target_type2 = 0
        end
      end
      
      # --------- --------------- ----------
      #        Bonus Capacités Spéciales
      # --------- --------------- ----------
      case user.ability
      when 18 # Flash Fire / Torche (ab)
        if user.ability_active and user_skill.type_fire?
          base_damage *= 1.5
        end
      when 37 # Huge Power / Coloforce (ab)
        if user_skill.physical
          atk *= 2
        end
      when 74 # Pure Power / Fore pure (ab)
        if user_skill.physical
          atk *= 2
        end
      when 55 # Agitation / Hustle (ab)
        if user_skill.physical
          atk *= 1.5
        end
      when 57 # Plus (ab)
        if target.ability == 58 and user_skill.special # Minus 
          atk *= 1.5
        end
      when 58 # Minus (ab)
        if target.ability == 57 and user_skill.special # Minus 
          atk *= 1.5
        end
      when 62 # Cran / Guts (ab)
        if user.status != 0 and user_skill.physical
          atk *= 1.5
        end
      when 65 # Overgrow / Engrais (ab)
        if user.hp < (user.max_hp/3) and user_skill.type_grass?
          base_damage *= 1.5
        end
      when 66 # Brasier / Blaze (ab)
        if user.hp < (user.max_hp/3) and user_skill.type_fire?
          base_damage *= 1.5
        end
      when 67 # Torrent (ab)
        if user.hp < (user.max_hp/3) and user_skill.type_water?
          base_damage *= 1.5
        end
      when 68 # Essaim / Swarm (ab)
        if user.hp < (user.max_hp/3) and user_skill.type_insect?
          base_damage *= 1.5
        end
      end
      
      case target.ability
      when 47 # Thick Fat / Isograisse (ab)
        if user_skill.type_fire? or user_skill.type_ice?
          atk /= 2
        end
      when 59 # Meteo / Forecast (ab)
        if target.ability_token == nil
          target.ability_token = 1 # Normal
        end
        target_type1 = target.ability_token
        target_type2 = 0
      when 16 # Déguisement / Color Change (ab)
        if target.ability_token == nil
          target.ability_token = 1
        end
        target_type1 = target.ability_token
        target_type2 = 0
      when 63 # Marvel Scale / Ecaille Spe (ab)
        if target.status != 0 and user_skill.physical
          dfe *= 1.5
        end
      end
      
      # --------- --------------- ---------- ---------------
      #        Dommages infligés et calculs coefficients
      # --------- --------------- ---------- ---------------
      pre_damage = (((level*2/5.0+2)*base_damage*atk/dfe)/50)
      multiplier = 1
      
      # Effets de la météo
      # Airlock / Cloud Nine (ab)
      if not([13, 77].include?(user.ability) or [13, 77].include?(target.ability))
        if ($battle_var.sunny? and skill_type == 2) or ($battle_var.rain? and skill_type == 3)
          # Ensoleillé + Feu ou Pluie + Eau
          multiplier *= 1.5
        end
        if ($battle_var.sunny? and skill_type == 3) or ($battle_var.rain? and skill_type == 2)
          # Ensoleillé + Eau ou Pluie + Feu
          multiplier *= 0.5
        end
        # Météo - Lance_Soleil / Solarbeam en cas de mauvais temps
        if user_skill.effect == 0x97 and [1, 3, 4].include?(weather)
          multiplier *= 0.5
        end
      end
      
      # Conversion / Adaptation
      if user.effect_list.include?(0x1E)
        index = user.effect_list.index(0x1E)
        user_type1 = user.effect[index][2]
        user_type2 = 0
      end
      
      # Forecast / Meteo (ab) // Déguisement / Color Change (ab)
      if user.ability == 59 or user.ability == 16
        user_type1 = user.ability_token
        user_type2 = 0
      end
      
      # Bonus même type
      if skill_type == user_type1 or skill_type == user_type2
        multiplier *= 1.5
      end
      
      # Weather Ball / Ball'Meteo
      if user_skill.effect == 0xCB
        case weather
        when 1 # Pluie
          skill_type = 3 # Eau
        when 2 # Sunny
          skill_type = 2 # Feu
        when 3 # Tempete Sable
          skill_type = 13 # Roche
        when 4 # Hail
          skill_type = 6 # Glace
        end
      end
        
      # Complément statut
      # Brulure // Guts / Cran (ab) // Facade
      if user.status == 3 and user_skill.physical and not(user.ability == 62) and
          user_skill.effect != 0xA9
        multiplier *= 0.5
      end
      
      # ------------------ ----------
      #  Programmation des attaques
      # ------------------ -----------
      # Protections et malus adverses
      for effect in target.effect_list
        case effect
        when 0x23 # Mur Lumière / Light Screen
          if user_skill.special and not(critical_hit)
            multiplier *= 0.5
          end
        when 0x41 # Reflet / Reflect
          if user_skill.physical and not(critical_hit)
            multiplier *= 0.5
          end
        when 0xAE # Charge / Chargeur
          if skill_type == 4
            multiplier *= 2
          end
        when 0xC9 # Mud Sport / Lance-Boue
          if skill_type == 4
            multiplier *= 0.5
          end
        when 0xD2 # Water Sport / Tourniquet
          if skill_type == 2
            multiplier *= 0.5
          end
        when 0x6C # Minimize / Lilliput
          if user_skill.effect == 0x96
            multiplier *= 2
          end
        when 0x71 # Clairvoyance / Foresight
          if [1, 7].include?(skill_type) and [target_type1, target_type2].include?(14)
            skill_type = 0
          end
        end
      end
      
      # Bonus efficacité du type // Struggle / Lutte
      rate = element_rate(target_type1, target_type2, skill_type)
      if user_skill.id != 165
        multiplier *= rate/100.0
      end
      # Etude efficacité
      if rate > 100.0 # Super efficace
        efficiency = 1
      elsif rate < 100.0 and rate > 0 # Pas très efficace
        efficiency = -1
      elsif rate == 0 # Innefficace
        efficiency = -2
      elsif rate == 100.0 # Normal
        efficiency = 0
      end
      
      # Propriétés attaque
      case user_skill.effect
      when 0xA1 # Relache / Stockage // Spit up / Stockpile
        if user.effect_list.include?(0xA0)
          index = user.effect_list.index(0xA0)
          multiplier *= user.effect[index][2]
          damage = pre_damage * multiplier
          return [Integer(damage), false, efficiency]
        end
      when 0xA9 # Facade
        if [1,2,3].include?(user.status)
          multiplier *= 2
        end
      when 0xAB # Smelling Salt / Stimulant
        if target.status == 2
          multiplier *= 2
        end
      end
      
      # Armurbaston / Battle Armor (ab) // Coque armure / Shell Armor (ab)
      if target.ability == 4 or target.ability == 75
        critical_hit = false
      end
      
      # Calcul IA
      if shortcut
        return base_damage * multiplier
      end
      
      # Damage weight
      multiplier *= (100 - rand(15)) / 100.0
      if critical_hit
        multiplier *= 2
      end
      
      # Minimum dommage
      damage = pre_damage * multiplier
      if damage < 1 and damage > 0
        damage = 1
      end
      
      return [Integer(damage), critical_hit, efficiency, data]
    end
    
    
    #------------------------------------------------
    #           Calcul de précision
    #------------------------------------------------    
    def accuracy_check(user_skill, user, target) #actor: user
      n = user_skill.accuracy
      
      # ------------- ------------------ --------------------
      #             Attaques self-cible, buff
      # ------------- ------------------- ------------------- 
      if user == target
        return n = 100
      end
      
      # ------------- ------------------ --------------------
      #                Capacités spéciales
      # ------------- ------------------- -------------------
      case target.ability
      when 8 # Voile Sable / Sand Veil (ab)
        if $battle_var.sandstorm?
          n *= 0.8
        end
      end
      
      case user.ability
      when 14 # Compoundeyes / Oeil Composé (ab)
        n *= 1.3
      when 55 # Hustle / Agitation (ab)
        if user_skill.physical
          n *= 0.8
        end
      end
      
      for effect in user.effect_list
        case effect
        when 0x5E # Verrouillage / Lock-on
          return n = 100
        end
      end
      
      # ------------- ------------------ --------------------
      #             Attaques intouchables
      # ------------- ------------------- -------------------
      for effect in target.effect_list
        case effect
        when 0x9B0 # Tunnel
          if not([0x7E, 0x93, 0x26].include?(user_skill.effect) and user_skill.type == 9)
            return n = 0
          end
        when 0x9B1 # Vol / Flying
          if not([0xCF, 0x92, 0x95, 0x98].include?(user_skill.effect) or user_skill.id == 18)
            return n = 0
          end
        when 0x9B2 # Bounce / Rebond
          if not([0xCF, 0x92, 0x95, 0x98].include?(user_skill.effect) or user_skill.id == 18)
            return n = 0
          end
        when 0x9B1 # Dive / Plongée
          if not([57, 250].include?(user_skill.id))
            return n = 0
          end
        end
      end  
      
      
      case user_skill.effect
      # ------------- ------------------ --------------------
      # Attaques réussites sous certaines conditions
      # ------------- ------------------- -------------------
      when 0x26 # OHKO
        if target.ability == 5 # Fermeté / Sturdy (ab)
          return n = 0
        end
        if user.level > target.level
          return n += (user.level - target.level)
        else
          return n = 0
        end
      when 0x08 # Dream eater / Devorêve
        if not(target.asleep?)
          return n = 0
        end
      when 0x1C # Whirlwind, Roar
        number = rand(256)
        if number*(user.level + target.level)/256 + 1 == target.level/4
          return n = 0
        end
      when 0x98 # Thunder
        if $battle_var.sunny? # Soleil
          return n = 50
        end
      end
      
      # ------------- ------------------ --------------------
      #             Attaques à précision nulle
      # ------------- ------------------- -------------------      
      if n == 0
        return n = 100
      end
      
      return n
    end
    
    # ----------- -------------- ------------- --------
    # Fonction détermination des coups critiques
    # ----------- -------------- ------------- --------
    def critical_decision(critical_special = 0)
      rate = 0
      rate += critical_special
      case rate
      when 0
        critical_chance = rand(15)
      when 1
        critical_chance = rand(7)
      when 2
        critical_chance = rand(3)
      when 3
        critical_chance = rand(2)
      end
      if rate >= 4
        critical_chance = rand(1)
      end
      if critical_chance == 0
        return true
      else
        return false
      end
    end
    
    #------------------------------------------------------------    
    # Taux de résultat aux dégâts
    #------------------------------------------------------------    
    def element_rate(target_type1, target_type2, skill_type)
      result = 100.0
      if $data_table_type[target_type1][skill_type] != nil
        result = result * $data_table_type[target_type1][skill_type]
      else
        print("Erreur : vérifiez la table des types, case #{target_type1} #{skill_type}")
      end
      if $data_table_type[target_type2][skill_type] != nil
        result = result * $data_table_type[target_type2][skill_type]
      else
        print("Erreur : vérifiez la table des types, case #{target_type2} #{skill_type}")
      end
      return result
    end
    
    #------------------------------------------------------------    
    #    Conséquences du switch sur le Pokémon
    #------------------------------------------------------------    
    def switch_effect(actor, enemy)
      # Reset de statut : Toxic
      if actor.toxic?
        actor.reset_toxic_count
      end
      # Reset de staut : Confuse
      if actor.confused?
        actor.cure_state
      end
      
      # Reset capacité spéciale
      actor.ability_active = false
      actor.ability_token = nil
      
      case actor.ability
      when 30 # Natural cure / Medic Nature (ab)
        actor.cure
      end
      
      # Effets supprimés lors du switch
      for effect in actor.effect
        case effect[0]
        when 0x1A # Bide / Patience
          actor.effect.delete(effect)
        when 0x1B # Mania / Thrash
          actor.effect.delete(effect)
        when 0x1E # Adaptation / Conversion
          actor.effect.delete(effect)
        when 0x2A # Multi turn Attack
          actor.effect.delete(effect)
        when 0x2F # Focus Energy / Puissance
          actor.effect.delete(effect)
        when 0x39 # Morphing / Transform
          index = actor.effect_list.index(0x39)
          actor.transform_effect( actor.effect[index][2] , true )
          actor.effect.delete(effect)
        when 0x51 # Rage / Frénésie
          actor.change_atk(-effect[2])
          actor.effect.delete(effect)
        when 0x52 # Copie / Mimic
          skill_index = actor.skills_set.index(effect[2][1])
          actor.skills_set[skill_index] = effect[2][0]
          actor.effect.delete(effect)
        when 0x54 # Vampigraine
          actor.effect.delete(effect)
        when 0x56 # Entrave / Disable
          index = effect[0][2]
          actor.skills_set[index].enable
          actor.effect.delete(effect)
        when 0x5A # Encore
          actor.effect.delete(effect)
        when 0x72 # Perish Song / Requiem
          actor.effect.delete(effect)
        when 0x6C # Minimize / Lilliput
          actor.effect.delete(effect)
        when 0x6D # Curse
          actor.effect.delete(effect)
        when 0x78 # Attraction
          actor.effect.delete(effect)
        when 0x9C # Boul'Armure / Defense Curl
          actor.effect.delete(effect)
        when 0x9F # Uproar / Brouhaha
          actor.effect.delete(effect)
        when 0xA5 # Torment / Tourment
          index = actor.effect_list.index(0xA5)
          skill_index = actor.effect[index][2]
          actor.skills_set[skill_index].enable
          actor.effect.delete(effect)
        when 0xAE # Charge / Chargeur
          actor.effect.delete(effect)
        when 0xAF # Provoc / Taunt
          for skill in actor.skills_set
            if skill.power == 0 and not(skill.enabled?)
              skill.enable
            end
          end
          actor.effect.delete(effect)
        when 0xC9 # Mud Sport
          actor.effect.delete(effect)
        when 0xD2 # Water Sport
          actor.effect.delete(effect)
        end
      end

      for effect in enemy.effect
        case effect[0]
        when 0x6A # Regard noir / Mean Look
          enemy.effect.delete(effect)
        when 0x71 # Clairvoyance / Foresight
          enemy.effect.delete(effect)
        when 0x78 # Attraction
          enemy.effect.delete(effect)
        when 0x54 # Vampigraine
          enemy.effect.delete(effect)
        end
      end
    end
    
  end
end
