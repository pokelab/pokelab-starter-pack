#==============================================================================
# ■ Pokemon_Name
# Pokemon Script Project - Krosk 
# 17/08/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier
#-----------------------------------------------------------------------------
# Renommage de Pokémon uniquement
#-----------------------------------------------------------------------------

module POKEMON_S
  class Pokemon_Name
    def initialize(pokemon, z_level = 10000)
      @pokemon = pokemon
      @z_level = z_level
    end
    
    def main
      Graphics.freeze
      @background = Sprite.new
      @background.bitmap = RPG::Cache.picture("name.png")
      @background.z = @z_level
      
      @edit_window = POKEMON_S::Pokemon_NameEdit.new(@pokemon)
      @edit_window.z = @z_level + 1
      
      @input_window = POKEMON_S::Pokemon_NameInput.new
      @input_window.z = @z_level + 2
      
      @done = false
      Graphics.transition
      loop do
        Graphics.update
        Input.update
        update
        if @done
          break
        end
      end
      Graphics.freeze
      @edit_window.dispose
      @input_window.dispose
      @background.dispose
      @background = nil
      @edit_window = nil
      @input_window = nil
    end
    #--------------------------------------------------------------------------
    # ● フレーム更新
    #--------------------------------------------------------------------------
    def update
      @edit_window.update
      @input_window.update
      # Effacement
      if Input.repeat?(Input::B)
        if @edit_window.index == 0
          return
        end
        $game_system.se_play($data_system.cancel_se)
        @edit_window.back
        return
      end
      # Confirmer
      if Input.trigger?(Input::C)
        # Bouton sur Confirmer
        if @input_window.character == nil
          # Pas de nom entrée => Par défaut
          if @edit_window.name == ""
            @edit_window.restore_default
            if @edit_window.name == ""
              $game_system.se_play($data_system.buzzer_se)
              return
            end
            $game_system.se_play($data_system.decision_se)
            return
          end
          
          @pokemon.given_name = @edit_window.name
          $string[0] = @pokemon.name
          $string[1] = @pokemon.given_name
          $game_system.se_play($data_system.decision_se)
          @done = true
          return
        end
        
        # Entrée d'un caractère de trop
        if @edit_window.index == 10
          $game_system.se_play($data_system.buzzer_se)
          return
        end
        
        # Entrée d'un mauvais caractère
        if @input_window.character == ""
          $game_system.se_play($data_system.buzzer_se)
          return
        end
        
        $game_system.se_play($data_system.decision_se)
        
        @edit_window.add(@input_window.character)
        return
      end
    end
  end
end