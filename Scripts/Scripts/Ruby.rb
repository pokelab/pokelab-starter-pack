class Array
  def shuffle
    sort_by { rand }
  end

  def shuffle!
    self.replace shuffle
  end
end

class Integer
  def sgn
    if self >= 0
      return 1
    else
      return -1
    end
  end
  
  def even?
     return self % 2 == 0
   end
   
  def odd?
    return self % 2 == 1
  end
end

class Array
  def switch( liste )
    tab = self.clone
    liste.each {|key, value|
      self[value] = tab[key]
    }
  end
end

def explore(path)
  if not(FileTest.exist?(path))
    return ["Aucun"]
  end
  command = []
  dir = []
  d = Dir.open(path)
  d = d.sort - [".", ".."]
  d.each {|file|
    case File.ftype(path+"/"+file)
    when "directory"
      command.push(file)
      dir.push(true)
    when "file"
      command.push(file)
      dir.push(false)
    end
  }
  
  temp = []
  
  for i in 0..command.length-1
    if dir[i]
      command[i] += "/"
      temp.push(command[i])
    end
  end
  
  dir.delete(true)
  for element in temp
    command.delete(element)
  end
  
  temp.reverse!
  
  for element in temp
    dir.unshift(true)
    command.unshift(element)
  end
  
  return [command, dir]
end