#==============================================================================
# ■ Item
# Pokemon Script Project - Krosk 
# 25/08/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------
# Données du Sac
#-----------------------------------------------------------------------------

module POKEMON_S
  #-------------------------------------------------------------
  # $data_ball
  #   [Nom, Catch_rate, sprite fermé, sprite ouvert, Couleur]
  #-------------------------------------------------------------

  
  $data_ball = []
  if FileTest.exist?("Data/data_ball.txt")
    begin
      file = File.open("Data/data_ball.txt", "rb")
      file.readchar
      file.readchar
      file.readchar
      file.each {|line| eval(line) }
      file.close
    rescue Exception => exception
      EXC::error_handler(exception, file)
    end
  else
    $data_ball = load_data("Data/data_ball.rxdata")
  end
  
  #-------------------------------------------------------------
  # $data_item
  #   [Nom, Icone, Type, Description, Prix, Profil, Texte à l'utilisation, data, log_data_A, log_data_B, log_data_C]
  #   Type: "ITEM"
  #         "BALL"
  #         "TECH"
  #         "BERRY"
  #         "KEY"
  #
  #   [ tenable/jetable, usage limité, usage en map, usage en combat, usage sur pokémon, apte/non apte]
  #
  #   data paramètre général: common_event_id, effect
  #
  #   log_data_A paramètre de soin: [recover_hp_rate, recover_hp, recover_pp_rate, recover_pp]
  #   log_data_B paramètre de statut: [minus_state]
  #-------------------------------------------------------------
  #-----------------------------------------------------------------------------
  # 0: Normal, 1: Poison, 2: Paralysé, 3: Brulé, 4:Sommeil, 5:Gelé, 8: Toxic
  # @confuse (6), @flinch (7)
  #-----------------------------------------------------------------------------
  
  $data_item = []
  if ITEM_CONVERSION
    $data_item[ 0 ]=["erreur","return.png","ITEM","erreur", 0 ,"" ,"" , {} ]
    if FileTest.exist?("Data/data_item.txt")
      begin
        file = File.open("Data/data_item.txt", "rb")
        file.readchar
        file.readchar
        file.readchar
        file.each {|line| eval(line) }
        file.close
      rescue Exception => exception
        EXC::error_handler(exception, file)
      end
    end
  end
  
  #-------------------------------------------------------------
  # La suite ne concerne qui si vous ne passez par pas la 
  # base de données pour configurer les objets
  #-------------------------------------------------------------
  # Cette liste n'est pas mise à jour, elle est à abandonner
  #-------------------------------------------------------------
  
  if false
    $data_item[ 0 ]=["erreur","return.png","ITEM","erreur", 0 ,"" ,"" , ["" , ] ]
    $data_item[ 1 ]=["MASTER BALL","masterball.png","BALL","La meilleure BALL. Capture le POKéMON à coup sûr.", 0 ,"BALL","" , ["ball", $data_ball[3] ] ]
    $data_item[ 2 ]=["HYPER BALL","hyperball.png","BALL","BALL au taux de réussite supérieur à la SUPER BALL.", 1200 ,"BALL","" , ["ball", $data_ball[2] ] ]
    $data_item[ 3 ]=["SUPER BALL","superball.png","BALL","BALL au taux de réussite supérieur à la POKé BALL.", 600 ,"BALL","" , ["ball", $data_ball[1] ] ]
    $data_item[ 4 ]=["POKé BALL","pokeball.png","BALL","Un objet qui permet d'attraper les POKéMON sauvages.", 200 ,"BALL","" , ["ball", $data_ball[0] ] ]
    $data_item[ 5 ]=["FILET BALL","filetball.png","BALL","Une BALL qui marche bien sur un POKéMON EAU ou INSECTE.", 0 ,"BALL","" , ["ball", $data_ball[4] ] ]
    $data_item[ 6 ]=["FAIBLO BALL","faibloball.png","BALL","Une BALL qui marche mieux avec les POKéMON faibles.", 0 ,"BALL","" , ["ball", $data_ball[5] ] ]
    $data_item[ 7 ]=[ ]
    $data_item[ 8 ]=[ ]
    $data_item[ 9 ]=["CHRONO BALL","chronoball.png","BALL","Plus efficace pendant les combats longs.", 0 ,"BALL","" , ["ball", $data_ball[8] ] ]
    $data_item[ 10 ]=["HONOR BALL","honorball.png","BALL","Une BALL rare créée à l'occasion d'un grand événement.", 0 ,"BALL","" , ["ball", $data_ball[9] ] ]
    $data_item[ 11 ]=["LUXE BALL","luxuryball.png","BALL","Une BALL confortable qui rend les POKéMON amicaux.", 0 ,"BALL","" , ["ball", $data_ball[10] ] ]
    $data_item[ 12 ]=[ ]
    $data_item[ 13 ]=["POTION","potion.png","ITEM","Restaure les PV d'un POKéMON de 20 points.", 350 ,"ITEM","Les PV de %s sont restaurés de %d points.", nil , [0,20,0,0] ]
    $data_item[ 14 ]=["SUPER POTION","superpotion.png","ITEM","Restaure les PV d'un POKéMON de 50 points.", 700 ,"ITEM","Les PV de %s sont restaurés de %d points.", nil , [0,50,0,0] ]
    $data_item[ 15 ]=["HYPER POTION","hyperpotion.png","ITEM","Restaure les PV d'un POKéMON de 200 points.", 1200 ,"ITEM","Les PV de %s sont restaurés de %d points.", nil , [0,200,0,0] ]
    $data_item[ 16 ]=["POTION MAX","potionmax.png","ITEM","Restaure tous les PV d'un POKéMON.", 2500 ,"ITEM","Les PV de %s sont restaurés de %d points.", nil , [100,0,0,0] ]
    $data_item[ 17 ]=["ANTIDOTE","antidote.png","ITEM","Soigne les POKéMON empoisonnés.", 100 ,"ITEM","Le statut de %s est restauré.", nil , nil , [1] ]
    $data_item[ 18 ]=["ANTI-BRULE","antibrule.png","ITEM","Soigne les POKéMON de leurs brûlures.", 250 ,"ITEM","Le statut de %s est restauré.", nil , nil , [3] ]
    $data_item[ 19 ]=["ANTIGEL","antigel.png","ITEM","Décongèle les POKéMON gelés.", 250 ,"ITEM","Le statut de %s est restauré.", nil , nil , [5] ]
    $data_item[ 20 ]=["REVEIL","reveil.png","ITEM","Réveille les POKéMON endormis.", 250 ,"ITEM","Le statut de %s est restauré.", nil , nil , [4] ]
    $data_item[ 21 ]=["ANTI-PARA","antipara.png","ITEM","Soigne les POKéMON paralysés.", 200 ,"ITEM","Le statut de %s est restauré.", nil , nil , [2] ]
    $data_item[ 22 ]=["TOTAL SOIN","totalsoin.png","ITEM","Soigne les changements de statut d'un POKéMON.", 600 ,"ITEM","Le statut de %s est restauré.", nil , nil , [1,2,3,4,5,6,7,8] ]
    $data_item[ 23 ]=["GUERISON","guerison.png","ITEM","Restaure tous les PV et le statut d'un POKéMON.", 3000 ,"ITEM","Les PV de %s sont restaurés de %d points.", nil , [100,0,0,0] , [1,2,3,4,5,6,7,8] ]
    $data_item[ 24 ]=["RAPPEL","rappel.png","ITEM","Réanime un POKéMON K.O. et restaure la moitié de ses PV.", 1500 ,"ITEM","%s récupère son énergie.", nil , [50,0,0,0] , [9] ]
    $data_item[ 25 ]=["RAPPEL MAX","rappelmax.png","ITEM","Réanime un POKéMON K.O. et restaure tous ses PV.", 4000 ,"ITEM","%s récupère son énergie.", nil , [100,0,0,0] , [9] ]
    $data_item[ 26 ]=["HUILE","huile.png","ITEM","Restaure 10 PP d'une attaque sélectionnée.", 1200 ,"ITEM","Les PP de %s sont restaurés.", nil , [0,0,0,10] ]
    $data_item[ 27 ]=["HUILE MAX","huilemax.png","ITEM","Restaure tous les PP d'une attaque sélectionnée.", 2000 ,"ITEM","Les PP de %s sont restaurés.", nil , [0,0,0,99] ]
    $data_item[ 28 ]=["ELIXIR","elixir.png","ITEM","Restaure 10 PP de toutes les attaques.", 3000 ,"ITEM","Les PP de %s sont restaurés.", nil , [0,0,1,10] ]
    $data_item[ 29 ]=["MAX ELIXIR","maxelixir.png","ITEM","Restaure tous les PP de toutes les attaques.", 4500 ,"ITEM","Les PP de %s sont restaurés.", nil , [0,0,1,99] ]
    $data_item[ 30 ]=[ ]
    $data_item[ 31 ]=[ ]
    $data_item[ 32 ]=[ ]
    $data_item[ 33 ]=[ ]
    $data_item[ 34 ]=[ ]
    $data_item[ 35 ]=[ ]
    $data_item[ 36 ]=[ ]
    $data_item[ 37 ]=[ ]
    $data_item[ 38 ]=[ ]
    $data_item[ 39 ]=[ ]
    $data_item[ 40 ]=[ ]
    $data_item[ 41 ]=["SUPER BONBON","superbonbon.png","ITEM","Fait progresser un POKéMON d'un niveau.", 4800 ,"PKMN","" , ["level_up", 1 ] ]
    $data_item[ 42 ]=["PV PLUS","pvplus.png","ITEM","Monte les PV d'un POKéMON.", 9800 ,"PKMN","" , ["boost", 0 ] ]
    $data_item[ 43 ]=["PROTEINE","proteine.png","ITEM","Monte l'ATTAQUE d'un POKéMON.", 9800 ,"PKMN","" , ["boost", 1 ] ]
    $data_item[ 44 ]=["FER","fer.png","ITEM","Monte la DEFENSE d'un POKéMON.", 9800 ,"PKMN","" , ["boost", 2 ] ]
    $data_item[ 45 ]=["CARBONE","carbone.png","ITEM","Monte la VITESSE d'un POKéMON.", 9800 ,"PKMN","" , ["boost", 3 ] ]
    $data_item[ 46 ]=["CALCIUM","calcium.png","ITEM","Monte l'ATQ. SPE. d'un POKéMON.", 9800 ,"PKMN","" , ["boost", 4 ] ]
    $data_item[ 47 ]=["ZINC","zinc.png","ITEM","Monte la DEF. SPE. d'un POKéMON.", 9800 ,"PKMN","" , ["boost", 5 ] ]
    $data_item[ 48 ]=[ ]
    $data_item[ 49 ]=[ ]
    $data_item[ 50 ]=["ATTAQUE +","attaque+.png","ITEM","Monte l'ATTAQUE pendant un combat.", 500 ,"BATTLE","ATTAQUE de %s augmentée.", ["battle_boost", 0 ] ]
    $data_item[ 51 ]=["DEFENSE +","defense+.png","ITEM","Monte la DEFENSE pendant un combat.", 550 ,"BATTLE","DEFENSE de %s augmentée.", ["battle_boost", 1 ] ]
    $data_item[ 52 ]=["VITESSE +","vitesse+.png","ITEM","Monte la VITESSE pendant un combat.", 350 ,"BATTLE","VITESSE de %s augmentée.", ["battle_boost", 2 ] ]
    $data_item[ 53 ]=["SPECIAL +","special+.png","ITEM","Monte l'ATQ. SPE. pendant un combat.", 350 ,"BATTLE","ATQ.SPE. de %s augmentée.", ["battle_boost", 3 ] ]
    $data_item[ 54 ]=["PRECISION +","precision+.png","ITEM","Monte la précision des attaques pendant un combat.", 950 ,"BATTLE","PRECISION de %s augmentée.", ["battle_boost", 6 ] ]
    $data_item[ 55 ]=[ ]
    $data_item[ 56 ]=[ ]
    $data_item[ 57 ]=["POKéPOUPEE","pokepoupee.png","ITEM","Pour s'enfuir d'un combat contre un POKéMON sauvage.", 1000 ,"BATTLE","" , ["flee"] ]
    $data_item[ 58 ]=["QUEUE SKITTY","queueskitty.png","ITEM","Pour s'enfuir d'un combat contre un POKéMON sauvage.", 1000 ,"BATTLE","" , ["flee"] ]
    $data_item[ 59 ]=[ ]
    $data_item[ 60 ]=[ ]
    $data_item[ 61 ]=[ ]
    $data_item[ 62 ]=[ ]
    $data_item[ 63 ]=[ ]
    $data_item[ 64 ]=[ ]
    $data_item[ 65 ]=[ ]
    $data_item[ 66 ]=[ ]
    $data_item[ 67 ]=[ ]
    $data_item[ 68 ]=[ ]
    $data_item[ 69 ]=[ ]
    $data_item[ 70 ]=["REPOUSSE","repousse.png","ITEM","Repousse les PKMN sauvages faibles durant 100 pas.", 350 ,"MAP","Ca va repousser les Pokémons sauvages.", ["repel", 100 ] ]
    $data_item[ 71 ]=["SUPEREPOUSSE","superrepousse.png","ITEM","Repousse les PKMN sauvages faibles durant 200 pas.", 500 ,"MAP","Ca va repousser les Pokémons sauvages.", ["repel", 200 ] ]
    $data_item[ 72 ]=["MAX REPOUSSE","maxrepousse.png","ITEM","Repousse les PKMN sauvages faibles durant 250 pas.", 700 ,"MAP","Ca va repousser les Pokémons sauvages.", ["repel", 250 ] ]
    $data_item[ 73 ]=[ ]
    $data_item[ 74 ]=["MOUCH.SOIE","mouchsoie.png","ITEM","Objet tenu montant la puissance des attaques NORMAL.", 9800 ,"HOLD","" ]
    $data_item[ 75 ]=["CHARBON","charbon.png","ITEM","Objet tenu montant la puissance des attaques FEU.", 9800 ,"HOLD","" ]
    $data_item[ 76 ]=["EAU MYSTIQUE","eaumystique.png","ITEM","Objet tenu montant la puissance des attaques EAU.", 9800 ,"HOLD","" ]
    $data_item[ 77 ]=["AIMANT","aimant.png","ITEM","Objet tenu montant la puissance des attaques ELECTRIK.", 9800 ,"HOLD","" ]
    $data_item[ 78 ]=["GRAIN MIRACL","grainmiracl.png","ITEM","Objet tenu montant la puissance des attaques PLANTE.", 9800 ,"HOLD","" ]
    $data_item[ 79 ]=["GLACETERNEL","glaceternel.png","ITEM","Objet tenu montant la puissance des attaques GLACE.", 9800 ,"HOLD","" ]
    $data_item[ 80 ]=["CEINT.NOIRE","ceintnoire.png","ITEM","Objet tenu montant la puissance des attaques COMBAT.", 9800 ,"HOLD","" ]  
    $data_item[ 81 ]=["PIC VENIN","picvenin.png","ITEM","Objet tenu montant la puissance des attaques POISON.", 9800 ,"HOLD","" ]
    $data_item[ 82 ]=["SABLE DOUX","sabledoux.png","ITEM","Objet tenu montant la puissance des attaques SOL.", 9800 ,"HOLD","" ]
    $data_item[ 83 ]=["BEC POINTU","becpointu.png","ITEM","Objet tenu montant la puissance des attaques VOL.", 9800 ,"HOLD","" ]
    $data_item[ 84 ]=["CUILLERTORDU","cuillertordu.png","ITEM","Objet tenu montant la puissance des attaques PSY.", 9800 ,"HOLD","" ]
    $data_item[ 85 ]=["POUDRE ARG.","poudrearg.png","ITEM","Objet tenu montant la puissance des attaques INSECTE.", 9800 ,"HOLD","" ]
    $data_item[ 86 ]=["PIERRE DURE","pierredure.png","ITEM","Objet tenu montant la puissance des attaques ROCHE.", 9800 ,"HOLD","" ]
    $data_item[ 87 ]=["RUNE SORT","runesort.png","ITEM","Objet tenu montant la puissance des attaques SPECTRE.", 9800 ,"HOLD","" ]
    $data_item[ 88 ]=["CROC DRAGON","crocdragon.png","ITEM","Objet tenu montant la puissance des attaques DRAGON.", 9800 ,"HOLD","" ]
    $data_item[ 89 ]=["PEAU METAL","peaumetal.png","ITEM","Objet tenu montant la puissance des attaques ACIER.", 9800 ,"HOLD","" ]
    $data_item[ 90 ]=["LUNET.NOIRES","lunetnoires.png","ITEM","Objet tenu montant la puissance des attaques TENEBRES.", 9800 ,"HOLD","" ]
    $data_item[ 91 ]=[ ]
    $data_item[ 92 ]=[ ]
    $data_item[ 93 ]=[ ]
    $data_item[ 94 ]=[ ]
    $data_item[ 95 ]=[ ]
    $data_item[ 96 ]=[ ]
    $data_item[ 97 ]=[ ]
    $data_item[ 98 ]=[ ]
    $data_item[ 99 ]=[ ]
    $data_item[ 100 ]=[ ]
    $data_item[ 101 ]=[ ]
    $data_item[ 102 ]=["RESTES","restes.png","ITEM","Objet tenu permettant de restaurer les PV au combat.", 200 ,"HOLD","" ]
    $data_item[ 103 ]=[ ]
    $data_item[ 104 ]=[ ]
    $data_item[ 105 ]=["MULTI EXP","multiexp.png","ITEM","Objet tenu pour partager l'EXP après un combat.", 3000 ,"HOLD","" ]
    $data_item[ 106 ]=[ ]
    $data_item[ 107 ]=["PIECE RUNE","piecerune.png","ITEM","Double l'argent gagné si le porteur se bat aussi.", 100 ,"HOLD","" ]
    $data_item[ 108 ]=[ ]
    $data_item[ 109 ]=[ ]
    $data_item[ 110 ]=[ ]
    $data_item[ 111 ]=[ ]
    $data_item[ 112 ]=[ ]
    $data_item[ 113 ]=[ ]
    $data_item[ 114 ]=[ ]
    $data_item[ 115 ]=[ ]
    $data_item[ 116 ]=[ ]
    $data_item[ 117 ]=[ ]
    $data_item[ 118 ]=["CT01-"+ Skill_Info.name(Skill_Info.id("MITRA-POING")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("MITRA-POING")) , 3000 ,"ABLE","" , [ "ct", 1 , Skill_Info.id("MITRA-POING")] ]
    $data_item[ 119 ]=["CT02-"+ Skill_Info.name(Skill_Info.id("DRACOGRIFFE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("DRACOGRIFFE")) , 3000 ,"ABLE","" , [ "ct", 2 , Skill_Info.id("DRACOGRIFFE")] ]
    $data_item[ 120 ]=["CT03-"+ Skill_Info.name(Skill_Info.id("VIBRAQUA")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("VIBRAQUA")) , 3000 ,"ABLE","" , [ "ct", 3 , Skill_Info.id("VIBRAQUA")] ]
    $data_item[ 121 ]=["CT04-"+ Skill_Info.name(Skill_Info.id("PLENITUDE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("PLENITUDE")) , 3000 ,"ABLE","" , [ "ct", 4 , Skill_Info.id("PLENITUDE")] ]
    $data_item[ 122 ]=["CT05-"+ Skill_Info.name(Skill_Info.id("HURLEMENT")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("HURLEMENT")) , 1000 ,"ABLE","" , [ "ct", 5 , Skill_Info.id("HURLEMENT")] ]
    $data_item[ 123 ]=["CT06-"+ Skill_Info.name(Skill_Info.id("TOXIK")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("TOXIK")) , 3000 ,"ABLE","" , [ "ct", 6 , Skill_Info.id("TOXIK")] ]
    $data_item[ 124 ]=["CT07-"+ Skill_Info.name(Skill_Info.id("GRELE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("GRELE")) , 3000 ,"ABLE","" , [ "ct", 7 , Skill_Info.id("GRELE")] ]
    $data_item[ 125 ]=["CT08-"+ Skill_Info.name(Skill_Info.id("GONFLETTE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("GONFLETTE")) , 3000 ,"ABLE","" , [ "ct", 8 , Skill_Info.id("GONFLETTE")] ]
    $data_item[ 126 ]=["CT09-"+ Skill_Info.name(Skill_Info.id("BALLE GRAINE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("BALLE GRAINE")) , 3000 ,"ABLE","" , [ "ct", 9 , Skill_Info.id("BALLE GRAINE")] ]
    $data_item[ 127 ]=["CT10-"+ Skill_Info.name(Skill_Info.id("PUIS. CACHEE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("PUIS. CACHEE")) , 3000 ,"ABLE","" , [ "ct", 10 , Skill_Info.id("PUIS. CACHEE")] ]
    $data_item[ 128 ]=["CT11-"+ Skill_Info.name(Skill_Info.id("ZENITH")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("ZENITH")) , 2000 ,"ABLE","" , [ "ct", 11 , Skill_Info.id("ZENITH")] ]
    $data_item[ 129 ]=["CT12-"+ Skill_Info.name(Skill_Info.id("PROVOC")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("PROVOC")) , 3000 ,"ABLE","" , [ "ct", 12 , Skill_Info.id("PROVOC")] ]
    $data_item[ 130 ]=["CT13-"+ Skill_Info.name(Skill_Info.id("LASER GLACE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("LASER GLACE")) , 3000 ,"ABLE","" , [ "ct", 13 , Skill_Info.id("LASER GLACE")] ]
    $data_item[ 131 ]=["CT14-"+ Skill_Info.name(Skill_Info.id("BLIZZARD")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("BLIZZARD")) , 5500 ,"ABLE","" , [ "ct", 14 , Skill_Info.id("BLIZZARD")] ]
    $data_item[ 132 ]=["CT15-"+ Skill_Info.name(Skill_Info.id("ULTRALASER")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("ULTRALASER")) , 7500 ,"ABLE","" , [ "ct", 15 , Skill_Info.id("ULTRALASER")] ]
    $data_item[ 133 ]=["CT16-"+ Skill_Info.name(Skill_Info.id("MUR LUMIERE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("MUR LUMIERE")) , 3000 ,"ABLE","" , [ "ct", 16 , Skill_Info.id("MUR LUMIERE")] ]
    $data_item[ 134 ]=["CT17-"+ Skill_Info.name(Skill_Info.id("ABRI")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("ABRI")) , 3000 ,"ABLE","" , [ "ct", 17 , Skill_Info.id("ABRI")] ]
    $data_item[ 135 ]=["CT18-"+ Skill_Info.name(Skill_Info.id("DANSE PLUIE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("DANSE PLUIE")) , 2000 ,"ABLE","" , [ "ct", 18 , Skill_Info.id("DANSE PLUIE")] ]
    $data_item[ 136 ]=["CT19-"+ Skill_Info.name(Skill_Info.id("GIGA-SANGSUE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("GIGA-SANGSUE")) , 3000 ,"ABLE","" , [ "ct", 19 , Skill_Info.id("GIGA-SANGSUE")] ]
    $data_item[ 137 ]=["CT20-"+ Skill_Info.name(Skill_Info.id("RUNE PROTECT")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("RUNE PROTECT")) , 3000 ,"ABLE","" , [ "ct", 20 , Skill_Info.id("RUNE PROTECT")] ]
    $data_item[ 138 ]=["CT21-"+ Skill_Info.name(Skill_Info.id("FRUSTRATION")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("FRUSTRATION")) , 1000 ,"ABLE","" , [ "ct", 21 , Skill_Info.id("FRUSTRATION")] ]
    $data_item[ 139 ]=["CT22-"+ Skill_Info.name(Skill_Info.id("LANCE-SOLEIL")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("LANCE-SOLEIL")) , 3000 ,"ABLE","" , [ "ct", 22 , Skill_Info.id("LANCE-SOLEIL")] ]
    $data_item[ 140 ]=["CT23-"+ Skill_Info.name(Skill_Info.id("QUEUE DE FER")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("QUEUE DE FER")) , 3000 ,"ABLE","" , [ "ct", 23 , Skill_Info.id("QUEUE DE FER")] ]
    $data_item[ 141 ]=["CT24-"+ Skill_Info.name(Skill_Info.id("TONNERRE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("TONNERRE")) , 3000 ,"ABLE","" , [ "ct", 24 , Skill_Info.id("TONNERRE")] ]
    $data_item[ 142 ]=["CT25-"+ Skill_Info.name(Skill_Info.id("FATAL-FOUDRE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("FATAL-FOUDRE")) , 5500 ,"ABLE","" , [ "ct", 25 , Skill_Info.id("FATAL-FOUDRE")] ]
    $data_item[ 143 ]=["CT26-"+ Skill_Info.name(Skill_Info.id("SEISME")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("SEISME")) , 3000 ,"ABLE","" , [ "ct", 26 , Skill_Info.id("SEISME")] ]
    $data_item[ 144 ]=["CT27-"+ Skill_Info.name(Skill_Info.id("RETOUR")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("RETOUR")) , 1000 ,"ABLE","" , [ "ct", 27 , Skill_Info.id("RETOUR")] ]
    $data_item[ 145 ]=["CT28-"+ Skill_Info.name(Skill_Info.id("TUNNEL")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("TUNNEL")) , 2000 ,"ABLE","" , [ "ct", 28 , Skill_Info.id("TUNNEL")] ]
    $data_item[ 146 ]=["CT29-"+ Skill_Info.name(Skill_Info.id("PSYKO")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("PSYKO")) , 2000 ,"ABLE","" , [ "ct", 29 , Skill_Info.id("PSYKO")] ]
    $data_item[ 147 ]=["CT30-"+ Skill_Info.name(Skill_Info.id("BALL'OMBRE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("BALL'OMBRE")) , 3000 ,"ABLE","" , [ "ct", 30 , Skill_Info.id("BALL'OMBRE")] ]
    $data_item[ 148 ]=["CT31-"+ Skill_Info.name(Skill_Info.id("CASSE-BRIQUE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("CASSE-BRIQUE")) , 3000 ,"ABLE","" , [ "ct", 31 , Skill_Info.id("CASSE-BRIQUE")] ]
    $data_item[ 149 ]=["CT32-"+ Skill_Info.name(Skill_Info.id("REFLET")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("REFLET")) , 2000 ,"ABLE","" , [ "ct", 32 , Skill_Info.id("REFLET")] ]
    $data_item[ 150 ]=["CT33-"+ Skill_Info.name(Skill_Info.id("PROTECTION")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("PROTECTION")) , 3000 ,"ABLE","" , [ "ct", 33 , Skill_Info.id("PROTECTION")] ]
    $data_item[ 151 ]=["CT34-"+ Skill_Info.name(Skill_Info.id("ONDE DE CHOC")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("ONDE DE CHOC")) , 3000 ,"ABLE","" , [ "ct", 34 , Skill_Info.id("ONDE DE CHOC")] ]
    $data_item[ 152 ]=["CT35-"+ Skill_Info.name(Skill_Info.id("LANCE-FLAMME")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("LANCE-FLAMME")) , 3000 ,"ABLE","" , [ "ct", 35 , Skill_Info.id("LANCE-FLAMME")] ]
    $data_item[ 153 ]=["CT36-"+ Skill_Info.name(Skill_Info.id("BOMB-BEURK")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("BOMB-BEURK")) , 1000 ,"ABLE","" , [ "ct", 36 , Skill_Info.id("BOMB-BEURK")] ]
    $data_item[ 154 ]=["CT37-"+ Skill_Info.name(Skill_Info.id("TEMPETESABLE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("TEMPETESABLE")) , 2000 ,"ABLE","" , [ "ct", 37 , Skill_Info.id("TEMPETESABLE")] ]
    $data_item[ 155 ]=["CT38-"+ Skill_Info.name(Skill_Info.id("DEFLAGRATION")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("DEFLAGRATION")) , 5500 ,"ABLE","" , [ "ct", 38 , Skill_Info.id("DEFLAGRATION")] ]
    $data_item[ 156 ]=["CT39-"+ Skill_Info.name(Skill_Info.id("TOMBEROCHE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("TOMBEROCHE")) , 3000 ,"ABLE","" , [ "ct", 39 , Skill_Info.id("TOMBEROCHE")] ]
    $data_item[ 157 ]=["CT40-"+ Skill_Info.name(Skill_Info.id("AEROPIQUE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("AEROPIQUE")) , 3000 ,"ABLE","" , [ "ct", 40 , Skill_Info.id("AEROPIQUE")] ]
    $data_item[ 158 ]=["CT41-"+ Skill_Info.name(Skill_Info.id("TOURMENTE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("TOURMENTE")) , 3000 ,"ABLE","" , [ "ct", 41 , Skill_Info.id("TOURMENTE")] ]
    $data_item[ 159 ]=["CT42-"+ Skill_Info.name(Skill_Info.id("FACADE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("FACADE")) , 3000 ,"ABLE","" , [ "ct", 42 , Skill_Info.id("FACADE")] ]
    $data_item[ 160 ]=["CT43-"+ Skill_Info.name(Skill_Info.id("FORCE CACHEE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("FORCE CACHEE")) , 3000 ,"ABLE","" , [ "ct", 43 , Skill_Info.id("FORCE CACHEE")] ]
    $data_item[ 161 ]=["CT44-"+ Skill_Info.name(Skill_Info.id("REPOS")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("REPOS")) , 3000 ,"ABLE","" , [ "ct", 44 , Skill_Info.id("REPOS")] ]
    $data_item[ 162 ]=["CT45-"+ Skill_Info.name(Skill_Info.id("ATTRACTION")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("ATTRACTION")) , 3000 ,"ABLE","" , [ "ct", 45 , Skill_Info.id("ATTRACTION")] ]
    $data_item[ 163 ]=["CT46-"+ Skill_Info.name(Skill_Info.id("LARCIN")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("LARCIN")) , 3000 ,"ABLE","" , [ "ct", 46 , Skill_Info.id("LARCIN")] ]
    $data_item[ 164 ]=["CT47-"+ Skill_Info.name(Skill_Info.id("AILE D'ACIER")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("AILE D'ACIER")) , 3000 ,"ABLE","" , [ "ct", 47 , Skill_Info.id("AILE D'ACIER")] ]
    $data_item[ 165 ]=["CT48-"+ Skill_Info.name(Skill_Info.id("ECHANGE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("ECHANGE")) , 3000 ,"ABLE","" , [ "ct", 48 , Skill_Info.id("ECHANGE")] ]
    $data_item[ 166 ]=["CT49-"+ Skill_Info.name(Skill_Info.id("SAISIE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("SAISIE")) , 3000 ,"ABLE","" , [ "ct", 49 , Skill_Info.id("SAISIE")] ]
    $data_item[ 167 ]=["CT50-"+ Skill_Info.name(Skill_Info.id("SURCHAUFFE")) , "ct.png","TECH", Skill_Info.description(Skill_Info.id("SURCHAUFFE")) , 3000 ,"ABLE","" , [ "ct", 50 , Skill_Info.id("SURCHAUFFE")] ]
    $data_item[ 168 ]=["CS01-"+ Skill_Info.name(Skill_Info.id("COUPE")) , "cs.png","TECH", Skill_Info.description(Skill_Info.id("COUPE")) , 8000 ,"CS","" , [ "cs", 1 , Skill_Info.id("COUPE")] ]
    $data_item[ 169 ]=["CS02-"+ Skill_Info.name(Skill_Info.id("VOL")) , "cs.png","TECH", Skill_Info.description(Skill_Info.id("VOL")) , 8000 ,"CS","" , [ "cs", 2 , Skill_Info.id("VOL")] ]
    $data_item[ 170 ]=["CS03-"+ Skill_Info.name(Skill_Info.id("SURF")) , "cs.png","TECH", Skill_Info.description(Skill_Info.id("SURF")) , 58000 ,"CS","" , [ "cs", 3 , Skill_Info.id("SURF")] ]
    $data_item[ 171 ]=["CS04-"+ Skill_Info.name(Skill_Info.id("FORCE")) , "cs.png","TECH", Skill_Info.description(Skill_Info.id("FORCE")) , 8000 ,"CS","" , [ "cs", 4 , Skill_Info.id("FORCE")] ]
    $data_item[ 172 ]=["CS05-"+ Skill_Info.name(Skill_Info.id("FLASH")) , "cs.png","TECH", Skill_Info.description(Skill_Info.id("FLASH")) , 8000 ,"CS","" , [ "cs", 5 , Skill_Info.id("FLASH")] ]
    $data_item[ 173 ]=["CS06-"+ Skill_Info.name(Skill_Info.id("ECLATE-ROC")) , "cs.png","TECH", Skill_Info.description(Skill_Info.id("ECLATE-ROC")) , 8000 ,"CS","" , [ "cs", 6 , Skill_Info.id("ECLATE-ROC")] ]
    $data_item[ 174 ]=["CS07-"+ Skill_Info.name(Skill_Info.id("CASCADE")) , "cs.png","TECH", Skill_Info.description(Skill_Info.id("CASCADE")) , 8000 ,"CS","" , [ "cs", 7 , Skill_Info.id("CASCADE")] ]
    $data_item[ 175 ]=["CS08-"+ Skill_Info.name(Skill_Info.id("PLONGEE")) , "cs.png","TECH", Skill_Info.description(Skill_Info.id("PLONGEE")) , 8000 ,"CS","" , [ "cs", 8 , Skill_Info.id("PLONGEE")] ]
    $data_item[ 176 ]=[ ]
    $data_item[ 177 ]=["PASS ZONE 1","cartemagnetique.png","KEY","Pass pour la Zone 1.", 0 ,"KEYITEM",""]
    $data_item[ 178 ]=["PASS ZONE 2","cartemagnetique.png","KEY","Pass pour la Zone 2.", 0 ,"KEYITEM",""]
    $data_item[ 179 ]=["PASS ZONE 3","cartemagnetique.png","KEY","Pass pour la Zone 3.", 0 ,"KEYITEM",""]
    $data_item[ 180 ]=["PASS ZONE 4","cartemagnetique.png","KEY","Pass pour la Zone 4.", 0 ,"KEYITEM",""]    
    $data_item[ 259 ]=["VELO COURSE", "velocourse.png", "KEY", "Un vélo pliable pour aller 2 fois plus vite qu'à pied.", 0, "KEYITEM", "", [ "event", 31 ] ]
  end
  
  if ITEM_CONVERSION
    if FileTest.exist?("Data/data_item.txt")
      $data_items       = load_data("Data/Items.rxdata")
      for i in 1..$data_items.length-1
        item = $data_items[i]
        
        if $data_item[i] == nil
          $data_item[i] = []
        end
        
        string = item.description.split('//')
        
        # Description après usage
        if string[1] != nil
          $data_item[i].unshift(string[1])
        else
          $data_item[i].unshift("")
        end
        
        # Profil perso          
        custom_list = [false, false, false, false, false, false]
        for element in item.element_set
          case element
          when 34
            custom_list[0] = true
          when 35
            custom_list[1] = true
          when 36
            custom_list[2] = true
          when 37
            custom_list[3] = true
          when 38
            custom_list[4] = true
          when 39
            custom_list[5] = true
          end
        end
        $data_item[i].unshift(custom_list)
        
        # Prix
        $data_item[i].unshift(item.price)
        
        # Description
        if string[0] != nil
          $data_item[i].unshift(string[0])
        else
          $data_item[i].unshift("")
        end
        
        # Poche
        case item.element_set[0]
        when 28
          $data_item[i].unshift("ITEM")
        when 29
          $data_item[i].unshift("BALL")
        when 30
          $data_item[i].unshift("TECH")
        when 31
          $data_item[i].unshift("BERRY")
        when 32
          $data_item[i].unshift("KEY")
        else
          $data_item[i].unshift("ITEM")
        end
        
        $data_item[i].unshift(item.icon_name)
        
        $data_item[i].unshift(item.name)
        
        $data_item[i][8] = [item.recover_hp_rate, item.recover_hp, item.recover_sp_rate, item.recover_sp]
        $data_item[i][9] = item.minus_state_set
        
        if $data_item[i][7] == nil
          $data_item[i][7] = {}
        end
        
        $data_item[i][7]["recover_hp_rate"] = item.recover_hp_rate
        $data_item[i][7]["recover_hp"] = item.recover_hp
        $data_item[i][7]["recover_pp_all"] = item.recover_sp_rate
        $data_item[i][7]["recover_pp"] = item.recover_sp
        $data_item[i][7]["recover_state"] = item.minus_state_set
        
        if item.common_event_id != 0
          $data_item[i][7]["event"] = item.common_event_id
        end
        
      end
    else
      $data_item = load_data("Data/data_item.rxdata")
    end
  end
  
  
  #=============================================================================  
  # Item
  #=============================================================================  
  # Inspection de $data_item
  #   renvoie les infos nécessaires sur un ibjet
  #   par la connaissance de son id
  #   marche en parallèle à Pokemon_Party_Menu
  #
  # (Ex: $item.methode(id))
  #=============================================================================  
  class Item
    def self.name(id)
      if id == 0
        return "AUCUN"
      end
      return $data_item[id][0]
    end
    
    def self.icon(id)
      if id == 0
        return "return.png"
      end
      return $data_item[id][1]
    end
    
    def self.descr(id)
      if id == 0
        return "Aucune."
      end
      return $data_item[id][3]
    end
    
    def self.price(id)
      if id == 0
        return 0
      end
      return $data_item[id][4]
    end
    
    def self.socket(id)
      socket = $data_item[id][2]
      case socket
      when "ITEM"
        return 1
      when "BALL"
        return 2
      when "TECH"
        return 3
      when "BERRY"
        return 4
      when "KEY"
        return 5
      end
      return 1
    end
    
    # [ tenable/jetable, usage limité, usage en map, usage en combat, usage sur pokémon, apte/non apte]
    def self.profile(id)
      return $data_item[id][5]
    end
    
    def self.battle_usable?(id)
      return profile(id)[3]
    end
    
    def self.map_usable?(id)
      return profile(id)[2]
    end
    
    def self.limited_use?(id)
      return profile(id)[1]
    end
    
    def self.holdable?(id)
      return profile(id)[0]
    end
    
    def self.soldable?(id)
      return profile(id)[0]
    end
    
    def self.use_on_pokemon?(id)
      return profile(id)[4]
    end
    
    def self.item_able_mode?(id)
      return profile(id)[5]
    end
    
    def self.use_string(id)
      return $data_item[id][6]
    end
    
    def self.data(id) # Renvoie Hash
      return $data_item[id][7]
    end
    
    def self.log_data_A(id)
      return $data_item[id][8]
    end
    
    def self.log_data_B(id)
      return $data_item[id][9]
    end
    
    def self.log_data_C(id)
      return $data_item[id][9]
    end
    
    def self.id(string)
      for item in $data_item
        if item != nil
          if string == item[0]
            return $data_item.index(item)
          end
        end
      end
      return 0
    end
    
    
    def self.able?(id, pokemon)
      # Analyse data
      if data(id) != nil
        if data(id)["stone"]
          if pokemon.evolve_check("stone", name(id))
            return true
          end
          #list = pokemon.evolve_list
          #for i in 1..list.length-1
          #  for j in 1..list[i].length-1
          #    if list[i][j][0] == "stone" # Paramètre évolution
          #      if list[i][j][1] == name(id) # Nom de la pierre si le param est bon
          #        return true
          #      end
          #    end
          #  end
          #end
        end
        if data(id)["ct"] != nil
          list = pokemon.skills_allow
          if list.include?(data(id)["ct"][0]) # ID de la CT
            return true
          end
        end
        if data(id)["cs"] != nil
          list = pokemon.skills_allow
          if list != nil
            if list.include?([data(id)["cs"][0]]) # [ID de la CS]
              return true
            end
          end
        end
      end
      return false
    end
    
    
    def self.effect(id)
      has_effect = false
      return_map = false
      
      if use_on_pokemon?(id)
        return [false, "Erreur."]
      end
      
      if data(id) != nil
        if data(id)["repel"] != nil
          has_effect = true
          $pokemon_party.repel_count += data(id)["repel"]
        end
        if data(id)["ball"] != nil
          has_effect = true
        end
        if data(id)["event"] != nil
          has_effect = true
          return_map = true
          $game_temp.common_event_id = data(id)["event"]
          $scene = Scene_Map.new
        end
      end
      
      if not(has_effect)
        return [false, "Ce n'est pas le moment d'utiliser ça.", false]
      end
      
      return [true, sprintf(use_string(id), Player.name), return_map]
    end
    
      
    
    def self.effect_on_pokemon(id, pokemon, scene) # renvoie [utilisé, texte]
      _name = pokemon.given_name
      _amount = 0
      # Vérification de cible
      if not(use_on_pokemon?(id))
        return [false, "Erreur."]
      end
      
      # ---------------------------------------------------
      # Cycle des effets
      has_effect = false
      heal_status = false
      heal_state = false
      heal = false
      # ---------------------------------------------------
      if data(id) != nil
        if data(id)["level_up"] != nil
          has_effect = true
          for i in 1..data(id)["level_up"]
            if pokemon.level >= MAX_LEVEL
              has_effect = false
              break
            end
            pokemon.level_up(scene)
            if pokemon.evolve_check != false
              scenebis = POKEMON_S::Pokemon_Evolve.new(pokemon, pokemon.evolve_check, scene.z_level + 300)
              scenebis.main
            end
          end
        end
           # variable qui contient la stat qui augmente.  
           type_boostage = "aucun"   
           if data(id)["boost"] != nil    
              # si un oeuf est choisi, rien ne se passe.  
              if pokemon.name != "OEUF"   
                list = [10,10,10,10,10,10]  
                list[data(id)["boost"]] = 10  
                 case data(id)["boost"]    
                    when 0     
                      # indique la stat qui sera inscrite dans le message à la fin.  
                      type_boostage = "PV MAX"   
                      has_effect = pokemon.add_bonus(list)  
                    when 1     
                      pokemon.atk = pokemon.atk + 2    
                      type_boostage = "ATTAQUE MAX"    
                    when 2     
                      pokemon.dfe = pokemon.dfe + 2    
                      type_boostage = "DEFENSE MAX"    
                    when 3     
                      pokemon.spd = pokemon.spd + 2    
                      type_boostage = "VITESSE MAX"    
                    when 4     
                      pokemon.ats = pokemon.ats + 2    
                      type_boostage = "ATQ.SPE. MAX"    
                    when 5     
                      pokemon.dfs = pokemon.dfs + 2    
                      type_boostage = "DEF.SPE. MAX"    
                   end    
               end    
            end   



        if data(id)["battle_boost"] != nil
          case data(id)["battle_boost"]
          when 0 #Atk
            amount = pokemon.change_atk(1)
          when 1 # Dfe
            amount = pokemon.change_dfe(1)
          when 2
            amount = pokemon.change_spd(1)
          when 3
            amount = pokemon.change_ats(1)
          when 4
            amount = pokemon.change_dfs(1)
          when 5
            amount = pokemon.change_eva(1)
          when 6
            amount = pokemon.change_acc(1)
          end
          if amount > 0
            has_effect = true
            pokemon.raise_loyalty
          end
        end
        
        if data(id)["stone"] != nil
          has_effect = true
          evolve_id = pokemon.evolve_check("stone", name(id))
          scenebis = POKEMON_S::Pokemon_Evolve.new(pokemon, evolve_id, scene.z_level + 300, true)
          scenebis.main
        end
        
        if data(id)["ct"]
          has_effect = true
          skill_id = data(id)["ct"][1]
          if not(pokemon.skill_learnt?(skill_id))
            scenebis = POKEMON_S::Pokemon_Skill_Learn.new(pokemon, skill_id, scene)
            scenebis.main
            return [scenebis.return_data, ""]
          else
            return [false, pokemon.name + " connaît déjà " + Skill_Info.name(skill_id) + '.' ]
          end
        end
        
        if data(id)["cs"]
          has_effect = true
          skill_id = data(id)["cs"][1]
          if not(pokemon.skill_learnt?(skill_id))
            scenebis = POKEMON_S::Pokemon_Skill_Learn.new(pokemon, skill_id, scene)
            scenebis.main
            return [scenebis.return_data, ""]
          else
            return [false, pokemon.name + " connaît déjà " + Skill_Info.name(skill_id) + '.' ]
          end
        end
      
        # ---------------------------------------------------
        # Cycle de soin des PP
        # Soin chiffré de toutes les attaques
        if data(id)["recover_pp_all"] == 1
          value = 0
          for i in 0..pokemon.skills_set.length-1
            value += pokemon.refill_skill(i, data(id)["recover_pp"])
          end
          if value > 0
            has_effect = true
          end
        # Soin d'une attaque == appel de skill
        elsif data(id)["recover_pp_all"] == 0 and data(id)["recover_pp"] > 0
          index = pokemon.skill_selection
          # N'a pas utilisé, retour au Sac
          if index == -1
            return [false, ""]
          end
          _amount = pokemon.refill_skill(index, data(id)["recover_pp"])
          # Si le skill visé n'est pas plein
          if _amount > 0
            has_effect = true
          end
        end
      
        # ---------------------------------------------------
        # Cycle de soin
        # Soin par pourcentage
        _amount += pokemon.max_hp * data(id)["recover_hp_rate"] / 100
        # Soin par points
        _amount += data(id)["recover_hp"]
        _amount = [pokemon.max_hp - pokemon.hp, _amount].min
        
        if data(id)["recover_hp_rate"] + data(id)["recover_hp"] > 0
          heal = true
        end
        
        if _amount == 0
          heal = false
        end
        # En cas de Pokémon mort: Ne fonctionne pas, excepté si passage dans 
        #   cycle de rappel
        if pokemon.dead?
          heal = false
        end
        # ---------------------------------------------------
      
        # ---------------------------------------------------
        # Cycle de rappel
        if data(id)["recover_state"].include?(9)
          if pokemon.dead?
            heal = true
          else
            heal = false
          end
        end
      
      
        # ---------------------------------------------------
        # Cycle de soin de statut
        # Statuts
        if data(id)["recover_state"].include?(pokemon.status)
          heal_status = true
        end
        # Confusion
        if data(id)["recover_state"].include?(6) and pokemon.confused?
          heal_state = true
        end
        
        # ---------------------------------------------------
        # Effets discrets
        if data(id)["loyalty"] != nil
          loyalty_pts = data(id)["loyalty"]
          if loyalty_pts < 0
            if pokemon.loyalty >= 200
              pokemon.loyalty += loyalty_pts - 5
            else
              pokemon.loyalty += loyalty_pts
            end
          else
            if pokemon.loyalty < 100
              pokemon.loyalty += loyalty_pts
            elsif pokemon.loyalty < 200 and loyalty_pts > 1
              pokemon.loyalty += loyalty_pts - 2
            elsif loyalty_pts > 2
              pokemon.loyalty += loyalty_pts - 3
            end
          end
        end
        
        
        
      end
      # ---------------------------------------------------
      
      # ---------------------------------------------------
      # Si il n'y a aucun effet
      # ---------------------------------------------------
      # vérifie si aucune stat n'a été boostée.  
      if type_boostage == "aucun"  
          if not(heal_status or heal_state or heal or has_effect)     
              return [false, "Ça n'aura aucun effet."]    
          end  
      else    
      # sinon, jouer la musique et indiquer quel pokémon augmente quel stat.  
          Audio.me_play("Audio/ME/PkmRB-Item.mid")    
        return [true, type_boostage + " de " + pokemon.name + " a augmenté!"]      
      end        
      
      # ---------------------------------------------------
      # Effets nécessitant le rafraichissement de la fenêtre
      # ---------------------------------------------------
      scene.item_refresh
      Graphics.update
      
      if heal_state
        pokemon.cure_state
      end
      
      if heal_status
        pokemon.cure
      end
      
      # Animation de soin (pas de condition pour que Rappel fonctionne)
      if heal
        for i in 1.._amount
          pokemon.hp += 1
          scene.hp_refresh
          if pokemon.hp == 1 # Vient de sortir du K.O.
            scene.refresh
          end
          Graphics.update
        end
      end
      
      return [true, sprintf(use_string(id), _name, _amount) ]
    end
    
  end
  
end