     module POKEMON_S  
      class Pokemon_Menu  
          
        def initialize(menu_index = 0)  
          @menu_index = menu_index  
        end  
        
        def main  
          Graphics.freeze  
          @z_level = 10000  
          # Image de fond du menu  
          @spriteset = Spriteset_Map.new  
          @background = Sprite.new  
          @background.bitmap = RPG::Cache.picture("Menu_Back.PNG")  
          @background.x = 0  
          @background.y = 0  
          @background.z = @z_level  
          @command = POKEMON_S::Window_Menu.new  
          @command.x = 0  
          @command.y = 0  
          @command.z = @z_level + 2  
          @command.opacity = 0  
          @cde0 = Sprite.new  
          @cde0.bitmap = RPG::Cache.picture("Menu0.PNG")  
          @cde0.y = 44+ 84 *(0 % 3)  
          @cde0.x = 48 + 160*(0 / 3)  
          @cde0.z = @z_level + 2  
          if not($data_pokedex[0])  
            @cde0.opacity = 0  
          end  
          @cde1 = Sprite.new  
          @cde1.bitmap = RPG::Cache.picture("Menu1.PNG")  
          @cde1.y = 44+ 84 *(1 % 3)  
          @cde1.x = 48 + 160*(1 / 3)  
          @cde1.z = @z_level + 2  
          if $pokemon_party.size == 0  
            @cde1.opacity = 0  
          end  
          @cde2 = Sprite.new  
          @cde2.bitmap = RPG::Cache.picture("Menu2.PNG")  
          @cde2.y = 44+ 84 *(2 % 3)  
          @cde2.x = 48 + 160*(2 / 3)  
          @cde2.z = @z_level + 2  
          @cde3 = Sprite.new  
          @cde3.bitmap = RPG::Cache.picture("Menu3.PNG")  
          @cde3.y = 44+ 84 *(3 % 3)  
          @cde3.x = 48 + 160*(3 / 3)  
          @cde3.z = @z_level + 2  
          @cde4 = Sprite.new  
          @cde4.bitmap = RPG::Cache.picture("Menu4.PNG")  
          @cde4.y = 44+ 84 *(4 % 3)  
          @cde4.x = 48 + 160*(4 / 3)  
          @cde4.z = @z_level + 2  
          @cde5 = Sprite.new  
          @cde5.bitmap = RPG::Cache.picture("Menu5.PNG")  
          @cde5.y = 44+ 84 *(5 % 3)  
          @cde5.x = 48 + 160*(5 / 3)  
          @cde5.z = @z_level + 2  
          if not($data_pokedex[0]) and @menu_index == 0  
            @menu_index = 1  
          end  
          if $pokemon_party.size == 0 and @menu_index == 1  
            @menu_index = 2  
          end  
          @curseur = Sprite.new  
          @curseur.bitmap = RPG::Cache.picture("Menu_curseur.PNG")  
          @curseur.y = 44+ 84 *(@menu_index % 3)  
          @curseur.x = 48 + 160*(@menu_index / 3)  
          @curseur.z = @z_level + 4  
            
          Graphics.transition  
          loop do  
            Graphics.update  
            Input.update  
            update  
            @curseur.dispose  
            @curseur = Sprite.new  
            @curseur.bitmap = RPG::Cache.picture("Menu_curseur.PNG")  
            @curseur.y = 44+ 84 *(@menu_index % 3)  
            @curseur.x = 48 + 160*(@menu_index / 3)  
            @curseur.z = @z_level + 4  
            @command.refresh  
            if $scene != self  
              break  
            end  
          end  
          Graphics.freeze  
          @curseur.dispose  
          @command.dispose  
          @background.dispose  
          @spriteset.dispose  
          @command.dispose  
          @cde0.dispose  
          @cde1.dispose  
          @cde2.dispose  
          @cde3.dispose  
          @cde4.dispose  
          @cde5.dispose  
        end  
          
      def update  
        @spriteset.update  
        a = 0  
        if Input.trigger?(Input::UP) and @menu_index>0  
          @menu_index -= 1  
          if @menu_index == 2  
            @menu_index = 3  
            a = 1  
          end  
          if not($data_pokedex[0]) and @menu_index == 0  
            @menu_index = 1  
            a = 1  
          end  
          if $pokemon_party.size == 0 and @menu_index == 1  
            @menu_index = 2  
            a = 1  
          end  
      
          if a == 0  
            $game_system.se_play($data_system.decision_se)  
          end  
        end  
        if Input.trigger?(Input::DOWN) and @menu_index<5  
          @menu_index += 1   
          if @menu_index == 3  
            @menu_index = 2  
            a = 1  
          end  
          if a == 0  
            $game_system.se_play($data_system.decision_se)  
          end  
        end  
        if Input.trigger?(Input::LEFT) and @menu_index>2  
          $game_system.se_play($data_system.decision_se)  
          @menu_index -= 3  
          if not($data_pokedex[0]) and @menu_index == 0  
            @menu_index = 1  
          end  
          if $pokemon_party.size == 0 and @menu_index == 1  
            @menu_index = 2  
          end  
        end  
        if Input.trigger?(Input::RIGHT) and @menu_index<3  
          $game_system.se_play($data_system.decision_se)  
          @menu_index += 3  
        end    
        if Input.trigger?(Input::B)  
          $game_system.se_play($data_system.cancel_se)  
          $scene = Scene_Map.new  
          return  
        end  
        if Input.trigger?(Input::C)  
          if @menu_index == 0 # Pokédex  
            if not($data_pokedex[0])  
              $game_system.se_play($data_system.buzzer_se)  
              return  
            end  
            $game_system.se_play($data_system.decision_se)  
            $scene = POKEMON_S::Pokemon_Pokedex.new  
          end    
          if @menu_index == 1 # Menu  
            if $pokemon_party.size == 0  
              $game_system.se_play($data_system.buzzer_se)  
              return  
            end  
            $game_system.se_play($data_system.decision_se)  
            $scene = POKEMON_S::Pokemon_Party_Menu.new  
          end  
          if @menu_index == 2 # Sac  
            $game_system.se_play($data_system.decision_se)  
            $scene = Pokemon_Item_Bag.new  
          end  
          if @menu_index == 3 # Carte dresseur  
            $game_system.se_play($data_system.decision_se)  
            $game_temp.common_event_id = 19  
            $scene = Scene_Map.new  
          end  
          if @menu_index == 4 # Sauvegarde  
            if $game_system.save_disabled  
              $game_system.se_play($data_system.buzzer_se)  
              return  
            end  
            $game_system.se_play($data_system.decision_se)  
            $scene = POKEMON_S::Pokemon_Save.new  
          end  
          if @menu_index == 5 # Quitter le menu  
            $game_system.se_play($data_system.decision_se)  
            $scene = Scene_Map.new  
          end  
          return  
        end  
      end        
    end  
        
      class Window_Location < Window_Base  
        #--------------------------------------------------------------------------  
        def initialize  
          super(0, 0, 300, 96)  
          self.contents = Bitmap.new(width - 32, height - 32)  
          self.contents.font.name = $fontface  
          self.contents.font.size = $fontsize  
          refresh  
        end  
        #--------------------------------------------------------------------------  
        def refresh  
          self.contents.clear  
          self.contents.font.color = Color.new(40,40,40,255)  
          self.contents.draw_text(6, 0, 300, 32,$data_mapzone[$game_map.map_id][1])  
          self.contents.draw_text(4, 2, 300, 32,$data_mapzone[$game_map.map_id][1])  
          self.contents.draw_text(6, 2, 300, 32,$data_mapzone[$game_map.map_id][1])  
          self.contents.font.color = Color.new(251,251,251,255)  
          self.contents.draw_text(4, 0, 300, 32,$data_mapzone[$game_map.map_id][1])  
        end  
      end  
      
      class Window_Argent < Window_Base  
      
        #--------------------------------------------------------------------------  
        def initialize  
          super(0, 0, 900, 96)  
          self.contents = Bitmap.new(width - 32, height - 32)  
          self.contents.font.name = $fontface  
          self.contents.font.size = $fontsize  
          refresh  
        end  
      
        #--------------------------------------------------------------------------  
        def refresh  
          self.contents.clear  
          self.contents.font.color = Color.new(40,40,40,255)  
          self.contents.draw_text(6, 0, 300, 32, $pokemon_party.money.to_s + "$", 2)  
          self.contents.draw_text(4, 2, 300, 32, $pokemon_party.money.to_s + "$", 2)  
          self.contents.draw_text(6, 2, 300, 32, $pokemon_party.money.to_s + "$", 2)  
          self.contents.font.color = Color.new(251,251,251,255)  
          self.contents.draw_text(4, 0, 300, 32,$pokemon_party.money.to_s + "$", 2)  
        end  
      end  
        
      class Window_Menu < Window_Base  
        #--------------------------------------------------------------------------  
        def initialize  
          super(0, 0, 640, 480)  
          self.contents = Bitmap.new(width - 32, height - 32)  
          self.contents.font.name = $fontface  
          self.contents.font.size = $fontsize  
          self.contents.clear  
          refresh  
        end  
        #--------------------------------------------------------------------------  
        def refresh  
          s1 = "POKéDEX"  
          s2 = "POKéMON"  
          s3 = "SAC"  
          s4 = Player.name  
          s5 = "SAUVER"  
          s6 = "QUITTER"  
          menu = [s1,s2,s3,s4,s5,s6]  
          for i in 0..5  
            if not($data_pokedex[0]) and i == 0  
              i = 1  
            end  
            if $pokemon_party.size == 0 and i == 1  
              i = 2  
            end  
            y = 46+ 84 *(i % 3) + 34  
            x = 72 + 160*(i / 3)  
            text = menu[i].to_s  
            a = self.contents.text_size(text).width / 2  
            self.contents.font.color = Color.new(107,107,107,255)  
            self.contents.draw_text((x-a)+2, y, 300, 32, text)  
            self.contents.draw_text((x-a), y+2, 300, 32, text)  
            self.contents.draw_text((x-a)+2, y+2, 300, 32, text)  
            self.contents.font.color = Color.new(251,251,251,255)  
            self.contents.draw_text((x-a), y, 300, 32,text)  
          end    
        end  
        def dispose  
          self.contents.clear  
        end  
      end  
      
    end  