    #==============================================================================  
    # ■ Pokemon_Pokedex  
      
    # 05/10/09 -  par Slash et sphinx  
    #-----------------------------------------------------------------------------  
    # Scène modifiable  
    #-----------------------------------------------------------------------------  
    # Pokédex  
    #-----------------------------------------------------------------------------  
      
    module POKEMON_S  
        
    #---------------------------------------------------------------------------------  
    # Class Pokedex Pokedex  
    # Cette class gere la page d'acceuil du pokedex  
    #---------------------------------------------------------------------------------    
      class Pokemon_Pokedex #(Numérique)  
        def initialize(index = 0, show = true)  
          @index = index  
          @show = show  
          @table = []  
          @commande = []  
          # Regional  
          if POKEMON_S._DEXREG  
            for id in 1..$data_pokedex.length-1  
              @table[Pokemon_Info.id_bis(id)] = id  
            end  
            @table.shift # débarasser l'élément 0  
            @table.compact!  
          else  
          # National  
            for id in 1..$data_pokedex.length-1  
              @table.push(id)  
            end  
          end  
        end  
          
        def main  
          # Fenêtre principale  
          # Fond  
          @background = Sprite.new  
          @background.bitmap = RPG::Cache.picture("COAA_pokedex.png")  
          @background.z = 5  
            
          # Liste  
          @list = []  
          for i in 0.. @table.length-1  
            if $data_pokedex[@table[i]][0]  
              @list.push(@table[i]) # liste des ID vus  
            end  
          end     
          # Vu/Capture  
          @pokemon_amount = Window_Base.new(27 - 16, 306, 640, 400)  
          @pokemon_amount.contents = Bitmap.new(640, 400-32)  
          @pokemon_amount.contents.font.name = $fontface  
          @pokemon_amount.contents.font.size = $fontsize  
          @pokemon_amount.contents.font.bold = true  
          @pokemon_amount.opacity = 0  
          viewed = @list.length.to_s  
          captured = 0  
          for element in @list  
            if $data_pokedex[element][1]  
              captured += 1  
            end  
          end  
      
          @pokemon_amount.contents.draw_text(-40, 10, 300, 35, "POKEMONS APERCUS", 2)  
          @pokemon_amount.contents.draw_text(250, 10, 300, 35, "POKEMONS ATTRAPES", 2)  
          @pokemon_amount.contents.draw_text(-40, 22, 300, 35, ". . . . . . . . . . . . . . . . . .", 2)  
          @pokemon_amount.contents.draw_text(250, 22, 300, 35, ". . . . . . . . . . . . . . . . . .", 2)  
          string_viewed = sprintf("%03d", viewed)  
          string_captured = sprintf("%03d", captured)  
          @pokemon_amount.contents.draw_text(-40, 47, 300, 35, "[ "+ string_viewed +" ]", 2)  
          @pokemon_amount.contents.draw_text(250, 47, 300, 35, "[ "+ string_captured +" ]", 2)  
            
          Graphics.transition  
          loop do  
            Graphics.update  
            Input.update  
            update  
            if $scene != self  
              break  
            end  
          end  
          Graphics.freeze  
          @background.dispose  
          @pokemon_amount.dispose  
        end  
          
        def update  
           
          if Input.trigger?(Input::B)  
            $game_system.se_play($data_system.cancel_se)  
            $scene = POKEMON_S::Pokemon_Menu.new(0)  
            return  
          end  
          if Input.trigger?(Input::C)  
            @pokemon_amount.dispose  
            @pokemon_amount = Window_Base.new(27 - 16, 306, 640, 400)  
            @pokemon_amount.contents = Bitmap.new(640, 400-32)  
            @pokemon_amount.contents.font.name = $fontface  
            @pokemon_amount.contents.font.size = $fontsize  
            @pokemon_amount.contents.font.bold = true  
            @pokemon_amount.opacity = 0  
            @pokemon_amount.contents.draw_text(120, 10, 300, 35, "CHARGEMENT DU POKEDEX", 2)  
            @pokemon_amount.contents.draw_text(112, 47, 300, 35, "PATIENTEZ UN INSTANT", 2)  
            $game_system.se_play($data_system.cancel_se)  
            $scene = POKEMON_S::Pokemon_Pokedex_List.new(@table, @list,true)  
            return  
          end  
        end  
      end  
        
    #---------------------------------------------------------------------------------  
    # Class Pokedex Pokedex List  
    # Cette class gere l'affichage de la page principale du pokédex  
    #---------------------------------------------------------------------------------    
      class Pokemon_Pokedex_List  
        def initialize (table, id_list = [],show = true,index = 0)  
          @index = index  
          @table = table  
          @list = id_list  
          @pokelist = []  
          @show = show  
          if show == true  
            n = 0   
            for i in 0.. @table.length-1    
              if $data_pokedex[@table.length-i][0]   
                n=@table.length-i   
                break   
              end    
            end   
            for i in 0..n-1 
              if POKEMON_S._DEXREG  
                ida = "   " + sprintf("%03d",Pokemon_Info.id_bis(@table[i]))  
              else  
                ida = "   " + sprintf("%03d",@table[i])  
              end  
              @pokelist.push(ida)  
            end  
          else  
        for i in 0.. @table.length-1
          if POKEMON_S._DEXREG
            ida = "   " + sprintf("%03d",Pokemon_Info.id_bis(@table[i]))
          else
            ida = "   " + sprintf("%03d",@table[i])
          end
          @pokelist.push(ida)
        end
      end
            
        end  
          
        def main  
          #affichage du background du pokedex  
          @back = Sprite.new  
          @back.bitmap = RPG::Cache.picture("Pokedexsl.png")  
          @back.x = 0  
          @back.y = 0  
          @back.z = 5  
          #affichage de l'avant du pokedex  
          @front = Sprite.new  
          @front.bitmap = RPG::Cache.picture("Pokedex_list.png")  
          @front.x = 0  
          @front.y = 0  
          @front.z = 10007        
          #affichage des pokemon ds le pokedex  
          @pokemon_window = POKEMON_S::Pokedex_Command.new(520, @table, @pokelist, $fontsize, 5, 104)  
          @pokemon_window.x = 54  
          @pokemon_window.y = -20  
          @pokemon_window.z = 6  
          @pokemon_window.height = 440  
          @pokemon_window.opacity  = 0  
          #affichage du nom des pokemon ds le pokedex  
          idn = @table[@pokemon_window.index]  
          @pokemonname = POKEMON_S::Pokedex_Name.new(idn)  
          @pokemonname.x = 145  
          @pokemonname.y = 337  
          @pokemonname.z = 10008  
          @pokemonname.opacity = 0  
          @pokemon_window.index = @index  
          Graphics.transition  
          loop do  
            Graphics.update  
            Input.update          
            @pokemon_window.update  
            #affichage du nom des pokemon ds le pokedex  
            @pokemonname.dispose  
            idn = @table[@pokemon_window.index]  
            @pokemonname = POKEMON_S::Pokedex_Name.new(idn)  
            @pokemonname.x = 145  
            @pokemonname.y = 337  
            @pokemonname.z = 10008  
            @pokemonname.opacity = 0  
            update  
            if $scene != self  
              break  
            end  
          end  
          @pokemonname.dispose  
          @pokemon_window.dispose  
          @back.dispose  
          @front.dispose  
            
        end  
        def update  
          if Input.trigger?(Input::C)  
            @index = @pokemon_window.index  
            pokemon_id = @table[@index]  
            seen = $data_pokedex[pokemon_id][0]  
            if seen == false  
              $game_system.se_play($data_system.buzzer_se)  
              return  
            end  
            $scene = Pokemon_Detail.new(pokemon_id, @show)  
            return  
          end    
            
          #si j'appuis sur ECHAP je retourne a la page de garde du pokedex  
          if Input.trigger?(Input::B)  
            $game_system.se_play($data_system.cancel_se)  
            $scene = POKEMON_S::Pokemon_Pokedex.new  
            return  
          end  
          #si j'appuis sur SHIFT je tronque ou détronque le pokedex  
          if Input.trigger?(Input::A)  
            $game_system.se_play($data_system.cancel_se)  
            if @show == true  
            $scene = POKEMON_S::Pokemon_Pokedex_List.new(@list,@table, false,0)  
            else  
            $scene = POKEMON_S::Pokemon_Pokedex_List.new(@list,@table, true,0)  
            end  
            return  
          end  
        end    
      end  
        
    #---------------------------------------------------------------------------------  
    # Class Pokedex Name  
    # Cette class gere l'affichage du nom des pokémon ds le pokedex  
    #---------------------------------------------------------------------------------  
      
      class Pokedex_Name < Window_Base  
        def initialize(index)  
          super(0, 0, 350,100)  
          @idn = index  
          self.contents = Bitmap.new(width - 32, height - 32)  
          self.contents.font.name = $fontface  
          self.contents.font.size = $fontsize  
          refresh  
        end  
      
        def refresh  
          if $data_pokedex[@idn][0]  
            name = $data_pokemon[@idn][0]  
          else  
            name = " "  
          end  
          x = self.contents.text_size(name).width / 2  
          self.contents.clear  
          self.contents.font.color = Color.new(107,107,107,255)  
          self.contents.draw_text(162-x, 0, 300, 32,name)  
          self.contents.draw_text(160-x, 2, 300, 32,name)  
          self.contents.draw_text(162-x, 2, 300, 32,name)  
          self.contents.font.color = Color.new(251,251,251,255)  
          self.contents.draw_text(160-x, 0, 300, 32,name)  
        end  
      end  
        
    #---------------------------------------------------------------------------------  
    # Class Pokedex Command  
    # Cette class gere l'affichage des pokémon ds le pokedex  
    #---------------------------------------------------------------------------------  
      class Pokedex_Command < Window_Selectable  
        attr_reader :item_max  
        attr_reader :commands  
        
        def initialize(width,table, commands, size = $fontsize, column = 1, height = nil)  
          @table = table  
          if size == $fontsize  
            @heightsize = 32  
          elsif size == $fontsizebig  
            @heightsize = 43  
          else  
            @heightsize = 32  
          end  
          if height != nil  
            @height = height  
          else  
            @height = @heightsize  
          end  
          super(0, 0, width, commands.size * @height + 32, @height)  
          @item_max = commands.size  
          @commands = commands  
          @column_max = column  
          self.contents = Bitmap.new(width, @item_max * @height)  
          self.contents.font.name = $fontface  
          self.contents.font.size = size  
          refresh  
          self.index = 0  
        end  
        
        def refresh  
          self.contents.clear  
          for i in 0...@item_max  
            draw_item(i, normal_color)  
          end  
        end      
      
        def draw_item(index, color = normal_color)  
          self.contents.font.color = color  
          # Modification pour le tracé du texte  
          rect = Rect.new(4 + 8 + (index % @column_max) * (width/@column_max),   
            @height * (index/@column_max) + (@height-@heightsize)/2, self.contents.width/@column_max, @heightsize)  
          self.contents.fill_rect(rect, Color.new(0, 0, 0, 0))  
          id = @table[index]  
          if $data_pokedex[id][0]  
            self.contents.font.color = Color.new(107, 107, 107, 255)  
          else  
            self.contents.font.color = Color.new(173, 173, 173, 255)  
          end    
          self.contents.font.bold = true  
          self.contents.draw_text(rect, @commands[index])  
          if $data_pokedex[id][0]  
            string = sprintf("%03d",id)  
            bitmap = RPG::Cache.battler("Icon/#{sprintf('%03d', id)}.png", 0)  
            self.contents.blt(8 + (index % @column_max) * (width/@column_max),   
             @height * (index/@column_max) + (@height-@heightsize)/2 + 24, bitmap, Rect.new(0, 0, 64, 64),128)  
          end          
          if $data_pokedex[id][1]  
            bitmap = RPG::Cache.picture("Pokedexball.png")  
            self.contents.blt(8 + (index % @column_max) * (width/@column_max),   
             @height * (index/@column_max) + (@height-@heightsize)/2 + 4, bitmap, Rect.new(0, 0, 24, 24))  
            string = sprintf("%03d",id)  
            bitmap = RPG::Cache.battler("Icon/#{sprintf('%03d', id)}.png", 0)  
            self.contents.blt(8 + (index % @column_max) * (width/@column_max),   
             @height * (index/@column_max) + (@height-@heightsize)/2 + 24, bitmap, Rect.new(0, 0, 64, 64))  
          end  
        end  
      
        def disable_item(index)  
          draw_item(index, disabled_color)  
        end  
        
        def enable_item(index)  
          # Nouvelle fonction  
          draw_item(index, normal_color)  
      
        end  
      end  
        
    #---------------------------------------------------------------------------------  
    # Class Pokedex Detail  
    # Cette class gere l'affichage des infos sur les pokemons  
    #---------------------------------------------------------------------------------  
      class Pokemon_Detail  
            def initialize(id, show, mode = 0, appel = "pkdx", z_level = 100)
      @id = id
      @show = show
      @mode = mode
      @appel = appel
      @z_level = z_level
      @table = []
      # Regional
      if POKEMON_S._DEXREG
        for id in 1..$data_pokedex.length-1
          @table[Pokemon_Info.id_bis(id)] = id
        end
        @table.shift # débarasser l'élément 0
        @table.compact!
      else
      # National
        for id in 1..$data_pokedex.length-1
          @table.push(id)
        end
      end
     
      @_break = false
    end
   
    def main
      # Fenêtre détail
      @background = Sprite.new
      @background.bitmap = RPG::Cache.picture("PokedexShfond1.png")
      @background.z = @z_level
     
      # Sprite
      @pokemon_sprite = Sprite.new
      @pokemon_sprite.x = 26
      @pokemon_sprite.y = 71
      @pokemon_sprite.z = 10 + @z_level
      @pokemon_sprite.visible = false
     
      # Identité
      @data_window = Window_Base.new(233-16, 76-16, 370+32, 196+32)
      @data_window.contents = Bitmap.new(370, 196)
      color = Color.new(60,60,60)
      @data_window.contents.font.name = $fontface
      @data_window.contents.font.size = $fontsizebig
      @data_window.contents.font.color = color
      @data_window.opacity = 0
      @data_window.z = 10 + @z_level
      @data_window.visible = false
     
      # Descr
      @text_window = Window_Base.new(60 - 16, 252 - 16 + 51, 550 + 32, 160 + 32)
      @text_window.contents = Bitmap.new(550 , 160)
      @text_window.contents.font.name = $fontface
      @text_window.contents.font.size = $fontsize
      @text_window.contents.font.color = color
      @text_window.opacity = 0
      @text_window.z = 10 + @z_level
      @text_window.visible = false
     
      @list = []
      for i in 0.. @table.length-1
        if $data_pokedex[@table[i]][0]
          @list.push(@table[i])
        end
      end
     
      if @mode == 0
        filename = "Audio/SE/Cries/" + sprintf("%03d", @id) + "Cry.wav"
        if FileTest.exist?(filename)
          Audio.se_play(filename)
        end
      end
     
      data_refresh
      @background.visible = false
      @pokemon_sprite.visible = false
      @data_window.visible = false
      @text_window.visible = false
      case @mode
      when 0
        @background.visible = true
        @pokemon_sprite.visible = true
        @data_window.visible = true
        @text_window.visible = true
      when 1
        refresh_zone
      when 2
        refresh_cri
      when 3
        refresh_tail
      end
       
      Graphics.transition
      loop do
        Graphics.update
        Input.update
        update
        if @_break
          break
        end
      end
      Graphics.freeze
      @background.dispose
      @data_window.dispose
      @pokemon_sprite.dispose
      @text_window.dispose
    end
   
    def update
      case @appel
      when "pkdx"
        if Input.trigger?(Input::B)
          $game_system.se_play($data_system.cancel_se)
          Graphics.freeze
          hide
          if @show
            index = @table.index(@id)
          else
            index = @list.index(@id)
          end
          @_break = true
          $scene = Pokemon_Pokedex.new(index, @show)
          return
        end
       
        if Input.trigger?(Input::C) and @mode == 1
          Graphics.freeze
          hide
          $game_system.se_play($data_system.decision_se)
          $game_temp.map_temp = ["PKDX", false, $game_map.map_id, $game_player.x,
            $game_player.y, $game_player.direction, $game_player.character_name,
            $game_player.character_hue, $game_player.step_anime,
            $game_system.menu_disabled, POKEMON_S::_MAPLINK, @id, @show]
          $game_temp.transition_processing = true
          $game_temp.transition_name = ""
          POKEMON_S::_MAPLINK = false

          @_break = true
          $scene = Scene_Map.new
          $game_map.setup(POKEMON_S::_WMAPID)
          $game_player.moveto(9, 7)
          $game_map.autoplay
          $game_map.update
          return
        end
       
        if Input.trigger?(Input::C) and @mode == 2
          filename = "Audio/SE/Cries/" + sprintf("%03d", @id) + "Cry.wav"
          if FileTest.exist?(filename)
            Audio.se_play(filename)
          end
        end
       
        if Input.trigger?(Input::DOWN)
          Graphics.freeze
          index = @list.index(@id)
          if @id == @list.last
            @id = @list.first
          else
            @id = @list[index+1]
          end
          hide
          Graphics.transition(5)
          Graphics.freeze
          @mode = 0
          data_refresh
          Graphics.transition
          filename = "Audio/SE/Cries/" + sprintf("%03d", @id) + "Cry.wav"
          if FileTest.exist?(filename)
            Audio.se_play(filename)
          end
        end
       
        if Input.trigger?(Input::UP)
          Graphics.freeze
          index = @list.index(@id)
          if @id == @list.first
            @id = @list.last
          else
            @id = @list[index-1]
          end
          hide
          Graphics.transition(5)
          Graphics.freeze
          @mode = 0
          data_refresh
          Graphics.transition
          filename = "Audio/SE/Cries/" + sprintf("%03d", @id) + "Cry.wav"
          if FileTest.exist?(filename)
            Audio.se_play(filename)
          end
        end
       
        if Input.trigger?(Input::A) and @pokemon_sprite.visible == false
          @show = @show ? false : true
          @index = 0
          @pokemon_list.dispose
          @pokemon_list = POKEMON_S::Pokemon_List.new(@list, @index, @show)
          @pokemon_list.active = true
          @pokemon_list.update
          @pokemon_list.refresh
        end
       
        if Input.trigger?(Input::LEFT)
          if @mode > 0
            Graphics.freeze
            hide
            #Graphics.transition(1)
            @mode -= 1
            #Graphics.freeze
            case @mode
            when 0
              data_refresh
            when 1
              refresh_zone
            when 2
              refresh_cri
            when 3
              refresh_tail
            end
            Graphics.transition(1)
          end
        end
       
        if Input.trigger?(Input::RIGHT)
          if @mode < 3
            Graphics.freeze
            hide
            #Graphics.transition(1)
            @mode += 1
            #Graphics.freeze
            case @mode
            when 0
              data_refresh
            when 1
              refresh_zone
            when 2
              refresh_cri
            when 3
              refresh_tail
            end
            Graphics.transition(1)
          end
        end
      when "map"
        if Input.trigger?(Input::B)
          $game_system.se_play($data_system.buzzer_se)
        end
       
        if Input.trigger?(Input::C)
          $game_system.se_play($data_system.cancel_se)
          Graphics.freeze
          @_break = true
          $scene = Scene_Map.new
          return
        end
       
        if Input.trigger?(Input::DOWN)
          $game_system.se_play($data_system.buzzer_se)
        end
       
        if Input.trigger?(Input::UP)
          $game_system.se_play($data_system.buzzer_se)
        end
       
        if Input.trigger?(Input::A)
          $game_system.se_play($data_system.buzzer_se)
        end
       
        if Input.trigger?(Input::LEFT)
          $game_system.se_play($data_system.buzzer_se)
        end
       
        if Input.trigger?(Input::RIGHT)
          $game_system.se_play($data_system.buzzer_se)
        end
      when "combat"
        if Input.trigger?(Input::B)
          $game_system.se_play($data_system.buzzer_se)
        end
       
        if Input.trigger?(Input::C)
          $game_system.se_play($data_system.cancel_se)
          Graphics.freeze
          @_break = true
        end
       
        if Input.trigger?(Input::DOWN)
          $game_system.se_play($data_system.buzzer_se)
        end
       
        if Input.trigger?(Input::UP)
          $game_system.se_play($data_system.buzzer_se)
        end
       
        if Input.trigger?(Input::A)
          $game_system.se_play($data_system.buzzer_se)
        end
       
        if Input.trigger?(Input::LEFT)
          $game_system.se_play($data_system.buzzer_se)
        end
       
        if Input.trigger?(Input::RIGHT)
          $game_system.se_play($data_system.buzzer_se)
        end
      end
    end
         
        def hide  
          @background.visible = false  
          @pokemon_sprite.visible = false  
          case @mode  
          when 0  
            @data_window.visible = false  
            @text_window.visible = false  
          when 1  
            @text.dispose  
          when 2  
            @data_window.visible = false  
            @text_window.visible = false  
          when 3  
            @trainer.dispose  
            @pokemon_new_sprite.dispose  
            @text.dispose  
          end  
        end  
             
         
        def data_refresh  
          @background.bitmap = RPG::Cache.picture("PokedexShfond1.png")  
          @pokemon_sprite.x = 26  
          @pokemon_sprite.y = 71  
          @data_window.x = 233-16  
          @data_window.y = 76-16  
          @pokemon_sprite.visible = true  
          @background.visible = true  
          @data_window.visible = true  
          @text_window.visible = true  
           
          @pokemon = $data_pokemon[@id]  
          show = $data_pokedex[@id][1]  
          ida = sprintf("%03d", @id)  
          @pokemon_sprite.bitmap = RPG::Cache.battler("Front_Male/" + ida + ".png", 0)  
      
          if POKEMON_S._DEXREG  
            name = "N." + sprintf("%03d", Pokemon_Info.id_bis(@id)) + "  " + @pokemon[0]  
          else  
            name = "N." + ida + "  " + @pokemon[0]  
          end  
      
          if show # Descr accessible  
            species = @pokemon[9][1]  
            height_data = @pokemon[9][2]  
            weight = @pokemon[9][3]  
          else  
            species = "???"  
            height_data = "???"  
            weight = "???  "  
          end  
           
          @data_window.contents.clear  
          @data_window.contents.draw_text(15, 0, 370, 47, name)  
          @data_window.contents.draw_text(15, 41, 370, 47, "Pokémon " + species)  
          @data_window.contents.draw_text(41, 95, 230, 47, "Taille: ")  
          @data_window.contents.draw_text(41, 95, 212, 47, height_data, 2)  
          @data_window.contents.draw_text(41, 148, 230, 47, "Poids: ")  
          @data_window.contents.draw_text(41, 148, 230, 47, weight, 2)  
           
          @text_window.contents.clear  
          if show  
            text = @pokemon[9][0]  
            string = string_builder(text, 51)  
            string1 = string[0]  
            string2 = string[1]  
            string3 = string[2]  
            string4 = string[3]  
            @text_window.contents.draw_text(0, 0, 550, 40, string1)  
            @text_window.contents.draw_text(0, 40, 550, 40, string2)  
            @text_window.contents.draw_text(0, 80, 550, 40, string3)  
            @text_window.contents.draw_text(0, 120, 550, 40, string4)  
          end  
        end  
         
        def refresh_zone  
          @background.bitmap = RPG::Cache.picture("PokedexShfond2.png")  
          @background.visible = true  
          @text = Window_Base.new(32-16, 223-16, 576+32, 47+32)  
          @text.contents = Bitmap.new(576, 47)  
          @text.contents.font.name = $fontface  
          @text.contents.font.size = $fontsizebig  
          @text.contents.font.color = Color.new(60,60,60)  
          @text.contents.draw_text(0, 0, 576, 47, "OUVRIR LA CARTE", 1)  
          @text.opacity = 0  
        end  
         
        def refresh_cri  
          @background.bitmap = RPG::Cache.picture("PokedexShfond3.png")  
          @pokemon_sprite.visible = true  
          @background.visible = true  
          @data_window.visible = true  
          @pokemon_sprite.x = 26  
          @pokemon_sprite.y = 75  
          @data_window.x = 211 - 16  
          @data_window.y = 75 - 5  
          @data_window.contents.clear  
          @data_window.contents.draw_text(50, 27, 339, 47, "CRI DE")  
          @data_window.contents.draw_text(50, 68, 339, 47, @pokemon[0])  
        end  
         
        def refresh_tail  
          @background.bitmap = RPG::Cache.picture("PokedexShfond4.png")  
          @background.visible = true  
          @pokemon_new_sprite = Sprite.new  
          ida = sprintf("%03d", @id)  
          @pokemon_new_sprite.bitmap = RPG::Cache.battler("Front_Male/" + ida + ".png", 0)  
          @pokemon_new_sprite.color = Color.new(0, 0, 0, 255)  
           
          @trainer = Sprite.new  
          @trainer.bitmap = RPG::Cache.battler("trainer000.png", 0)  
          @trainer.color = Color.new(0, 0, 0, 255)  
           
          sizes = []  
          for sprite in [@trainer, @pokemon_new_sprite]  
            i = j = 0  
            while sprite.bitmap.get_pixel(i,j).alpha == 0  
              i += 1  
              if i > sprite.bitmap.width  
                i = 0  
                j += 1  
              end  
            end  
            up_pix = j  
            i = 0  
            j = sprite.bitmap.height  
            while sprite.bitmap.get_pixel(i,j).alpha == 0  
              i += 1  
              if i > sprite.bitmap.width  
                i = 0  
                j -= 1  
              end  
            end  
            down_pix = j  
            sizes.push( down_pix-up_pix + 0.0 )  
            sizes.push( down_pix )  
          end  
           
          if $data_pokemon[@id][9][2].to_f > 1.50  
            zoom_pok = 1.00  
            zoom_dre = 1.50 / $data_pokemon[@id][9][2].to_f * sizes[2] / sizes[0]  
          else  
            zoom_pok = $data_pokemon[@id][9][2].to_f/1.50 * sizes[0] / sizes[2]  
            zoom_dre = 1.00  
          end  
           
          @pokemon_new_sprite.ox = @pokemon_new_sprite.bitmap.width/2  
          @pokemon_new_sprite.oy = sizes[3]#@pokemon_new_sprite.bitmap.height  
          @pokemon_new_sprite.x = 141 + @pokemon_new_sprite.ox  
          @pokemon_new_sprite.y = 92 + 160#@pokemon_new_sprite.oy  
           
          @trainer.ox = @trainer.bitmap.width/2  
          @trainer.oy = sizes[1]#@trainer.bitmap.height  
          @trainer.x = 339 + @trainer.ox  
          @trainer.y = 92 + 160 #@trainer.oy  
           
          @pokemon_new_sprite.zoom_x = @pokemon_new_sprite.zoom_y = zoom_pok  
          @trainer.zoom_x = @trainer.zoom_y = zoom_dre  
           
          @text = Window_Base.new(32-16, 367-16, 576+32, 47+32)  
          @text.contents = Bitmap.new(576, 47)  
          @text.contents.font.name = $fontface  
          @text.contents.font.size = $fontsizebig  
          @text.contents.font.color = Color.new(60,60,60)  
          @text.contents.draw_text(0, 0, 576, 47, "TAILLE COMPARE A #{Player.name}", 1)  
          @text.opacity = 0  
        end  
         
        def string_builder(text, limit)  
          length = text.length  
          full1 = false  
          full2 = false  
          full3 = false  
          full4 = false  
          string1 = ""  
          string2 = ""  
          string3 = ""  
          string4 = ""  
          word = ""  
          for i in 0..length  
            letter = text[i..i]  
            if letter != " " and i != length  
              word += letter.to_s  
            else  
              word = word + " "  
              if (string1 + word).length < limit and not(full1)  
                string1 += word  
                word = ""  
              else  
                full1 = true  
              end  
               
              if (string2 + word).length < limit and not(full2)  
                string2 += word  
                word = ""  
              else  
                full2 = true  
              end  
               
              if (string3 + word).length < limit and not(full3)  
                string3 += word  
                word = ""  
              else  
                full3 = true  
              end  
               
              if (string4 + word).length < limit and not(full4)  
                string4 += word  
                word = ""  
              else  
                full4 = true  
              end  
            end  
          end  
          return [string1, string2, string3, string4]  
        end  
         
         
      end  
        
    end  