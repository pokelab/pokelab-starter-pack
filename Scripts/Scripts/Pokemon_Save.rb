#==============================================================================
# ■ Scene_Save (Version Pokémon)
# Pokemon Script Project - Krosk
# 01/08/07
#------------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#------------------------------------------------------------------------------
module POKEMON_S
  class Pokemon_Save
    def initialize
      Graphics.freeze
      number = 0 # Nombre de cadres de sauvegarde à générer
      for i in 0..2
        if FileTest.exist?("Save#{i+1}.rxdata")
          number += 1
        end
      end
      @number = number
      @save_game_window_list = []
      for i in 1..@number
        window = Window_Base.new(30, 30 + 83*(i-1), 580, 80)
        window.contents = Bitmap.new(548, 48)
        @save_game_window_list.push(window)
      end
      
      # Cadre nouvelle sauvegarde
      @new_save_window = Window_Base.new(30, 30 + 83*@number, 580, 80)
      @new_save_window.contents = Bitmap.new(548, 48)
      set_window(@new_save_window)
      @new_save_window.contents.draw_text(9, 3, 548, 48, "NOUVEAU FICHIER")
      if SAVEBOUNDSLOT
        @new_save_window.y = 30
      end
      
      captured = 0
      for i in 1..$data_pokedex.length-1
        if $data_pokedex[i][1]
          captured += 1
        end
      end
      $read_data = [Player.name, Player.id, captured, $map_link]
      
      @index = 0
      
      latest_time = Time.at(0)
      for i in 0..3
        filename = "Save#{i + 1}.rxdata"
        if FileTest.exist?(filename)
          file = File.open(filename, "r")
          if file.mtime > latest_time
            latest_time = file.mtime
            @index = i
          end
          file.close
        end
      end
      
      if SAVEBOUNDSLOT
        @index = POKEMON_S::_SAVESLOT
        if @index != @number
          @new_save_window.visible = false
        end
      end
      
    end
    
    def main
      refresh_all
      if not SAVEBOUNDSLOT
        @background = Plane.new(Viewport.new(0,0,640,480))
        @background.bitmap = RPG::Cache.picture("fondsaved.png")
        @background.z -= 10
      else
        @background = Spriteset_Map.new
      end
      Graphics.transition
      loop do
        Graphics.update
        Input.update
        update
        if $scene != self
          break
        end
      end
      Graphics.freeze
      for window in @save_game_window_list
        window.dispose
      end
      @new_save_window.dispose
      @background.dispose
    end
    
    def update
      if SAVEBOUNDSLOT
        @background.update
      end
      
      if Input.trigger?(Input::DOWN) and not SAVEBOUNDSLOT
        @index += @index == @number ? 0 : @index == 2 ? 0 : 1
        refresh_all
      end
      
      if Input.trigger?(Input::UP) and not SAVEBOUNDSLOT
        @index -= @index == 0 ? 0 : 1
        refresh_all
      end
      
      if Input.trigger?(Input::C)
        if @index == @number # Nouvelle sauvegarde
          $game_system.se_play($data_system.save_se)
          filename = "Save#{@number + 1}.rxdata"
          file = File.open(filename, "wb")
          write_save_data(file)
          file.close
          # イベントから呼び出されている場合
          if $game_temp.save_calling
            # セーブ呼び出しフラグをクリア
            $game_temp.save_calling = false
            # マップ画面に切り替え
            $scene = Scene_Map.new
            return
          end
          # メニュー画面に切り替え
          $scene = POKEMON_S::Pokemon_Menu.new(4)
        else
          decision = draw_choice
          if decision
            $game_system.se_play($data_system.save_se)
            filename = "Save#{@index + 1}.rxdata"
            file = File.open(filename, "wb")
            write_save_data(file)
            file.close
            # イベントから呼び出されている場合
            if $game_temp.save_calling
              # セーブ呼び出しフラグをクリア
              $game_temp.save_calling = false
              # マップ画面に切り替え
              $scene = Scene_Map.new
              return
            end
            # メニュー画面に切り替え
            $scene = POKEMON_S::Pokemon_Menu.new(4)
          else
            return
          end
        end
      end
      
      if Input.trigger?(Input::B)
        if $game_temp.save_calling
          $game_temp.save_calling = false
        end
        $scene = POKEMON_S::Pokemon_Menu.new(4)
        return
      end
      
    end
    
    def refresh_all
      for i in 0..@number-1
        if i < @index
          @save_game_window_list[i].opacity = 128
          @save_game_window_list[i].y = 30 + 83*i
          @save_game_window_list[i].height = 80
          @save_game_window_list[i].contents = Bitmap.new(548, 48)
          set_window(@save_game_window_list[i])
          @save_game_window_list[i].contents.draw_text(9, 3, 530, 48, "SAUVEGARDER PARTIE "+(i+1).to_s)
          if SAVEBOUNDSLOT
            @save_game_window_list[i].visible = false
          end
        elsif i == @index
          @save_game_window_list[i].opacity = 255
          @save_game_window_list[i].y = 30 + 83*i
          @save_game_window_list[i].height = 163
          @save_game_window_list[i].contents = Bitmap.new(548, 131)
          set_window(@save_game_window_list[i])
          if SAVEBOUNDSLOT
            @save_game_window_list[i].contents.draw_text(9, 3, 530, 48, "SAUVEGARDER PARTIE")
            @save_game_window_list[i].y = 30
          else
            @save_game_window_list[i].contents.draw_text(9, 3, 530, 48, "SAUVEGARDER PARTIE "+(i+1).to_s)
          end
          @filename = "Save#{i + 1}.rxdata"
          list = read_preview(@filename)
          data = list[0]
          time_sec = list[1]
          hour = (time_sec / 3600).to_s
          minute = "00"
          minute += ((time_sec%3600)/60).to_s
          minute = minute[minute.size-2, 2]
          time =  hour + ":" + minute
          name = data[0].to_s
          id = data[1].to_s
          captured = data[2].to_s
          @save_game_window_list[i].contents.draw_text(9, 42, 252, 48, "NOM:")
          @save_game_window_list[i].contents.draw_text(9, 42, 252, 48, name, 2)
          @save_game_window_list[i].contents.draw_text(285, 42, 252, 48, "ID:")
          @save_game_window_list[i].contents.draw_text(285, 42, 252, 48, id, 2)
          @save_game_window_list[i].contents.draw_text(9, 81, 252, 48, "POKéDEX:")
          @save_game_window_list[i].contents.draw_text(9, 81, 252, 48, captured, 2)
          @save_game_window_list[i].contents.draw_text(285, 81, 252, 48, "DUREE:")
          @save_game_window_list[i].contents.draw_text(285, 81, 252, 48, time, 2)
        elsif i > @index
          @save_game_window_list[i].opacity = 128
          @save_game_window_list[i].y = 30 + 83*i + 83
          @save_game_window_list[i].height = 80
          @save_game_window_list[i].contents = Bitmap.new(548, 48)
          set_window(@save_game_window_list[i])
          @save_game_window_list[i].contents.draw_text(9, 3, 548, 48, "SAUVEGARDER PARTIE "+(i+1).to_s)
          if SAVEBOUNDSLOT
            @save_game_window_list[i].visible = false
          end
        end
      end
      if @index == @number and @number < 3
        @new_save_window.opacity = 255
        @new_save_window.y = 30 + 83*@number
      elsif @number < 3
        @new_save_window.opacity = 128
        @new_save_window.y = 30 + 83*(@number+1)
      else
        @new_save_window.visible = false
      end
    end
    
    def set_window(window)
      window.contents.font.name = $fontface
      window.contents.font.size = $fontsizebig
      window.contents.font.color = window.normal_color
    end
    
    def read_preview(filename)
      file = File.open(filename, "r")
      time_stamp      = file.mtime
      characters      = Marshal.load(file)
      frame_count     = Marshal.load(file)
      game_system     = Marshal.load(file)
      game_switches   = Marshal.load(file)
      game_variables  = Marshal.load(file)
      total_sec = frame_count / Graphics.frame_rate
      
      # Non utilisé
      game_self_switches = Marshal.load(file)
      game_screen     = Marshal.load(file)
      game_actors     = Marshal.load(file)
      game_party      = Marshal.load(file)
      game_troop      = Marshal.load(file)
      game_map        = Marshal.load(file)
      game_player     = Marshal.load(file)
      # ------------
      read_data       = Marshal.load(file)
      file.close
      return [read_data, total_sec]
    end
    
    #--------------------------------------------------------------------------
    # 
    #--------------------------------------------------------------------------
    def write_save_data(file)
      characters = []
      for i in 0...$game_party.actors.size
        actor = $game_party.actors[i]
        characters.push([actor.character_name, actor.character_hue])
      end
      
      Marshal.dump(characters, file)
      
      Marshal.dump(Graphics.frame_count, file)
      
      $game_system.save_count += 1
      $game_system.magic_number = $data_system.magic_number
      
      Marshal.dump($game_system, file)
      Marshal.dump($game_switches, file)
      Marshal.dump($game_variables, file)
      Marshal.dump($game_self_switches, file)
      Marshal.dump($game_screen, file)
      Marshal.dump($game_actors, file)
      Marshal.dump($game_party, file)
      Marshal.dump($game_troop, file)
      Marshal.dump($game_map, file)
      Marshal.dump($game_player, file)
      Marshal.dump($read_data, file)
      #Marshal.dump(Player.code, file)
      #Marshal.dump(Player.id, file)
      #Marshal.dump(Player.name, file)
      #Marshal.dump(Player.battler , file)
      Marshal.dump($pokemon_party, file)
      Marshal.dump($random_encounter, file)
      Marshal.dump($data_pokedex, file)
      Marshal.dump($data_storage, file)
      Marshal.dump($battle_var, file)
      Marshal.dump($existing_pokemon, file)
      Marshal.dump($string, file)
    end
    
    def draw_choice
      @command = Window_Command.new(120, ["OUI", "NON"], $fontsizebig)
      @command.x = 517
      @command.y = 359
      loop do
        Graphics.update
        Input.update
        @command.update
        if Input.trigger?(Input::C) and @command.index == 0
          @command.dispose
          @command = nil
          Input.update
          return true
        end
        if Input.trigger?(Input::C) and @command.index == 1
          @command.dispose
          @command = nil
          Input.update
          return false
        end
      end
    end
  end
end

class Interpreter
  def forcer_sauvegarde
    if not SAVEBOUNDSLOT
      return
    end
    captured = 0
    for i in 1..$data_pokedex.length-1
      if $data_pokedex[i][1]
        captured += 1
      end
    end
    $read_data = [Player.name, Player.id, captured, $map_link]
    filename = "Save#{POKEMON_S::_SAVESLOT + 1}.rxdata"
    file = File.open(filename, "wb")
    characters = []
    for i in 0...$game_party.actors.size
      actor = $game_party.actors[i]
      characters.push([actor.character_name, actor.character_hue])
    end
    Marshal.dump(characters, file)
    Marshal.dump(Graphics.frame_count, file)
    $game_system.save_count += 1
    $game_system.magic_number = $data_system.magic_number
    game_system = $game_system.clone
    game_system.reset_interpreter
    Marshal.dump(game_system, file)
    Marshal.dump($game_switches, file)
    Marshal.dump($game_variables, file)
    Marshal.dump($game_self_switches, file)
    Marshal.dump($game_screen, file)
    Marshal.dump($game_actors, file)
    Marshal.dump($game_party, file)
    Marshal.dump($game_troop, file)
    Marshal.dump($game_map, file)
    Marshal.dump($game_player, file)
    Marshal.dump($read_data, file)
    Marshal.dump($pokemon_party, file)
    Marshal.dump($random_encounter, file)
    Marshal.dump($data_pokedex, file)
    Marshal.dump($data_storage, file)
    Marshal.dump($battle_var, file)
    Marshal.dump($existing_pokemon, file)
    Marshal.dump($string, file)
    file.close
  end
end