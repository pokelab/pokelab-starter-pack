#==============================================================================
# ■ Pokemon_NameEdit
# Pokemon Script Project - Krosk 
# 17/08/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier
#-----------------------------------------------------------------------------

module POKEMON_S
  class Pokemon_NameEdit < Window_Base

    attr_reader   :name                     # 名前
    attr_reader   :index                    # カーソル位置

    def initialize(pokemon)
      super(99, 27, 442, 114)
      self.contents = Bitmap.new(width - 32, height - 32)
      self.contents.font.name = $fontface
      self.contents.font.size = $fontsizebig
      self.contents.font.color = normal_color
      self.opacity = 0
      @pokemon = pokemon
      @name = pokemon.given_name
      @max_char = 10 #10 caractères
      name_array = @name.split(//)[0...@max_char]
      
      @name = ""
      
      for i in 0...name_array.size
        @name += name_array[i]
      end
      
      if pokemon.given_name.size >= 10
        @name = pokemon.given_name[0..10]
      else
        @name = pokemon.given_name[0..pokemon.given_name.size-1]
      end
      
      if pokemon.name.size >= 10
        @default_name = pokemon.name[0..10]
      else
        @default_name = pokemon.name[0..pokemon.name.size-1]
      end
      
      @index = name_array.size
      
      refresh
      update_cursor_rect
    end
    #--------------------------------------------------------------------------
    # 
    #--------------------------------------------------------------------------
    def restore_default
      @name = @default_name
      @index = @name.split(//).size
      refresh
      update_cursor_rect
    end
    #--------------------------------------------------------------------------
    # ● 文字の追加
    #     character : 追加する文字
    #--------------------------------------------------------------------------
    def add(character)
      if @index < @max_char and character != ""
        @name += character
        @index += 1
        refresh
        update_cursor_rect
      end
    end
    #--------------------------------------------------------------------------
    # ● 文字の削除
    #--------------------------------------------------------------------------
    def back
      if @index > 0
        # 一字削除
        name_array = @name.split(//)
        @name = ""
        for i in 0...name_array.size-1
          @name += name_array[i]
        end
        @index -= 1
        refresh
        update_cursor_rect
      end
    end
    #--------------------------------------------------------------------------
    # ● リフレッシュ
    #--------------------------------------------------------------------------
    def refresh
      self.contents.clear
      
      name_array = @name.split(//)
      for i in 0...@max_char
        c = name_array[i]
        if c == nil
          c = "_"
        end
        x = 120 + i * 21
        draw_text(x, 33, 24, 44, c, 1, normal_color)
      end
      
      self.contents.font.size = $fontsize
      self.contents.draw_text(69, 6, 321, 39, "Surnom de " + @pokemon.name + "?", 1)
      self.contents.font.size = $fontsizebig
      
      src_rect = Rect.new(0, 0, 64, 64)
      bitmap = RPG::Cache.battler(@pokemon.icon, 0)
      self.contents.blt(12, 3, bitmap, src_rect, 255)
    end
    #--------------------------------------------------------------------------
    # ● カーソルの矩形更新
    #--------------------------------------------------------------------------
    def update_cursor_rect
      x = 115 + @index * 21
      self.cursor_rect.set(x, 40, 32, 32)
    end
    #--------------------------------------------------------------------------
    # ● フレーム更新
    #--------------------------------------------------------------------------
    def update
      super
      update_cursor_rect
    end
  end
end