#==============================================================================
# ■ Main
#------------------------------------------------------------------------------
# 　各クラスの定義が終わった後、ここから実際の処理が始まります。
#==============================================================================

begin
  $style = "DP"
  # Change the $fontface variable to change the font style
  $fontface = ["Pokemon DP", "Pokemon FRLG", "Pokemon RS", "Trebuchet MS"]
  $fontfacebis = ["Pokemon RS", "Pokemon DP"]
  $fontsizebis = 28
  # Change the $fontsize variable to change the font size
  if $style == "DP"
    # DP # Tailles étalon: 15 (1px) 31 (2px) et 47 (3px)
    $fontface = ["Pokemon DP", "Trebuchet MS"]
    $fontsizesmall = 15 # // hauteur min 14
    $fhs = 14
    $fontsize = 31 # // hauteur min 28
    $fh = 28
    $fontsizebig = 47
    $fhb = 42
  end
  if $style == "FRLG"
    # FRLG # Tailles étalon: 21 (1px)  38 (2px) 57 (3px)
    $fontface = ["Pokemon FRLG", "Trebuchet MS"]
    $fontsizesmall = 21 # // hauteur min 13
    $fhs = 13
    $fontsize = 38  # // hauteur min 26
    $fh = 26
    $fontsizebig = 57  # // hauteur min 39
    $fhb = 39
  end
  $fontsmall = ["Pokemon Emerald Small", "Pokemon DP", "Trebuchet MS"]
  # Pokemon Emerald Small 2px: 25 , 3px: 37
  $fontsmallsize = 37
  $fs = 34
  
  $fontnarrow = ["Pokemon Emerald Narrow", "Pokemon DP", "Trebuchet MS"]
  # Pokemon Emerald Narrow 3px: 47
  $fontnarrowsize = 47
  $fn = 42
  
  # トランジション準備
  splash.dispose
  Graphics.transition(5)
  Graphics.freeze
  # シーンオブジェクト (タイトル画面) を作成
  $scene = Scene_Title.new
  # $scene が有効な限り main メソッドを呼び出す
  while $scene != nil
    $scene.main
  end
  # フェードアウト
  Graphics.transition(20)
rescue Exception => exception
  EXC::error_handler(exception)
rescue Errno::ENOENT
  # 例外 Errno::ENOENT を補足
  # ファイルがオープンできなかった場合、メッセージを表示して終了する
  filename = $!.message.sub("Ne trouve pas le fichier ou le répertoire - ", "")
  print("Le ficher #{filename} n'a pas été trouvé.")
end
