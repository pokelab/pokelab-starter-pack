module POKEMON_S
  
  def self._WMAPID=(value)
    $game_variables[6] = value
  end
  
  def self._SAVESLOT
    return $game_variables[8]
  end
  
  def self.set_SAVESLOT=(value)
    $game_variables[8] = value
  end
  
  def self.nuit?
    return (Time.now.hour >= 18 or Time.now.hour < 6)
  end
  
  def self.jour?
    return (Time.now.hour < 18 and Time.now.hour >= 6)
  end
  
  def self._DEXREG
    return $game_switches[6]
  end
end


  # --------------------------------------------------------
  # error_handler
  # --------------------------------------------------------

module EXC
  def self.error_handler(exception, file_arg = nil)
    if exception.type == SystemExit
      return
    end
    
    if exception.message == "" # Reset
      raise
    end
    
    # Sauvegarde de secours
    if $game_player != nil
      file = File.open("SaveAuto.rxdata", "wb")
      characters = []
      for i in 0...$game_party.actors.size
        actor = $game_party.actors[i]
        characters.push([actor.character_name, actor.character_hue])
      end
      Marshal.dump(characters, file)
      Marshal.dump(Graphics.frame_count, file)
      $game_system.save_count += 1
      $game_system.magic_number = $data_system.magic_number
      $game_system.reset_interpreter
      Marshal.dump($game_system, file)
      Marshal.dump($game_switches, file)
      Marshal.dump($game_variables, file)
      Marshal.dump($game_self_switches, file)
      Marshal.dump($game_screen, file)
      Marshal.dump($game_actors, file)
      Marshal.dump($game_party, file)
      Marshal.dump($game_troop, file)
      Marshal.dump($game_map, file)
      Marshal.dump($game_player, file)
      Marshal.dump($read_data, file)
      Marshal.dump($pokemon_party, file)
      Marshal.dump($random_encounter, file)
      Marshal.dump($data_pokedex, file)
      Marshal.dump($data_storage, file)
      Marshal.dump($battle_var, file)
      Marshal.dump($existing_pokemon, file)
      Marshal.dump($string, file)
      file.close
    end
    
    script = load_data("Data/Scripts.rxdata")
    
    source = script[exception.backtrace[0].split(":")[0].sub("Section", "").to_i][1]
    source_line = exception.backtrace[0].split(":")[1]
    
    if file_arg != nil
      file = file_arg
      source = file.path
    end
    if source == "Interpreter Bis" and source_line == "444"
      source = "évènement"
    end
      
    print("Erreur dans le script #{source}, inspectez le rapport Log.txt.")
    
    logfile = File.open("Log.txt", "w")
    
    # Entete
    logfile.write("---------- Erreur de script : #{source} ----------\n")
    
    # Type
    logfile.write("----- Type\n")
    logfile.write(exception.type.to_s + "\n\n")
    
    # Message
    logfile.write("----- Message\n")
    if exception.type == NoMethodError
      logfile.write("- ARGS - #{exception.args.inspect}\n")
    end
    logfile.write(exception.message + "\n\n")
    
    # Position en fichier
    if file_arg != nil
      logfile.write("----- Position dans #{file.path}\n")
      logfile.write("Ligne #{file.lineno}\n")
      logfile.write(IO.readlines(file.path)[file.lineno-1] + "\n")
    elsif source == "évènement"
      logfile.write("----- Position de l'évènement\n")
      logfile.write($running_script + "\n\n")
    else
      logfile.write("----- Position dans #{source}\n")
      logfile.write("Ligne #{source_line}\n\n")
    end
    
    # Backtrace
    logfile.write("----- Backtrace\n")    
    for trace in exception.backtrace
      location = trace.split(":")
      script_name = script[location[0].sub("Section", "").to_i][1]
      logfile.write("Script : #{script_name} | Ligne : #{location[1]}")
      if location[2] != nil
        logfile.write(" | Méthode : #{location[2]}")
      end
      logfile.write("\n")
    end
    logfile.close
    
    raise
  end
end