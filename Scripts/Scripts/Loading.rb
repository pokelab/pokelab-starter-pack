#==============================================================================
# ■ Loading
# Pokemon Script Project - Krosk 
# 28/08/08
#-----------------------------------------------------------------------------
# Ecran de chargement
#-----------------------------------------------------------------------------
# Ne pas y toucher... ou avec modération, ce sont mes crédits
#-----------------------------------------------------------------------------

begin
  
  white = Sprite.new
  white.bitmap = RPG::Cache.title("white.png")
  white.z = 0
  
  texte = UI::Text.new(0,0,300,26)
  texte.color = Color.new(0,0,0)
  loop do
    Graphics.update
    Input.update([0x01,0x42])
    Input.mouse_update
    Console.log(Input.mouse_x) if(Input.trigger?(0x01))
    texte.text = "#{Graphics.frame_rate} FPS"
    break if(Input.trigger?(0x42))
  end
  
  # Affichage de l'écran blanc en fond
  Graphics.freeze
  white = Scaling_Sprite.new
  white.bitmap = RPG::Cache.title("white.png")
  white.z = 0
  Graphics.transition(10)
  
  Input.mouse_visible = false
  
  a = UI::MessageBox.new(20,20,548)
  a.z = 1
  a.text = $lorem
  a.color = Color.new(0,0,0)
  a.active
  a.dispose
  
  Graphics.wait(200)
  
  # Affichage scrollbox
  scroll = UI::ScrollBox.new(Rect.new(40,200,560,180),400,18,"Bonjour. Comment vas-tu ?",24,UI::WindowSkin.new("scrollbar.png",[4,4,4,4]))
  scroll.color = Color.new(0,0,0)
  
  loop do
    Graphics.update
    Input.update([0x1B,0x41,0x42])
    Input.mouse_update
    scroll.update
    break if Input.trigger?(0x1B)
  end
  
  # Supression de la boîte de dialogue
  Graphics.freeze
  message.dispose
  Graphics.transition(10)
  Graphics.wait(10)
  
  # Affichage du logo du studio
  Graphics.freeze
  studio_pokelab = Scaling_Sprite.new
  studio_pokelab.bitmap = RPG::Cache.title("studio_pokelab.png")
  studio_pokelab.z = 2000
  highlight_pokelab = Scaling_Sprite.new
  highlight_pokelab.bitmap = RPG::Cache.title("highlight_pokelab.png")
  highlight_pokelab.z = 1
  highlight_pokelab.x = 192
  highlight_pokelab.y = -192
  Graphics.transition(20)
  # Animation studio
  12.times do
    highlight_pokelab.move_x(-30)
    highlight_pokelab.move_y(30)
    Graphics.update
  end
  highlight_pokelab.x = 192
  highlight_pokelab.y = -192
  12.times do
    highlight_pokelab.move_x(-30)
    highlight_pokelab.move_y(30)
    Graphics.update
  end
  Graphics.wait(20)
  Graphics.freeze
  highlight_pokelab.dispose
  studio_pokelab.dispose
  Graphics.transition(20)
  Graphics.wait(20)
  
  # Affichage du logo de Pokélab
  Graphics.freeze
  pokelab = Scaling_Sprite.new
  pokelab.bitmap = RPG::Cache.title("pack_pokelab.png")
  pokelab.src_rect = Rect.new(0,0,640,480)
  pokelab.z = 1
  Graphics.transition(20)
  # Animation Pokélab
  2.times do
    3.times do |i|
      pokelab.src_rect = Rect.new(640 * i,0,640,480)
      Graphics.wait(10)
    end
    Graphics.wait(30)
  end
  Graphics.freeze
  pokelab.dispose
  white.dispose
  splash = Scaling_Sprite.new
  splash.bitmap = RPG::Cache.title("pkssplash.png")
  textbox = UI::Text.new(20,10,100,20,"Linear")
  textbox2 = UI::Text.new(20,60,500,20,"InQuad")
  textbox3 = UI::Text.new(20,110,500,20,"OutQuad")
  textbox4 = UI::Text.new(20,160,500,20,"InOutQuad")
  textbox5 = UI::Text.new(20,210,500,20,"Animate")
  textbloc = UI::TextBox.new(200,10,200,72,"Bonjour, je suis un très très long texte, et j'aimerais être bien affiché, s'il vous plaît !",24)
  Graphics.transition(20)
  Graphics.wait(100)
  50.times do |i|
    textbox.move_to(100,110,i,50)
    textbox2.move_to(100,160,i,50,:inQuad)
    textbox3.move_to(100,210,i,50,:outQuad)
    textbox4.move_to(100,260,i,50,:inOutQuad)
    Graphics.update
  end
  textbloc.text.length.times do |i|
    textbloc.draw_char(i)
    Graphics.update
  end
  Graphics.wait(100)
  50.times do |i|
    textbox.animate_color(Color.new(255,0,0), i, 50)
    Graphics.update
  end
  textbloc.text = "Ceci est un autre texte. Je trouve qu'il est bien aussi, n'est-ce pas ? HUHU !"
  textbloc.text.length.times do |i|
    textbloc.draw_char(i)
    Graphics.update
  end
  Graphics.wait(100)
  Graphics.wait(100)
  Graphics.freeze
  Console.title("Démarrage du kit")
end

























#==============================================================================
# ■ Cimetière
# Pokemon Script Project - Krosk 
# 07/08/08
#-----------------------------------------------------------------------------
# N'y touchez pas
#-----------------------------------------------------------------------------
if false
  data_pokemon = Array.new(494)
  
  for i in 1..493
    data_pokemon[i] = []
    data_pokemon[i][1] = i
    data_pokemon[i][2] = []
    data_pokemon[i][4] = []
    data_pokemon[i][5] = []
    data_pokemon[i][7] = []
    data_pokemon[i][9] = []
    data_pokemon[i][9][4] = []
  end
  
  begin
    # data_pokemon[id][0]
    i = 0
    file = File.open("database/noms.txt", "rb")
    file.readchar
    file.readchar
    file.readchar
    file.each {|line| 
      i += 1
      data_pokemon[i][0] = line.sub("\r\n", "")
      }
    file.close
    
    # data_pokemon[id][2][jd]
    j = 0
    for filename in ["base_hp.txt", "base_atk.txt", "base_dfe.txt", "base_spd.txt", "base_ats.txt", "base_dfs.txt"]
      i = 0
      file = File.open("database/#{filename}", "rb")
      file.readchar
      file.readchar
      file.readchar
      file.each {|line| 
        i += 1
        data_pokemon[i][2][j] = line.sub("\r\n", "").to_i
        }
      file.close
      j += 1
    end
    
    # data_pokemon[id][3]
    i = 0
    file = File.open("database/skill_list.txt", "rb")
    file.readchar
    file.readchar
    file.readchar
    file.each {|line| 
      i += 1
      clean_line = line.sub('\r\n', "")
      data_pokemon[i][3] = eval("[#{clean_line}]")
      }
    file.close
    
    # data_pokemon[id][4]
    i = 0
    file = File.open("database/tech_list.txt", "rb")
    file.readchar
    file.readchar
    file.readchar
    file.each {|line| 
      i += 1
      clean_line = line.sub('\r\n', "")
      for j in 0..clean_line.length/2-1
        tech = clean_line[2*j..2*j+1]
        if tech.include?("H")
          tech.sub!("H", "")
          data_pokemon[i][4].push( eval("[#{tech.to_i}]") )
        else
          data_pokemon[i][4].push( tech.to_i ) if tech.to_i != 0
        end
      end
      }
    file.close
    
    # data_pokemon[id][5][0]
    i = 0
    file = File.open("database/exp.txt", "rb")
    file.readchar
    file.readchar
    file.readchar
    file.each {|line| 
      i += 1
      clean_line = line.sub('\r\n', "")
      data_pokemon[i][5][0] = clean_line.to_i
      }
    file.close
    
    # data_pokemon[id][5][1+]
    i = 0
    file = File.open("database/evolve.txt", "rb")
    file.readchar
    file.readchar
    file.readchar
    file.each {|line| 
      i += 1
      clean_line = line.sub('\r\n', "")
      array = clean_line.split(';')
      for evo in array
        data_pokemon[i][5].push(eval("[#{evo}]"))
      end
      }
    file.close
    
    # data_pokemon[id][6]
    i = 0
    file = File.open("database/type.txt", "rb")
    file.readchar
    file.readchar
    file.readchar
    file.each {|line| 
      i += 1
      clean_line = line.sub('\r\n', "")
      data_pokemon[i][6] = eval("[#{clean_line}]")
      }
    file.close
    
    # data_pokemon[id][7][jd]
    j = 0
    for filename in ["rarete.txt", "taux_femelle.txt", "loyaute.txt", 
        "capa_spe.txt", "breed_group.txt", "breed_move.txt", "hatch_step.txt"]
      i = 0
      file = File.open("database/#{filename}", "rb")
      file.readchar
      file.readchar
      file.readchar
      file.each {|line| 
        i += 1
        clean_line = line.sub("\r\n", "")
        next if i >= 494
        clean_line.upcase! if j == 3
        if j == 3
          data_pokemon[i][7][3] = clean_line.split(',')
        elsif j == 4 or j == 5
          data_pokemon[i][7][j] = eval("[#{clean_line}].uniq")
        else
          data_pokemon[i][7][j] = clean_line.to_i
        end
        }
      file.close
      j += 1
    end
    
    # data_pokemon[id][8]
    i = 0
    file = File.open("database/battle_list.txt", "rb")
    file.readchar
    file.readchar
    file.readchar
    file.each {|line| 
      i += 1
      clean_line = line.sub("\r\n", "")
      data_pokemon[i][8] = eval("[#{clean_line}]")
      }
    file.close
    
    # data_pokemon[id][8][6]
    i = 0
    file = File.open("database/base_exp.txt", "rb")
    file.readchar
    file.readchar
    file.readchar
    file.each {|line| 
      i += 1
      clean_line = line.sub("\r\n", "")
      data_pokemon[i][8].push(clean_line.to_i)
      }
    file.close
    
    # data_pokemon[id][9][jd]
    j = 0
    for filename in ["description.txt", "espece.txt", "taille.txt", "poids.txt"]
      i = 0
      file = File.open("database/#{filename}", "rb")
      file.readchar
      file.readchar
      file.readchar
      file.each {|line| 
        i += 1
        if j == 2 or j == 3
          clean_line = line.sub("\r\n", "")
          data_pokemon[i][9][j] = clean_line.sub(/ \(.*\)/, "")
        else
          data_pokemon[i][9][j] = line.sub("\r\n", "")
        end
        }
      file.close
      j += 1
    end
      
    $data_pokemon = data_pokemon
  rescue Exception => exception
    EXC::error_handler(exception, file)
  end
    
  file = File.open("data.txt", "w")
  for i in 1..493
    file.write("$data_pokemon[#{i}] = #{$data_pokemon[i].inspect}\n")
  end
  file.close
  
  Dir.rmdir("database")
end