#==============================================================================
# ■ Scene_Title (Version Pokémon)
# Pokemon Script Project - Krosk
# 01/08/07
#------------------------------------------------------------------------------
# Scène modifiable
#------------------------------------------------------------------------------
# Intègre le menu de chargement
#------------------------------------------------------------------------------


class Scene_Title
  include POKEMON_S
  
  def main
    if $BTEST
      battle_test
      return
    end
    
    if FileTest.exist?("SaveAuto.rxdata")
      print("Sauvegarde de secours détectée.\n\nAppuyez sur Entrée, puis sur Echap pour charger, ou Entrée pour continuer.")
      loop do
        Input.update
        if Input.trigger?(Input::C)
          File.delete("SaveAuto.rxdata")
          break
        end
        if Input.trigger?(Input::B)
          @auto_load = true
          break
        end
      end
    end
    
    if $game_system == nil
      $data_actors        = load_data("Data/Actors.rxdata")
      $data_classes       = load_data("Data/Classes.rxdata")
      $data_skills        = load_data("Data/Skills.rxdata")
      $data_items         = load_data("Data/Items.rxdata")
      $data_weapons       = load_data("Data/Weapons.rxdata")
      $data_armors        = load_data("Data/Armors.rxdata")
      $data_enemies       = load_data("Data/Enemies.rxdata")
      $data_troops        = load_data("Data/Troops.rxdata")
      $data_states        = load_data("Data/States.rxdata")
      $data_animations    = load_data("Data/Animations.rxdata")
      $data_tilesets      = load_data("Data/Tilesets.rxdata")
      $data_common_events = load_data("Data/CommonEvents.rxdata")
      $data_system        = load_data("Data/System.rxdata")
      $picture_data       = load_data("Data/Library.rxdata")
      
      $game_system = Game_System.new
    end
    
    if not @auto_load and not MAPINTRO
    
      # -----------------------------------------------------------------
      #     La scène d'intro commence ici (non MAPINTRO)
      # -----------------------------------------------------------------
      # Elle est bien sûr modifiable à votre goût.
      # -----------------------------------------------------------------
      view = Viewport.new(0,0,640,480)
      view.z = -5
      #view = Viewport.new(0,25,640,430)
      #view.z = 0
      background = Scaling_Plane.new(view)
      background.bitmap = RPG::Cache.title("Opening2.jpg")
      #background.oy += 25
      background.z = 0
      background.tone = Tone.new(0,0,0,255)
      
      #view = Viewport.new(0,0,640,480)
      #view.z = -5
      #background_bis = Scaling_Plane.new(view)
      #background_bis.bitmap = RPG::Cache.title("Opening2.jpg")
      #background_bis.z = -5
      
      band_top = Scaling_Sprite.new
      band_top.bitmap = RPG::Cache.title("Opening3.png")
      band_top.z = 5
      band_bottom = Scaling_Sprite.new
      band_bottom.bitmap = RPG::Cache.title("Opening3.png")
      band_bottom.z = 5
      band_bottom.y = 450
      
      band_mid = Scaling_Plane.new(Viewport.new(0,0,640,480))
      band_mid.bitmap = RPG::Cache.title("Opening4.png")
      band_mid.z = 5
      
      middle = Scaling_Sprite.new
      middle.bitmap = RPG::Cache.title("Opening1.png")
      middle.z = 2
      
      title1 = Scaling_Sprite.new
      title1.bitmap = RPG::Cache.title("Opening6.png")
      title1.z = 3
      title1.opacity = 0
      title1.color = Color.new(255,255,255,255)
      
      title2 = Scaling_Sprite.new
      title2.bitmap = RPG::Cache.title("Opening7.png")
      title2.z = 4
      title2.opacity = 0
      title2.color = Color.new(255,255,255,255)
      
      flash = Scaling_Sprite.new
      flash.bitmap = RPG::Cache.title("Opening8.png")
      flash.z = 10
      flash.opacity = 0
      
      mid = Scaling_Sprite.new
      mid.bitmap = RPG::Cache.title("Opening5.png")
      mid.z = 5
      mid.visible = false
      
      screen = Scaling_Sprite.new
      screen.bitmap = RPG::Cache.title("Opening10.png")
      screen.z = 2
      screen.visible = false
      
      start = Scaling_Sprite.new
      start.bitmap = RPG::Cache.title("Opening11.png")
      start.z = 5
      start.visible = false
      
      Graphics.transition(5)
      
      $game_system.bgm_play($data_system.title_bgm)
      Audio.me_stop
      Audio.bgs_stop
      
      timer = 0
      loop do
        Graphics.update
        Input.update
        
        timer += 1
        background.ox -= 12
        #background_bis.ox += 6
        
        if timer == 210
          background.tone.gray = 0
        end
        
        if timer > 210 and background.tone.red > 0
          background.tone.red -= 25
          background.tone.green -= 25
        end
          
        if timer > 210 and timer % (150 + rand(50)) == 0
          background.tone.red = background.tone.green = 250
        end
        
        if flash.opacity > 0
          flash.opacity -= 15
        end
        
        if timer > 5 and not band_top.disposed?
          band_top.x += 13
          if band_top.x > 640
            band_top.dispose
          end
        end
        
        if timer > 73 and not band_bottom.disposed?
          band_bottom.x -= 13
          if band_bottom.x < -640
            band_bottom.dispose
          end
        end
        
        if timer > 150 and not band_mid.disposed?
          band_mid.ox -= 20
          if timer > 210
            band_mid.dispose
            mid.visible = true
          end
        end
        
        if timer > 210 and not title1.disposed? and title1.opacity < 255
          title1.opacity += 15
          title1.color.alpha -= 15
        end
        
        if timer > 240 and not title1.disposed? and title1.opacity == 255
          title1.color.alpha += 15
        end
        
        if timer == 270
          flash.opacity = 255
          title1.dispose
          mid.dispose
          middle.dispose
          screen.visible = true
        end
        
        if timer > 270 and not title2.disposed? and title2.opacity < 255
          title2.opacity += 15
          title2.color.alpha -= 15
          if title2.opacity >= 255
            title2.dispose
          end
        end
        
        if timer > 290 and timer%20 == 0
          start.visible = !start.visible
        end
        
        if Input.trigger?(Input::C)
          $game_system.se_play($data_system.decision_se)
          break
        end
        
        if timer > 3750
          Audio.bgm_fade(1000*5)
        end
        
      end
      
      Graphics.freeze
      background.dispose if not background.disposed?
      #background_bis.dispose if not background_bis.disposed?
      band_top.dispose if not band_top.disposed?
      band_bottom.dispose if not band_bottom.disposed?
      band_mid.dispose if not band_mid.disposed?
      mid.dispose if not mid.disposed?
      middle.dispose if not middle.disposed?
      title1.dispose if not title1.disposed?
      title2.dispose if not title2.disposed?
      flash.dispose if not flash.disposed?
      start.dispose if not start.disposed?
      screen.dispose if not screen.disposed?
      Graphics.transition
      Graphics.freeze
      
      Audio.bgm_stop
      
      # -----------------------------------------------------------------
      #     Fin de la scène d'intro
      # -----------------------------------------------------------------
    
    elsif MAPINTRO.type == Array and $_temp_map_intro == nil
      $_temp_map_intro = true
      $map_link = {}
      Audio.bgm_stop
      Graphics.frame_count = 0
      $game_temp          = Game_Temp.new
      $game_system        = Game_System.new
      $game_switches      = Game_Switches.new
      $game_variables     = Game_Variables.new
      $game_self_switches = Game_SelfSwitches.new
      $game_screen        = Game_Screen.new
      $game_actors        = Game_Actors.new
      $game_party         = Game_Party.new
      $game_troop         = Game_Troop.new
      $game_map           = Game_Map.new
      $game_player        = Game_Player.new
      # ---- Pas forcément nécessaire
      $pokemon_party = POKEMON_S::Pokemon_Party.new
      $data_storage = [[0]]
      $pokemon_party.create_box
      Player.set_trainer_code(rand(2**32))
      $random_encounter = Array.new(7)
      $random_encounter[0] = 0
      $battle_var = POKEMON_S::Pokemon_Battle_Variable.new
      $existing_pokemon = []
      $string = []
      POKEMON_S::set_SAVESLOT = @index
      Player.trainer_trade_code
      # ---- Pas forcément nécessaire
      $game_party.setup_starting_members
      $game_map.setup(MAPINTRO[0])
      $game_player.moveto(MAPINTRO[1], MAPINTRO[2])
      $game_player.refresh
      $game_map.autoplay
      $game_map.update
      $scene = Scene_Map.new
      return
    end
    
    Audio.bgm_stop
    $_temp_map_intro = nil
    
    @sprite = Scaling_Sprite.new
    @sprite.bitmap = RPG::Cache.title($data_system.title_name)
    
    number = 0 # Nombre de cadres de sauvegarde à générer
    for i in 0..2
      if FileTest.exist?("Save#{i+1}.rxdata")
        number += 1
      end
    end
    @number = number
    
    @new_game_window = Window_Base.new(30, 30 + 83*@number, 580, 80)
    @new_game_window.contents = Bitmap.new(548, 48)
    set_window(@new_game_window)
    @new_game_window.contents.draw_text(9,3,548,48,"NOUVELLE PARTIE")
    
    @save_game_window_list = []
    for i in 1..@number
      window = Window_Base.new(30, 30 + 83*(i-1), 580, 80)
      window.contents = Bitmap.new(548, 48)
      @save_game_window_list.push(window)
    end
    
    @index = 0
    
    @background = Scaling_Plane.new(Viewport.new(0,0,640,480))
    @background.bitmap = RPG::Cache.picture("fondsaved.png")
    @background.z -= 10
    
    refresh_all
    Graphics.transition
    
    loop do
      Graphics.update
      Input.update
      update
      if $scene != self
        break
      end
    end
    
    Graphics.freeze
    
    @new_game_window.dispose
    @background.dispose
    for window in @save_game_window_list
      window.dispose
    end
  end
end