#==============================================================================
# ■ Pokemon_Shop
# Pokemon Script Project - Krosk 
# 23/08/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------
# Interface d'un magasin
#-----------------------------------------------------------------------------

module POKEMON_S
  class Pokemon_Shop
    def initialize(shop_list)
      @shop_list = shop_list
      shop_list_conversion
    end
    
    def shop_list_conversion
      for i in 0..@shop_list.length-1
        if @shop_list[i].type == String
          @shop_list[i] = Item.id(@shop_list[i])
        end
      end
    end
    
    def main
      @spriteset = Spriteset_Map.new
      
      @text_window = Window_Base.new(21, 342, 597, 126)
      @text_window.contents = Bitmap.new(597 - 32, 126 - 32)
      @text_window.contents.font.name = $fontface
      @text_window.contents.font.size = $fontsize
      @text_window.contents.font.color = @text_window.normal_color
      @text_window.opacity = 0
      @text_window.visible = false
      @text_window.z = 300
      
      @dummy = Sprite.new
      @dummy.bitmap = RPG::Cache.picture(POKEMON_S::MSG)
      @dummy.y = 336
      @dummy.visible = false
      @dummy.z = 290
      
      @command_window = Window_Command.new(160, ["ACHETER", "VENDRE", "QUITTER"])
      @command_window.active = false
      @command_window.visible = false
      @command_window.x = 3
      @command_window.y = 3
      @command_window.z = 5
      
      Graphics.transition
      #draw_text("Bienvenue!")
      #wait_hit
      draw_text("En quoi puis-je vous aider?")
      @command_window.active = true
      @command_window.visible = true
      
      loop do
        Graphics.update
        Input.update
        update
        if $scene != self
          break
        end
      end
      Graphics.freeze
      @command_window.dispose
      @spriteset.dispose
      @dummy.dispose
      @text_window.dispose
    end

    def update
      @command_window.update
      
      if Input.trigger?(Input::C)
        $game_system.se_play($data_system.decision_se)
        case @command_window.index
        when 0
          @command_window.visible = false
          draw_text("")
          scene = Pokemon_Shop_Buy.new(@shop_list)
          scene.main
          @command_window.visible = true
          draw_text("Que puis-je faire d'autre", "pour vous?")
          Graphics.transition
          return
        when 1
          draw_text("")
          scene = Pokemon_Item_Bag.new($pokemon_party.bag_index, 100, "sell")
          scene.main
          draw_text("Que puis-je faire d'autre", "pour vous?")
          Graphics.transition
          return
        when 2
          draw_text("")
          $scene = Scene_Map.new
          return
        end
      end
      
      if Input.trigger?(Input::B)
        $game_system.se_play($data_system.cancel_se)
        $scene = Scene_Map.new
        return
      end
    end
    
    def call_item_amount(data, mode = nil, z_level = 200)
      @counter = Window_Base.new(102,230,177,64)
      @counter.contents = Bitmap.new(@counter.width - 32, @counter.height - 32)
      @counter.contents.font.name = $fontface
      @counter.contents.font.size = $fontsize
      @counter.contents.font.color = @counter.normal_color
      @counter.visible = false
      @counter.active = false
      @counter.z = z_level + 5
      
      @money_window = Window_Base.new(3,3,147,64)
      @money_window.contents = Bitmap.new(147-32,64-32)
      @money_window.contents.font.name = $fontface
      @money_window.contents.font.size = $fontsize
      @money_window.contents.font.color = @money_window.normal_color
      @money_window.contents.draw_text(0,0, 147-32, 64-32, $pokemon_party.money.to_s + "$", 2)
      @money_window.visible = false
      @money_window.z = z_level + 5
      
      @text_window.z = z_level + 10
      @dummy.z = z_level + 8
      @z_level = z_level
      @mode = mode
      @data = data
      
      @amount = 1
      
      if @mode == "sell"
        draw_text(Item.name(@data[0]) + "?", "Combien voulez-vous en vendre?")
        wait_hit
      end
      if @mode == "buy"
        draw_text(Item.name(@data[0]) + "? Bien sûr.", "Combien en voulez-vous?")
        wait_hit
      end
      
      refresh_counter
      @counter.visible = true
      @counter.active = true
      @money_window.visible = true
      loop do
        Input.update
        Graphics.update
        if @counter.visible
          update_counter
        end
        if @counter_done
          draw_text("")
          break
        end
      end
      @counter_done = false
      @counter.dispose
      @money_window.dispose
    end
      
    def wait_hit
      Graphics.update
      Input.update
      until Input.trigger?(Input::C)
        Input.update
        Graphics.update
      end
    end
    
    def update_counter
      if Input.trigger?(Input::B)
        $game_system.se_play($data_system.cancel_se)
        @counter_done = true
        return
      end
      if Input.trigger?(Input::C)
        $game_system.se_play($data_system.decision_se)
        @counter.visible = false
        confirm_call
        return
      end
      if Input.repeat?(Input::UP)
        @amount += 1
        if @mode == "sell" and @amount > @data[1]
          @amount = @data[1]
        end
        if @mode == "buy"
          until @amount*Item.price(@data[0]) <= $pokemon_party.money
            @amount -= 1
          end
        end
        refresh_counter
        return
      end
      if Input.repeat?(Input::DOWN)
        @amount -= 1
        if @amount < 1
          @amount = 1
        end
        refresh_counter
        return
      end
    end
    
    def confirm_call
      if @mode == "sell"
        draw_text("Je peux vous en donner ", (Item.price(@data[0])*@amount/2).to_s + "$, ça vous va?")
        if decision
          $pokemon_party.sell_item(@data[0],@amount)
          refresh_money
          draw_text("Obtenu " + (Item.price(@data[0])*@amount/2).to_s + "$", "pour cette vente.")
          wait_hit
          @counter_done = true
        else
          @counter_done = true
        end
      end
      if @mode == "buy"
        draw_text(Item.name(@data[0]) + "? Vous en voulez " + @amount.to_s + "?", 
          "Ca fera " + (@amount*Item.price(@data[0])).to_s + "$.")
        if decision
          $pokemon_party.buy_item(@data[0], @amount)
          refresh_money
          draw_text("Vous avez acheté " + @amount.to_s + " " + Item.name(@data[0]), 
            "pour " + (@amount*Item.price(@data[0])).to_s + "$." )
          wait_hit
          @counter_done = true
        else
          @counter_done = true
        end
      end
    end
    
    def decision
      @command = Window_Command.new(120, ["OUI", "NON"], $fontsize)
      @command.x = 159
      @command.y = 294 - @command.height
      @command.z = @z_level + 30
      loop do
        Graphics.update
        Input.update
        @command.update
        if Input.trigger?(Input::C) and @command.index == 0
          @command.dispose
          @command = nil
          return true
        end
        if (Input.trigger?(Input::C) and @command.index == 1) or Input.trigger?(Input::B)
          @command.dispose
          @command = nil
          return false
        end
      end
    end
    
    def refresh_counter
      @counter.contents.clear
      @counter.contents.draw_text(0,0,177-32,32,"x " + @amount.to_s, 0)
      if @mode == "buy"
        @counter.contents.draw_text(0,0,177-32,32,(Item.price(@data[0])*@amount).to_s + "$", 2)
      elsif @mode == "sell"
        @counter.contents.draw_text(0,0,177-32,32,(Item.price(@data[0])*@amount/2).to_s + "$", 2)
      end
    end
    
    def refresh_money
      @money_window.contents.clear
      @money_window.contents.draw_text(0,0,147-32, 32, $pokemon_party.money.to_s + "$", 2)
    end
    
    
    def draw_text(string = "", string2 = "")
      if string == ""
        @text_window.contents.clear
        @dummy.visible = false
        @text_window.visible = false
      else
        @text_window.contents.clear
        @text_window.visible = true
        @dummy.visible = true
        @text_window.contents.draw_text(0,0,597-32,32, string)
        @text_window.contents.draw_text(0,32,597-32,32, string2)
      end
    end
    
  end
  
#-----------------------------------------------------------------------------
# Interface du Sac
#-----------------------------------------------------------------------------  
  class Pokemon_Shop_Buy
    def initialize(shop_list)
      @shop_list = shop_list
      @z_level = 100
    end
      
    def main
      Graphics.freeze
      #@spriteset = Spriteset_Map.new
      
      @background = Sprite.new
      @background.bitmap = RPG::Cache.picture("shopfond.png")
      @background.z = @z_level + 1
      
      @item_list = POKEMON_S::Pokemon_Shop_List.new(@shop_list)
      @item_list.opacity = 0
      @item_list.z = @z_level + 10
      @item_list.active = true
      @item_list.visible = true
      
      @item_icon = Sprite.new
      @item_icon.z = @z_level + 4
      @item_icon.x = 12
      @item_icon.y = 213
      @item_icon.zoom_x = 3
      @item_icon.zoom_y = 3
      @item_icon.bitmap = RPG::Cache.icon(item_icon)
      
      @money_window = Window_Base.new(3,3,147,64)
      @money_window.contents = Bitmap.new(147-32,64-32)
      @money_window.contents.font.name = $fontface
      @money_window.contents.font.size = $fontsize
      @money_window.contents.font.color = @money_window.normal_color
      @money_window.contents.draw_text(0,0, 147-32, 64-32, $pokemon_party.money.to_s + "$", 2)
      @money_window.z = @z_level + 5
      
      @text_window = Window_Base.new(6, 286, 273, 193)
      @text_window.opacity = 0
      @text_window.contents = Bitmap.new(241, 161)
      @text_window.contents.font.name = $fontface
      @text_window.contents.font.size = $fontsizebig
      @text_window.contents.font.color = @text_window.normal_color
      @text_window.z = @z_level + 5
      
      refresh
      
      Graphics.transition
      
      loop do
        Input.update
        Graphics.update
        @item_list.update
        update
        if @done
          break
        end
      end
      Graphics.freeze
      @item_icon.dispose
      @item_list.dispose
      @text_window.dispose
      @background.dispose
      @money_window.dispose
    end
    
    def update
      if Input.repeat?(Input::UP) or Input.repeat?(Input::DOWN)
        refresh_nolist
      end
      
      if Input.trigger?(Input::C)
        $game_system.se_play($data_system.decision_se)
        if @item_list.index == @item_list.size
          @done = true
          return
        end
        id_item = @shop_list[@item_list.index]
        if $pokemon_party.money < Item.price(id_item)
          $scene.draw_text("Vous n'avez pas assez d'argent", "pour acheter cela.")
          $scene.wait_hit
          $scene.draw_text("")
          return
        end
        $scene.call_item_amount([id_item], "buy", @z_level + 100)
        refresh_money
        return
      end
      
      if Input.trigger?(Input::B)
        $game_system.se_play($data_system.cancel_se)
        @done = true
        return
      end
    end
    
    def refresh
      @item_list.refresh
      refresh_nolist
    end
    
    def refresh_nolist
      # Tracage de l'icone objet
      @item_icon.bitmap = RPG::Cache.icon(item_icon)
      # Texte de description
      text_window_draw(string_builder(item_descr, 15))
    end
    
    def refresh_money
      @money_window.contents.clear
      @money_window.contents.draw_text(0,0,147-32, 32, $pokemon_party.money.to_s + "$", 2)
    end
    
    def text_window_draw(list)
      @text_window.contents.clear
      for i in 0..list.length-1
        @text_window.contents.draw_text(0, 38*i, 241, $fhb, list[i])
      end
    end
    
    def item_icon
      if @item_list.index == @item_list.size
        return "return.png"
      else
        return Item.icon(item_id)
      end
    end
    
    def item_descr
      if @item_list.index == @item_list.size
        return "Retourner au magasin."
      else
        return Item.descr(item_id)
      end
    end
    
    def item_id
      return @shop_list[@item_list.index]
    end
    
    def string_builder(text, limit)
      length = text.length
      full1 = false
      full2 = false
      full3 = false
      full4 = false
      string1 = ""
      string2 = ""
      string3 = ""
      string4 = ""
      word = ""
      for i in 0..length
        letter = text[i..i]
        if letter != " " and i != length
          word += letter.to_s
        else
          word = word + " "
          if (string1 + word).length < limit and not(full1)
            string1 += word
            word = ""
          else
            full1 = true
          end
          
          if (string2 + word).length < limit and not(full2)
            string2 += word
            word = ""
          else
            full2 = true
          end
          
          if (string3 + word).length < limit and not(full3)
            string3 += word
            word = ""
          else
            full3 = true
          end
          
          #if (string4 + word).length < limit and not(full4)
            string4 += word
            word = ""
          #else
          #  full4 = true
          #end
        end
      end
      if string4.length > 1
        string4 = string4[0..string4.length-2]
      end
      return [string1, string2, string3, string4]
    end
  end
  
#-----------------------------------------------------------------------------
# Interface de la liste
#-----------------------------------------------------------------------------  
  class Pokemon_Shop_List < Window_Selectable
    def initialize(shop_list)
      super(287, 32, 356, 416, $fn)
      self.contents = Bitmap.new(324, 384)
      self.contents.font.name = $fontnarrow
      self.contents.font.size = $fontnarrowsize
      self.contents.font.color = normal_color
      @shop_list = shop_list
      @item_max = size + 1
      self.index = 0
    end
    
    def refresh
      self.contents.clear
      self.contents = Bitmap.new(356, $fhb*(size+1))
      self.contents.font.name = $fontnarrow
      self.contents.font.size = $fontnarrowsize
      self.contents.font.color = normal_color
      hl = $fn
      i = 0
      for id in @shop_list
        self.contents.draw_text(14, hl*i, 224, hl, Item.name(id))
        string = (Item.price(id).to_s)
        self.contents.draw_text(14, hl*i, 304, hl, string + "$", 2)
        i += 1
      end
      self.contents.font.color = normal_color
      self.contents.draw_text(14, hl*i, 304, hl, "RETOUR")
    end
    
    def size
      return @shop_list.length
    end
  end
end