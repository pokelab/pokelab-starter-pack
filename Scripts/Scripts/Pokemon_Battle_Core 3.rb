#==============================================================================
# ■ Pokemon_Battle_Core
# Pokemon Script Project - Krosk 
# 20/07/07
# Pokemon_Battle_Core_Safari
# Therand - 01/2015
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------
# Système de Combat - Safari
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# Cette scene définit les actions propres au parc safari, vous pouvez modifier
# les choix d'actions (BALL, APPAT, CAILLOU et FUITE) et leur effets direct
# (initialisation de compteurs, tour à effectuer, action préliminaire,...)
#-----------------------------------------------------------------------------

module POKEMON_S
  #------------------------------------------------------------  
  # Pokemon_Battle_Core_Safari
  #   noyau possédant les fonctions propres au parc safari
  #------------------------------------------------------------  
  #------------------------------------------------------------  
  # Fonctions à définir à l'extérieur
  #   initialize
  #   pre_battle_animation
  #   enemy_skill_decision
  #   end_battle_check
  #   actor_item_use
  #   catch_pokemon
  #   run_able?
  #   end_battle_victory
  #------------------------------------------------------------  
  class Pokemon_Battle_Core_Safari
    attr_accessor :z_level
    attr_accessor :actor_status
    attr_accessor :actor
    attr_accessor :actor_sprite
    
    #------------------------------------------------------------  
    # main
    #------------------------------------------------------------
    def main
      # Pré-création des Sprites
      # Fond
      if @battleback_name != ""
        @battleback_name = $game_map.battleback_name + ".png"
        @ground_name = "ground" + $game_map.battleback_name + ".png"
      else
        print("Attention, réglez le BattleBack du Tileset.")
        @battleback_name = "battle0.png"
        @ground_name = "groundbattle0.png"
      end
      @background = Sprite.new
      @background.z = @z_level
      
      # Fond du message
      @message_background = Sprite.new
      @message_background.y = 336
      @message_background.z = @z_level + 19
      
      # Sprite de flash
      @flash_sprite = Sprite.new
      @flash_sprite.bitmap = RPG::Cache.picture("black.png")
      @flash_sprite.color = Color.new(255,255,255)
      @flash_sprite.opacity = 0
      @flash_sprite.z = @z_level + 13
      
      # Fenetre de texte
      @text_window = Window_Base.new(4, 340, 632, 136)
      @text_window.opacity = 0
      @text_window.z = @z_level + 20
      @text_window.contents = Bitmap.new(600 + 32, 104 + 32)
      @text_window.contents.font.name = $fontface
      @text_window.contents.font.size = $fontsizebig
      
      # Fenetre d'action
      s1 = "BALL"
      s2 = "CAILLOU"
      s3 = "APPAT"
      s4 = "FUITE"
      
      @action_window = Window_Command.new(320, [s1, s3, s2, s4], $fontsizebig, 2, 56)
      @action_window.x = 320
      @action_window.y = 336
      @action_window.z = @z_level + 21
      @action_window.height = 144
      @action_window.active = false
      @action_window.visible = false
      @action_window.index = 0
      
      # Viewport
      battle_viewport = Viewport.new(0, 0, 640, 336)
      battle_viewport.z = @z_level + 15
      
      # Sprites acteurs # Positions par défaut des centres
      @enemy_sprite = RPG::Sprite.new(battle_viewport)
      @enemy_sprite.x = 464
      @enemy_sprite.y = 104
      @enemy_sprite.z = @z_level + 15
      @enemy_ground = RPG::Sprite.new
      @enemy_ground.x = 464
      @enemy_ground.y = 149
      @enemy_ground.z = @z_level + 11
      @actor_sprite = RPG::Sprite.new(battle_viewport)
      @actor_sprite.x = 153
      @actor_sprite.y = 336
      @actor_sprite.z = @z_level + 15
      @actor_ground = RPG::Sprite.new
      @actor_ground.x = 153
      @actor_ground.y = 386
      @actor_ground.z = @z_level + 11
      
      # Création fenêtre de statut
      @actor_status = Pokemon_Battle_Status.new(@actor, false, @z_level + 15)
      @enemy_status = Pokemon_Battle_Status.new(@enemy, true, @z_level + 15)
      @actor_status.visible = false
      @enemy_status.visible = false
      @enemy_caught = false
      
      @actor_party_status = Pokemon_Battle_Party_Status.new(@party, @battle_order, false, @z_level + 10)
      @enemy_party_status = Pokemon_Battle_Party_Status.new($battle_var.enemy_party, $battle_var.enemy_battle_order, true, @z_level + 10)
      @actor_party_status.visible = false
      @enemy_party_status.visible = false
      # note: .active = true activera les animations liées à ces fenêtres
      @actor_party_status.active = false
      @enemy_party_status.active = false
      
      # Lancement des animations
      pre_battle_transition
      pre_battle_animation
      
      # Effets pré-premier round
      post_round_effect
      
      Graphics.transition
      loop do
        Graphics.update
        Input.update
        update
        if $scene != self
          break
        end
      end
      
      # Fin de scene
      Graphics.freeze
      @background.dispose
      @message_background.dispose
      @flash_sprite.dispose
      @text_window.dispose
      @action_window.dispose
      @enemy_ground.dispose
      @actor_ground.dispose
      if @skill_window != nil
        @skills_window.dispose
      end
      if @ball_sprite != nil
        @ball_sprite.dispose
      end
      @enemy_sprite.dispose
      @actor_sprite.dispose
      @actor_status.dispose
      @enemy_status.dispose
      @actor_party_status.dispose
      @enemy_party_status.dispose
    end
    
    #------------------------------------------------------------  
    # Déroulement
    #------------------------------------------------------------
    def update
      # Animation test : séquence de test d'une animation
      if false
        if $temp == nil
          $temp = false
          @actor_sprite.register_position
          @enemy_sprite.register_position
        end
        animation = $data_animations[15] # tappez le numéro de l'anim à tester
        if not @enemy_sprite.effect? and not @actor_sprite.effect?
          if $temp
            @enemy_sprite.reset_position
            @actor_sprite.register_position
            @actor_sprite.animation(animation, true, true)
            $temp = !$temp
          else
            @actor_sprite.reset_position
            @enemy_sprite.register_position
            @enemy_sprite.animation(animation, true)
            $temp = !$temp
          end
        end
        @actor_sprite.update
        @enemy_sprite.update
        return
      end
      
      case @phase
      when 0 # Phase d'initialisation
        @phase = 1
        
        # Création fenêtre de skill
        list = []
        for skill in @actor.skills_set
          list.push(skill.name)
        end
        while list.size < 4
          list.push("  ---")
        end
        @skills_window = Window_Command.new(512, list, $fontsizebig, 2, 56)
        @skills_window.x = 0
        @skills_window.y = 336
        @skills_window.height = 144
        @skills_window.visible = false
        @skills_window.active = false
        
        # Compétences bloquées
        for i in 0..@actor.skills_set.length-1
          skill = @actor.skills_set[i]
          if not(skill.usable?)
            @skills_window.disable_item(i)
          end
        end
        
        # Curseur sur le dernier choix
        if $battle_var.last_index == nil
          $battle_var.last_index = 0
          @skills_window.index = 0
        else
          @skills_window.index = $battle_var.last_index
        end
        
        # Création fenêtre description de skill
        @skill_descr = Window_Base.new(512, 336, 128, 144)
        @skill_descr.contents = Bitmap.new(96, 144)
        @skill_descr.contents.font.name = $fontface
        @skill_descr.contents.font.size = $fontsizebig
        @skill_descr.visible = false
        skill_descr_refresh
        
        # Activation fenêtre
        @actor_status.visible = true
        @enemy_status.visible = true
                
        # ------- ---------- --------- --------
        #    Saut de phase de sélection actor
        # ------- ---------- --------- --------
        jumped = phase_jump
        
        # Activations fenêtres
        if not(jumped)
          draw_text("Que doit faire", "" + Player.name + " ?")
          @action_window.visible = true
          @action_window.active= true
          $battle_var.action_id = 0
        end
        
      when 1 # Phase d'attente d'action
        @action_window.update
        @skills_window.update
        if @skills_window.active and input
          skill_descr_refresh
        end
        
        if Input.trigger?(Input::C) and @action_window.active
          case @action_window.index
          when 0 # Selection BALL
            $game_system.se_play($data_system.decision_se)
            @action_window.active = false
            @action_window.visible = false
              # Si le nombre de ball == 0, le joueur n'a plus de BALL
              if $pokemon_party.item_number(12) == 0
                draw_text("Vous n'avez plus de SAFARI BALL!")
                wait(40)
                #On revient à la phase d'attente du joueur.
                @phase = 0
                @ball = 0
              else 
                # Sinon on continue l'action
                # en retirant une SAFARI BALL
                $pokemon_party.add_item(12, -1)
                $battle_var.action_id = 1 
                #Ceci renvoit à la méthode de capture incluse dans Battle_Safari
                catch_pokemon(12)
              end
            # Si la ball ne capture pas le pokemon, on continue l'action
            if $battle_var.action_id == 1
              @phase = 0
              # @ball = 1 sert à indiquer au Battle_Core 4 que le joueur à lancé une BALL
              @ball = 1
              #ce sera donc à l'enemy
              scene = enemy_skill_decision
            end
            # Si la capture réussi
            if $battle_var.action_id == 2
              #on donne les bonnes variables pour la ball utilisée dans le pokédex
              @item_id = 12
              $data_ball = 12
              # On ajoute le pokémon à l'équipe, avec les donné de la ball (fake)
              ball_data = Item.data(@item_id)["ball"]
              @enemy.given_name = @enemy.name
              @enemy.ball_data = ball_data
              # Changement de surnom
              string1 = "Voulez-vous changer le"
              string2 = "surnom de " + @enemy.given_name + "?"
              draw_text(string1, string2)
            if draw_choice
            draw_text("")
            scene = POKEMON_S::Pokemon_Name.new(@enemy, @z_level + 50)
            scene.main
            end
              # Intégration au PC
            if $pokemon_party.size < 6
            $pokemon_party.add(@enemy)
            else
            $pokemon_party.store_captured(@enemy)
            string1 = @enemy.given_name
            string2 = "est envoyé au PC."
            draw_text(string1, string2)
            wait(40)
            end
            #On fini le combat
            $battle_var.result_win = true
            end_battle
            end
             
          when 1 # Selection APPAT
            $game_system.se_play($data_system.decision_se)
            @action_window.active = false
            @action_window.visible = false
            # On initialise/calcul les compteurs de faim, colère
            @ball = 0
            @angry = 0
            @eating += 1 + rand(6)
            #@chance_safari est incluse dans le calcul de capture
            #vous le trouverez dans Pokemon_Safari
            @chance_safari /= 2
            #Si @chance_safari == 0 alors l'augmenter de un,
            #tout simplement pour éviter d'avoir une erreur/un drop rate incorrecte
            #si le joueur utilise plusieurs appat de suite (le @chance_safari doit 
            #toujours valoir 1 OU un multiple de 2)
             if@chance_safari == 0
               @chance_safari += 1
             end
              # Le lancement de l'appat
              @actor_action = 1
              @phase = 2
              @actor_skill = Skill.new(691) # APPAT
            
          when 2 # Selection CAILLOU
            $game_system.se_play($data_system.decision_se)
            @action_window.active = false
            @action_window.visible = false
            # On initialise/calcul les compteurs de faim, colère et doublage du rate
            @ball = 0
            @angry += 1 + rand(6)
            @eating = 0
            @chance_safari *= 2
            #juste au cas ou...
             if@chance_safari == 0
               @chance_safari += 1
             end
              # Le lancement du caillou
              @actor_action = 1
              @phase = 2
              @actor_skill = Skill.new(692) # CAILLOU
              return  
            
          when 3 # sélection FUITE
            # ------- ---------- --------- --------
            #    Vérification fuite permise
            # ------- ---------- --------- --------
            @action_window.visible = false
            if not(flee_able(@actor, @enemy))
              $game_system.se_play($data_system.buzzer_se)
              @action_window.visible = true
              draw_text("Que doit faire", "" + Player.name + " ?")
              return
            end
            @action_window.visible = true
            
            @action_window.active = false
            @action_window.visible = false
            @text_window.contents.clear
            run
          end
          return
        end
        
        if Input.trigger?(Input::C) and @skills_window.active
          index = @skills_window.index
          skill = @actor.skills_set[index]
          if skill != nil and skill.usable?
            @actor_action = 1
            @phase = 2
            @skills_window.active = false
            @skills_window.visible= false
            @skill_descr.visible = false
            @action_window.active = false
            @action_window.visible= false
            @actor_skill = @actor.skills_set[index]
            $battle_var.last_index = @skills_window.index
          else
            $game_system.se_play($data_system.buzzer_se)
          end
        end
        
        if Input.trigger?(Input::B) and @skills_window.active
          $game_system.se_play($data_system.decision_se)
          @skills_window.active = false
          @skills_window.visible = false
          @skill_descr.visible = false
          @action_window.active = true
          @phase = 0
        end
        
        
      when 2 # Phase d'action automatisée
        @action_window.visible = false
        @action_window.active = false
        
        enemy_skill_decision
        
        statistic_refresh
        turn_order
        phase2
        phase3
        
        # Phase de switch de fin de tour
        $battle_var.action_id = 4
        end_battle_check
        
        if $battle_var.battle_end?
          return
        end
        
        # Fin de tour / Post_Round effects
        post_round_effect
        @actor_status.refresh
        @enemy_status.refresh
        
        if $battle_var.battle_end?
          return
        end
        
        # Phase de switch post_round
        $battle_var.action_id = 6
        end_battle_check
        @phase = 0
        
        if $battle_var.battle_end?
          return
        end
        
        # Incrémentation nombre de tours
        $battle_var.round += 1
        
      end
      return
    end
    
    #------------------------------------------------------------  
    # Vérifications préliminaires et ordre d'action
    #------------------------------------------------------------
    def statistic_refresh
      @actor.statistic_refresh
      @enemy.statistic_refresh
    end
    
    #Recherche de priorité
    def turn_order 
      # Comparaison des priorités
      if @actor_skill == nil or @enemy_skill == nil
        @strike_first = true
        return
      end
      
      if @actor_action != 1 # Attaque
        @strike_first = false
        return
      end  
      
      if @actor_skill.priority > @enemy_skill.priority 
        @strike_first = true
      elsif @actor_skill.priority < @enemy_skill.priority 
        @strike_first = false
      else
        
      # En cas d'égalité
        if @enemy.spd > @actor.spd
          @strike_first = false
        elsif @enemy.spd < @actor.spd
          @strike_first = true
        else
          @strike_first = rand(2)>0 ? true : false
        end
      end
    end
    
    #------------------------------------------------------------  
    # Rounds
    #------------------------------------------------------------            
    def phase2 # Pré_Rounds
      @action_window.visible = false
      @action_window.active = false
      @actor_status.visible = true
      @enemy_status.visible = true
      @actor_status.refresh
      @enemy_status.refresh
      draw_text("","")
      
      # Préround 1: Fuite
      if @actor_action == 4
        run
      end
      if @enemy_action == 4
        enemy_run
      end
      
      # Préround 2: Item
      if @actor_action == 3
        actor_item_use
      end
      if @enemy_action == 3
        enemy_item_use
      end
      
      # Préround 3: Switch Pokémon
      if @actor_action == 2
        actor_pokemon_switch
      end
      if @enemy_action == 2
        enemy_pokemon_switch
      end
      
      @actor_status.refresh
      @enemy_status.refresh
    end
        
    # Round: Attaques
    def phase3 
      if @strike_first
        if @actor_action == 1 and not(@actor.dead?)
          attack_action(@actor, @actor_skill, @enemy)
        end
      else
        if not(@enemy_caught) and @enemy_action == 1 and not(@enemy.dead?)
          attack_action(@enemy, @enemy_skill, @actor)
        end
      end
      
      faint_check
      
      if @actor.dead? or @enemy.dead? or $battle_var.battle_end?
        return
      end
      
      if not(@strike_first)
        if @actor_action == 1 and not(@actor.dead?)
          attack_action(@actor, @actor_skill, @enemy)
        end
      else
        if not(@enemy_caught) and @enemy_action == 1 and not(@enemy.dead?)
          attack_action(@enemy, @enemy_skill, @actor)
        end
      end
      
      faint_check
      
      if @actor.dead? or @enemy.dead? or $battle_var.battle_end?
        return
      end
    end
    
    #------------------------------------------------------------  
    # Fonctions auxiliaires
    #------------------------------------------------------------     
    def switch(list, id1, id2)
      if id1 <= id2
        list.insert(id1, list[id2])
        list.delete_at(id2+1)
        list.insert(id2 + 1, list[id1+1])
        list.delete_at(id1+1)
        return list
      else
        switch(list, id2, id1)
      end
    end
    
    # Fonction auxiliaire
    def input
      if Input.trigger?(Input::C) or Input.trigger?(Input::B) or
        Input.trigger?(Input::UP) or Input.trigger?(Input::DOWN) or
        Input.trigger?(Input::LEFT) or Input.trigger?(Input::RIGHT)
        return true
      end
      return false
    end
    
    def draw_text(line1 = "", line2 = "")
      if line1.type == Array
        if line1[1] != nil
          draw_text(line1[0], line1[1])
        else
          draw_text(line1[0])
        end
      else
        Graphics.freeze
        @text_window.contents.clear
        @text_window.draw_text(12, 0, 460, 50, line1)
        @text_window.draw_text(12, 55, 460, 50, line2)
        Graphics.transition(5)
      end
    end
    
    def draw_text_valid(line1 = "", line2 = "")
      draw_text(line1, line2)
      loop do
        Graphics.update
        Input.update
        if Input.trigger?(Input::C)
          $game_system.se_play($data_system.decision_se)
          break
        end
      end
    end
    
    def wait(frame)
      i = 0
      loop do
        i += 1
        Graphics.update
        if i >= frame
          break
        end
      end
    end
    
    def wait_hit
      loop do
        Graphics.update
        Input.update
        if input
          $game_system.se_play($data_system.decision_se)
          break
        end
      end
    end
    
    
    def update_sprite
      @actor_sprite.bitmap = RPG::Cache.battler(@actor.battler_back, 0)
      @actor_sprite.ox = @actor_sprite.bitmap.width / 2
      @actor_sprite.oy = @actor_sprite.bitmap.height
      @enemy_sprite.bitmap = RPG::Cache.battler(@enemy.battler_face, 0)
      @enemy_sprite.ox = @enemy_sprite.bitmap.width / 2
      @enemy_sprite.oy = @enemy_sprite.bitmap.height / 2
    end
    
    #------------------------------------------------------------  
    # ----------------------- Interface -------------------------
    #------------------------------------------------------------
    
    #------------------------------------------------------------  
    # Fenêtre de description
    #------------------------------------------------------------        
    def skill_descr_refresh
      @skill_descr.contents.clear
      index = @skills_window.index
      skill = @actor.skills_set[index]
      if skill != nil
        string = skill.pp.to_s + "/" + skill.ppmax.to_s
        type = skill.type
      else
        string = "---"
        type = 0
      end
      normal_color = Color.new(60,60,60)
      @skill_descr.contents.font.color = normal_color
      #@skill_descr.contents.draw_text(0,6,60,39, "PP:")
      @skill_descr.contents.draw_text(0,6,96,39, string, 1)
      #@skill_descr.contents.draw_text(0,60,140,39, "TP:")
      draw_type(0, 60, type)
    end  
      
    def draw_type(x, y, type)
      src_rect = Rect.new(0, 0, 96, 42)
      bitmap = RPG::Cache.picture("T" + type.to_s + ".png")
      @skill_descr.contents.blt(x, y, bitmap, src_rect, 255)
    end

    #------------------------------------------------------------  
    # ------------------ Fonctions de combat --------------------
    #------------------------------------------------------------
    
    #------------------------------------------------------------    
    # Fonctions spéciales - programmation des attaques
    #------------------------------------------------------------ 
    def heal(user, user_sprite, user_status, bonus)
      value = bonus.abs
      for i in 1..value
        if bonus >= 0
          user.add_hp(1)
        else
          user.remove_hp(1)
        end
        if user.max_hp >= 144 and i % (user.max_hp / 144 + 1) != 0
          next
        end
        user_status.refresh
        Graphics.update
        Graphics.update
        if user.hp >= user.max_hp or user.dead?
          break
        end
      end
    end
    
    def self_damage(user, user_sprite, user_status, damage)
      if damage > 0
        Audio.se_play("Audio/SE/Hit.wav", 100)
        blink(user_sprite)
      end
      for i in 1..damage
        user.remove_hp(1)
        user_status.refresh
        if user.max_hp >= 144 and i % (user.max_hp / 144 + 1) != 0
          next
        end
        Graphics.update
        Graphics.update
        if user.dead?
          break
        end
      end
    end
    
    #------------------------------------------------------------    
    # Fonctions communes - Programmation des attaques
    #------------------------------------------------------------ 
    # 0: Normal, 1: Poison, 2: Paralysé, 3: Brulé, 4:Sommeil, 5:Gelé
    # 6: Confus, 7: Flinch, 8: Toxic
    #------------------------------------------------------------ 
    # 1 Normal  2 Feu  3 Eau 4 Electrique 5 Plante 6 Glace 7 Combat 8 Poison 9 Sol
    # 10 vol 11 psy 12insecte 13 roche 14 spectre 15 dragon 16 acier 17 tenebre
    #------------------------------------------------------------ 
    
    # Fonction à appeler en cas d'effets sur le statut
    def status_check(target, status, forcing = false)
      # Immunités
      # Poison
      if (target.type_poison? or target.type_steel?) and 
          (status == 1 or status == 8)
        draw_text(target.given_name + " est insensible", "au poison!")
        wait(40)
        return
      end
      # Freeze
      if status == 5 and target.type_ice?
        draw_text(target.given_name + " est insensible", "au gel!")
        wait(40)
        return
      end
      # Burn
      if status == 3 and target.type_fire?
        draw_text(target.given_name + " est insensible", "aux brûlures!")
        wait(40)
        return
      end
      # Soleil
      if status == 5 and $battle_var.sunny?
        draw_text("Le soleil empêche " + target.given_name, "de geler!")
        wait(40)
        return
      end
      # Lumber / Echauffement (ab)
      if status == 2 and target.ability == 7
        draw_text(target.ability_name + " de " + target.given_name , "empêche la paralysie.")
        wait(40)
        return
      end
      # Ignifu-voile / Water Veil (ab)
      if status == 3 and target.ability == 41
        draw_text(target.ability_name + " de " + target.given_name , "empêche les brûlures.")
        wait(40)
        return
      end
      # Insomnia (ab) // Vital Spirit / Esprit Vital (ab)
      if status == 4 and (target.ability == 15 or target.ability == 72)
        draw_text(target.ability_name + " de " + target.given_name , "empêche le sommeil.")
        wait(40)
        return
      end
      # Vaccin / Immunity (ab)
      if [1, 8].include?(status) and target.ability == 17
        draw_text(target.ability_name + " de " + target.given_name , "empêche l'empoisonnement.")
        wait(40)
        return
      end
      # Armumagma / Magma Armor (ab)
      if target.ability == 40 and status == 5
        draw_text(target.ability_name + " de " + target.given_name , "empêche le gel.")
        wait(40)
        return
      end
      # Tempo Perso / Own Tempo (ab)
      if status == 6 and target.ability == 20
        draw_text(target.ability_name + " de " + target.given_name , "empêche la confusion.")
        wait(40)
        return
      end
      # Attention / Inner focus (ab)
      if target.ability == 39 and status == 7
        draw_text(target.ability_name + " de " + target.given_name , "empêche la peur.")
        wait(40)
        return
      end
      # Synchronize (ab)
      if target.ability == 28 and [1, 2, 3, 8].include?(status)
        target.ability_token = status
        if status == 8
          target.ability_token = 1
        end
      end
      
      if [1,2,3,4,5,8].include?(target.status) and not(forcing) and not([6, 7].include?(status))
        status_string(target, -target.status) # animation
      elsif status == 6 and target.confused? and not(forcing)
        status_string(target, -6)
      elsif target.effect_list.include?(0x7C) and
          status != 7 # Rune Protect/Safeguard 
        draw_text(target.given_name + "est", "protégé des altérations!")
        wait(40)
      elsif target.effect_list.include?(0x9F) and
          status == 4 # Uproar
        draw_text(target.given_name + " ne peux pas dormir", "à cause du brouhaha!")
        wait(40)
      else
        case status
        when 1
          target.status_poison(forcing)
        when 2 
          target.status_paralyze(forcing)
        when 3
          target.status_burn(forcing)
        when 4
          target.status_sleep(forcing)
        when 5
          target.status_frozen(forcing)
        when 6
          target.status_confuse
        when 7
          target.status_flinch
        when 8
          target.status_toxic(forcing)
        end
        status_string(target, status)
      end
    end
    
    def accuracy_stage(user, target)
      stage = user.acc_stage - target.eva_stage
      stage = stage < -6 ? -6 : stage > 6 ? 6 : stage
      
      # --------------- ---------------- --------------
      #           Programmation des attaques
      # --------------- ---------------- --------------
      # Clairvoyayance / Foresight
      if target.effect_list.include?(0x71)
        stage = user.acc_stage
      end
      # --------------- ---------------- --------------
      # --------------- ---------------- --------------
      
      case stage
      when -6
        return 33.0/100
      when -5
        return 36.0/100
      when -4
        return 43.0/100
      when -3
        return 50.0/100
      when -2
        return 60.0/100
      when -1
        return 75.0/100
      when 0
        return 1
      when 1
        return 133.0/100
      when 2
        return 166.0/100
      when 3
        return 2
      when 4
        return 250.0/100
      when 5
        return 133.0/50
      when 6
        return 3
      end
    end
    
    #------------------------------------------------------------  
    # Post_round
    #------------------------------------------------------------         
    def post_round_effect
      # --------- -------------- --------------------
      # Fin des effets "at the end of a round"
      # --------- -------------- --------------------      
      # Suppression état appeuré (ne dure que un tour)
      @actor.flinch_check
      @enemy.flinch_check
      # Suppression état autre
      if @actor.dead?
        @actor.cure
        @actor.cure_state
      end
      if @enemy.dead?
        @enemy.cure
        @enemy.cure_state
      end
      
      # --------- -------------- --------------------
      # Programmation des attaques en Post-round
      # --------- -------------- --------------------
      #      Cycle commun 0 - Souhait et Météo
      # --------- -------------- --------------------
      post_round_cycle0
      
      # --------- -------------- --------------------
      #         Cycle individuel 1
      #      Programmation des attaques
      #           Effets du statut
      # --------- -------------- --------------------      
      if @strike_first
        post_round_cycle_1(@actor, @enemy)
        post_round_cycle_1(@enemy, @actor)
      else
        post_round_cycle_1(@enemy, @actor)
        post_round_cycle_1(@actor, @enemy)
      end
      
      # --------- -------------- --------------------
      #                Cycle 2
      #         Programmation des attaques
      #            Dommages finaux
      # --------- -------------- --------------------
      if @strike_first
        post_round_cycle_2(@actor, @enemy)
        post_round_cycle_2(@enemy, @actor)
      else
        post_round_cycle_2(@enemy, @actor)
        post_round_cycle_2(@actor, @enemy)
      end
      
      @actor.skill_effect_clean
      @enemy.skill_effect_clean
      
      faint_check
      
      # Round suivant
      if $battle_var.round == nil
        $battle_var.round = 0
      end
      $battle_var.round += 1
    end
    
    
    # --------- -------------- --------------------
    #     Cycle commun 0 - Météo et Souhait
    # --------- -------------- --------------------
    def post_round_cycle0
      if @strike_first
        list = [[@actor, @actor_sprite, @actor_status], [@enemy, @enemy_sprite, @enemy_status]]
      else 
        list = [[@enemy, @enemy_sprite, @enemy_status], [@actor, @actor_sprite, @actor_status]]
      end
      
      # Suppression du contrôle pour un pokémon mort
      for array in list
        if array[0].dead?
          list.delete(array)
        end
      end
      
      for array in list
        actor = array[0]
        actor.skill_effect_end_turn
        for effect in actor.effect_list
          case effect
          when 0x56 # Entrave / Disable
            index = actor.effect_list.index(0x56)
            if actor.effect[index][1] == 0
              skill_id = actor.effect[index][2]
              skill = actor.skills_set[skill_id]
              skill.enable
              draw_text(skill.name + " de "+ actor.given_name, "est rétablie!")
              wait(40)
            end
          when 0x5A # Encore
            index = actor.effect_list.index(0x5A)
            if actor.skills_set[index].pp == 0 
              actor.effect[index][1] = 0 # Fin de l'effet
            end
          when 0x75 # Rollout
            index = actor.effect_list.index(0x75)
            ## N'a pas fait de dégât ce tour ci >> Supprimé
            #if actor.effect[index][2] != actor.effect[index][1]
            #  actor.effect.delete_at(index)
            #end
            if actor.asleep? or actor.frozen?
              actor.effect.delete_at(index)
            end
          when 0x77 # Taillade / Fury Cutter
            index = actor.effect_list.index(0x77)
            # N'a pas fait de dégât ce tour ci >> Supprimé
            if actor.effect[index][2] != actor.effect[index][1]
              actor.effect.delete_at(index)
            end
          end
        end
      end
      
      weather = $battle_var.weather[0]
      $battle_var.weather[1] -= 1
      count = $battle_var.weather[1]
      
      # Souhait -- Programmation des attaques
      for array in list
        target = array[0]
        target_sprite = array[1]
        target_status = array[2]
        if target.effect_list.include?(0xB3)
          bonus = target.hp / 2
          draw_text("Un souhait est réalisé.")
          heal(target, target_sprite, target_status, bonus)
          wait(40)
        end
      end
      
      # Pluie
      if $battle_var.rain? and count != 0
        draw_text("La pluie continue de", "tomber.")
        animation = $data_animations[493]
        @actor_sprite.animation(animation, true)
        loop do
          @actor_sprite.update
          Graphics.update
          Input.update
          if not(@actor_sprite.effect?) #and Input.trigger?(Input::C)
            break
          end
        end
        wait(20)
      elsif $battle_var.rain? and count == 0
        draw_text("La pluie s'est arrêtée.")
        wait(40)
        $battle_var.reset_weather
      end
      
      # Ensoleillé
      if $battle_var.sunny? and count != 0
        draw_text("Les rayons du soleil","tapent fort.")
        animation = $data_animations[492]
        @actor_sprite.animation(animation, true)
        loop do
          @actor_sprite.update
          Graphics.update
          Input.update
          if not(@actor_sprite.effect?) #and Input.trigger?(Input::C)
            break
          end
        end
        wait(20)
      elsif $battle_var.sunny? and count == 0
        draw_text("Le soleil est parti.")
        wait(40)
        $battle_var.reset_weather
      end
      
      # Tempete de sable
      if $battle_var.sandstorm? and count != 0
        draw_text("La tempête de sable souffle.")
        animation = $data_animations[494]
        @actor_sprite.animation(animation, true)
        loop do
          @actor_sprite.update
          Graphics.update
          Input.update
          if not(@actor_sprite.effect?) #and Input.trigger?(Input::C)
            break
          end
        end
        
        # Dégats
        for array in list
          target = array[0]
          target_sprite = array[1]
          target_status = array[2]
          if target.type_ground? or target.type_rock? or target.type_steel?
              target.effect_list.include?(0x9B0) or target.effect_list.include?(0x9B3) or
              target.ability == 8
            next
          end
          damage = target.max_hp / 16
          heal(target, target_sprite, target_status, -damage)
        end
      elsif $battle_var.sandstorm? and count == 0
        draw_text("La tempête de sable s'est arretée.")
        wait(40)
        $battle_var.reset_weather
      end
      
      # Grêle
      if $battle_var.hail? and count > 0
        draw_text("Il grêle...")
        animation = $data_animations[495]
        @actor_sprite.animation(animation, true)
        loop do
          @actor_sprite.update
          Graphics.update
          Input.update
          if not(@actor_sprite.effect?) #and Input.trigger?(Input::C)
            break
          end
        end
        
        # Dégâts
        for array in list
          target = array[0]
          target_sprite = array[1]
          target_status = array[2]
          if target.type_ice? or
              target.effect_list.include?(0x9B0) or target.effect_list.include?(0x9B3)
            next
          end
          damage = target.max_hp / 16
          heal(target, target_sprite, target_status, -damage)
        end
      elsif $battle_var.hail? and count == 0
        draw_text("La grêle s'est arrêtée.")
        wait(40)
        $battle_var.reset_weather
      end
      
    end
    
    
    
    
    # --------- -------------- --------------------
    #              Cycle individuel 1
    # --------- -------------- -------------------- 
    def post_round_cycle_1(actor, enemy)
      if actor == @actor
        actor_status = @actor_status
        actor_sprite = @actor_sprite
        enemy_status = @enemy_status
        enemy_sprite = @enemy_sprite
      elsif actor == @enemy
        actor_status = @enemy_status
        actor_sprite = @enemy_sprite
        enemy_status = @actor_status
        enemy_sprite = @actor_sprite
      end
      
      # Suppression du contrôle pour un Pokémon mort
      if actor.dead?
        return
      end
      
      # --------- -------------- --------------------
      #    Programmation des attaques et des capa
      # --------- -------------- --------------------
      for effect in actor.effect_list
        case effect
        when 0xB5 # Ingrain / Racines
          bonus = actor.max_hp / 16
          draw_text(actor.given_name + " puise", "de l'énergie dans la terre.")
          heal(actor, actor_sprite, actor_status, bonus)
          wait(40)
        end
      end
      
      case actor.ability
      when 44 # Cuvette / Rain Dish (ab)
        if $battle_var.rain?
          bonus = actor.max_hp / 16
          draw_text(actor.ability_name + " de " + actor.given_name, "restaure les PV.")
          heal(actor, actor_sprite, actor_status, bonus)
          wait(40)
        end
      when 54 # Absentéisme / Truant (ab)
        if actor.ability_token == nil
          actor.ability_token = true
        end
        if actor.ability_token == true
          actor.ability_token = false
        elsif actor.ability_token == false
          actor.ability_token = true
        end
      when 61 # Mue / Shed skin (ab)
        if actor.status != 0 and rand(100) < 30
          actor.cure
          draw_text(actor.ability_name + " de " + actor.given_name, "guérit le statut.")
          wait(40)
        end
      end
      
      for effect in enemy.effect_list
        case effect
        when 0x54 # Leech Seed / Vampigraine
          malus = actor.max_hp / 8
          draw_text("L'énergie de " + actor.given_name,"est drainée!")
          heal(actor, actor_sprite, actor_status, -malus)
          heal(enemy, enemy_sprite, enemy_status, malus)
          wait(40)
        when 0x2A # Multi_turn attack
          damage = actor.max_hp / 16
          draw_text(actor.given_name, "est piégé!")
          self_damage(actor, actor_sprite, actor_status, damage)
          wait(40)
        end
      end
      
      if actor.dead?
        return
      end
      
      # --------- -------------- --------------------
      #          Inspection des statuts
      # --------- -------------- --------------------
      if actor.status == 1 # Poison
        damage = actor.poison_effect
        draw_text(actor.given_name + " souffre", "du poison.")
        status_animation(actor_sprite, actor.status)
        heal(actor, actor_sprite, actor_status, -damage)
        wait(20)
      end
      if actor.status == 8 # Toxic
        damage = actor.toxic_effect
        draw_text(actor.given_name + " souffre", "gravement du poison.")
        status_animation(actor_sprite, actor.status)
        heal(actor, actor_sprite, actor_status, -damage)
        wait(20)
      end
      if actor.status == 3 #Burn
        damage = actor.burn_effect
        draw_text(actor.given_name + " souffre", "de ses brûlures.")
        status_animation(actor_sprite, actor.status)
        heal(actor, actor_sprite, actor_status, -damage)
        wait(20)
      end
      
      actor.confuse_decrement
      if actor.dead?
        return
      end
      
      # --------- -------------- --------------------
      #         Programmation des attaques
      # --------- -------------- --------------------      
      for effect in actor.effect_list
        case effect
        when 0x6B # Nightmare / Cauchemar
          if actor.asleep?
            damage = actor.max_hp / 4
            draw_text(actor.given_name + " fait", "un chauchemar!")
            heal(actor, actor_sprite, actor_status, -damage)
            wait(20)
          else
            index = actor.effect_list.index(0x6B)
            actor.effect.delete_at(index)
          end
        when 0x6D # Curse
          damage = actor.max_hp / 4
          draw_text(actor.given_name + " est", "maudit!")
          heal(actor, actor_sprite, actor_status, -damage)
          wait(20)
        when 0x9F # Uproar / Brouhaha
          if actor.asleep?
            actor.cure
            draw_text(actor.given_name + " se réveille", "à cause du brouhaha!")
            wait(40)
          end
          if actor.frozen? #Fin de l'effet
            index = actor.effect_list.index(0x9F)
            actor.effect.delete_at(index)
          end
        when 0xAF # Taunt / Provoc
          index = actor.effect_list.index(0xAF)
          for skill in actor.skills_set
            if skill.power == 0 and actor.effect[index][1] > 0
              draw_text(skill.name + " est bloqué!")
              skill.disable
              wait(40)
            elsif actor.effect[index][1] == 0
              draw_text(skill.name + " est rétablit.")
              skill.enable
              wait(40)
            end
          end
        when 0xBB # Yawn / Baillement
          if actor.status == 0
            status_check(actor, 4)
            actor_status.refresh
          end
        end
      end
      
      if actor.dead?
        return
      end
      # --------- -------------- --------------------
      #                  Berry check
      # --------- -------------- --------------------  
      if Item.data(actor.item_hold)["leftovers"] and actor.hp != actor.max_hp
        draw_text(actor.given_name + " récupère un peu", "de vie avec " + Item.name(actor.item_hold) + ".")
        bonus = actor.max_hp / 16
        if bonus == 0
          bonus = 1
        end
        heal(actor, actor_sprite, actor_status, bonus)
        wait(40)
      end
    end
      
    # --------- -------------- --------------------
    #              Cycle individuel 2
    # --------- -------------- --------------------     
    def post_round_cycle_2(actor, enemy)
      # Redéfinition
      if actor == @actor
        actor_status = @actor_status
        actor_sprite = @actor_sprite
        enemy_status = @enemy_status
        enemy_sprite = @enemy_sprite
      elsif actor == @enemy
        actor_status = @enemy_status
        actor_sprite = @enemy_sprite
        enemy_status = @actor_status
        enemy_sprite = @actor_sprite
      end
      
      # Suppression du contrôle pour un pokémon mort
      if actor.dead?
        return
      end
      
      # --------- -------------- --------------------
      #         Programmation des capacités
      # --------- -------------- --------------------
      case actor.ability
      when 2 # Crachin / Drizzle (ab)
        if not($battle_var.rain?) # Pluie
          draw_text(actor.ability_name + " de " + actor.given_name, "invoque la pluie.")
          wait(40)
          animation = $data_animations[493]
          @actor_sprite.animation(animation, true)
          loop do
            @actor_sprite.update
            Graphics.update
            Input.update
            if not(@actor_sprite.effect?)
              break
            end
          end
        end
        $battle_var.set_rain
      when 45 # Sable Volant / Sand stream (ab)
        if not($battle_var.sandstorm?) # Tempete Sable
          draw_text(actor.ability_name + " de " + actor.given_name, "réveille une tempête.")
          wait(40)
          animation = $data_animations[494]
          @actor_sprite.animation(animation, true)
          loop do
            @actor_sprite.update
            Graphics.update
            Input.update
            if not(@actor_sprite.effect?)
              break
            end
          end
        end
        $battle_var.set_sandstorm
      when 70 # Sècheresse / Drought (ab)
        if not($battle_var.sunny?) # Soleil
          draw_text(actor.ability_name + " de " + actor.given_name, "intensifie le soleil.")
          wait(40)
          animation = $data_animations[492]
          @actor_sprite.animation(animation, true)
          loop do
            @actor_sprite.update
            Graphics.update
            Input.update
            if not(@actor_sprite.effect?)
              break
            end
          end
        end
        $battle_var.set_sandstorm
      when 3 # Speed Boost (ab)
        draw_text(actor.ability_name + " de " + actor.given_name, "augmente la Vitesse.")
        actor.change_spd(+1)
        stage_animation(actor_sprite, $data_animations[482])
        wait(40)
      when 22 # Intimidate (ab)
        if not(actor.ability_active)
          actor.ability_active = true
          draw_text(actor.ability_name + " de " + actor.given_name, "réduit l'Attaque de " + enemy.given_name + ".")
          enemy.change_atk(-1)
          stage_animation(enemy_sprite, $data_animations[479])
          wait(40)
        end
      when 59 # Forecast / Meteo (ab)
        if $battle_var.sunny? and not(actor.type_fire?)
          draw_text("#{actor.ability_name} de #{actor.given_name}", "change son type en FEU!")
          actor.ability_token = 2
          actor.form = 2
          update_sprite
          wait(40)
        elsif $battle_var.rain? and not(actor.type_water?)
          draw_text("#{actor.ability_name} de #{actor.given_name}", "change son type en EAU!")
          actor.ability_token = 3
          actor.form = 3
          update_sprite
          wait(40)
        elsif $battle_var.hail? and not(actor.type_ice?)
          draw_text("#{actor.ability_name} de #{actor.given_name}", "change son type en GLACE!")
          actor.ability_token = 6
          actor.form = 6
          update_sprite
          wait(40)
        elsif not(actor.type_normal?)
          draw_text("#{actor.ability_name} de #{actor.given_name}", "change son type en NORMAL!")
          actor.ability_token = 1
          actor.form = 0
          update_sprite
          wait(40)
        end
      end
      # --------- -------------- --------------------
      #         Programmation des attaques
      # --------- -------------- --------------------
      for effect in actor.effect_list
        case effect
        when 0x72 # Requiem / Perish Song
          index = actor.effect_list.index(0x72)
          number = actor.effect[index][1]
          if number > 0
            if number > 1
              string = "#{number.to_s} tours"
            elsif number == 1
              string = "#{number.to_s} tour"
            end
            draw_text("Plus que #{string}", "pour #{actor.given_name}...")
            wait(40)
          else
            draw_text("#{actor.given_name} est", "K.O. par REQUIEM!")
            damage = actor.hp
            heal(actor, actor_sprite, actor_status, -damage)
            wait(40)
          end
        end
      end
      
      # --------- -------------- --------------------
      #      Nettoyage des compteurs d'effets
      # --------- -------------- --------------------
      for effect in actor.effect
        case effect
        when [0x10, 0] # Reflet / Reflect
          draw_text("L'effet de REFLET est", "terminé.")
          wait(40)
        when [0x23, 0] # Light Screen 
          draw_text("L'effet de MUR LUMIERE est", "terminé.")
          wait(40)
        when [0x2E, 0] # Brume / Mist
          draw_text("La brume se dissipe.")
          wait(40)
        when [0x7C, 0] # Rune Protect / Safeguard
          draw_text("L'effet de RUNE PROTECT", "est terminé.")
          wait(40)
        end
      end
      
      if actor.dead?
        return
      end
    end
    
    
    
    #------------------------------------------------------------  
    # Items
    #------------------------------------------------------------     
    def actor_item_use # items à utiliser
      # Item déjà utilisé ie remplacé par 0
      if @item_id == 0
        return
      end
    end

    #------------------------------------------------------------  
    # Switch de pokémon
    #------------------------------------------------------------         
    def actor_pokemon_switch
      if @switch_id != -1
        if not(@actor.dead?)
          @actor_status.visible = true
        else
          @actor_status.visible = false
        end
        
        switch_effect(@actor, @enemy)
        
        if not(@actor.dead?)
          recall_pokemon
        end
        
        @battle_order = switch(@battle_order, 0, @switch_id)
        @actor = @party.actors[@battle_order[0]]
        @actor_sprite.bitmap = RPG::Cache.battler(@actor.battler_back, 0)
        @actor_status = Pokemon_Battle_Status.new(@actor, false)
        @actor_status.visible = false
        if not($battle_var.have_fought.include?(@actor.party_index))
          $battle_var.have_fought.push(@actor.party_index)
        end
        
        launch_pokemon
        @actor_status.visible = true
        @switch_id = -1
      end
    end
    
    def enemy_pokemon_switch
      if @enemy_switch_id != -1
        if not(@enemy.dead?)
          @enemy_status.visible = true
        else
          @enemy_status.visible = false
        end
        
        switch_effect(@enemy, @actor)
        
        if not(@enemy.dead?)
          recall_enemy_pokemon
        end
        
        @enemy_battle_order = switch($battle_var.enemy_battle_order, 0, @enemy_switch_id)
        @enemy = $battle_var.enemy_party.actors[$battle_var.enemy_battle_order[0]]
        $data_pokedex[@enemy.id][0] = true
        @enemy_sprite.bitmap = RPG::Cache.battler(@enemy.battler_face, 0)
        @enemy_status = Pokemon_Battle_Status.new(@enemy, true)
        @enemy_status.visible = false
        
        launch_enemy_pokemon
        @enemy_status.visible = true
        @enemy_switch_id = -1
      end
    end
    
    #------------------------------------------------------------  
    # Fuite
    #------------------------------------------------------------           
    def run
      if run_able?(@actor, @enemy)
        $battle_var.run_count += 1
        @action_window.active = false
        @action_window.visible = false
        end_battle_flee
      else
        $battle_var.run_count += 1
        fail_flee
        @phase = 2
        @actor_action = 0
        $battle_var.action_id = 0
      end
    end
    
    def run_able?(runner, opponent)
      x = (Integer(opponent.spd/4) % 255)
      rate = Integer(runner.spd*32/x)+(30*($battle_var.run_count))
      if not(flee_able(runner, opponent))
        return false
      end
      if opponent.spd <= runner.spd
        return true
      elsif x == 0
        return true
      elsif rate > 255
        return true
      elsif rand(256) <= rate
        return true
      else
        return false
      end
    end
    
    def run_enemy
      if run_able?(@enemy, @actor)
        end_battle_flee_enemy
      end
    end
    
    #------------------------------------------------------------  
    # Animations supplémentaires au combat
    #------------------------------------------------------------   
    # Défaites / KO
    #------------------------------------------------------------       
    def enemy_down
      # Si déjà vaincu
      if @enemy_sprite.zoom_y == 0
        return
      end
      # Sinon
      @enemy_sprite.oy = @enemy_sprite.bitmap.height
      @enemy_sprite.y += @enemy_sprite.bitmap.height / 2
      if FileTest.exist?(@enemy.cry)
        Audio.se_play(@enemy.cry)
      end
      
      wait(50)
      Audio.se_play("Audio/SE/Down.wav")
      
      loop do
        @enemy_status.x -= 20
        #@enemy_sprite.zoom_y -= 0.05
        @enemy_sprite.y += 8
        @enemy_sprite.opacity -= 20
        Graphics.update
        #if @enemy_sprite.zoom_y <= 0.0
        if @enemy_sprite.y >= 348
          @enemy_sprite.zoom_y = 0
          break
        end
      end
      @enemy_sprite.oy = @enemy_sprite.bitmap.height / 2
      @enemy_sprite.y -= @enemy_sprite.bitmap.height
      draw_text(@enemy.given_name, "est K.O.!")
      wait(40)
    end
    
    def actor_down
      # Si déjà vaincu
      #if @actor_sprite.zoom_y <= 0.0
      if @actor_sprite.y >= 576
        return
      end
      # Sinon
      if FileTest.exist?(@actor.cry)
        Audio.se_play(@actor.cry)
      end
      wait(50)
      Audio.se_play("Audio/SE/Down.wav")
      loop do
        @actor_status.x += 20
        #@actor_sprite.zoom_y -= 0.05
        @actor_sprite.y += 12
        @actor_sprite.opacity -= 20
        Graphics.update
        #if @actor_sprite.zoom_y <= 0.0
        if @actor_sprite.y >= 576
          break
        end
      end
      
      draw_text(@actor.given_name, "est K.O.!")
      wait(40)
    end
    
    #------------------------------------------------------------       
    # Attaques
    #------------------------------------------------------------
    def attack_animation(info, hit, miss, user, user_skill, user_sprite, target_sprite)
      if miss
        wait(40)
        draw_text("Mais cela échoue!")
        wait(40)
        return
      end
      
      if user == @enemy
        reverse = true
      else
        reverse = false
      end
      
      efficiency = info[2]
      if hit and efficiency != -2
        # Animation utilisateur
        animation_user = $data_animations[user_skill.user_anim_id]
        user_sprite.register_position
        
        if animation_user != nil
          user_sprite.animation(animation_user, true, reverse)
          until not(user_sprite.effect?)
            user_sprite.update
            Graphics.update
          end
        end
        
        user_sprite.reset_position
        
        user_sprite.update
        Graphics.update
        
        # Animation Cible
        animation_target = $data_animations[user_skill.target_anim_id]
        target_sprite.register_position
        
        if animation_target != nil
          target_sprite.animation(animation_target, true, reverse)
          until not(target_sprite.effect?)
            target_sprite.update
            Graphics.update
          end
        end
        
        target_sprite.reset_position
        
        target_sprite.update
        Graphics.update
        
        if info[0] > 0
          case efficiency
          when 0 # Normal
            Audio.se_play("Audio/SE/Hit.wav", 100)
            blink(target_sprite, 3, 3)
          when 1 # Super efficace
            Audio.se_play("Audio/SE/Hitplus.wav", 100)
            blink(target_sprite, 2, 5)
          when -1 # Peu efficace
            Audio.se_play("Audio/SE/Hitlow.wav", 100)
            blink(target_sprite, 4, 2)
          end
        end
      elsif not(hit)
        wait(40)
        draw_text(user.given_name, "rate son attaque!")
        wait(40)
      end
    end
    
    def blink(sprite, frame = 4, number = 3)
      for i in 0..number
        wait(frame)
        sprite.opacity = 0
        Graphics.update
        wait(frame)
        sprite.opacity = 255
        Graphics.update
      end
    end
    
    def post_attack(info, damage, power)
      efficiency = info[2]
      if damage == 0 and (efficiency != -2 or power == 0)
        return
      end
      critical = info[1]
      if critical  and efficiency != -2 #critical_hit
        draw_text("Coup critique!")
        wait(40)
      end
      case efficiency
      when 1
        draw_text("C'est super efficace!")
        wait(40)
      when -1
        draw_text("Ce n'est pas très efficace...")
        wait(40)
      when -2
        draw_text("Ca ne l'affecte pas...")
        wait(40)
      end
    end
    
    def faint_check(user = nil)
      if user == nil
        faint_check(@actor)
        faint_check(@enemy)
      end
      if user == @actor and user.dead?
        actor_down
      end
      if user == @enemy and user.dead?
        enemy_down
      end
    end
        
    
    #------------------------------------------------------------       
    # Statut et stats
    #------------------------------------------------------------    
    def status_animation(sprite, status)
      animation = $data_animations[469 + status]
      sprite.animation(animation, true)
      loop do
        sprite.update
        Graphics.update
        Input.update
        if not(sprite.effect?)
          break
        end
      end
    end
    
    def stage_animation(sprite, animation)
      sprite.animation(animation, true)
      loop do
        sprite.update
        Graphics.update
        Input.update
        if not(sprite.effect?)
          wait(20)
          break
        end
      end
    end

    # 1 Normal  2 Feu  3 Eau 4 Electrique 5 Plante 6 Glace 7 Combat 8 Poison 9 Sol
    # 10 Vol 11 Psy 12 Insecte 13 Roche 14 Spectre 15 Dragon 16 Acier 17 Tenebres
    def type_string(type)
      case type
      when 0
        return "???"
      when 1
        return "NORMAL"
      when 2
        return "FEU"
      when 3
        return "EAU"
      when 4
        return "ELECTRIK"
      when 5
        return "PLANTE"
      when 6
        return "GLACE"
      when 7
        return "COMBAT"
      when 8
        return "POISON"
      when 9
        return "SOL"
      when 10
        return "VOL"
      when 11
        return "PSY"
      when 12
        return "INSECTE"
      when 13
        return "ROCHE"
      when 14
        return "SPECTRE"
      when 15
        return "DRAGON"
      when 16
        return "ACIER"
      when 17
        return "TENEBRES"
      end
    end
    
    
    # Changement (ou pas) de statut
    def status_string(actor, status)
      string = actor.given_name
      case status
      when -1
        draw_text(string + " est", "déjà empoisonné!")
        wait(40)
      when -2
        draw_text(string + " est", "déjà paralysé!")
        wait(40)
      when -3
        draw_text(string,"brûle déjà!")
        wait(40)
      when -4
        draw_text(string,"dort déjà!")
        wait(40)
      when -5
        draw_text(string, "est déjà gelé!")
        wait(40)
      when -6
        draw_text(string, "est déjà confus!")
        wait(40)
      when -8
        draw_text(string + " est", "déjà gravement empoisonné!")
        wait(40)
      when 1
        draw_text(string, "est empoisonné!")
        wait(40)
      when 2
        draw_text(string, "est paralysé!")
        wait(40)
      when 3
        draw_text(string,"brûle!")
        wait(40)
      when 4
        draw_text(string,"s'endort!")
        wait(40)
      when 5
        draw_text(string,"gèle!")
        wait(40)
      when 6
        draw_text("Cela rend " + string, "confus!")
        wait(40)
      when 7
        draw_text(string, "est appeuré!")
        wait(40)
      when 8
        draw_text(string + " est", "gravement empoisonné!")
        wait(40)
      end
    end
    
    # S'occupe du texte et de l'animation
    def raise_stat(string, actor, n = 0)
      if actor == @actor
        actor_sprite = @actor_sprite
      elsif actor == @enemy
        actor_sprite = @enemy_sprite
      end
      
      if n == 1
        text = actor.given_name + " augmente!"
      elsif n > 1
        text = actor.given_name + " augmente beaucoup!"
      end
      
      if n != 0
        case string
        when "ATK"
          draw_text("Ah, Attaque de",text)
          stage_animation(actor_sprite, $data_animations[478])
        when "DFE"
          draw_text("Ah, Défense de",text)
          stage_animation(actor_sprite, $data_animations[480])
        when "ATS"
          draw_text("Ah, Attaque Spéciale de",text)
          stage_animation(actor_sprite, $data_animations[484])
        when "DFS"
          draw_text("Ah, Défense Spéciale de",text)
          stage_animation(actor_sprite, $data_animations[486])
        when "SPD"
          draw_text("Ah, Vitesse de",text)
          stage_animation(actor_sprite, $data_animations[482])
        when "EVA"
          draw_text("Ah, Esquive de",text)        
          stage_animation(actor_sprite, $data_animations[488])
        when "ACC"
          draw_text("Ah, Précision de",text)
          stage_animation(actor_sprite, $data_animations[490])
        end
      elsif n == 0
        case string
        when "ATK"
          draw_text("Ah, Attaque de",actor.given_name + " n'ira pas plus haut!")
          wait(40)
        when "DFE"
          draw_text("Ah, Défense de",actor.given_name + " n'ira pas plus haut!")
          wait(40)
        when "ATS"
          draw_text("Ah, Attaque Spéciale de",actor.given_name + " n'ira pas plus haut!")
          wait(40)
        when "DFS"
          draw_text("Ah, Défense Spéciale de",actor.given_name + " n'ira pas plus haut!")
          wait(40)
        when "SPD"
          draw_text("Ah, Vitesse de",actor.given_name + " n'ira pas plus haut!")
          wait(40)
        when "EVA"
          draw_text("Ah, Esquive de",actor.given_name + " n'ira pas plus haut!")        
          wait(40)
        when "ACC"
          draw_text("Ah, Précision de ",actor.given_name + " n'ira pas plus haut!")
          wait(40)
        when 0
          draw_text("Les effets positifs sont supprimés!")
          wait(40)
        end
      end
    end
    
    def reduce_stat(string, actor, n = true, self_inflicted = false)
      # Mist/Brume
      if actor.effect_list.include?(0x2E)
        draw_text(actor.given_name + " est", "protégé par la brume!")
        wait(40)
        return
      end
      # Clear Body / Corps Sain (ab) // White Smoke / Ecran fumée (ab)
      if (actor.ability == 29 or actor.ability == 73) and not(self_inflicted)
        draw_text(actor.ability_name + " de " + actor.given_name, "empêche la réduction!")
        wait(40)
        return
      end
      # Keen Eye / Regard Vif (ab)
      if actor.ability == 51 and string == "ACC"
        draw_text(actor.ability_name + " de " + actor.given_name, "conserve la Précision!")
        wait(40)
        return
      end
      # Hyper Cutter (ab)
      if actor.ability == 52 and string == "ATK"
        draw_text(actor.ability_name + " de " + actor.given_name, "conserve l'Attaque!")
        wait(40)
        return
      end
      
      if actor == @actor
        actor_sprite = @actor_sprite
      elsif actor == @enemy
        actor_sprite = @enemy_sprite
      end
      
      if n == -1
        text = actor.given_name + " baisse!"
      elsif n < -1
        text = actor.given_name + " baisse beaucoup!"
      end
      
      if n != 0
        case string
        when "ATK"
          draw_text("Ah, Attaque de",text)
          stage_animation(actor_sprite, $data_animations[479])
        when "DFE"
          draw_text("Ah, Défense de",text)
          stage_animation(actor_sprite, $data_animations[481])
        when "ATS"
          draw_text("Ah, Attaque Spéciale de",text)
          stage_animation(actor_sprite, $data_animations[485])
        when "DFS"
          draw_text("Ah, Défense Spéciale de",text)
          stage_animation(actor_sprite, $data_animations[487])
        when "SPD"
          draw_text("Ah, Vitesse de",text)
          stage_animation(actor_sprite, $data_animations[483])
        when "EVA"
          draw_text("Ah, Esquive de",text)        
          stage_animation(actor_sprite, $data_animations[489])
        when "ACC"
          draw_text("Ah, Précision de",text)
          stage_animation(actor_sprite, $data_animations[491])
        end
      elsif n == 0
        case string
        when "ATK"
          draw_text("Ah, Attaque de",actor.given_name + " n'ira pas plus bas!")
          wait(40)
        when "DFE"
          draw_text("Ah, Défense de",actor.given_name + " n'ira pas plus bas!")
          wait(40)
        when "ATS"
          draw_text("Ah, Attaque Spéciale de",actor.given_name + " n'ira pas plus bas!")
          wait(40)
        when "DFS"
          draw_text("Ah, Défense Spéciale de",actor.given_name + " n'ira pas plus bas!")
          wait(40)
        when "SPD"
          draw_text("Ah, Vitesse de",actor.given_name + " n'ira pas plus bas!")
          wait(40)
        when "EVA"
          draw_text("Ah, Esquive de",actor.given_name + " n'ira pas plus bas!")        
          wait(40)
        when "ACC"
          draw_text("Ah, Précision de",actor.given_name + " n'ira pas plus bas!")
          wait(40)
        when 0
          draw_text("Les effets positifs sont supprimés!")
          wait(40)
        end
      end
    end
    
    #------------------------------------------------------------       
    # Appel / Rappel de Pokémon
    #------------------------------------------------------------           
    def recall_pokemon
      draw_text("Ca suffit, " + @actor.given_name + "!", "Reviens!")
      @actor_sprite.ox = @actor_sprite.bitmap.width / 2
      @actor_sprite.oy = @actor_sprite.bitmap.height
      @actor_sprite.y = 336
      @actor_sprite.x = 153
      @actor_sprite.color = @actor.ball_color
      @actor_sprite.color.alpha = 0
      
      until @actor_sprite.color.alpha >= 255
        @flash_sprite.opacity += 25
        @actor_sprite.color.alpha += 25
        Graphics.update
      end
      
      Audio.se_play("Audio/SE/Pokeopen.wav")
      loop do
        @actor_status.x += 20
        @actor_sprite.opacity -= 25
        @actor_sprite.color.alpha += 25
        @actor_sprite.zoom_x -= 0.1
        @actor_sprite.zoom_y -= 0.1
        @flash_sprite.opacity -= 25
        Graphics.update
        if @actor_status.x >= 711
          @actor_status.visible = false
          @actor_status.x = 711
          @actor_sprite.color.alpha = 0
          @actor_sprite.opacity = 0
          Graphics.update
          break
        end
      end
    end
    
    def launch_pokemon
      @actor_sprite.x = 153
      @actor_sprite.y = 336
      @actor_sprite.ox = @actor_sprite.bitmap.width / 2
      @actor_sprite.oy = @actor_sprite.bitmap.height
      @actor_sprite.zoom_x = 0
      @actor_sprite.zoom_y = 0
      
      #if @actor_party_status.active
      #  @actor_party_status.x = 0
      #  @actor_party_status.visible = true
      #end
      
      name = @actor.given_name
      text = [name + "! Go!", name + "! A toi!", name + "! A l'attaque!", name + "! Fonce!"][rand(4)]
      draw_text(text)
      
      @ball_sprite = Sprite.new
      @ball_sprite.bitmap = RPG::Cache.picture(@actor.ball_sprite)
      @ball_sprite.ox = @ball_sprite.bitmap.width / 2
      @ball_sprite.oy = @ball_sprite.bitmap.height / 2
      @ball_sprite.x = -44
      @ball_sprite.y = 324
      @ball_sprite.z = @z_level + 14
      
      t = 0
      pi = 3.14
      
      loop do
        t += 1
        @ball_sprite.x += 5
        @ball_sprite.y = 336 - 130 * Math.sin(t/40.0*pi)
        @ball_sprite.angle = - t*63
        #if @actor_party_status.active
        #  @actor_party_status.x -= 80
        #end
        Graphics.update
        if t == 40
          @ball_sprite.bitmap = RPG::Cache.picture(@actor.ball_open_sprite)
          Audio.se_play("Audio/SE/Pokeopen.wav")
          #if @actor_party_status.active
          #  @actor_party_status.x = 0
          #  @actor_party_status.visible = false
          #end
          break
        end
      end
      @actor_sprite.opacity = 0
      @actor_sprite.color = @actor.ball_color
      
      until @actor_sprite.zoom_x >= 0.9
        @flash_sprite.opacity += 25
        @ball_sprite.opacity -= 25
        @actor_sprite.zoom_x += 0.1
        @actor_sprite.zoom_y += 0.1
        @actor_sprite.opacity += 25
        Graphics.update
      end
      
      if FileTest.exist?(@actor.cry)
        Audio.se_play(@actor.cry)
      end
      
      @actor_sprite.zoom_x = 1
      @actor_sprite.zoom_y = 1
      @actor_sprite.opacity = 255
      
      @actor_status.x = 711
      @actor_status.visible = true
      
      if @actor.shiny
        animation = $data_animations[496]
        @actor_sprite.animation(animation, true)
      end
      
      until @actor_status.x == 311
        @background.update
        @actor_ground.update
        @enemy_ground.update
        @actor_status.x -= 20
        @actor_sprite.color.alpha -= 25
        @flash_sprite.opacity -= 25
        @actor_sprite.update
        Graphics.update
      end
      
      until not(@actor_sprite.effect?)
        @actor_sprite.update
        Graphics.update
      end
      
      @actor_status.x = 311
      @actor_sprite.color.alpha = 0
      @flash_sprite.opacity = 0
      @ball_sprite.dispose
      Graphics.update
    end
    
    def launch_enemy_pokemon
      @enemy_sprite.x = 464
      @enemy_sprite.y = 104
      @enemy_sprite.ox = @enemy_sprite.bitmap.width / 2
      @enemy_sprite.oy = @enemy_sprite.bitmap.height / 2
      @enemy_sprite.zoom_x = 0
      @enemy_sprite.zoom_y = 0
      
      string = Trainer_Info.type(@trainer_id) + " " + Trainer_Info.name(@trainer_id)
      draw_text(@enemy.name + " est envoyé", "par " + string + "!")
      
      @ball_sprite = Sprite.new
      @ball_sprite.bitmap = RPG::Cache.picture(@enemy.ball_sprite)
      @ball_sprite.ox = @ball_sprite.bitmap.width / 2
      @ball_sprite.oy = @ball_sprite.bitmap.height / 2
      @ball_sprite.x = 663
      @ball_sprite.y = 104
      @ball_sprite.z = @z_level + 14
      
      t = 0
      pi = 3.14
      
      loop do
        t += 1
        @ball_sprite.x -= 5
        @ball_sprite.y = 128 - 80 * Math.sin(t/40.0*pi)
        @ball_sprite.angle = - t*63
        Graphics.update
        if t == 40
          @ball_sprite.bitmap = RPG::Cache.picture(@enemy.ball_open_sprite)
          Audio.se_play("Audio/SE/Pokeopen.wav")
          break
        end
      end
      @enemy_sprite.opacity = 0
      @enemy_sprite.color = @enemy.ball_color
      
      until @enemy_sprite.zoom_x >= 0.9
        @flash_sprite.opacity += 25
        @ball_sprite.opacity -= 25
        @enemy_sprite.zoom_x += 0.08
        @enemy_sprite.zoom_y += 0.08
        @enemy_sprite.opacity += 25
        Graphics.update
      end
      
      if FileTest.exist?(@enemy.cry)
        Audio.se_play(@enemy.cry)
      end
      
      @enemy_sprite.zoom_x = 1
      @enemy_sprite.zoom_y = 1
      @enemy_sprite.opacity = 255
      
      @enemy_status.x = -377
      @enemy_status.visible = true
      
      if @enemy.shiny
        animation = $data_animations[496]
        @enemy_sprite.animation(animation, true)
      end
      
      until @enemy_status.x == 23
        @background.update
        @actor_ground.update
        @enemy_ground.update
        @enemy_status.x += 20
        @enemy_sprite.color.alpha -= 25
        @flash_sprite.opacity -= 25
        @enemy_sprite.update
        Graphics.update
      end
      
      until not(@enemy_sprite.effect?)
        @enemy_sprite.update
        Graphics.update
      end
      
      @enemy_sprite.x = 464
      @enemy_status.x = 23
      @enemy_sprite.color.alpha = 0
      @flash_sprite.opacity = 0
      @ball_sprite.dispose
      Graphics.update
    end
    
    #------------------------------------------------------------  
    # Fin de combat
    #------------------------------------------------------------      
    def end_battle(result = 0)      
      # Reset des variables et effets
      $battle_var.reset
      @actor.skill_effect_reset
      @actor.reset_stat_stage
      @actor.cure_state
      @actor.ability_active = false
      @enemy.skill_effect_reset
      @enemy.reset_stat_stage
      @enemy.cure_state
      @enemy.ability_active = false
      # -----------------------------------
      Audio.me_stop
      wait(10)
      $game_system.bgm_play($game_temp.map_bgm)
      wait(10)
      Graphics.freeze
      # -----------------------------------
      if $game_temp.battle_proc != nil
        $game_temp.battle_proc.call(result)
        $game_temp.battle_proc = nil
      end
      # Défaite
      $scene = Scene_Map.new
    end
    
    def end_battle_flee(expulsion = false)
      $battle_var.result_flee = true
      $game_system.se_play($data_system.escape_se)
      if expulsion
        draw_text(@actor.given_name, "est expulsé du combat!")
        loop do
          if @actor_sprite.x > -160
            @actor_sprite.x -= 20
          end
          Graphics.update
          Input.update
          if @actor_sprite.x <= -160
            wait(40)
            break
          end
        end
      else
        draw_text("Vous prenez la fuite!")
        wait(40)
      end
      end_battle(1)
    end
    
    def fail_flee
      draw_text("Vous ne pouvez pas","vous enfuir!")
      wait(40)
    end
    
    def end_battle_flee_enemy(expulsion = false)
      $battle_var.result_flee = true
      $game_system.se_play($data_system.escape_se)
      if expulsion
        draw_text(@enemy.given_name, "est expulsé du combat!")
      else
        draw_text(@enemy.given_name + " s'échappe!")
      end
      loop do
        if @enemy_sprite.x < 800
          @enemy_sprite.x += 20
        end
        Graphics.update
        Input.update
        if @enemy_sprite.x >= 800
          wait(40)
          break
        end
      end
      end_battle(1)
    end
    
    def end_battle_defeat
      $battle_var.result_defeat = true
      draw_text("Tous vos Pokémons", "ont été vaincus!")
      wait(40)
      $pokemon_party.money /= 2
      if not(@lose)
        if $game_variables[1] == 0
          print("Réglez votre point de retour!")
        else
          $game_map.setup($game_variables[1])
          $game_map.display_x = $game_variables[2]
          $game_map.display_y = $game_variables[3]
          $game_player.moveto($game_variables[2], $game_variables[3])
        end
        $game_temp.common_event_id = 2
      end
      $game_temp.map_bgm = $game_map.bgm
      end_battle(2)
    end
    
    def draw_choice
      @command = Window_Command.new(120, ["OUI", "NON"], $fontsizebig)
      @command.x = 517
      @command.y = 215
      loop do
        Graphics.update
        Input.update
        @command.update
        if Input.trigger?(Input::C) and @command.index == 0
          $game_system.se_play($data_system.decision_se)
          @command.dispose
          @command = nil
          Input.update
          return true
        end
        if Input.trigger?(Input::C) and @command.index == 1
          $game_system.se_play($data_system.decision_se)
          @command.dispose
          @command = nil
          Input.update
          return false
        end
      end
    end
  end
  
  #------------------------------------------------------------  
  # Fenêtre de statut
  #------------------------------------------------------------  
  class Pokemon_Battle_Status < Window_Base
    def initialize(pokemon, enemy, z_level = 15)
      @enemy = enemy # True / False
      if @enemy
        super(23,0,332,116)
      else
        super(311,203,341,140)
      end
      self.contents = Bitmap.new(width - 32, height - 32)
      self.contents.font.name = $fontsmall
      self.contents.font.size = $fontsmallsize
      # self.contents.font.bold = true
      self.opacity = 0
      self.z = z_level
      @pokemon = pokemon
      refresh
    end
    
    def refresh
      self.contents.clear
      level = @pokemon.hp.to_f / @pokemon.maxhp_basis.to_f
      normal_color = Color.new(0,0,0,255)
      if @enemy
        src_rect = Rect.new(0, 0, 300, 84)
        bitmap = RPG::Cache.picture("battle_sprite1.png")
        self.contents.blt(0, 0, bitmap, src_rect, 255)
        draw_hp_bar(69,45, level)
        draw_text(15, 6, 249, $fs, @pokemon.name, 0, normal_color)
        draw_text(15, 6, 249, $fs, "N." + @pokemon.level.to_s, 2, normal_color)
        width_text = self.contents.text_size(@pokemon.name).width + 3
        draw_gender(15 + width_text, 15, @pokemon.gender)
        if $data_pokedex[@pokemon.id][1]
          src_rect = Rect.new(0, 0, 21, 21)
          bitmap = RPG::Cache.picture("ballbattlestatus.png")
          self.contents.blt(27, 45, bitmap, src_rect, 255)
        end
        if @pokemon.status != 0
          string = "stat" + @pokemon.status.to_s + ".png"
          src_rect = Rect.new(0, 0, 60, 24)
          bitmap = RPG::Cache.picture(string)
          self.contents.blt(9, 42, bitmap, src_rect, 255)
        end
      else
        src_rect = Rect.new(0, 0, 309, 108)
if $game_variables[50] == 1
          bitmap = RPG::Cache.picture("battle_sprite3.png")
          self.contents.blt(0, 0, bitmap, src_rect, 255)
          #draw_hp_bar(93,45, level)
          draw_text(39, 58, 249, $fs, Player.name, 0, normal_color)
          #draw_text(39, 6, 249, $fs, "N." + @pokemon.level.to_s, 2, normal_color)
          string = @pokemon.hp < 0 ? 0 : @pokemon.hp
          #draw_text(43, 60, 233, $fs, string.to_s + " / " + @pokemon.maxhp_basis.to_s, 2, normal_color)
          if @pokemon.level < 100
          level = @pokemon.next_exp.to_f / 
            (@pokemon.exp_list[@pokemon.level+1] - @pokemon.exp_list[@pokemon.level]).to_f
          else
          level = 0
          end
          #draw_exp_bar(93, 99, 1.0 - level, 192)
          #width_text = self.contents.text_size(@pokemon.given_name).width + 3
          #draw_gender(39 + width_text, 15, @pokemon.gender)
        else
          bitmap = RPG::Cache.picture("battle_sprite2.png")
          self.contents.blt(0, 0, bitmap, src_rect, 255)
          draw_hp_bar(93,45, level)
          draw_text(39, 6, 249, $fs, @pokemon.given_name, 0, normal_color)
          draw_text(39, 6, 249, $fs, "N." + @pokemon.level.to_s, 2, normal_color)
          string = @pokemon.hp < 0 ? 0 : @pokemon.hp
          draw_text(43, 60, 233, $fs, string.to_s + " / " + @pokemon.maxhp_basis.to_s, 2, normal_color)
          if @pokemon.level < 100
          level = @pokemon.next_exp.to_f / 
            (@pokemon.exp_list[@pokemon.level+1] - @pokemon.exp_list[@pokemon.level]).to_f
          else
          level = 0
          end
          draw_exp_bar(93, 99, 1.0 - level, 192)
          width_text = self.contents.text_size(@pokemon.given_name).width + 3
          draw_gender(39 + width_text, 15, @pokemon.gender)
        end
        if @pokemon.status != 0
          string = "stat" + @pokemon.status.to_s + ".png"
          src_rect = Rect.new(0, 0, 60, 24)
          bitmap = RPG::Cache.picture(string)
          self.contents.blt(42, 66, bitmap, src_rect, 255)
        end
      end
    end
    
    def exp_refresh
      level = @pokemon.next_exp.to_f / 
        (@pokemon.exp_list[@pokemon.level+1] - @pokemon.exp_list[@pokemon.level]).to_f
      draw_exp_bar(93, 99, 1.0 - level, 192)
    end
      
    def damage_refresh(info)
      damage = info[0]
      if damage == 0
        return
      end
      
      for i in 1..damage
        @pokemon.remove_hp(1)
        Graphics.update
        Graphics.update
        if @pokemon.hp >= @pokemon.max_hp or @pokemon.dead?
          break
        end
      end
    end
    
    def dispose
      super
    end
    
    def draw_hp_bar(x, y, level, small = false)
      src_rect = Rect.new(0, 0, 198, 24)
      bitmap = RPG::Cache.picture("hpbar.png")
      if small
        bitmap = RPG::Cache.picture("hpbarsmall.png")
      end
      self.contents.blt(x, y, bitmap, src_rect, 255)
      rect1 = Rect.new(x + 45, y + 6, level*144.to_i, 3)
      rect2 = Rect.new(x + 45, y + 9, level*144.to_i, 6)
      if small
        rect1 = Rect.new(x + 45, y + 6, level*129.to_i, 3)
        rect2 = Rect.new(x + 45, y + 9, level*129.to_i, 6)
      end
      
      if level < 0.1
        color1 = Color.new(170, 70, 70, 255)
        color2 = Color.new(250, 90, 60, 255)
      elsif level >= 0.1 and level < 0.5
        color1 = Color.new(200, 170, 0, 255)
        color2 = Color.new(250, 225, 50, 255)
      else
        color1 = Color.new(90, 210, 125, 255)
        color2 = Color.new(110, 250, 170, 255)
      end
      self.contents.fill_rect(rect1, color1)
      self.contents.fill_rect(rect2, color2)
    end
    
    def draw_exp_bar(x, y, level, width)
      rect1 = Rect.new(x, y, level*192.to_i, 6)
      self.contents.fill_rect(rect1, Color.new(160, 160, 255, 255))
    end
    
    def draw_gender(x, y, gender)
      if gender == 1
        rect = Rect.new(0, 0, 18, 33)
        bitmap = RPG::Cache.picture("Maleb.png")
        self.contents.blt(x, y, bitmap, rect, 255)
      end
      if gender == 2
        rect = Rect.new(0, 0, 18, 33)
        bitmap = RPG::Cache.picture("Femaleb.png")
        self.contents.blt(x, y, bitmap, rect, 255)        
      end
    end
  end
  
  
  #------------------------------------------------------------  
  # Fenêtre de statut de l'équipe
  #------------------------------------------------------------  
  class Pokemon_Battle_Party_Status < Window_Base
    attr_accessor :battle_order
    
    def initialize(party, order, enemy, z_level = 15)
      @enemy = enemy # True / False
      @battle_order = order
      if @enemy
        super(0-16,63-16,315+32,42+32)
      else
        super(325-16, 261-16, 315+32,42+32)
      end
      self.contents = Bitmap.new(width - 32, height - 32)
      self.opacity = 0
      self.z = z_level
      @party = party
      refresh
    end
    
    def refresh
      self.contents.clear
      src_rect = Rect.new(0, 0, 315, 42)
      if @enemy
        bitmap = RPG::Cache.picture("partystatusenemy.png")
      else
        bitmap = RPG::Cache.picture("partystatus.png")
      end
      self.contents.blt(0, 0, bitmap, src_rect, 255)
      
      src_rect = Rect.new(0, 0, 21, 21)
      if @enemy
        ball_x = 231
        coeff = -1
      else
        ball_x = 63
        coeff = 1
      end
      
      for i in 1..@party.size
        bitmap = RPG::Cache.picture("ballpartystatus.png")
        if @party.actors[@battle_order[i-1]].dead?
          bitmap = RPG::Cache.picture("ballpartystatusko.png")
        end
        self.contents.blt(ball_x + coeff*30*(i-1), 3, bitmap, src_rect, 255)
      end
    end
    
    def reset_position
      if @enemy
        self.x = -16
      else
        self.x = 325-16
      end
      refresh
    end
  end
  
  
  
  
  class Pokemon_Battle_Variable

    attr_accessor :weather
    attr_accessor :actor_last_used
    attr_accessor :enemy_last_used
    attr_accessor :battle_order
    attr_accessor :enemy_battle_order
    attr_accessor :in_battle
    attr_accessor :actor_last_taken_damage
    attr_accessor :enemy_last_taken_damage
    attr_accessor :have_fought #liste des pokémons ayant participé par leur index
    attr_accessor :enemy_party
    attr_accessor :action_id
    attr_accessor :window_index
    attr_accessor :result_flee
    attr_accessor :result_win
    attr_accessor :result_defeat
    attr_accessor :last_index
    attr_accessor :round
    attr_accessor :run_count
    attr_accessor :money
    
    # Weather: [ catégorie, nombre de tours ]
    # catégorie: 0: Normal, 1: Pluie, 2: Ensoleillé, 
    #            3: Tempête de Sable, 4: Grêle
    
    def initialize
      @weather = [0, 0]
      @actor_last_used = nil
      @enemy_last_used = nil
      @battle_order = (0..5).to_a
      @enemy_battle_order = (0..5).to_a
      @in_battle = false
      @actor_last_taken_damage = 0
      @enemy_last_taken_damage = 0
      @have_fought = []
      @enemy_party = Pokemon_Party.new
      @action_id = 0
      @window_index = 0
      @result_flee = false
      @result_win = false
      @result_defeat = false
      @last_index = 0
      @round = 0
      @run_count = 0
      @money = 0
    end
    
    def reset
      @weather = [0, 0]
      @actor_last_used = nil
      @enemy_last_used = nil
      @battle_order = (0..5).to_a
      @enemy_battle_order = (0..5).to_a
      @in_battle = false
      @actor_last_taken_damage = 0
      @enemy_last_taken_damage = 0
      @have_fought = []
      @enemy_party = Pokemon_Party.new
      @action_id = 0
      @window_index = 0
      @last_index = 0
      @round = 0
      @run_count = 0
      @money = 0
    end
    
    def reset_weather
      @weather = [0, 0]
    end
    
    def set_rain(duration = -1)
      @weather = [1, duration]
    end
    
    def rain?
      if @weather[0] == 1
        return true
      else
        return false
      end
    end
    
    def set_sunny(duration = -1)
      @weather = [2, duration]
    end
    
    def sunny?
      if @weather[0] == 2
        return true
      else
        return false
      end
    end
    
    def sandstorm?
      if @weather[0] == 3
        return true
      else
        return false
      end
    end
    
    def set_sandstorm(duration = -1)
      @weather = [3, duration]
    end
    
    def hail?
      if @weather[0] == 4
        return true
      else
        return false
      end
    end
    
    def set_hail(duration = -1)
      @weather = [4, duration]
    end
    
    def battle_end?
      if @result_flee or @result_win or @result_defeat
        return true
      else
        return false
      end
    end
    
    def add_money(amount)
      if @money == nil
        @money = 0
      end
      @money += amount
    end
    
  end
  
end

