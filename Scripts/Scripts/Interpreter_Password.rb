        class Interpreter
          #---------------------------------------------------------------------------
          # password
          #   Permet à l'utilisateur de saisir un mot de passe.
          #---------------------------------------------------------------------------
          def password(texte,nbrecar)
            $game_temp.name_max_char = nbrecar
            name_scene = Scene_Password.new
            name_scene.main(texte)
            name_scene = nil
            Graphics.transition
            return $password
          end
        end