#==============================================================================
# ■ Pokemon_Battle_Trainer // IA
# Pokemon Script Project - Krosk 
# 30/10/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------
# Système de Combat - Pokémon Dresseur et Sauvage
#-----------------------------------------------------------------------------
# Gestion de l'IA
#-----------------------------------------------------------------------------

module POKEMON_S
  class Pokemon_Battle_Core
    def ia_rate_calculation(skill, user, target)
      rate = 0.5
      type1 = target.type1
      type2 = target.type2
      # Rate damage
      if skill.power > 0
        # Efficacité
        rate = damage_calculation(skill, user, target, 0, true)
        
        case skill.effect
        # Multi-hit ?
        when 0x1D
          rate *= 1.8
        # Multi-hit 2
        when 0x2C, 0x4D
          rate *= 1.8
        # Multi-hit 3
        when 0x68
          rate *= 2.6
        # Auto-destruction
        when 0x07
          rate /= 4
        # 2 tours strict
        when 0x27, 0x1B, 0x4B, 0x50, 0x91, 0x97, 0x9B1, 0x9B2, 0x9B3, 0x9B0
          rate /= 1.1
        # Effet de statut sur l'adversaire
        when 0x02, 0x04, 0x05, 0x06, 0x1F, 0x24, 0x5C, 0x7D, 0x92, 0x96, 0x98, 0x9E, 0xC8, 0xCA, 0xD1
          rate *= 1.1
        # Effet de stat sur l'adversaire
        when 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4B, 0x4C, 0x4D
          rate *= 1.1
        # Effet de stat sur l'utilisateur
        when 0x8A, 0x8B
          rate *= 1.1
        when 0x8C, 0xCC
          rate *= 1.5
        # Recoil hp ou stat
        when 0x30, 0xC6, 0x8E, 0xB6
          rate /= 1.1
        # Critical
        when 0x2B, 0x4B, 0xC8, 0xD1
          rate *= 1.2
        # Leech
        when 0x08, 0x03
          rate *= 1.1
        end
        
        return rate
        
      else
        # Statut adverse
        rate = 30
        
        case skill.effect
        # Sans immunité (sommeil, amoureux)
        when 0x01, 0x78
          rate *= 1.5
        # Avec immunité par type de l'attaque (paralysie)
        when 0x43
          if element_rate(type1, type2, skill.type) == 0
            rate = 0.5
          else
            rate *= 1.5
          end
        # Immunitées spéciales (type de la défense)
        # Poison
        when 0x42, 0x21
          rate *= 1.5
          if target.type_steel? or target.type_poison?
            rate = 0.5
          end
        # Brule
        when 0xA7
          rate *= 1.5
          if target.type_fire?
            rate = 0.5
          end
        end
        
        # Annulation si le statut n'est pas "normal", et que le 
        #    pokémon va etre soumis à un autre effet de statut
        #    => variation de rate
        if target.status != 0 and rate != 30
          rate = 0.5
        end
        
        case skill.effect
        # Protections
        when 0x6F, 0x7C
          rate *= 2.5
        # Tenacité
        when 0x74 
          if user.hp <= user.max_hp * 10 / 100
            rate *= 5
          end
        # Soin
        when 0x20, 0x25, 0x84, 0x85, 0x86, 0x9D
          if user.hp <= user.max_hp * 20 / 100
            rate *= 15
          end
        # Weather
        when 0x88, 0x89, 0xA4, 
          if $battle_var.weather == 0
            rate *= 4
          end
        # Statut amélioration
        when 0x0A
          if user.atk_stage == 6
            rate = 0.5
          end
          rate *= 1.5*((6-user.atk_stage)/6.0)
        when 0xD4
          if user.atk_stage == 6
            rate = 0.5
          end
          rate *= 3*((6-user.atk_stage)/6.0)
        when 0x0B
          if user.dfe_stage == 6
            rate = 0.5
          end
          rate *= 1.5*((6-user.dfe_stage)/6.0)
        when 0x9C
          if user.dfe_stage == 6
            rate = 0.5
          end
          rate *= 3*((6-user.dfe_stage)/6.0)
        when 0x0D
          if user.ats_stage == 6
            rate = 0.5
          end
          rate *= 1.5*((6-user.ats_stage)/6.0)
        when 0xD3
          if user.ats_stage == 6
            rate = 0.5
          end
          rate *= 3*((6-user.ats_stage)/6.0)
        when 0x10
          if user.eva_stage == 6
            rate = 0.5
          end
          rate *= 2*((6-user.eva_stage)/6.0)
        # Statut decrease
        when 0x12
          if target.atk_stage == -6
            rate = 0.5
          end
          rate *= 1.5*((6+target.atk_stage)/6.0)
        when 0x3A
          if target.atk_stage == -6
            rate = 0.5
          end
          rate *= 3*((6+target.atk_stage)/6.0)
        when 0x13
          if target.dfe_stage == -6
            rate = 0.5
          end
          rate *= 1.5*((6+target.dfe_stage)/6.0)
        when 0x3B
          if target.dfe_stage == -6
            rate = 0.5
          end
          rate *= 3*((6+target.dfe_stage)/6.0)
        when 0x14
          if target.spd_stage == -6
            rate = 0.5
          end
          rate *= 1.5*((6+target.spd_stage)/6.0)
        when 0x3C
          if target.spd_stage == -6
            rate = 0.5
          end
          rate *= 3*((6+target.spd_stage)/6.0)
        when 0x3E
          if target.dfs_stage == -6
            rate = 0.5
          end
          rate *= 1.5*((6+target.dfs_stage)/6.0)
        when 0x17
          if target.acc_stage == -6
            rate = 0.5
          end
          rate *= 3.5*((6+target.acc_stage)/6.0)
        when 0x18
          if target.eva_stage == -6
            rate = 0.5
          end
          rate *= 3.5*((6+target.eva_stage)/6.0)
        when 0xA8
          rate /= 2
        # Aide aux attaques/défense de type
        when 0xAE, 0xD2
          rate *= 2
        # Trempette
        when 0x55
          rate = 1
        end
        # Effet général d'accumulation d'effets
        if target.effect_list.include?(skill.effect)
          rate = 0.5
        end
        if user.effect_list.include?(skill.effect)
          rate = 0.5
        end
      
        return rate
      end
      
    end
    
  end
end