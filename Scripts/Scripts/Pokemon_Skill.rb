#==============================================================================
# ● Base de données - Compétences
# Pokemon Script Project - Krosk 
# 20/07/07
#==============================================================================

module POKEMON_S

  if SKILL_CONVERSION == false # Database par défaut à ne pas modifier
    $data_skills_pokemon = Array.new(355)
    #------------------------------------------------
    # $data_skills_pokemon[id] = [
    #   Nom,
    #   Base_damage,
    #   Type,
    #   Précision (en %),
    #   PPmax
    #   Special (en %),
    #   Special_id (id de fonction)
    #   Cible
    #   Tag contact
    #   Tag priorité (0 à 11), priorité normale = 6
    #   Tag Map_use: faux, ou numéro de l'event comun
    #   Descr   
    #   Animation utilisateur
    #   Animation cible
    # ]
    #
    #
    #  1 Normal  2 Feu  3 Eau 4 Electrique 5 Plante 6 Glace 7 Combat 8 Poison 9 Sol
    #  10 vol 11 psy 12insecte 13 roche 14 spectre 15 dragon 16 acier 17 tenebre
    #------------------------------------------------
    
    $data_skills_pokemon[ 1 ]=["ECRAS'FACE", 40 , 1 , 100 , 35 , 0 , 0x0 , 0 , true , 6 , 0 ,"Ecrase l'ennemi avec les pattes avant ou la queue.", 0 , 1 ]
    $data_skills_pokemon[ 2 ]=["POING-KARATE", 50 , 7 , 100 , 25 , 0 , 0x2B , 0 , true , 6 , 0 ,"Une attaque tranchante à taux de critiques élevé.", 0 , 2 ]
    $data_skills_pokemon[ 3 ]=["TORGNOLES", 15 , 1 , 85 , 10 , 0 , 0x1D , 0 , true , 6 , 0 ,"Gifle rapidement l'ennemi de 2 à 5 fois.", 0 , 3 ]
    $data_skills_pokemon[ 4 ]=["POING COMETE", 18 , 1 , 85 , 15 , 0 , 0x1D , 0 , true , 6 , 0 ,"Frappe rapidement l'ennemi de 2 à 5 fois.", 0 , 4 ]
    $data_skills_pokemon[ 5 ]=["ULTIMAPOING", 80 , 1 , 85 , 20 , 0 , 0x0 , 0 , true , 6 , 0 ,"Un coup de poing d'une puissance incroyable.", 0 , 5 ]
    $data_skills_pokemon[ 6 ]=["JACKPOT", 40 , 1 , 100 , 20 , 100 , 0x22 , 0 , false , 6 , 0 ,"Lance des pièces. L'argent est récupéré plus tard.", 0 , 6 ]
    $data_skills_pokemon[ 7 ]=["POING DE FEU", 75 , 2 , 100 , 15 , 10 , 0x4 , 0 , true , 6 , 0 ,"Un poing de feu pouvant brûler l'ennemi.", 0 , 7 ]
    $data_skills_pokemon[ 8 ]=["POINGLACE", 75 , 6 , 100 , 15 , 10 , 0x5 , 0 , true , 6 , 0 ,"Un poing de glace pouvant geler l'ennemi.", 0 , 8 ]
    $data_skills_pokemon[ 9 ]=["POING-ECLAIR", 75 , 4 , 100 , 15 , 10 , 0x6 , 0 , true , 6 , 0 ,"Un poing électrique pouvant paralyser l'ennemi.", 0 , 9 ]
    $data_skills_pokemon[ 10 ]=["GRIFFE", 40 , 1 , 100 , 35 , 0 , 0x0 , 0 , true , 6 , 0 ,"Lacère l'ennemi avec des griffes acérées.", 0 , 10 ]
    $data_skills_pokemon[ 11 ]=["FORCE POIGNE", 55 , 1 , 100 , 30 , 0 , 0x0 , 0 , true , 6 , 0 ,"Empoigne l'ennemi avec de puissantes pinces.", 0 , 11 ]
    $data_skills_pokemon[ 12 ]=["GUILLOTINE", 1 , 1 , 30 , 5 , 0 , 0x26 , 0 , true , 6 , 0 ,"Un puissant coup de pince qui peut mettre K.O.", 0 , 12 ]
    $data_skills_pokemon[ 13 ]=["COUPE-VENT", 80 , 1 , 100 , 10 , 0 , 0x27 , 8 , false , 6 , 0 ,"Une attaque en 2 tours qui frappe au 2e tour.", 0 , 13 ]
    $data_skills_pokemon[ 14 ]=["DANSE-LAMES", 0 , 1 , 0 , 30 , 0 , 0x32 , 10 , false , 6 , 0 ,"Une danse de combat pour booster l'Attaque.", 0 , 14 ]
    $data_skills_pokemon[ 15 ]=["COUPE", 50 , 1 , 95 , 30 , 0 , 0x0 , 0 , true , 6 , 21 ,"Coupe l'ennemi avec des lames, des griffes, etc.", 0 , 15 ]
    $data_skills_pokemon[ 16 ]=["TORNADE", 40 , 10 , 100 , 35 , 0 , 0x95 , 0 , false , 6 , 0 ,"Envoie une rafale de vent en battant des ailes.", 0 , 16 ]
    $data_skills_pokemon[ 17 ]=["CRU-AILE", 60 , 10 , 100 , 35 , 0 , 0x0 , 0 , true , 6 , 0 ,"Frappe l'ennemi en déployant les ailes.", 0 , 17 ]
    $data_skills_pokemon[ 18 ]=["CYCLONE", 0 , 1 , 100 , 20 , 0 , 0x1C , 0 , false , 0 , 0 ,"Envoie une rafale de vent et met fin au combat.", 0 , 18 ]
    $data_skills_pokemon[ 19 ]=["VOL", 70 , 10 , 95 , 15 , 0 , 0x9B1 , 0 , true , 6 , 22 ,"S'envole au premier tour et frappe au second tour.", 0 , 19 ]
    $data_skills_pokemon[ 20 ]=["ETREINTE", 15 , 1 , 75 , 20 , 100 , 0x2A , 0 , true , 6 , 0 ,"Ligote l'ennemi et l'écrase pendant 2 à 5 tours.", 0 , 20 ]
    $data_skills_pokemon[ 21 ]=["SOUPLESSE", 80 , 1 , 75 , 20 , 0 , 0x0 , 0 , true , 6 , 0 ,"Fouette l'ennemi avec sa queue, une liane, etc.", 0 , 21 ]
    $data_skills_pokemon[ 22 ]=["FOUET LIANES", 35 , 5 , 100 , 10 , 0 , 0x0 , 0 , true , 6 , 0 ,"Fouette l'ennemi avec de fines lianes.", 0 , 22 ]
    $data_skills_pokemon[ 23 ]=["ECRASEMENT", 65 , 1 , 100 , 20 , 30 , 0x96 , 0 , true , 6 , 0 ,"Ecrase avec un énorme pied. Peut apeurer l'ennemi.", 0 , 23 ]
    $data_skills_pokemon[ 24 ]=["DOUBLE PIED", 30 , 7 , 100 , 30 , 0 , 0x2C , 0 , true , 6 , 0 ,"Un double coup de pied qui frappe l'ennemi deux fois.", 0 , 24 ]
    $data_skills_pokemon[ 25 ]=["ULTIMAWASHI", 120 , 1 , 75 , 5 , 0 , 0x0 , 0 , true , 6 , 0 ,"Un coup de pied super puissant et intense.", 0 , 25 ]
    $data_skills_pokemon[ 26 ]=["PIED SAUTE", 70 , 7 , 95 , 25 , 0 , 0x2D , 0 , true , 6 , 0 ,"Un coup de pied sauté. Peut blesser le lanceur si raté.", 0 , 26 ]
    $data_skills_pokemon[ 27 ]=["MAWASHI GERI", 60 , 7 , 85 , 15 , 30 , 0x1F , 0 , true , 6 , 0 ,"Un coup de pied tournoyant et extrêmement rapide.", 0 , 27 ]
    $data_skills_pokemon[ 28 ]=["JET DE SABLE", 0 , 9 , 100 , 15 , 0 , 0x17 , 0 , false , 6 , 0 ,"Lance du sable et baisse la précision de l'ennemi.", 0 , 28 ]
    $data_skills_pokemon[ 29 ]=["COUP D'BOULE", 70 , 1 , 100 , 15 , 30 , 0x1F , 0 , true , 6 , 0 ,"Une charge qui peut apeurer l'ennemi.", 0 , 29 ]
    $data_skills_pokemon[ 30 ]=["KOUD'KORNE", 65 , 1 , 100 , 25 , 0 , 0x0 , 0 , true , 6 , 0 ,"Frappe l'ennemi d'un coup de corne pointue.", 0 , 30 ]
    $data_skills_pokemon[ 31 ]=["FURIE", 15 , 1 , 85 , 20 , 0 , 0x1D , 0 , true , 6 , 0 ,"Frappe l'ennemi 2 à 5 fois avec des cornes, etc.", 0 , 31 ]
    $data_skills_pokemon[ 32 ]=["EMPAL'KORNE", 1 , 1 , 30 , 5 , 0 , 0x26 , 0 , true , 6 , 0 ,"Empale l'ennemi d'un coup de corne. Met K.O. en un coup.", 0 , 32 ]
    $data_skills_pokemon[ 33 ]=["CHARGE", 35 , 1 , 95 , 35 , 0 , 0x0 , 0 , true , 6 , 0 ,"Charge l'ennemi avec un violent plaquage.", 0 , 33 ]
    $data_skills_pokemon[ 34 ]=["PLAQUAGE", 85 , 1 , 100 , 15 , 30 , 0x6 , 0 , true , 6 , 0 ,"Une attaque en charge. Peut paralyser l'ennemi.", 0 , 34 ]
    $data_skills_pokemon[ 35 ]=["LIGOTAGE", 15 , 1 , 85 , 20 , 100 , 0x2A , 0 , true , 6 , 0 ,"Ligote l'ennemi et l'étouffe 2 à 5 fois avec des lianes.", 0 , 35 ]
    $data_skills_pokemon[ 36 ]=["BELIER", 90 , 1 , 85 , 20 , 0 , 0x30 , 0 , true , 6 , 0 ,"Une charge violente qui blesse aussi le lanceur.", 0 , 36 ]
    $data_skills_pokemon[ 37 ]=["MANIA", 90 , 1 , 100 , 20 , 100 , 0x1B , 4 , true , 6 , 0 ,"Une attaque furieuse de 2 à 3 tours. Lanceur confus.", 0 , 37 ]
    $data_skills_pokemon[ 38 ]=["DAMOCLES", 120 , 1 , 100 , 15 , 0 , 0xC6 , 0 , true , 6 , 0 ,"Une dangereuse charge qui blesse aussi le lanceur.", 0 , 38 ]
    $data_skills_pokemon[ 39 ]=["MIMI-QUEUE", 0 , 1 , 100 , 30 , 0 , 0x13 , 8 , false , 6 , 0 ,"Remue la queue pour baisser la Défense de l'ennemi.", 0 , 39 ]
    $data_skills_pokemon[ 40 ]=["DARD-VENIN", 15 , 8 , 100 , 35 , 30 , 0x2 , 0 , false , 6 , 0 ,"Une attaque de dards, etc., pouvant empoisonner.", 0 , 40 ]
    $data_skills_pokemon[ 41 ]=["DOUBLE-DARD", 25 , 12 , 100 , 20 , 20 , 0x4D , 0 , false , 6 , 0 ,"Les dards des pattes avant frappent deux fois.", 0 , 41 ]
    $data_skills_pokemon[ 42 ]=["DARD-NUEE", 14 , 12 , 85 , 20 , 0 , 0x1D , 0 , false , 6 , 0 ,"Envoie des dards qui frappent 2 à 5 fois.", 0 , 42 ]
    $data_skills_pokemon[ 43 ]=["GROZ'YEUX", 0 , 1 , 100 , 30 , 0 , 0x13 , 8 , false , 6 , 0 ,"Lance un regard noir et baisse la Défense.", 0 , 43 ]
    $data_skills_pokemon[ 44 ]=["MORSURE", 60 , 17 , 100 , 25 , 30 , 0x1F , 0 , true , 6 , 0 ,"Mord méchamment l'ennemi et peut l'apeurer.", 0 , 44 ]
    $data_skills_pokemon[ 45 ]=["RUGISSEMENT", 0 , 1 , 100 , 40 , 0 , 0x12 , 8 , false , 6 , 0 ,"Rugit gentiment et baisse l'Attaque de l'ennemi.", 0 , 45 ]
    $data_skills_pokemon[ 46 ]=["HURLEMENT", 0 , 1 , 100 , 20 , 0 , 0x1C , 0 , false , 0 , 0 ,"Fait fuir l'ennemi pour mettre fin au combat.", 0 , 46 ]
    $data_skills_pokemon[ 47 ]=["BERCEUSE", 0 , 1 , 55 , 15 , 0 , 0x1 , 0 , false , 6 , 0 ,"Berceuse plongeant l'ennemi dans un profond sommeil.", 0 , 47 ]
    $data_skills_pokemon[ 48 ]=["ULTRASON", 0 , 1 , 55 , 20 , 0 , 0x31 , 0 , false , 6 , 0 ,"Etranges ondes sonores pouvant rendre confus.", 0 , 48 ]
    $data_skills_pokemon[ 49 ]=["SONICBOOM", 1 , 1 , 90 , 20 , 0 , 0x82 , 0 , false , 6 , 0 ,"Envoie des ondes de choc infligeant 20 PV de dégâts.", 0 , 49 ]
    $data_skills_pokemon[ 50 ]=["ENTRAVE", 0 , 1 , 55 , 20 , 0 , 0x56 , 0 , false , 6 , 0 ,"Empêche l'utilisation d'une des attaques de l'ennemi.", 0 , 50 ]
    $data_skills_pokemon[ 51 ]=["ACIDE", 40 , 8 , 100 , 30 , 10 , 0x45 , 8 , false , 6 , 0 ,"Pulvérise un acide. Peut baisser la Défense ennemie.", 0 , 51 ]
    $data_skills_pokemon[ 52 ]=["FLAMMECHE", 40 , 2 , 100 , 25 , 10 , 0x4 , 0 , false , 6 , 0 ,"Une faible attaque de feu pouvant brûler l'ennemi.", 0 , 52 ]
    $data_skills_pokemon[ 53 ]=["LANCE-FLAMME", 95 , 2 , 100 , 15 , 10 , 0x4 , 0 , false , 6 , 0 ,"Une puissante attaque de feu pouvant brûler l'ennemi.", 0 , 53 ]
    $data_skills_pokemon[ 54 ]=["BRUME", 0 , 6 , 0 , 30 , 0 , 0x2E , 10 , false , 6 , 0 ,"Crée une brume stoppant la réduction des capacités.", 0 , 54 ]
    $data_skills_pokemon[ 55 ]=["PISTOLET A O", 40 , 3 , 100 , 25 , 0 , 0x0 , 0 , false , 6 , 0 ,"Attaque l'ennemi en projetant de l'eau.", 0 , 55 ]
    $data_skills_pokemon[ 56 ]=["HYDROCANON", 120 , 3 , 80 , 5 , 0 , 0x0 , 0 , false , 6 , 0 ,"Envoie un puissant jet d'eau pour frapper l'ennemi.", 0 , 56 ]
    $data_skills_pokemon[ 57 ]=["SURF", 95 , 3 , 100 , 15 , 0 , 0x0 , 8 , false , 6 , 23 ,"Crée une énorme vague qui s'écrase sur l'ennemi.", 0 , 57 ]
    $data_skills_pokemon[ 58 ]=["LASER GLACE", 95 , 6 , 100 , 10 , 10 , 0x5 , 0 , false , 6 , 0 ,"Envoie un rayon de glace pouvant geler l'ennemi.", 0 , 58 ]
    $data_skills_pokemon[ 59 ]=["BLIZZARD", 120 , 6 , 70 , 5 , 10 , 0x5 , 8 , false , 6 , 0 ,"Déclenche une tempête pouvant geler l'ennemi.", 0 , 59 ]
    $data_skills_pokemon[ 60 ]=["RAFALE PSY", 65 , 11 , 100 , 20 , 10 , 0x4C , 0 , false , 6 , 0 ,"Etrange rayon pouvant rendre l'ennemi confus.", 0 , 60 ]
    $data_skills_pokemon[ 61 ]=["BULLES D'O", 65 , 3 , 100 , 20 , 10 , 0x46 , 0 , false , 6 , 0 ,"Envoie des bulles pouvant baisser la Vitesse.", 0 , 61 ]
    $data_skills_pokemon[ 62 ]=["ONDE BOREALE", 65 , 6 , 100 , 20 , 10 , 0x44 , 0 , false , 6 , 0 ,"Envoie un rayon arc-en-ciel pouvant baisser l'Attaque.", 0 , 62 ]
    $data_skills_pokemon[ 63 ]=["ULTRALASER", 150 , 1 , 90 , 5 , 0 , 0x50 , 0 , false , 6 , 0 ,"Puissant, mais immobilise le lanceur pour un tour.", 0 , 63 ]
    $data_skills_pokemon[ 64 ]=["PICPIC", 35 , 10 , 100 , 35 , 0 , 0x0 , 0 , true , 6 , 0 ,"Attaque l'ennemi avec un bec pointu, etc.", 0 , 64 ]
    $data_skills_pokemon[ 65 ]=["BEC VRILLE", 80 , 10 , 100 , 20 , 0 , 0x0 , 0 , true , 6 , 0 ,"Une attaque utilisant le bec comme une perceuse.", 0 , 65 ]
    $data_skills_pokemon[ 66 ]=["SACRIFICE", 80 , 7 , 80 , 25 , 0 , 0x30 , 0 , true , 6 , 0 ,"Une charge violente qui blesse aussi le lanceur.", 0 , 66 ]
    $data_skills_pokemon[ 67 ]=["BALAYAGE", 1 , 7 , 100 , 20 , 0 , 0xC4 , 0 , true , 6 , 0 ,"Un coup de pied faisant plus mal aux ennemis lourds.", 0 , 67 ]
    $data_skills_pokemon[ 68 ]=["RIPOSTE", 1 , 7 , 100 , 20 , 0 , 0x59 , 0 , true , 1 , 0 ,"Renvoie toute attaque physique, 2 fois plus fort.", 0 , 68 ]
    $data_skills_pokemon[ 69 ]=["FRAPPE ATLAS", 1 , 7 , 100 , 20 , 0 , 0x57 , 0 , true , 6 , 0 ,"Inflige des dégâts selon le niveau du lanceur.", 0 , 69 ]
    $data_skills_pokemon[ 70 ]=["FORCE", 80 , 1 , 100 , 15 , 0 , 0x0 , 0 , true , 6 , 24 ,"Accumule de la puissance puis frappe l'ennemi.", 0 , 70 ]
    $data_skills_pokemon[ 71 ]=["VOL-VIE", 20 , 5 , 100 , 20 , 0 , 0x3 , 0 , false , 6 , 0 ,"Une attaque absorbant la moitié des dégâts infligés.", 0 , 71 ]
    $data_skills_pokemon[ 72 ]=["MEGA-SANGSUE", 40 , 5 , 100 , 10 , 0 , 0x3 , 0 , false , 6 , 0 ,"Une attaque absorbant la moitié des dégâts infligés.", 0 , 72 ]
    $data_skills_pokemon[ 73 ]=["VAMPIGRAINE", 0 , 5 , 90 , 10 , 0 , 0x54 , 0 , false , 6 , 0 ,"Plante des graines pour voler des PV à chaque tour.", 0 , 73 ]
    $data_skills_pokemon[ 74 ]=["CROISSANCE", 0 , 1 , 0 , 40 , 0 , 0xD , 10 , false , 6 , 0 ,"Développe le corps et augmente l'Atq. Spé.", 0 , 74 ]
    $data_skills_pokemon[ 75 ]=["TRANCH'HERBE", 55 , 5 , 95 , 25 , 0 , 0x2B , 8 , false , 6 , 0 ,"Tranche avec des feuilles. Taux de critiques élevé.", 0 , 75 ]
    $data_skills_pokemon[ 76 ]=["LANCE-SOLEIL", 120 , 5 , 100 , 10 , 0 , 0x97 , 0 , false , 6 , 0 ,"Absorbe la lumière et attaque le tour suivant.", 0 , 76 ]
    $data_skills_pokemon[ 77 ]=["POUDRE TOXIK", 0 , 8 , 75 , 35 , 0 , 0x42 , 0 , false , 6 , 0 ,"Répand une poudre toxique pouvant empoisonner.", 0 , 77 ]
    $data_skills_pokemon[ 78 ]=["PARA-SPORE", 0 , 5 , 75 , 30 , 0 , 0x43 , 0 , false , 6 , 0 ,"Répand une poudre pouvant paralyser l'ennemi.", 0 , 78 ]
    $data_skills_pokemon[ 79 ]=["POUDRE DODO", 0 , 5 , 75 , 15 , 0 , 0x1 , 0 , false , 6 , 0 ,"Répand une poudre pouvant endormir l'ennemi.", 0 , 79 ]
    $data_skills_pokemon[ 80 ]=["DANSE-FLEUR", 70 , 5 , 100 , 20 , 100 , 0x1B , 4 , true , 6 , 0 ,"Une attaque furieuse de 2 à 3 tours. Lanceur confus.", 0 , 80 ]
    $data_skills_pokemon[ 81 ]=["SECRETION", 0 , 12 , 95 , 40 , 0 , 0x14 , 8 , false , 6 , 0 ,"Ligote l'ennemi afin de réduire sa Vitesse.", 0 , 81 ]
    $data_skills_pokemon[ 82 ]=["DRACO-RAGE", 1 , 15 , 100 , 10 , 0 , 0x29 , 0 , false , 6 , 0 ,"Envoie des ondes de choc infligeant 40 PV de dégâts.", 0 , 82 ]
    $data_skills_pokemon[ 83 ]=["DANSEFLAMME", 15 , 2 , 70 , 15 , 100 , 0x2A , 0 , false , 6 , 0 ,"Encercle l'ennemi de flammes pendant 2 à 5 tours.", 0 , 83 ]
    $data_skills_pokemon[ 84 ]=["ECLAIR", 40 , 4 , 100 , 30 , 10 , 0x6 , 0 , false , 6 , 0 ,"Une attaque électrique pouvant paralyser l'ennemi.", 0 , 84 ]
    $data_skills_pokemon[ 85 ]=["TONNERRE", 95 , 4 , 100 , 15 , 10 , 0x6 , 0 , false , 6 , 0 ,"Forte attaque électrique pouvant paralyser l'ennemi.", 0 , 85 ]
    $data_skills_pokemon[ 86 ]=["CAGE-ECLAIR", 0 , 4 , 100 , 20 , 0 , 0x43 , 0 , false , 6 , 0 ,"Un faible choc électrique qui paralyse l'ennemi.", 0 , 86 ]
    $data_skills_pokemon[ 87 ]=["FATAL-FOUDRE", 120 , 4 , 70 , 10 , 30 , 0x98 , 0 , false , 6 , 0 ,"Une attaque foudroyante pouvant paralyser l'ennemi.", 0 , 87 ]
    $data_skills_pokemon[ 88 ]=["JET-PIERRES", 50 , 13 , 90 , 15 , 0 , 0x0 , 0 , false , 6 , 0 ,"Envoie des cailloux pour frapper l'ennemi.", 0 , 88 ]
    $data_skills_pokemon[ 89 ]=["SEISME", 100 , 9 , 100 , 10 , 0 , 0x93 , 20 , false , 6 , 0 ,"Tremblement de terre sans effet sur les adversaires volants.", 0 , 89 ]
    $data_skills_pokemon[ 90 ]=["ABIME", 1 , 9 , 30 , 5 , 0 , 0x26 , 0 , false , 6 , 0 ,"Fait tomber l'ennemi dans une crevasse et le met K.O.", 0 , 90 ]
    $data_skills_pokemon[ 91 ]=["TUNNEL", 60 , 9 , 100 , 10 , 0 , 0x9B0 , 0 , true , 6 , 29 ,"Creuse sous terre au 1er tour et frappe au 2e.", 0 , 91 ]
    $data_skills_pokemon[ 92 ]=["TOXIK", 0 , 8 , 85 , 10 , 100 , 0x21 , 0 , false , 6 , 0 ,"Empoisonne l'ennemi avec une puissante toxine.", 0 , 92 ]
    $data_skills_pokemon[ 93 ]=["CHOC MENTAL", 50 , 11 , 100 , 25 , 10 , 0x4C , 0 , false , 6 , 0 ,"Une attaque Psy pouvant rendre l'ennemi confus.", 0 , 93 ]
    $data_skills_pokemon[ 94 ]=["PSYKO", 90 , 11 , 100 , 10 , 10 , 0x48 , 0 , false , 6 , 0 ,"Une puissante attaque Psy pouvant baisser la Déf. Spé.", 0 , 94 ]
    $data_skills_pokemon[ 95 ]=["HYPNOSE", 0 , 11 , 60 , 20 , 0 , 0x1 , 0 , false , 6 , 0 ,"Une attaque hypnotique pouvant endormir l'ennemi.", 0 , 95 ]
    $data_skills_pokemon[ 96 ]=["YOGA", 0 , 11 , 0 , 40 , 0 , 0xA , 10 , false , 6 , 0 ,"Méditation pacifique pour augmenter l'Attaque.", 0 , 96 ]
    $data_skills_pokemon[ 97 ]=["HATE", 0 , 11 , 0 , 30 , 0 , 0x34 , 10 , false , 6 , 0 ,"Relaxation du corps pour booster la Vitesse.", 0 , 97 ]
    $data_skills_pokemon[ 98 ]=["VIVE-ATTAQUE", 40 , 1 , 100 , 30 , 0 , 0x67 , 0 , true , 7 , 0 ,"Attaque fulgurante qui permet de frapper d'abord.", 0 , 98 ]
    $data_skills_pokemon[ 99 ]=["FRENESIE", 20 , 1 , 100 , 20 , 0 , 0x51 , 0 , true , 6 , 0 ,"Augmente l'Attaque du lanceur à chaque coup reçu.", 0 , 99 ]
    $data_skills_pokemon[ 100 ]=["TELEPORT", 0 , 11 , 0 , 20 , 0 , 0x99 , 10 , false , 6 , 0 ,"Attaque Psy permettant de prendre aussitôt la fuite.", 0 , 100 ]
    $data_skills_pokemon[ 101 ]=["TENEBRES", 1 , 14 , 100 , 15 , 0 , 0x57 , 0 , false , 6 , 0 ,"Inflige des dégâts selon le niveau du lanceur.", 0 , 101 ]
    $data_skills_pokemon[ 102 ]=["COPIE", 0 , 1 , 100 , 10 , 0 , 0x52 , 0 , false , 6 , 0 ,"Imite une des attaques utilisées par l'ennemi.", 0 , 102 ]
    $data_skills_pokemon[ 103 ]=["GRINCEMENT", 0 , 1 , 85 , 40 , 0 , 0x3B , 0 , false , 6 , 0 ,"Cri strident qui baisse la Défense ennemie.", 0 , 103 ]
    $data_skills_pokemon[ 104 ]=["REFLET", 0 , 1 , 0 , 15 , 0 , 0x10 , 10 , false , 6 , 0 ,"Crée des images-miroirs pour augmenter l'esquive.", 0 , 104 ]
    $data_skills_pokemon[ 105 ]=["SOIN", 0 , 1 , 0 , 20 , 0 , 0x20 , 10 , false , 6 , 0 ,"Regagne jusqu'à la moitié des PV max du lanceur.", 0 , 105 ]
    $data_skills_pokemon[ 106 ]=["ARMURE", 0 , 1 , 0 , 30 , 0 , 0x0B , 10 , false , 6 , 0 ,"Contracte les muscles pour augmenter la Défense.", 0 , 106 ]
    $data_skills_pokemon[ 107 ]=["LILLIPUT", 0 , 1 , 0 , 20 , 0 , 0x6C , 10 , false , 6 , 0 ,"Rapetisse le lanceur pour augmenter l'esquive.", 0 , 107 ]
    $data_skills_pokemon[ 108 ]=["BROUILLARD", 0 , 1 , 100 , 20 , 0 , 0x17 , 0 , false , 6 , 0 ,"Baisse la précision ennemie utilisant fumée, encre, etc.", 0 , 108 ]
    $data_skills_pokemon[ 109 ]=["ONDE FOLIE", 0 , 14 , 100 , 10 , 0 , 0x31 , 0 , false , 6 , 0 ,"Un horrible rayon qui rend l'ennemi confus.", 0 , 109 ]
    $data_skills_pokemon[ 110 ]=["REPLI", 0 , 3 , 0 , 40 , 0 , 0x0B , 10 , false , 6 , 0 ,"Se recroqueville dans sa carapace. Monte la Défense.", 0 , 110 ]
    $data_skills_pokemon[ 111 ]=["BOUL'ARMURE", 0 , 1 , 0 , 40 , 0 , 0x9C , 10 , false , 6 , 0 ,"S'enroule, cache ses points faibles. Monte la Défense.", 0 , 111 ]
    $data_skills_pokemon[ 112 ]=["BOUCLIER", 0 , 11 , 0 , 30 , 0 , 0x33 , 10 , false , 6 , 0 ,"Crée une barrière pour booster la Défense.", 0 , 112 ]
    $data_skills_pokemon[ 113 ]=["MUR LUMIERE", 0 , 11 , 0 , 30 , 0 , 0x23 , 10 , false , 6 , 0 ,"Crée un mur lumineux et réduit les dégâts d'Atq. Spé.", 0 , 113 ]
    $data_skills_pokemon[ 114 ]=["BUEE NOIRE", 0 , 6 , 0 , 30 , 0 , 0x19 , 10 , false , 6 , 0 ,"Crée un nuage noir annulant tout changement d'état.", 0 , 114 ]
    $data_skills_pokemon[ 115 ]=["PROTECTION", 0 , 11 , 0 , 20 , 0 , 0x41 , 10 , false , 6 , 0 ,"Crée un mur lumineux. Affaiblit att. physiques.", 0 , 115 ]
    $data_skills_pokemon[ 116 ]=["PUISSANCE", 0 , 1 , 0 , 30 , 0 , 0x2F , 10 , false , 6 , 0 ,"Concentration permettant un taux de critiques élevé.", 0 , 116 ]
    $data_skills_pokemon[ 117 ]=["PATIENCE", 1 , 1 , 100 , 10 , 0 , 0x1A , 10 , true , 6 , 0 ,"Encaisse les attaques sur 2 tours et renvoie le double.", 0 , 117 ]
    $data_skills_pokemon[ 118 ]=["METRONOME", 0 , 1 , 0 , 10 , 0 , 0x53 , 0 , false , 6 , 0 ,"Remue un doigt et lance une attaque PoKéMoN au hasard.", 0 , 118 ]
    $data_skills_pokemon[ 119 ]=["MIMIQUE", 0 , 10 , 0 , 20 , 0 , 0x9 , 0 , false , 6 , 0 ,"Contre l'attaque de l'ennemi avec la même attaque.", 0 , 119 ]
    $data_skills_pokemon[ 120 ]=["DESTRUCTION", 200 , 1 , 100 , 5 , 0 , 0x7 , 20 , false , 6 , 0 ,"Inflige de sérieux dégâts, mais met le lanceur K.O.", 0 , 120 ]
    $data_skills_pokemon[ 121 ]=["BOMB'OEUF", 100 , 1 , 75 , 10 , 0 , 0x0 , 0 , false , 6 , 0 ,"Envoie violemment un oeuf en direction de l'ennemi.", 0 , 121 ]
    $data_skills_pokemon[ 122 ]=["LECHOUILLE", 20 , 14 , 100 , 30 , 30 , 0x6 , 0 , true , 6 , 0 ,"Blesse avec un long coup de langue. Peut paralyser.", 0 , 122 ]
    $data_skills_pokemon[ 123 ]=["PUREDPOIS", 20 , 8 , 70 , 20 , 40 , 0x2 , 0 , false , 6 , 0 ,"Un gaz nocif pouvant empoisonner l'ennemi.", 0 , 123 ]
    $data_skills_pokemon[ 124 ]=["DETRITUS", 65 , 8 , 100 , 20 , 30 , 0x2 , 0 , false , 6 , 0 ,"Blesse en envoyant des détritus. Peut empoisonner.", 0 , 124 ]
    $data_skills_pokemon[ 125 ]=["MASSD'OS", 65 , 9 , 85 , 20 , 10 , 0x1F , 0 , false , 6 , 0 ,"Cogne l'ennemi avec un os. Peut apeurer l'ennemi.", 0 , 125 ]
    $data_skills_pokemon[ 126 ]=["DEFLAGRATION", 120 , 2 , 85 , 5 , 10 , 0x4 , 0 , false , 6 , 0 ,"Une explosion qui carbonise tout. Peut brûler l'ennemi.", 0 , 126 ]
    $data_skills_pokemon[ 127 ]=["CASCADE", 80 , 3 , 100 , 15 , 0 , 0x0 , 0 , true , 6 , 27 ,"Charge rapide permettant de franchir des cascades.", 0 , 127 ]
    $data_skills_pokemon[ 128 ]=["CLAQUOIR", 35 , 3 , 75 , 10 , 100 , 0x2A , 0 , true , 6 , 0 ,"Piège l'ennemi et l'écrase pendant 2 à 5 tours.", 0 , 128 ]
    $data_skills_pokemon[ 129 ]=["METEORES", 60 , 1 , 0 , 20 , 0 , 0x11 , 8 , false , 6 , 0 ,"Envoie des rayons en forme d'étoile. Touche toujours.", 0 , 129 ]
    $data_skills_pokemon[ 130 ]=["COUD'KRANE", 100 , 1 , 100 , 15 , 0 , 0x91 , 0 , true , 6 , 0 ,"Rentre la tête et attaque au tour suivant.", 0 , 130 ]
    $data_skills_pokemon[ 131 ]=["PICANON", 20 , 1 , 100 , 15 , 0 , 0x1D , 0 , false , 6 , 0 ,"Lance des pointes qui frappent 2 à 5 fois.", 0 , 131 ]
    $data_skills_pokemon[ 132 ]=["CONSTRICTION", 10 , 1 , 100 , 35 , 10 , 0x46 , 0 , true , 6 , 0 ,"Etrangle pour blesser. Peut baisser la Vitesse.", 0 , 132 ]
    $data_skills_pokemon[ 133 ]=["AMNESIE", 0 , 11 , 0 , 20 , 0 , 0x36 , 10 , false , 6 , 0 ,"Oublie quelque chose et booste la Déf. Spé.", 0 , 133 ]
    $data_skills_pokemon[ 134 ]=["TELEKINESIE", 0 , 11 , 80 , 15 , 0 , 0x17 , 0 , false , 6 , 0 ,"Distrait l'ennemi. Peut baisser la précision.", 0 , 134 ]
    $data_skills_pokemon[ 135 ]=["E-COQUE", 0 , 1 , 100 , 10 , 0 , 0x9D , 10 , false , 6 , 0 ,"Regagne jusqu'à la moitié des PV max du lanceur.", 0 , 135 ]
    $data_skills_pokemon[ 136 ]=["PIED VOLTIGE", 85 , 7 , 90 , 20 , 0 , 0x2D , 0 , true , 6 , 0 ,"Coup de pied sauté. S'il est raté, le lanceur se blesse.", 0 , 136 ]
    $data_skills_pokemon[ 137 ]=["INTIMIDATION", 0 , 1 , 75 , 30 , 0 , 0x43 , 0 , false , 6 , 0 ,"Intimide l'ennemi et le paralyse de terreur.", 0 , 137 ]
    $data_skills_pokemon[ 138 ]=["DEVOREVE", 100 , 11 , 100 , 15 , 0 , 0x8 , 0 , false , 6 , 0 ,"Aspire la moitié des dégâts infligés à l'ennemi endormi.", 0 , 138 ]
    $data_skills_pokemon[ 139 ]=["GAZ TOXIK", 0 , 8 , 55 , 40 , 0 , 0x42 , 0 , false , 6 , 0 ,"Enveloppe l'ennemi d'un gaz toxique. Peut empoisonner.", 0 , 139 ]
    $data_skills_pokemon[ 140 ]=["PILONNAGE", 15 , 1 , 85 , 20 , 0 , 0x1D , 0 , false , 6 , 0 ,"Projette 2 à 5 fois des objets vers l'ennemi.", 0 , 140 ]
    $data_skills_pokemon[ 141 ]=["VAMPIRISME", 20 , 12 , 100 , 15 , 0 , 0x3 , 0 , true , 6 , 0 ,"Une attaque qui aspire la moitié des dégâts infligés.", 0 , 141 ]
    $data_skills_pokemon[ 142 ]=["GROBISOU", 0 , 1 , 75 , 10 , 0 , 0x1 , 0 , false , 6 , 0 ,"Demande un bisou grimaçant. Peut endormir.", 0 , 142 ]
    $data_skills_pokemon[ 143 ]=["PIQUE", 140 , 10 , 90 , 5 , 30 , 0x4B , 0 , false , 6 , 0 ,"Cherche les points faibles et frappe au tour suivant.", 0 , 143 ]
    $data_skills_pokemon[ 144 ]=["MORPHING", 0 , 1 , 0 , 10 , 0 , 0x39 , 0 , false , 6 , 0 ,"Modifie ses cellules pour copier l'ennemi.", 0 , 144 ]
    $data_skills_pokemon[ 145 ]=["ECUME", 20 , 3 , 100 , 30 , 10 , 0x46 , 8 , false , 6 , 0 ,"Une attaque de bulles pouvant baisser la Vitesse.", 0 , 145 ]
    $data_skills_pokemon[ 146 ]=["UPPERCUT", 70 , 1 , 100 , 10 , 20 , 0x4C , 0 , true , 6 , 0 ,"Un coup de poing cadencé. Peut rendre confus.", 0 , 146 ]
    $data_skills_pokemon[ 147 ]=["SPORE", 0 , 5 , 100 , 15 , 0 , 0x1 , 0 , false , 6 , 0 ,"Répand un nuage de spores qui endort toujours.", 0 , 147 ]
    $data_skills_pokemon[ 148 ]=["FLASH", 0 , 1 , 70 , 20 , 0 , 0x17 , 0 , false , 6 , 25 ,"Explosion lumineuse qui fait baisser la précision.", 0 , 148 ]
    $data_skills_pokemon[ 149 ]=["VAGUE PSY", 1 , 11 , 80 , 15 , 0 , 0x58 , 0 , false , 6 , 0 ,"Envoie une onde PSY d'intensité variable.", 0 , 149 ]
    $data_skills_pokemon[ 150 ]=["TREMPETTE", 0 , 1 , 0 , 40 , 0 , 0x55 , 10 , false , 6 , 0 ,"Eclabousse l'ennemi... N'a aucun effet.", 0 , 150 ]
    $data_skills_pokemon[ 151 ]=["ACIDARMURE", 0 , 8 , 0 , 40 , 0 , 0x33 , 10 , false , 6 , 0 ,"Liquéfie le corps du lanceur et booste sa Défense.", 0 , 151 ]
    $data_skills_pokemon[ 152 ]=["PINCE-MASSE", 90 , 3 , 85 , 10 , 0 , 0x2B , 0 , true , 6 , 0 ,"Cogne avec une pince. Taux de critiques élevé.", 0 , 152 ]
    $data_skills_pokemon[ 153 ]=["EXPLOSION", 250 , 1 , 100 , 5 , 0 , 0x7 , 20 , false , 6 , 0 ,"Inflige de sérieux dégâts, mais met le lanceur K.O.", 0 , 153 ]
    $data_skills_pokemon[ 154 ]=["COMBO-GRIFFE", 18 , 1 , 80 , 15 , 0 , 0x1D , 0 , true , 6 , 0 ,"Ecorche l'ennemi avec des griffes, etc., de 2 à 5 fois.", 0 , 154 ]
    $data_skills_pokemon[ 155 ]=["OSMERANG", 50 , 9 , 90 , 10 , 0 , 0x2C , 0 , false , 6 , 0 ,"Lance un boomerang en os qui frappe deux fois.", 0 , 155 ]
    $data_skills_pokemon[ 156 ]=["REPOS", 0 , 11 , 0 , 10 , 0 , 0x25 , 10 , false , 6 , 0 ,"Le lanceur dort 2 tours et regagne PV et statut.", 0 , 156 ]
    $data_skills_pokemon[ 157 ]=["EBOULEMENT", 75 , 13 , 90 , 10 , 30 , 0x1F , 8 , false , 6 , 0 ,"Envoie de gros rochers. Peut apeurer l'ennemi.", 0 , 157 ]
    $data_skills_pokemon[ 158 ]=["CROC DE MORT", 80 , 1 , 90 , 15 , 10 , 0x1F , 0 , true , 6 , 0 ,"Blesse d'un coup de croc. Peut apeurer l'ennemi.", 0 , 158 ]
    $data_skills_pokemon[ 159 ]=["AFFUTAGE", 0 , 1 , 0 , 30 , 0 , 0x0A , 10 , false , 6 , 0 ,"Réduit les polygones et augmente l'Attaque.", 0 , 159 ]
    $data_skills_pokemon[ 160 ]=["ADAPTATION", 0 , 1 , 0 , 30 , 0 , 0x1E , 10 , false , 6 , 0 ,"Change le type du lanceur en celui d'une capacité.", 0 , 160 ]
    $data_skills_pokemon[ 161 ]=["TRIPLATTAQUE", 80 , 1 , 100 , 10 , 20 , 0x24 , 0 , false , 6 , 0 ,"Envoie trois types de rayons en même temps.", 0 , 161 ]
    $data_skills_pokemon[ 162 ]=["CROC FATAL", 1 , 1 , 90 , 10 , 0 , 0x28 , 0 , true , 6 , 0 ,"Coup de croc qui fait baisser les PV de moitié.", 0 , 162 ]
    $data_skills_pokemon[ 163 ]=["TRANCHE", 70 , 1 , 100 , 20 , 0 , 0x2B , 0 , true , 6 , 0 ,"Coups de griffe, etc. Taux de critiques élevé.", 0 , 163 ]
    $data_skills_pokemon[ 164 ]=["CLONAGE", 0 , 1 , 0 , 10 , 0 , 0x4F , 10 , false , 6 , 0 ,"Crée un leurre avec 1/4 des PV max du lanceur.", 0 , 164 ]
    $data_skills_pokemon[ 165 ]=["LUTTE", 50 , 1 , 100 , 1 , 0 , 0x30 , 0 , true , 6 , 0 ,"Fonctionne s'il ne reste plus de PP. Blesse un peu.", 0 , 165 ]
    $data_skills_pokemon[ 166 ]=["GRIBOUILLE", 0 , 1 , 0 , 1 , 0 , 0x5F , 0 , false , 6 , 0 ,"Copie en permanence la dernière attaque ennemie.", 0 , 166 ]
    $data_skills_pokemon[ 167 ]=["TRIPLE PIED", 10 , 7 , 90 , 10 , 0 , 0x68 , 0 , true , 6 , 0 ,"Trois coups de pied de plus en plus puissants.", 0 , 167 ]
    $data_skills_pokemon[ 168 ]=["LARCIN", 40 , 17 , 100 , 10 , 100 , 0x69 , 0 , true , 6 , 0 ,"Peut voler l'objet tenu par l'ennemi pendant l'attaque.", 0 , 168 ]
    $data_skills_pokemon[ 169 ]=["TOILE", 0 , 12 , 100 , 10 , 0 , 0x6A , 0 , false , 6 , 0 ,"Capture l'ennemi et empêche toute fuite ou changement.", 0 , 169 ]
    $data_skills_pokemon[ 170 ]=["LIRE-ESPRIT", 0 , 1 , 100 , 5 , 0 , 0x5E , 0 , false , 6 , 0 ,"Pressent l'action pour réussir l'attaque suivante.", 0 , 170 ]
    $data_skills_pokemon[ 171 ]=["CAUCHEMAR", 0 , 14 , 100 , 15 , 0 , 0x6B , 0 , false , 6 , 0 ,"Inflige 1/4 de dégâts à un ennemi endormi chaque tour.", 0 , 171 ]
    $data_skills_pokemon[ 172 ]=["ROUE DE FEU", 60 , 2 , 100 , 25 , 10 , 0x7D , 0 , false , 6 , 0 ,"Une charge enflammée qui peut brûler l'ennemi.", 0 , 172 ]
    $data_skills_pokemon[ 173 ]=["RONFLEMENT", 40 , 1 , 100 , 15 , 30 , 0x5C , 0 , false , 6 , 0 ,"Attaque sonore qui ne peut être lancée qu'endormi.", 0 , 173 ]
    $data_skills_pokemon[ 174 ]=["MALEDICTION", 0 , 0 , 0 , 10 , 0 , 0x6D , 0 , false , 6 , 0 ,"Coup qui fonctionne autrement pour les Spectres.", 0 , 174 ]
    $data_skills_pokemon[ 175 ]=["FLEAU", 1 , 1 , 100 , 15 , 0 , 0x63 , 0 , true , 6 , 0 ,"Inflige plus de dégâts quand les PV sont faibles.", 0 , 175 ]
    $data_skills_pokemon[ 176 ]=["CONVERSION 2", 0 , 1 , 100 , 30 , 0 , 0x5D , 10 , false , 6 , 0 ,"Permet de résister au type de la dernière attaque.", 0 , 176 ]
    $data_skills_pokemon[ 177 ]=["AEROBLAST", 100 , 10 , 95 , 5 , 0 , 0x2B , 0 , false , 6 , 0 ,"Implosion à taux de coups critiques élevé.", 0 , 177 ]
    $data_skills_pokemon[ 178 ]=["SPORE COTON", 0 , 5 , 85 , 40 , 0 , 0x3C , 0 , false , 6 , 0 ,"Envoie des spores pour baisser la Vitesse.", 0 , 178 ]
    $data_skills_pokemon[ 179 ]=["CONTRE", 1 , 7 , 100 , 15 , 0 , 0x63 , 0 , true , 6 , 0 ,"Inflige plus de dégâts quand les PV sont faibles.", 0 , 179 ]
    $data_skills_pokemon[ 180 ]=["DEPIT", 0 , 14 , 100 , 10 , 0 , 0x64 , 0 , false , 6 , 0 ,"Réduit méchamment les PP de la dernière att. ennemie.", 0 , 180 ]
    $data_skills_pokemon[ 181 ]=["POUDREUSE", 40 , 6 , 100 , 25 , 10 , 0x5C , 8 , false , 6 , 0 ,"Envoie une rafale de neige. Peut geler l'ennemi.", 0 , 181 ]
    $data_skills_pokemon[ 182 ]=["ABRI", 0 , 1 , 0 , 10 , 0 , 0x6F , 10 , false , 9 , 0 ,"Esquive l'attaque, mais peut échouer si réutilisé.", 0 , 182 ]
    $data_skills_pokemon[ 183 ]=["MACH PUNCH", 40 , 7 , 100 , 30 , 0 , 0x67 , 0 , true , 7 , 0 ,"Coup de poing fulgurant. Permet de frapper le 1er.", 0 , 183 ]
    $data_skills_pokemon[ 184 ]=["GRIMACE", 0 , 1 , 90 , 10 , 0 , 0x3C , 0 , false , 6 , 0 ,"Effraye avec une grimace pour baisser la Vitesse.", 0 , 184 ]
    $data_skills_pokemon[ 185 ]=["FEINTE", 60 , 17 , 0 , 20 , 0 , 0x11 , 0 , false , 6 , 0 ,"Attire l'ennemi et frappe sans jamais échouer.", 0 , 185 ]
    $data_skills_pokemon[ 186 ]=["DOUX BAISER", 0 , 1 , 75 , 10 , 0 , 0x31 , 0 , false , 6 , 0 ,"Demande un bisou tout mignon. Peut rendre confus.", 0 , 186 ]
    $data_skills_pokemon[ 187 ]=["COGNOBIDON", 0 , 1 , 0 , 10 , 0 , 0x8E , 10 , false , 6 , 0 ,"Améliore l'Attaque en sacrifiant des PV.", 0 , 187 ]
    $data_skills_pokemon[ 188 ]=["BOMB-BEURK", 90 , 8 , 100 , 10 , 30 , 0x2 , 0 , false , 6 , 0 ,"Envoie des détritus pour blesser. Peut empoisonner.", 0 , 188 ]
    $data_skills_pokemon[ 189 ]=["COUD'BOUE", 20 , 9 , 100 , 10 , 100 , 0x49 , 0 , false , 6 , 0 ,"Envoie de la boue au visage pour baisser la précision.", 0 , 189 ]
    $data_skills_pokemon[ 190 ]=["OCTAZOOKA", 65 , 3 , 85 , 10 , 50 , 0x49 , 0 , false , 6 , 0 ,"Jet d'encre qui blesse et baisse la précision.", 0 , 190 ]
    $data_skills_pokemon[ 191 ]=["PICOTS", 0 , 9 , 0 , 20 , 0 , 0x70 , 0 , false , 6 , 0 ,"Pointes blessant lors d'un changement d'adversaire.", 0 , 191 ]
    $data_skills_pokemon[ 192 ]=["ELECANON", 100 , 4 , 50 , 5 , 100 , 0x6 , 0 , false , 6 , 0 ,"Puissant et paralysant, mais pas très précis.", 0 , 192 ]
    $data_skills_pokemon[ 193 ]=["CLAIRVOYANCE", 0 , 1 , 100 , 40 , 0 , 0x71 , 0 , false , 6 , 0 ,"Empêche l'ennemi d'améliorer son esquive.", 0 , 193 ]
    $data_skills_pokemon[ 194 ]=["PRLVT DESTIN", 0 , 14 , 100 , 5 , 0 , 0x62 , 10 , false , 6 , 0 ,"Si le lanceur est mis K.O., l'ennemi le sera également.", 0 , 194 ]
    $data_skills_pokemon[ 195 ]=["REQUIEM", 0 , 1 , 0 , 5 , 0 , 0x72 , 10 , false , 6 , 0 ,"Un PoKéMoN qui entend ce requiem est K.O. en 3 tours.", 0 , 195 ]
    $data_skills_pokemon[ 196 ]=["VENT GLACE", 55 , 6 , 95 , 15 , 100 , 0x46 , 8 , false , 6 , 0 ,"Attaque glaciale. Baisse la Vitesse de l'ennemi.", 0 , 196 ]
    $data_skills_pokemon[ 197 ]=["DETECTION", 0 , 7 , 0 , 5 , 0 , 0x6F , 10 , false , 9 , 0 ,"Esquive l'attaque, mais peut échouer si réutilisé.", 0 , 197 ]
    $data_skills_pokemon[ 198 ]=["CHARGE-OS", 25 , 9 , 80 , 10 , 0 , 0x1D , 0 , false , 6 , 0 ,"Frappe l'ennemi 2 à 5 fois avec un os.", 0 , 198 ]
    $data_skills_pokemon[ 199 ]=["VERROUILLAGE", 0 , 1 , 100 , 5 , 0 , 0x5E , 0 , false , 6 , 0 ,"Le coup suivant sera réussi à coup sûr.", 0 , 199 ]
    $data_skills_pokemon[ 200 ]=["COLERE", 90 , 15 , 100 , 15 , 100 , 0x1B , 4 , true , 6 , 0 ,"Une attaque furieuse sur 2 à 3 tours. Lanceur confus.", 0 , 200 ]
    $data_skills_pokemon[ 201 ]=["TEMPETESABLE", 0 , 13 , 0 , 10 , 0 , 0x73 , 10 , false , 6 , 0 ,"Une tempête de sable qui souffle plusieurs tours.", 0 , 201 ]
    $data_skills_pokemon[ 202 ]=["GIGA-SANGSUE", 60 , 5 , 100 , 5 , 0 , 0x3 , 0 , false , 6 , 0 ,"Une attaque qui absorbe la moitié des dégâts infligés.", 0 , 202 ]
    $data_skills_pokemon[ 203 ]=["TENACITE", 0 , 1 , 0 , 10 , 0 , 0x74 , 10 , false , 9 , 0 ,"Encaisse les attaques du tour et conserve 1 PV.", 0 , 203 ]
    $data_skills_pokemon[ 204 ]=["CHARME", 0 , 1 , 100 , 20 , 0 , 0x3A , 0 , false , 6 , 0 ,"Charme l'ennemi et baisse brusquement son Attaque.", 0 , 204 ]
    $data_skills_pokemon[ 205 ]=["ROULADE", 30 , 13 , 90 , 20 , 0 , 0x75 , 0 , true , 6 , 0 ,"Une attaque sur 5 tours, de plus en plus puissante.", 0 , 205 ]
    $data_skills_pokemon[ 206 ]=["FAUX-CHAGE", 40 , 1 , 100 , 40 , 0 , 0x65 , 0 , true , 6 , 0 ,"Une attaque qui laisse au moins 1 PV à l'ennemi.", 0 , 206 ]
    $data_skills_pokemon[ 207 ]=["VANTARDISE", 0 , 1 , 90 , 15 , 100 , 0x76 , 0 , false , 6 , 0 ,"Rend l'ennemi confus et booste l'Attaque.", 0 , 207 ]
    $data_skills_pokemon[ 208 ]=["LAIT A BOIRE", 0 , 1 , 0 , 10 , 0 , 0x9D , 10 , false , 6 , 0 ,"Regagne jusqu'à la moitié des PV max du lanceur.", 0 , 208 ]
    $data_skills_pokemon[ 209 ]=["ETINCELLE", 65 , 4 , 100 , 20 , 30 , 0x6 , 0 , true , 6 , 0 ,"Une charge électrique qui peut paralyser l'ennemi.", 0 , 209 ]
    $data_skills_pokemon[ 210 ]=["TAILLADE", 10 , 12 , 95 , 20 , 0 , 0x77 , 0 , true , 6 , 0 ,"Une attaque qui s'intensifie après chaque coup.", 0 , 210 ]
    $data_skills_pokemon[ 211 ]=["AILE D'ACIER", 70 , 16 , 90 , 25 , 10 , 0x8A , 0 , true , 6 , 0 ,"Frappe l'ennemi en déployant ses ailes d'acier.", 0 , 211 ]
    $data_skills_pokemon[ 212 ]=["REGARD NOIR", 0 , 1 , 100 , 5 , 0 , 0x6A , 0 , false , 6 , 0 ,"Lance un regard méchant pour empêcher la fuite.", 0 , 212 ]
    $data_skills_pokemon[ 213 ]=["ATTRACTION", 0 , 1 , 100 , 15 , 0 , 0x78 , 0 , false , 6 , 0 ,"Le genre opposé a moins de chances d'attaquer.", 0 , 213 ]
    $data_skills_pokemon[ 214 ]=["BLABLA DODO", 0 , 1 , 0 , 10 , 0 , 0x61 , 0 , false , 6 , 0 ,"Utilise un coup au hasard en dormant.", 0 , 214 ]
    $data_skills_pokemon[ 215 ]=["GLAS DE SOIN", 0 , 1 , 0 , 5 , 0 , 0x66 , 10 , false , 6 , 0 ,"Carillon apaisant qui signe tout changement de statut.", 0 , 215 ]
    $data_skills_pokemon[ 216 ]=["RETOUR", 1 , 1 , 100 , 20 , 0 , 0x79 , 0 , true , 6 , 0 ,"Attaque dont la puissance dépend de l'amitié.", 0 , 216 ]
    $data_skills_pokemon[ 217 ]=["CADEAU", 1 , 1 , 90 , 15 , 0 , 0x7A , 0 , false , 6 , 0 ,"Cadeau en forme de bombe. Peut restaurer des PV.", 0 , 217 ]
    $data_skills_pokemon[ 218 ]=["FRUSTRATION", 1 , 1 , 100 , 20 , 0 , 0x7B , 0 , true , 6 , 0 ,"Attaque plus puissante si le Dresseur est mal aimé.", 0 , 218 ]
    $data_skills_pokemon[ 219 ]=["RUNE PROTECT", 0 , 1 , 0 , 25 , 0 , 0x7C , 10 , false , 6 , 0 ,"Force mystique empêchant tout changement de statut.", 0 , 219 ]
    $data_skills_pokemon[ 220 ]=["BALANCE", 0 , 1 , 100 , 20 , 0 , 0x5B , 0 , false , 6 , 0 ,"Ajoute les PV des deux combattants et les partage.", 0 , 220 ]
    $data_skills_pokemon[ 221 ]=["FEU SACRE", 100 , 2 , 95 , 5 , 50 , 0x7D , 0 , false , 6 , 0 ,"Une attaque de feu mystique pouvant brûler.", 0 , 221 ]
    $data_skills_pokemon[ 222 ]=["AMPLEUR", 1 , 9 , 100 , 30 , 0 , 0x7E , 20 , false , 6 , 0 ,"Un tremblement de terre d'intensité variable.", 0 , 222 ]
    $data_skills_pokemon[ 223 ]=["DYNAMOPOING", 100 , 7 , 50 , 5 , 100 , 0x4C , 0 , true , 6 , 0 ,"Puissant et rend confus, mais pas très précis.", 0 , 223 ]
    $data_skills_pokemon[ 224 ]=["MEGACORNE", 120 , 12 , 85 , 10 , 0 , 0x0 , 0 , true , 6 , 0 ,"Une charge violente, toutes cornes en avant.", 0 , 224 ]
    $data_skills_pokemon[ 225 ]=["DRACOSOUFFLE", 60 , 15 , 100 , 20 , 30 , 0x6 , 0 , false , 6 , 0 ,"Frappe l'ennemi grâce à un souffle super puissant.", 0 , 225 ]
    $data_skills_pokemon[ 226 ]=["RELAIS", 0 , 1 , 0 , 40 , 0 , 0x7F , 10 , false , 6 , 0 ,"Change de lanceur, mais garde les effets en cours.", 0 , 226 ]
    $data_skills_pokemon[ 227 ]=["ENCORE", 0 , 1 , 100 , 5 , 0 , 0x5A , 0 , false , 6 , 0 ,"Oblige l'ennemi à répéter sa dernière action 2 à 6 tours.", 0 , 227 ]
    $data_skills_pokemon[ 228 ]=["POURSUITE", 40 , 17 , 100 , 20 , 0 , 0x80 , 0 , false , 6 , 0 ,"Inflige de sérieux dégâts lorsque l'ennemi change.", 0 , 228 ]
    $data_skills_pokemon[ 229 ]=["TOUR RAPIDE", 20 , 1 , 100 , 40 , 0 , 0x81 , 0 , false , 6 , 0 ,"Frappe l'ennemi en tournant sur soi-même.", 0 , 229 ]
    $data_skills_pokemon[ 230 ]=["DOUX PARFUM", 0 , 1 , 100 , 20 , 0 , 0x18 , 8 , false , 6 , 0 ,"Séduit l'ennemi pour baisser son esquive.", 0 , 230 ]
    $data_skills_pokemon[ 231 ]=["QUEUE DE FER", 100 , 16 , 75 , 15 , 30 , 0x45 , 0 , true , 6 , 0 ,"Attaque avec sa queue. Peut baisser la Défense.", 0 , 231 ]
    $data_skills_pokemon[ 232 ]=["GRIFFE ACIER", 50 , 16 , 95 , 35 , 10 , 0x8B , 0 , true , 6 , 0 ,"Attaque avec des griffes. Peut augmenter l'Attaque.", 0 , 232 ]
    $data_skills_pokemon[ 233 ]=["CORPS PERDU", 70 , 7 , 100 , 10 , 0 , 0x4E , 0 , true , 5 , 0 ,"L'attaque dure longtemps, mais n'échoue jamais.", 0 , 233 ]
    $data_skills_pokemon[ 234 ]=["AURORE", 0 , 1 , 0 , 5 , 0 , 0x84 , 10 , false , 6 , 0 ,"Restaure les PV. Varie suivant le temps.", 0 , 234 ]
    $data_skills_pokemon[ 235 ]=["SYNTHESE", 0 , 5 , 0 , 5 , 0 , 0x85 , 10 , false , 6 , 0 ,"Restaure les PV. Varie suivant le temps.", 0 , 235 ]
    $data_skills_pokemon[ 236 ]=["RAYON LUNE", 0 , 1 , 0 , 5 , 0 , 0x86 , 10 , false , 6 , 0 ,"Restaure les PV. Varie suivant le temps.", 0 , 236 ]
    $data_skills_pokemon[ 237 ]=["PUIS. CACHEE", 1 , 1 , 100 , 15 , 0 , 0x87 , 0 , false , 6 , 0 ,"L'efficacité de l'attaque dépend du lanceur.", 0 , 237 ]
    $data_skills_pokemon[ 238 ]=["COUP-CROIX", 100 , 7 , 80 , 5 , 0 , 0x2B , 0 , true , 6 , 0 ,"2 attaques tranchantes. Taux de critiques élevé.", 0 , 238 ]
    $data_skills_pokemon[ 239 ]=["OURAGAN", 40 , 15 , 100 , 20 , 20 , 0x92 , 8 , false , 6 , 0 ,"Déclenche un terrible ouragan blessant l'ennemi.", 0 , 239 ]
    $data_skills_pokemon[ 240 ]=["DANSE PLUIE", 0 , 3 , 0 , 5 , 0 , 0x88 , 10 , false , 6 , 0 ,"Améliore les attaques Eau pendant 5 tours.", 0 , 240 ]
    $data_skills_pokemon[ 241 ]=["ZENITH", 0 , 2 , 0 , 5 , 0 , 0x89 , 10 , false , 6 , 0 ,"Améliore les attaques Feu pendant 5 tours.", 0 , 241 ]
    $data_skills_pokemon[ 242 ]=["MACHOUILLE", 80 , 17 , 100 , 15 , 20 , 0x48 , 0 , true , 6 , 0 ,"Utilise ses crocs pointus. Peut baisser la Déf. Spé.", 0 , 242 ]
    $data_skills_pokemon[ 243 ]=["VOILE MIROIR", 1 , 11 , 100 , 20 , 0 , 0x90 , 0 , false , 1 , 0 ,"Renvoie l'attaque spéciale ennemie 2 fois plus fort.", 0 , 243 ]
    $data_skills_pokemon[ 244 ]=["BOOST", 0 , 1 , 0 , 10 , 0 , 0x8F , 0 , false , 6 , 0 ,"Copie les effets de l'ennemi et les passe au lanceur.", 0 , 244 ]
    $data_skills_pokemon[ 245 ]=["VIT.EXTREME", 80 , 1 , 100 , 5 , 0 , 0x67 , 0 , false , 7 , 0 ,"Une attaque extrêmement rapide et puissante.", 0 , 245 ]
    $data_skills_pokemon[ 246 ]=["POUV.ANTIQUE", 60 , 13 , 100 , 5 , 10 , 0x8C , 0 , true , 6 , 0 ,"Une attaque pouvant augmenter toutes les stats.", 0 , 246 ]
    $data_skills_pokemon[ 247 ]=["BALL'OMBRE", 80 , 14 , 100 , 15 , 20 , 0x48 , 0 , false , 6 , 0 ,"Projette une grande tache qui peut baisser la Déf. Spé.", 0 , 247 ]
    $data_skills_pokemon[ 248 ]=["PRESCIENCE", 80 , 11 , 90 , 15 , 0 , 0x94 , 0 , false , 6 , 0 ,"Accumule de l'énergie pour frapper 2 tours plus tard.", 0 , 248 ]
    $data_skills_pokemon[ 249 ]=["ECLATE-ROC", 20 , 7 , 100 , 15 , 50 , 0x45 , 0 , true , 6 , 26 ,"Un attaque puissante qui peut baisser la Défense.", 0 , 249 ]
    $data_skills_pokemon[ 250 ]=["SIPHON", 15 , 3 , 70 , 15 , 100 , 0x2A , 0 , false , 6 , 28 ,"Piège l'ennemi dans un tourbillon pendant 2 à 5 tours.", 0 , 250 ]
    $data_skills_pokemon[ 251 ]=["BASTON", 10 , 17 , 100 , 10 , 0 , 0x9A , 0 , false , 6 , 0 ,"Appelle un PoKéMoN qui se joint à l'attaque.", 0 , 251 ]
    $data_skills_pokemon[ 252 ]=["BLUFF", 40 , 1 , 100 , 10 , 0 , 0x9E , 0 , false , 7 , 0 ,"Une attaque en début de bataille pour apeurer.", 0 , 252 ]
    $data_skills_pokemon[ 253 ]=["BROUHAHA", 50 , 1 , 100 , 10 , 100 , 0x9F , 4 , false , 6 , 0 ,"Brouhaha de 2 à 5 tours. Empêche de dormir.", 0 , 253 ]
    $data_skills_pokemon[ 254 ]=["STOCKAGE", 0 , 1 , 0 , 10 , 0 , 0xA0 , 10 , false , 6 , 0 ,"Accumule de la puissance pendant 3 tours maximum.", 0 , 254 ]
    $data_skills_pokemon[ 255 ]=["RELACHE", 100 , 1 , 100 , 10 , 0 , 0xA1 , 0 , false , 6 , 0 ,"Libère la puissance précédemment accumulée.", 0 , 255 ]
    $data_skills_pokemon[ 256 ]=["AVALE", 0 , 1 , 0 , 10 , 0 , 0xA2 , 10 , false , 6 , 0 ,"Absorbe la puissance accumulée et restaure les PV.", 0 , 256 ]
    $data_skills_pokemon[ 257 ]=["CANICULE", 100 , 2 , 90 , 10 , 10 , 0x4 , 8 , false , 6 , 0 ,"Un souffle ardent pouvant brûler l'ennemi.", 0 , 257 ]
    $data_skills_pokemon[ 258 ]=["GRELE", 0 , 6 , 0 , 10 , 0 , 0xA4 , 10 , false , 6 , 0 ,"Tempête de grêle qui frappe à chaque tour.", 0 , 258 ]
    $data_skills_pokemon[ 259 ]=["TOURMENTE", 0 , 17 , 100 , 15 , 0 , 0xA5 , 0 , false , 6 , 0 ,"Perturbe l'ennemi. Empêche la réutilisation d'un att.", 0 , 259 ]
    $data_skills_pokemon[ 260 ]=["FLATTERIE", 0 , 17 , 100 , 15 , 0 , 0xA6 , 0 , false , 6 , 0 ,"Rend l'ennemi confus, mais augmente son Atq. Spé.", 0 , 260 ]
    $data_skills_pokemon[ 261 ]=["FEU FOLLET", 0 , 2 , 75 , 15 , 0 , 0xA7 , 0 , false , 6 , 0 ,"Inflige une douloureuse brûlure à l'ennemi.", 0 , 261 ]
    $data_skills_pokemon[ 262 ]=["SOUVENIR", 0 , 17 , 100 , 10 , 0 , 0xA8 , 0 , false , 6 , 0 ,"Le lanceur est mis K.O. et baisse les cap. ennemies.", 0 , 262 ]
    $data_skills_pokemon[ 263 ]=["FACADE", 70 , 1 , 100 , 20 , 0 , 0xA9 , 0 , true , 6 , 0 ,"Améliore l'Attaque si brûlé, paralysé ou empoisonné.", 0 , 263 ]
    $data_skills_pokemon[ 264 ]=["MITRA-POING", 150 , 7 , 100 , 20 , 0 , 0xAA , 0 , true , 3 , 0 ,"Lanceur apeuré si touché. Attaque puissante.", 0 , 264 ]
    $data_skills_pokemon[ 265 ]=["STIMULANT", 60 , 1 , 100 , 10 , 0 , 0xAB , 0 , true , 6 , 0 ,"Utile contre les ennemis paralysés, mais les soigne.", 0 , 265 ]
    $data_skills_pokemon[ 266 ]=["PAR ICI", 0 , 1 , 100 , 20 , 0 , 0xAC , 10 , false , 9 , 0 ,"Attire l'attention. Les adv. n'attaquent que le lanceur.", 0 , 266 ]
    $data_skills_pokemon[ 267 ]=["FORCE-NATURE", 0 , 1 , 95 , 20 , 0 , 0xAD , 0 , false , 6 , 0 ,"Le type de l'attaque change selon le lieu du combat.", 0 , 267 ]
    $data_skills_pokemon[ 268 ]=["CHARGEUR", 0 , 4 , 100 , 20 , 0 , 0xAE , 10 , false , 6 , 0 ,"Charge de la puissance pour l'att. électrique suivante.", 0 , 268 ]
    $data_skills_pokemon[ 269 ]=["PROVOC", 0 , 17 , 100 , 20 , 0 , 0xAF , 0 , false , 6 , 0 ,"Provoque l'ennemi. L'oblige à utiliser ses attaques.", 0 , 269 ]
    $data_skills_pokemon[ 270 ]=["COUP D'MAIN", 0 , 1 , 100 , 20 , 0 , 0xB0 , 10 , true , 11 , 0 ,"Améliore la puissance d'attaque du receveur.", 0 , 270 ]
    $data_skills_pokemon[ 271 ]=["TOURMAGIK", 0 , 11 , 100 , 10 , 0 , 0xB1 , 0 , false , 6 , 0 ,"Oblige la cible à échanger les objets tenus.", 0 , 271 ]
    $data_skills_pokemon[ 272 ]=["IMITATION", 0 , 11 , 100 , 10 , 0 , 0xB2 , 0 , false , 6 , 0 ,"Imite la cible et copie sa capacité spéciale.", 0 , 272 ]
    $data_skills_pokemon[ 273 ]=["VOEU", 0 , 1 , 100 , 10 , 0 , 0xB3 , 10 , false , 6 , 0 ,"Un voeu qui restaure les PV. Demande un certain temps.", 0 , 273 ]
    $data_skills_pokemon[ 274 ]=["ASSISTANCE", 0 , 1 , 100 , 20 , 0 , 0xB4 , 0 , false , 6 , 0 ,"Attaque au hasard avec un coup d'un des partenaires.", 0 , 274 ]
    $data_skills_pokemon[ 275 ]=["RACINES", 0 , 5 , 100 , 20 , 0 , 0xB5 , 10 , false , 6 , 0 ,"Plante des racines, regagne PV, ne peut plus changer.", 0 , 275 ]
    $data_skills_pokemon[ 276 ]=["SURPUISSANCE", 120 , 7 , 100 , 5 , 0 , 0xB6 , 0 , false , 6 , 0 ,"Booste la force, mais baisse les capacités.", 0 , 276 ]
    $data_skills_pokemon[ 277 ]=["REFLET MAGIK", 0 , 11 , 100 , 15 , 0 , 0xB7 , 0 , false , 10 , 0 ,"Renvoie les effets spéciaux vers le lanceur.", 0 , 277 ]
    $data_skills_pokemon[ 278 ]=["RECYCLAGE", 0 , 1 , 100 , 10 , 0 , 0xB8 , 10 , false , 6 , 0 ,"Recycle un objet pour l'utiliser une fois de plus.", 0 , 278 ]
    $data_skills_pokemon[ 279 ]=["VENDETTA", 60 , 7 , 100 , 10 , 0 , 0xB9 , 0 , true , 2 , 0 ,"Gagne en puissance quand blessé par l'ennemi.", 0 , 279 ]
    $data_skills_pokemon[ 280 ]=["CASSE-BRIQUE", 75 , 7 , 100 , 15 , 0 , 0xBA , 0 , true , 6 , 0 ,"Détruit les barrières comme PROTECTION et blesse.", 0 , 280 ]
    $data_skills_pokemon[ 281 ]=["BAILLEMENT", 0 , 1 , 100 , 10 , 0 , 0xBB , 0 , false , 6 , 0 ,"Fait bâiller l'ennemi, qui s'endort au tour suivant.", 0 , 281 ]
    $data_skills_pokemon[ 282 ]=["SABOTAGE", 20 , 17 , 100 , 20 , 100 , 0xBC , 0 , true , 6 , 0 ,"Frappe l'objet tenu pour le rendre inutilisable.", 0 , 282 ]
    $data_skills_pokemon[ 283 ]=["EFFORT", 1 , 1 , 100 , 5 , 0 , 0xBD , 0 , true , 6 , 0 ,"Gagne en puissance si PV inférieurs aux PV ennemis.", 0 , 283 ]
    $data_skills_pokemon[ 284 ]=["ERUPTION", 150 , 2 , 100 , 5 , 0 , 0xBE , 8 , true , 6 , 0 ,"Inflige plus de dégâts si PV du lanceur sont élevés.", 0 , 284 ]
    $data_skills_pokemon[ 285 ]=["ECHANGE", 0 , 11 , 100 , 10 , 0 , 0xBF , 0 , false , 6 , 0 ,"Le lanceur et la cible échangent leurs cap. spéc.", 0 , 285 ]
    $data_skills_pokemon[ 286 ]=["POSSESSIF", 0 , 11 , 100 , 10 , 0 , 0xC0 , 10 , false , 6 , 0 ,"Empêche les ennemis d'utiliser les coups du lanceur.", 0 , 286 ]
    $data_skills_pokemon[ 287 ]=["REGENERATION", 0 , 1 , 100 , 20 , 0 , 0xC1 , 10 , false , 6 , 0 ,"Soigne un empoisonnement, une paralysie, une brûlure.", 0 , 287 ]
    $data_skills_pokemon[ 288 ]=["RANCUNE", 0 , 14 , 100 , 5 , 0 , 0xC2 , 10 , false , 6 , 0 ,"Si mis K.O., supprime les PP du coup ennemi.", 0 , 288 ]
    $data_skills_pokemon[ 289 ]=["SAISIE", 0 , 17 , 100 , 10 , 0 , 0xC3 , 0 , false , 10 , 0 ,"Vole les effets de la prochaine attaque ennemie.", 0 , 289 ]
    $data_skills_pokemon[ 290 ]=["FORCE CACHEE", 70 , 1 , 100 , 20 , 30 , 0xC5 , 0 , false , 6 , 30 ,"Les effets de l'attaque dépendent du lieu.", 0 , 290 ]
    $data_skills_pokemon[ 291 ]=["PLONGEE", 60 , 3 , 100 , 10 , 0 , 0x9B3 , 0 , false , 6 , 0 ,"Plonge en apnée au premier tour et frappe au second.", 0 , 291 ]
    $data_skills_pokemon[ 292 ]=["COGNE", 15 , 7 , 100 , 20 , 0 , 0x1D , 0 , true , 6 , 0 ,"Coups de poing arrêtés qui frappent 2 à 5 fois.", 0 , 292 ]
    $data_skills_pokemon[ 293 ]=["CAMOUFLAGE", 0 , 1 , 100 , 20 , 0 , 0xD5 , 10 , false , 6 , 0 ,"Change le type du PoKéMoN selon le lieu.", 0 , 293 ]
    $data_skills_pokemon[ 294 ]=["LUMIQUEUE", 0 , 12 , 100 , 20 , 0 , 0x35 , 10 , false , 6 , 0 ,"Flash lumineux qui booste l'Atq. Spé.", 0 , 294 ]
    $data_skills_pokemon[ 295 ]=["LUMI-ECLAT", 70 , 11 , 100 , 5 , 50 , 0x48 , 0 , false , 6 , 0 ,"Envoie une lueur intense. Peut baisser Déf. Spé.", 0 , 295 ]
    $data_skills_pokemon[ 296 ]=["BALL'BRUME", 70 , 11 , 100 , 5 , 50 , 0x47 , 0 , false , 6 , 0 ,"Envoie une rafale de duvet. Peut baisser Atq. Spé.", 0 , 296 ]
    $data_skills_pokemon[ 297 ]=["DANSE-PLUME", 0 , 10 , 100 , 15 , 0 , 0x3A , 0 , false , 6 , 0 ,"Enveloppe l'ennemi de duvet et baisse l'Attaque.", 0 , 297 ]
    $data_skills_pokemon[ 298 ]=["DANSE-FOLLE", 0 , 1 , 100 , 20 , 0 , 0xC7 , 20 , false , 6 , 0 ,"Rend tous les PoKéMoN engagés confus.", 0 , 298 ]
    $data_skills_pokemon[ 299 ]=["PIED BRULEUR", 85 , 2 , 90 , 10 , 10 , 0xC8 , 0 , true , 6 , 0 ,"Coup de pied à taux de critiques élevé. Peut brûler.", 0 , 299 ]
    $data_skills_pokemon[ 300 ]=["LANCE-BOUE", 0 , 9 , 100 , 15 , 0 , 0xC9 , 10 , false , 6 , 0 ,"Couvre le lanceur de boue. Augmente résist. électr.", 0 , 300 ]
    $data_skills_pokemon[ 301 ]=["BALL'GLACE", 30 , 6 , 90 , 20 , 0 , 0x75 , 0 , true , 6 , 0 ,"Une attaque en 5 tours de plus en plus puissante.", 0 , 301 ]
    $data_skills_pokemon[ 302 ]=["POING DARD", 60 , 5 , 100 , 15 , 30 , 0x96 , 0 , true , 6 , 0 ,"Attaque avec bras épineux. Peut apeurer.", 0 , 302 ]
    $data_skills_pokemon[ 303 ]=["PARESSE", 0 , 1 , 100 , 10 , 0 , 0x20 , 10 , false , 6 , 0 ,"Se détend et récupère la moitié des PV maximum.", 0 , 303 ]
    $data_skills_pokemon[ 304 ]=["MEGAPHONE", 90 , 1 , 100 , 10 , 0 , 0x0 , 8 , false , 6 , 0 ,"Attaque bruyante blessant avec des ondes sonores.", 0 , 304 ]
    $data_skills_pokemon[ 305 ]=["CROCHETVENIN", 50 , 8 , 100 , 15 , 30 , 0xCA , 0 , true , 6 , 0 ,"Morsure pouvant sérieusement empoisonner l'ennemi.", 0 , 305 ]
    $data_skills_pokemon[ 306 ]=["ECLATEGRIFFE", 75 , 1 , 95 , 10 , 50 , 0x45 , 0 , true , 6 , 0 ,"Lacère avec des griffes. Peut baisser la Défense.", 0 , 306 ]
    $data_skills_pokemon[ 307 ]=["RAFALE FEU", 150 , 2 , 90 , 5 , 0 , 0x50 , 0 , false , 6 , 0 ,"Puissant, mais immobilise le lanceur le tour suivant.", 0 , 307 ]
    $data_skills_pokemon[ 308 ]=["HYDROBLAST", 150 , 3 , 90 , 5 , 0 , 0x50 , 0 , false , 6 , 0 ,"Puissant, mais immobilise le lanceur le tour suivant.", 0 , 308 ]
    $data_skills_pokemon[ 309 ]=["POING METEOR", 100 , 16 , 85 , 10 , 20 , 0x8B , 0 , true , 6 , 0 ,"Coup de poing météore. Peut augmenter l'Attaque.", 0 , 309 ]
    $data_skills_pokemon[ 310 ]=["ETONNEMENT", 30 , 14 , 100 , 15 , 30 , 0x96 , 0 , true , 6 , 0 ,"Une attaque qui peut choquer et apeurer l'ennemi.", 0 , 310 ]
    $data_skills_pokemon[ 311 ]=["BALL'METEO", 50 , 1 , 100 , 10 , 0 , 0xCB , 0 , false , 6 , 0 ,"Type et puissance de l'attaque selon le climat.", 0 , 311 ]
    $data_skills_pokemon[ 312 ]=["AROMATHERAPI", 0 , 5 , 0 , 5 , 0 , 0x66 , 10 , false , 6 , 0 ,"Parfum apaisant soignant les changements de statut.", 0 , 312 ]
    $data_skills_pokemon[ 313 ]=["CROCO LARME", 0 , 17 , 100 , 20 , 0 , 0x3E , 0 , false , 6 , 0 ,"Fait semblant de pleurer pour baisser la Déf. Spé.", 0 , 313 ]
    $data_skills_pokemon[ 314 ]=["TRANCH'AIR", 55 , 10 , 95 , 25 , 0 , 0x2B , 8 , false , 6 , 0 ,"Déclenche vent tranchant. Taux de critiques élevé.", 0 , 314 ]
    $data_skills_pokemon[ 315 ]=["SURCHAUFFE", 140 , 2 , 90 , 5 , 100 , 0xCC , 0 , true , 6 , 0 ,"Attaque à pleine puissance, mais baisse Atq. Spé.", 0 , 315 ]
    $data_skills_pokemon[ 316 ]=["FLAIR", 0 , 1 , 100 , 40 , 0 , 0x71 , 0 , false , 6 , 0 ,"Empêche l'ennemi d'augmenter son esquive.", 0 , 316 ]
    $data_skills_pokemon[ 317 ]=["TOMBEROCHE", 50 , 13 , 80 , 10 , 100 , 0x46 , 0 , false , 6 , 0 ,"Immobilise l'ennemi avec des rochers. Baisse la Vitesse.", 0 , 317 ]
    $data_skills_pokemon[ 318 ]=["VENT ARGENTE", 60 , 12 , 100 , 5 , 10 , 0x8C , 0 , false , 6 , 0 ,"Attaque poudreuse qui peut augmenter les capacités.", 0 , 318 ]
    $data_skills_pokemon[ 319 ]=["STRIDO-SON", 0 , 16 , 85 , 40 , 0 , 0x3E , 0 , false , 6 , 0 ,"Cri horrible qui baisse la Déf. Spé. énormément.", 0 , 319 ]
    $data_skills_pokemon[ 320 ]=["SIFFL'HERBE", 0 , 5 , 55 , 15 , 0 , 0x1 , 0 , false , 6 , 0 ,"Endort l'ennemi en sifflant une douce mélodie.", 0 , 320 ]
    $data_skills_pokemon[ 321 ]=["CHATOUILLE", 0 , 1 , 100 , 20 , 0 , 0xCD , 0 , false , 6 , 0 ,"Fait rire l'ennemi et baisse son Attaque et sa Défense.", 0 , 321 ]
    $data_skills_pokemon[ 322 ]=["FORCE COSMIK", 0 , 11 , 0 , 20 , 0 , 0xCE , 10 , false , 6 , 0 ,"Pouvoir mystique qui monte la Défense et la Déf. Spé.", 0 , 322 ]
    $data_skills_pokemon[ 323 ]=["GICLEDO", 150 , 3 , 100 , 5 , 0 , 0xBE , 8 , false , 6 , 0 ,"Inflige plus de dégâts si le lanceur a beaucoup de PV.", 0 , 323 ]
    $data_skills_pokemon[ 324 ]=["RAYON SIGNAL", 75 , 12 , 100 , 15 , 10 , 0x4C , 0 , false , 6 , 0 ,"Un étrange rayon qui peut rendre l'ennemi confus.", 0 , 324 ]
    $data_skills_pokemon[ 325 ]=["POING OMBRE", 60 , 14 , 0 , 20 , 0 , 0x11 , 0 , true , 6 , 0 ,"Un coup de poing inévitable sortant de l'ombre.", 0 , 325 ]
    $data_skills_pokemon[ 326 ]=["EXTRASENSEUR", 80 , 11 , 100 , 30 , 10 , 0x96 , 0 , false , 6 , 0 ,"Pouvoir singulier qui peut apeurer l'ennemi.", 0 , 326 ]
    $data_skills_pokemon[ 327 ]=["STRATOPERCUT", 85 , 7 , 90 , 15 , 0 , 0xCF , 0 , true , 6 , 0 ,"Un uppercut porté en direction du ciel.", 0 , 327 ]
    $data_skills_pokemon[ 328 ]=["TOURBI-SABLE", 15 , 9 , 70 , 15 , 100 , 0x2A , 0 , false , 6 , 0 ,"Piège l'ennemi dans un cyclone de 2 à 5 tours.", 0 , 328 ]
    $data_skills_pokemon[ 329 ]=["GLACIATION", 1 , 6 , 30 , 5 , 0 , 0x26 , 0 , false , 6 , 0 ,"Attaque glacée qui met K.O. quand elle est réussie.", 0 , 329 ]
    $data_skills_pokemon[ 330 ]=["OCROUPI", 95 , 3 , 85 , 10 , 30 , 0x49 , 8 , false , 6 , 0 ,"Attaque d'eau croupie. Peut baisser la précision.", 0 , 330 ]
    $data_skills_pokemon[ 331 ]=["BALLE GRAINE", 10 , 5 , 100 , 30 , 0 , 0x1D , 0 , false , 6 , 0 ,"Mitraille l'ennemi avec 2 à 5 rafales à la suite.", 0 , 331 ]
    $data_skills_pokemon[ 332 ]=["AEROPIQUE", 60 , 10 , 0 , 20 , 0 , 0x11 , 0 , true , 6 , 0 ,"Attaque extrêmement rapide et inévitable.", 0 , 332 ]
    $data_skills_pokemon[ 333 ]=["STALAGTITE", 10 , 6 , 100 , 30 , 0 , 0x1D , 0 , false , 6 , 0 ,"Mitraille l'ennemi avec 2 à 5 glaçons à la suite.", 0 , 333 ]
    $data_skills_pokemon[ 334 ]=["MUR DE FER", 0 , 16 , 0 , 15 , 0 , 0x33 , 10 , false , 6 , 0 ,"Endurcit le corps pour booster la Défense.", 0 , 334 ]
    $data_skills_pokemon[ 335 ]=["BARRAGE", 0 , 1 , 100 , 5 , 0 , 0x6A , 0 , false , 6 , 0 ,"Bloque la route de l'ennemi pour empêcher sa fuite.", 0 , 335 ]
    $data_skills_pokemon[ 336 ]=["GRONDEMENT", 0 , 1 , 0 , 40 , 0 , 0x0A , 10 , false , 6 , 0 ,"Grogne pour se rassurer et booster l'Attaque.", 0 , 336 ]
    $data_skills_pokemon[ 337 ]=["DRACOGRIFFE", 80 , 15 , 100 , 15 , 0 , 0x0 , 0 , true , 6 , 0 ,"Tranche l'ennemi avec des griffes acérées.", 0 , 337 ]
    $data_skills_pokemon[ 338 ]=["VEGE-ATTAK", 150 , 5 , 90 , 5 , 0 , 0x50 , 0 , false , 6 , 0 ,"Puissant, mais immobilise le lanceur le tour suivant.", 0 , 338 ]
    $data_skills_pokemon[ 339 ]=["GONFLETTE", 0 , 7 , 0 , 20 , 0 , 0xD0 , 10 , false , 6 , 0 ,"Se gonfle pour booster l'Attaque et la Défense.", 0 , 339 ]
    $data_skills_pokemon[ 340 ]=["REBOND", 85 , 10 , 85 , 5 , 30 , 0x9B2 , 0 , true , 6 , 0 ,"Saute et tombe le tour suivant. Peut paralyser.", 0 , 340 ]
    $data_skills_pokemon[ 341 ]=["TIR DE BOUE", 55 , 9 , 95 , 15 , 100 , 0x46 , 0 , false , 6 , 0 ,"Envoie de la boue à l'ennemi pour réduire sa Vitesse.", 0 , 341 ]
    $data_skills_pokemon[ 342 ]=["QUEUE-POISON", 50 , 8 , 100 , 25 , 10 , 0xD1 , 0 , true , 6 , 0 ,"Haut taux de critiques. Peut aussi empoisonner.", 0 , 342 ]
    $data_skills_pokemon[ 343 ]=["IMPLORE", 40 , 1 , 100 , 40 , 100 , 0x69 , 0 , false , 6 , 0 ,"Demande gentiment un objet détenu par l'ennemi.", 0 , 343 ]
    $data_skills_pokemon[ 344 ]=["ELECTACLE", 120 , 4 , 100 , 15 , 0 , 0xC6 , 0 , true , 6 , 0 ,"Une dangereuse charge qui blesse aussi le lanceur.", 0 , 344 ]
    $data_skills_pokemon[ 345 ]=["FEUILLEMAGIK", 60 , 5 , 0 , 20 , 0 , 0x11 , 0 , false , 6 , 0 ,"Envoie des feuilles ne pouvant être esquivées.", 0 , 345 ]
    $data_skills_pokemon[ 346 ]=["TOURNIQUET", 0 , 3 , 100 , 15 , 0 , 0xD2 , 10 , false , 6 , 0 ,"Mouille le lanceur et augmente sa résistance au Feu.", 0 , 346 ]
    $data_skills_pokemon[ 347 ]=["PLENITUDE", 0 , 11 , 0 , 20 , 0 , 0xD3 , 10 , false , 6 , 0 ,"Méditation pour augmenter l'Atq. Spé. et la Déf. Spé.", 0 , 347 ]
    $data_skills_pokemon[ 348 ]=["LAME-FEUILLE", 70 , 5 , 100 , 15 , 0 , 0x2B , 0 , true , 6 , 0 ,"Tranche avec une feuille. Taux de critiques élevé.", 0 , 348 ]
    $data_skills_pokemon[ 349 ]=["DANSE DRACO", 0 , 15 , 0 , 20 , 0 , 0xD4 , 10 , false , 6 , 0 ,"Danse mystique augmentant l'Attaque et la Vitesse.", 0 , 349 ]
    $data_skills_pokemon[ 350 ]=["BOULE ROC", 25 , 13 , 80 , 10 , 0 , 0x1D , 0 , false , 6 , 0 ,"Projette des rochers sur l'ennemi 2 à 5 fois de suite.", 0 , 350 ]
    $data_skills_pokemon[ 351 ]=["ONDE DE CHOC", 60 , 4 , 0 , 20 , 0 , 0x11 , 0 , false , 6 , 0 ,"Attaque électrique rapide et impossible à esquiver.", 0 , 351 ]
    $data_skills_pokemon[ 352 ]=["VIBRAQUA", 60 , 3 , 100 , 20 , 20 , 0x4C , 0 , false , 6 , 0 ,"Envoi d'ondes aquatiques. Peut rendre confus.", 0 , 352 ]
    $data_skills_pokemon[ 353 ]=["CARNAREKET", 120 , 16 , 85 , 5 , 0 , 0x94 , 0 , false , 6 , 0 ,"Récupère la lumière pour frapper 2 tours plus tard.", 0 , 353 ]
    $data_skills_pokemon[ 354 ]=["PSYCHO BOOST", 140 , 11 , 90 , 5 , 100 , 0xCC , 0 , false , 6 , 0 ,"Attaque à pleine puissance, mais baisse Atq. Spé.", 0 , 354 ]

  else

    # -------------------------------------------------------------
    # Conversion BDD -> Script
    # -------------------------------------------------------------
    $data_skills = load_data("Data/Skills.rxdata")
    $data_skills_pokemon = Array.new($data_skills.length)
    for id in 1..$data_skills.length-1
      skill = $data_skills[id]
      $data_skills_pokemon[id] = []
      # Nom
      $data_skills_pokemon[id].push(skill.name) 
      # Power(Base_damage) : Attaque + Evasion
      $data_skills_pokemon[id].push(skill.atk_f + skill.eva_f) 
      # Type (sans type: 0)
      if skill.element_set[0] != nil
        $data_skills_pokemon[id].push(skill.element_set[0]) # Type
      else
        $data_skills_pokemon[id].push(0)
      end
      # Accuracy
      $data_skills_pokemon[id].push(skill.hit) 
      # PPMax: Cout en SP
      $data_skills_pokemon[id].push(skill.sp_cost) 
      # Effect_chance: Défense physique
      $data_skills_pokemon[id].push(skill.pdef_f) 
      # ID de l'Effet: Taux d'effet
      $data_skills_pokemon[id].push(skill.power) # Effect id
      # Target
      case skill.scope 
      when 0
        $data_skills_pokemon[id].push(1) # Aucun
      when 1
        $data_skills_pokemon[id].push(0) # Un ennemi
      when 2
        $data_skills_pokemon[id].push(8) # Tous les ennemis
      when 3
        $data_skills_pokemon[id].push(4) # Un allié
      when 4
        $data_skills_pokemon[id].push(20) # Tous les alliés
      when 5
        $data_skills_pokemon[id].push(40) # Spécial (Allié mort)
      when 6
        $data_skills_pokemon[id].push(40) # Spécial (Alliés morts)
      when 7
        $data_skills_pokemon[id].push(10) # Utilisateur
      end
      # Tag attaque de contact: Variance != 0
      if skill.variance != 0
        $data_skills_pokemon[id].push(true)
      else
        $data_skills_pokemon[id].push(false)
      end
      # Tag priorité
      $data_skills_pokemon[id].push(skill.mdef_f)
      # Tag event commun
      #if skill.occasion == 0 # Carte et combat
        $data_skills_pokemon[id].push(skill.common_event_id)
      #else
      #  $data_skills_pokemon[id].push(0)
      #end
      # Description
      $data_skills_pokemon[id].push(skill.description) 
      # Animation utilisateur
      $data_skills_pokemon[id].push(skill.animation1_id) 
      # Animation cible
      $data_skills_pokemon[id].push(skill.animation2_id) 
      # Physique / Special / Status
      $data_skills_pokemon[id].push(skill.str_f) 
    end
  end
    
    #------------------------------------------------
    # $data_skills_pokemon[id] = [
    #   Nom,
    #   Base_damage,
    #   Type,
    #   Précision (en %),
    #   PPmax
    #   Special (en %),
    #   Special_id (id de fonction)
    #   Cible
    #   Tag
    #   Descr   
    #   Animation utilisateur
    #   Animation cible
    # ]
    #------------------------------------------------
    
  class Skill_Info
    def self.id(skill_name)
      for i in 1..$data_skills_pokemon.length-1
        if skill_name == name(i)
          return i
        end
      end
      return 1
    end
    
    def self.name(id)
      return $data_skills_pokemon[id][0]
    end
    
    def self.base_damage(id)
      return $data_skills_pokemon[id][1]
    end
    
    def self.type(id)
      return $data_skills_pokemon[id][2]
    end
    
    def self.accuracy(id)
      return $data_skills_pokemon[id][3]
    end
    
    def self.effect_chance(id)
      return $data_skills_pokemon[id][5]
    end
    
    def self.effect(id)
      return $data_skills_pokemon[id][6]
    end
    
    def self.target(id)
      return $data_skills_pokemon[id][7]
    end
    
    def self.tag(id)
      return $data_skills_pokemon[id][8]
    end
    
    def self.priority(id)
      return $data_skills_pokemon[id][9]
    end
    
    def self.map_use(id)
      return $data_skills_pokemon[id][10]
    end
    
    def self.description(id)
      return $data_skills_pokemon[id][11]
    end
    
    def self.user_anim_id(id)
      return $data_skills_pokemon[id][12]
    end
    
    def self.target_anim_id(id)
      return $data_skills_pokemon[id][13]
    end
    
    def self.kind(id)
      return $data_skills_pokemon[id][14]
    end
  end
    
  #------------------------------------------------
  # Class Skill - Skill individuel
  #------------------------------------------------
  class Skill
    attr_reader :id
    #attr_reader :target
    attr_reader :ppmax
    #attr_reader :effect
    #attr_reader :effect_chance
    attr_reader :physical #Type physique
    attr_reader :special  #Type special
    attr_accessor :pp
    attr_accessor :usable

    #------------------------------------------------
    # Initalize - Génération d'un Skill - Skill.new(id)
    #------------------------------------------------
    # $data_skills_pokemon[id] = [
    #   Nom,
    #   Base_damage,
    #   Type,
    #   Précision (en %),
    #   PPmax
    #   Special (en %),
    #   Special_id (id de fonction)
    #   Cible
    #   Tag
    #   Descr   
    #   Animation utilisateur
    #   Animation cible
    # ]
    #------------------------------------------------
    # target: 
    #   00: Opposant simple choisi
    #   01: Pas de cible
    #   04: Opposant simple choisi au hasard
    #   10: User
    #   20: Tous les non-users
    #   40: Special
    #------------------------------------------------
    def initialize(id)
      @id = id
      @ppmax = $data_skills_pokemon[id][4]
      @pp = @ppmax.to_i
      @usable = true
      define_type # Physique ou Special
    end
    
    def name
      return Skill_Info.name(id).capitalize
    end
    
    def power
      return Skill_Info.base_damage(id)
    end
    
    def type
      return Skill_Info.type(id)
    end
    
    def accuracy
      return Skill_Info.accuracy(id)
    end
    
    def effect_chance
      return Skill_Info.effect_chance(id)
    end
    
    def effect
      return Skill_Info.effect(id)
    end
    
    def target
      return Skill_Info.target(id)
    end
    
    def description
      return Skill_Info.description(id)
    end
    
    def user_anim_id
      return Skill_Info.user_anim_id(id)
    end
    
    def target_anim_id
      return Skill_Info.target_anim_id(id)
    end
    
    def direct?
      return Skill_Info.tag(id)
    end
    
    def priority
      return Skill_Info.priority(id)
    end
    
    def map_use
      return Skill_Info.map_use(id)
    end
    
    
    def physical
      return Skill_Info.kind(id) == 0 if ATTACKKIND
      return @physical
    end
    
    def special
      return Skill_Info.kind(id) == 1 if ATTACKKIND
      return @special
    end
    
    def status
      return Skill_Info.kind(id) == 2 if ATTACKKIND
      return false
    end
    
    #------------------------------------------------
    # Fonctions de définition
    #------------------------------------------------    
    #  1 Normal  2 Feu  3 Eau 4 Electrique 5 Plante 6 Glace 7 Combat 8 Poison 9 Sol
    #  10 vol 11 psy 12insecte 13 roche 14 spectre 15 dragon 16 acier 17 tenebre    
    
    def define_type
      p = [1, 7, 8, 9, 10, 12, 13, 14, 16]
      s = [0, 2, 3, 4, 5, 6, 11, 15, 17]
      if p.include?(type)
        @physical = true
        @special = false
      elsif s.include?(type)
        @physical = false
        @special = true
      end
    end
    
    def define_priority_useless
      @priority = 0
      if [270].include?(@id) #coupd'main/helping hand
        @priority = 5
      end
      if [277, 289].include?(@id) #reflet magique / magic coat #saisie / snatch
        @priority = 4
      end
      if [182, 197, 203, 266].include?(@id)
        @priority = 3
      end
      if [98, 183, 245, 252].include?(@id)
        @priority = 1
      end
      if [233].include?(@id) #corps perdu/vital throw
        @priority = -1
      end
      if [264].include?(@id) #Mitra poing/focus punch
        @priority = -3
      end
      if [279].include?(@id) #vendetta/revenge
        @priority = -4
      end
      if [68, 243].include?(@id) #riposte/counter #voile miroir/mirror coat
        @priority = -5
      end
      if [18, 46].include?(@id) #cyclone /whirlwind - hurlement/roar
        @priority = -6
      end      
    end
    
    #------------------------------------------------
    # Fonctions pp
    #------------------------------------------------    
    def refill
      @pp = @ppmax
    end
    
    def set_points(value)
      @pp = value
    end
    
    def add_ppmax(number)
      @ppmax += number
    end
    
    def define_ppmax(number)
      @ppmax = number
    end
    
    def raise_ppmax
      if @ppmax < $data_skills_pokemon[id][4]*8/5
        @ppmax += $data_skills_pokemon[id][4]/5
      end
    end
    
    #------------------------------------------------
    # Fonctions d'activation
    #------------------------------------------------    
    
    def use
      if @usable
        @pp -= 1
      end
    end
    
    def enabled?
      return @usable
    end
    
    def disable
      @usable = false
    end
    
    def enable
      @usable = true
    end
    
    def usable?
      if @pp <= 0
        return false
      end
      return @usable
    end
    
    #------------------------------------------------
    # Fonctions de simplification
    #------------------------------------------------    
    #  1 Normal  2 Feu  3 Eau 4 Electrique 5 Plante 6 Glace 7 Combat 8 Poison 9 Sol
    #  10 vol 11 psy 12insecte 13 roche 14 spectre 15 dragon 16 acier 17 tenebre    
    
    def type_normal?
      return type == 1
    end
    
    def type_fire?
      return type == 2
    end
    
    def type_water?
      return type == 3
    end
    
    def type_electric?
      return type == 4
    end
    
    def type_grass?
      return type == 5
    end
    
    def type_ice?
      return type == 6
    end
    
    def type_fighting?
      return type == 7
    end
    
    def type_poison?
      return type == 8
    end
    
    def type_ground?
      return type == 9
    end
    
    def type_fly?
      return type == 10
    end
    
    def type_psy?
      return type == 11
    end
    
    def type_insect?
      return type == 12
    end
    
    def type_rock?
      return type == 13
    end
    
    def type_ghost?
      return type == 14
    end
    
    def type_dragon?
      return type == 15
    end
    
    def type_steel?
      return type == 16
    end
    
    def type_dark?
      return type == 17
    end
    
  end  
end