#==============================================================================
# ■ Game_System
#------------------------------------------------------------------------------
# 　システム周りのデータを扱うクラスです。BGM などの管理も行います。このクラス
# のインスタンスは $game_system で参照されます。
#==============================================================================

class Game_System
  def battle_bgm
    if not $game_switches[3]
      if @battle_bgm1 == nil
        return $data_system.battle_bgm
      else
        return @battle_bgm1
      end
    else
      if @battle_bgm2 == nil
        return $data_system.battle_bgm
      else
        return @battle_bgm2
      end
    end
  end

  def battle_bgm=(battle_bgm)
    if not $game_switches[3]
      @battle_bgm1 = battle_bgm
    else
      @battle_bgm2 = battle_bgm
    end
  end

  def battle_end_me
    if @battle_end_me == nil
      return $data_system.battle_end_me
    else
      return @battle_end_me
    end
  end

  def battle_end_me=(battle_end_me)
    @battle_end_me = battle_end_me
  end
end
