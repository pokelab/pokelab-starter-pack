#==============================================================================
# ■ Game_Player
# Pokemon Script Project - Krosk 
# 18/07/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------

class Game_Player < Game_Character
  def update
    # ローカル変数に移動中かどうかを記憶
    last_moving = moving?
    # 移動中、イベント実行中、移動ルート強制中、
    # メッセージウィンドウ表示中のいずれでもない場合
    unless moving? or $game_system.map_interpreter.running? or
           @move_route_forcing or $game_temp.message_window_showing
      # 方向ボタンが押されていれば、その方向へプレイヤーを移動
      case Input.dir4
      when 2
        move_down
      when 4
        move_left
      when 6
        move_right
      when 8
        move_up
      end
    end
    # ローカル変数に座標を記憶
    last_real_x = @real_x
    last_real_y = @real_y
    super
    # キャラクターが下に移動し、かつ画面上の位置が中央より下の場合
    if @real_y > last_real_y and @real_y - $game_map.display_y > CENTER_Y
      # マップを下にスクロール
      $game_map.scroll_down(@real_y - last_real_y)
    end
    # キャラクターが左に移動し、かつ画面上の位置が中央より左の場合
    if @real_x < last_real_x and @real_x - $game_map.display_x < CENTER_X
      # マップを左にスクロール
      $game_map.scroll_left(last_real_x - @real_x)
    end
    # キャラクターが右に移動し、かつ画面上の位置が中央より右の場合
    if @real_x > last_real_x and @real_x - $game_map.display_x > CENTER_X
      # マップを右にスクロール
      $game_map.scroll_right(@real_x - last_real_x)
    end
    # キャラクターが上に移動し、かつ画面上の位置が中央より上の場合
    if @real_y < last_real_y and @real_y - $game_map.display_y < CENTER_Y
      # マップを上にスクロール
      $game_map.scroll_up(last_real_y - @real_y)
    end
    # 移動中ではない場合
    unless moving?
      # 前回プレイヤーが移動中だった場合
      if last_moving
        # 同位置のイベントとの接触によるイベント起動判定
        result = check_event_trigger_here([1,2])
        # 起動したイベントがない場合
        if result == false
          # デバッグモードが ON かつ CTRL キーが押されている場合を除き
          unless $DEBUG and Input.press?(Input::CTRL)
            rate = $random_encounter[0]
            if rand(2874) < rate * 16
              @encounter_count = 0
            else
              @encounter_count = 1
            end
          end
        end
      end
      # C ボタンが押された場合
      if Input.trigger?(Input::C)
        # 同位置および正面のイベント起動判定
        check_event_trigger_here([0])
        check_event_trigger_there([0,1,2])
        # Implémentation Surf
        if $game_map.passable?(front_tile[0],front_tile[1], 10 - $game_player.direction) and 
            terrain_tag != 7 and $game_map.terrain_tag(front_tile[0], front_tile[1]) == 7 and 
            not $game_system.map_interpreter.running?
          $game_temp.common_event_id = POKEMON_S::Skill_Info.map_use(POKEMON_S::Skill_Info.id("SURF"))
        end
      end
            # Implémentation chaussures de sport
      # Utilise l'interrupteur n°20 par défaut
      # Par défaut il faut appuyer sur la touche Shift pour utiliser les chaussures
      @name = @character_name if @name == nil     
      if Input.press?(Input::A) and $game_switches[20] == true
        if Input.press?(Input::UP) or Input.press?(Input::DOWN) or
            Input.press?(Input::RIGHT) or Input.press?(Input::LEFT)           
          if $game_map.passable?(front_tile[0],front_tile[1], 10 - $game_player.direction)
            $game_player.set_map_character(@name + "_sport", $game_player.direction)
            @move_speed = 5
          else         
            $game_player.set_map_character(@name, $game_player.direction) 
            @move_speed = 4         
          end         
        else
          $game_player.set_map_character(@name, $game_player.direction) 
          @move_speed = 4   
        end               
      else
        if @character_name.include?("_sport")
          $game_player.set_map_character(@name, $game_player.direction)
          @move_speed = 4
        end       
      end
    end
  end
  
  def increase_steps
    super
    unless @move_route_forcing
      $pokemon_party.increase_steps
      # Poison damage
      if $pokemon_party.steps % 4 == 0
        # スリップダメージチェック
        $pokemon_party.check_map_slip_damage
      end
      # Loyalty
      if $pokemon_party.steps % 512 == 0
        for pokemon in $pokemon_party.actors
          pokemon.loyalty += 1
          if pokemon.loyalty > 255
            pokemon.loyalty = 255
          end
        end
      end
    end
  end
  
  # Renvoie les coordonnées de la case devant le héros
  def front_tile
    xf = @x + (@direction == 6 ? 1 : @direction == 4 ? -1 : 0)
    yf = @y + (@direction == 2 ? 1 : @direction == 8 ? -1 : 0)
    return [xf, yf]
  end
  
  # Renvoie l'event
  def front_tile_event
    xf = front_tile[0]
    yf = front_tile[1]
    for event in $game_map.events.values
      if event.x == xf and event.y == yf
        return event
      end
    end
    return nil
  end
  
  # Détecte le nom de l'objet devant soi
  def front_name_detect(name)
    if front_tile_event != nil and front_tile_event.event.name == name
      return true
    end
    return false
  end
  
  # Renvoie l'ID de l'objet devant soi
  def front_tile_id
    if front_tile_event != nil
      return front_tile_event.event.id
    end
    return 0
  end
end