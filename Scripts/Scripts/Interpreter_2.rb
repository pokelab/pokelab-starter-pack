class Interpreter
  def nmbrCatched
    table = []
    if POKEMON_S._DEXREG
      for id in 1..$data_pokedex.length-1
        table[Pokemon_Info.id_bis(id)] = id
      end
      table.shift # débarasser l'élément 0
      table.compact!
    else
    # National
      for id in 1..$data_pokedex.length-1
        table.push(id)
      end
    end
    
    list = []
    for i in 0..table.length-1
      if $data_pokedex[table[i]][0]
        list.push(table[i]) # liste des ID vus
      end
    end
    $game_variables[13] = 0
    for element in list
      if $data_pokedex[element][1]
        $game_variables[13] += 1
      end
    end
  end
end