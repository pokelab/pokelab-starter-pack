#==============================================================================
# ● Base de données - Pokémons sauvages
# Pokemon Script Project - Krosk 
# 29/09/07
#==============================================================================


module POKEMON_S
  $data_encounter = []
  $data_encounter[0] = [0, 0, [0], 0]
  
  # id = Groupe de monstre
  #$data_encounter[id] = [
  #tag terrain, 
  #tag jour/nuit, 0 = jour/nuit, J = jour, N = nuit
  #[
  #  écart, 
  #  [Pokémon 1, niveau, rareté locale], 
  #  [Pokémon 2, niveau, rareté locale], 
  #  [Pokémon 3, niveau, rareté locale], 
  #  [Pokémon 4, niveau, rareté locale], 
  #  [Pokémon 5, niveau, rareté locale], 
  #  [Pokémon 6, niveau, rareté locale]
  #],
  # condition numéro de switch, 0 si rien
  #]
  
  $data_troops       = load_data("Data/Troops.rxdata")
  for i in 1..$data_troops.length-1
    $data_encounter[i] = []
    name = $data_troops[i].name
    data = (name.split('/'))[0]
    if data == nil # Sans nom
      $data_encounter[i] = [0, 0, [0], 0]
      next
    end
    data = data.split(',')
    
    # Tag terrain
    t_tag = data[0].to_i
    if [1,2,3,4,5,6,7].include?(t_tag)
      $data_encounter[i][0] = t_tag
    else
      $data_encounter[i] = $data_encounter[0]
      next
    end
    
    # Tag jour/nuit
    if data[1] != nil
      case data[1]
      when "J" # Jour
        dn_tag = 1
      when "N" # Nuit
        dn_tag = 2
      else # Indifferent
        dn_tag = 0
      end
    else # Indifferent
      dn_tag = 0
    end
    $data_encounter[i][1] = dn_tag
    
    # Ennemi
    list = [3] # Ecart de 3 niveaux par défaut
    for member in $data_troops[i].members
      list.push([member.enemy_id])
    end
    
    # Vérification page event
    if $data_troops[i].pages == []
      # Effacage car invalide
      $data_encounter[i] = [0, 0, [0], 0]
      next
    end
    
    # Event Page1
    event_list = $data_troops[i].pages[0].list
    
    # Effacage si aucun event
    if event_list == []
      $data_encounter[i] = [0, 0, [0], 0]
      next
    end
    
    # Condition par switch
    if $data_troops[i].pages[0].condition.switch_valid
      $data_encounter[i][3] = $data_troops[i].pages[0].condition.switch_id
    else
      $data_encounter[i][3] = 0
    end
    
    for event in event_list
      case event.code
      when 331 # Ajout de PV => Ecart level
        target = event.parameters[0]
        operation = event.parameters[1]
        operand_type = event.parameters[2]
        operand = event.parameters[3]
        if operand_type == 0
          value = operand
        else
          if $game_variables == nil or $game_variables[operand] == nil
            value = 5
          else
            value = $game_variables[operand]
          end
        end
        
        value = value.abs
        
        case target
        when -1 # Tous les ennemis => ecart
          list[0] = -value
        end
      when 332 # Ajout de MP => Définition Level
        target = event.parameters[0]
        operation = event.parameters[1]
        operand_type = event.parameters[2]
        operand = event.parameters[3]
        if operand_type == 0
          value = operand
        else
          if $game_variables == nil or $game_variables[operand] == nil
            value = 5
          else
            value = $game_variables[operand]
          end
        end
        
        value = value.abs
        
        case target
        when -1 # Tous les ennemis
          for element in list
            element[1] = value
          end
        else # Ennemi défini
          if list[target+1] != nil
            list[target+1][1] = value
          end
        end
      when 338 # Rareté locale
        target = event.parameters[1]
        operand_type = event.parameters[2]
        operand = event.parameters[3]
        if operand_type == 0
          value = operand
        else
          if $game_variables[operand] == nil
            value = 0
          else
            value = $game_variables[operand]
          end
        end
        value = value.abs
        
        if target != -1 and list[target+1] != nil
          list[target+1][2] = value
        end
        
      # Ecriture script:
      #   Ecart
      #   [ Niveau, rareté locale ]
      #   [ Niveau, rareté locale ]
      #   [ Niveau, rareté locale ]
      #   etc
      when 355 # Script
        index_script = 0
        script = event.parameters[0]
        if eval(script).type == Fixnum
          list[0] = eval(script)
        end
      when 655 # Script suite
        index_script += 1
        script = event.parameters[0]
        if eval(script).type == Array and list[index_script] != nil
          list[index_script][1] = eval(script)[0]
          list[index_script][2] = eval(script)[1]
        end
      end
    end
        
    # Vérification: expulsion des Pokémons sans niveau
    for pokemon in list
      if pokemon.type != Array
        next
      end
      if pokemon[1] == nil or pokemon[0] == nil
        index = list.index(pokemon)
        list[index] = nil
      end
    end
    
    list.compact!
    
    $data_encounter[i][2] = list
  end
  
  
  map_infos = load_data("Data/MapInfos.rxdata")
  map_infos.each { |key, value|
    zone = (value.name.split('/'))
    if zone.length == 1
      next
    end
    zone = eval(zone[0])
    if zone.type == Array
      zone = zone[0]
    end
    map = load_data(sprintf("Data/Map%03d.rxdata", key))
    for id in map.encounter_list
      for pokemon in $data_encounter[id][2]
        if pokemon.type == Array
          if $data_pokemon[pokemon[0]][9][4] == nil
            $data_pokemon[pokemon[0]][9][4] = []
          end
          $data_pokemon[pokemon[0]][9][4].push(zone)
          $data_pokemon[pokemon[0]][9][4].uniq!
        end
      end
    end
  }
  
  
  
end