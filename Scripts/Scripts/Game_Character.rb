#==============================================================================
# ■ Game_Character
# Pokemon Script Project - Krosk 
# 10/11/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------

class Game_Character
  def passable?(x, y, d)
    # 新しい座標を求める
    new_x = x + (d == 6 ? 1 : d == 4 ? -1 : 0)
    new_y = y + (d == 2 ? 1 : d == 8 ? -1 : 0)
    # 座標がマップ外の場合
    unless $game_map.valid?(new_x, new_y)
      # 通行不可
      return false
    end
    # すり抜け ON の場合
    if @through
      # 通行可
      return true
    end
    # 移動元のタイルから指定方向に出られない場合
    unless $game_map.passable?(x, y, d, self)
      # 通行不可
      return false
    end
    # 移動先のタイルに指定方向から入れない場合
    unless $game_map.passable?(new_x, new_y, 10 - d, self)
      # 通行不可
      return false
    end
    # 全イベントのループ
    for event in $game_map.events.values
      # イベントの座標が移動先と一致した場合
      if event.x == new_x and event.y == new_y
        # すり抜け OFF なら
        unless event.through
          # 自分がイベントの場合
          if self != $game_player
            # 通行不可
            return false
          end
          # 自分がプレイヤーで、相手のグラフィックがキャラクターの場合
          if event.character_name != ""
            # 通行不可
            return false
          end
        end
      end
    end
    # プレイヤーの座標が移動先と一致した場合
    if $game_player.x == new_x and $game_player.y == new_y
      # すり抜け OFF なら
      unless $game_player.through
        # 自分のグラフィックがキャラクターの場合
        if @character_name != ""
          # 通行不可
          return false
        end
      end
    end
    # 通行可
    return true
  end
end
# Implémentation Surf