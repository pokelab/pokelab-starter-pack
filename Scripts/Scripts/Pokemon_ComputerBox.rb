#==============================================================================
# ¦ Pokemon_Computer
# Pokemon Script Project - Krosk 
# 20/07/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------
# Interface PC
#-----------------------------------------------------------------------------

module POKEMON_S
  class Pokemon_Computer
    #--------------------------------------------------------------------------
    #
    #--------------------------------------------------------------------------
    def initialize(window_active = 0, menu_index = 0)
      @window_active = window_active # 0 = PC, 1 = PC STOCK, 2 = PC PERSO
      @menu_index = menu_index
    end
    #--------------------------------------------------------------------------
    # 
    #--------------------------------------------------------------------------
    def main
      @spriteset = Spriteset_Map.new
      s1 = "PC DE STOCKAGE"
      s2 = "PC DE #{Player.name}"
      s3 = "DECONNEXION"
      @command_window = Window_Command.new(260, [s1, s2, s3], $fontsize)
      @command_window.x = 3
      @command_window.y = 3
      
      s1 = "DEPLACER POKéMON"
      s2 = "CREER UNE BOITE"
      s3 = "ETEINDRE"
      @store_window = Window_Command.new(260, [s1, s2, s3], $fontsize)
      @store_window.x = 3
      @store_window.y = 3
      
      s1 = "STOCKAGE OBJET"      
      s2 = "RETIRER OBJET"  
      s3 = "ETEINDRE"  
      @item_window = Window_Command.new(260, [s1, s2,s3], $fontsize)
      @item_window.x = 3
      @item_window.y = 3
      
      @message_window = POKEMON_S::Pokemon_Window_Help.new
      
      case @window_active
      when 0
        @store_window.visible = false
        @store_window.active = false
        @item_window.visible = false
        @item_window.active = false
      when 1
        @item_window.visible = false
        @item_window.active = false
        @command_window.visible = false
        @command_window.active = false
      when 2
        @command_window.visible = false
        @command_window.active = false
        @store_window.visible = false
        @store_window.active = false
      end
      
      Graphics.transition
      loop do
        Graphics.update
        Input.update
        update
        if $scene != self
          break
        end
      end
      Graphics.freeze
      @command_window.dispose
      @store_window.dispose
      @item_window.dispose
      @message_window.dispose
      @spriteset.dispose
    end
    
    def update
      @command_window.update
      @item_window.update
      @store_window.update
      @spriteset.update
      if @command_window.active
        update_command
        return
      end
      if @item_window.active
        update_item
        return
      end
      if @store_window.active
        update_store
        return
      end
      @message_window.update
    end
    
    def update_command
      @message_window.draw_text("Accéder à quel PC?")
      
      if Input.trigger?(Input::B)
        $game_system.se_play($data_system.cancel_se)
        $scene = Scene_Map.new
        return
      end
      # C ??????????
      if Input.trigger?(Input::C)
        # ???????????????????
        case @command_window.index
        when 0
          if $pokemon_party.size == 0
            $game_system.se_play($data_system.buzzer_se)
            return
          end
          $game_system.se_play($data_system.decision_se)
          @command_window.visible = false
          @command_window.active = false
          @store_window.visible = true
          @store_window.active = true
        when 1  # ??
          $game_system.se_play($data_system.decision_se)
          @command_window.visible = false
          @command_window.active = false
          @item_window.visible = true
          @item_window.active = true
        when 2  # ???
          $game_system.se_play($data_system.decision_se)
          $scene = Scene_Map.new
          return
        end
        return
      end
    end
    
    def update_store
      case @store_window.index
      when 0
        @message_window.draw_text("Organiser les POKéMON dans les BOITES et dans l'équipe.")
      when 1
        @message_window.draw_text("Créer une nouvelle BOITE.")
      when 2
        @message_window.draw_text("Retour au menu précédent.")
      end

      if Input.trigger?(Input::B)
        for i in $pokemon_party.actors
          ok = true if not i.dead?()
        end
        if ok
          $game_system.se_play($data_system.decision_se)
          @store_window.visible = false
          @store_window.active = false
          @command_window.visible = true
          @command_window.active = true
        else
          $game_system.se_play($data_system.buzzer_se)
           @message_window.draw_text("Vous n'avez aucun Pokémon apte !")
           active = false
           wait_hit
           wait(10)
           active = true
        end
      end

      if Input.trigger?(Input::C)
        case @store_window.index
        when 0
          $game_system.se_play($data_system.decision_se)
          $scene = POKEMON_S::Pokemon_Box.new
        when 1
          $game_system.se_play($data_system.decision_se)
          $pokemon_party.create_box
        when 2
          for i in $pokemon_party.actors
            ok = true if not i.dead?()
          end
          if ok
            $game_system.se_play($data_system.decision_se)
            @store_window.visible = false
            @store_window.active = false
            @command_window.visible = true
            @command_window.active = true
          else
            $game_system.se_play($data_system.buzzer_se)
             @message_window.draw_text("Vous n'avez aucun Pokémon apte !")
             active = false
             wait_hit
             wait(10)
             active = true
          end
        end
        return
      end
    end
    
    def update_item
      @message_window.draw_text("Que voulez-vous faire?")
      
      if Input.trigger?(Input::B)
        $game_system.se_play($data_system.decision_se)
        @item_window.visible = false
        @item_window.active = false
        @command_window.visible = true
        @command_window.active = true
      end
      
      if Input.trigger?(Input::C)
        
    case @item_window.index        when 0  
              $game_system.se_play($data_system.decision_se)  
              $scene = Pokemon_Item_Bag_Stock.new  
            when 1  
              $game_system.se_play($data_system.decision_se)  
              $scene = Pokemon_Item_Bag_Retirer.new  
              when 2  
                $game_system.se_play($data_system.decision_se)  
              @item_window.visible = false  
              @item_window.active = false  
              @command_window.visible = true  
              @command_window.active = true  
              return  
            end  
      end
    end
	
	def wait_hit
      loop do
        Graphics.update
        Input.update
        if Input.trigger?(Input::C)
          $game_system.se_play($data_system.decision_se)
          break
        end
      end
    end
    
    def wait(frame)
      i = 0
      loop do
        i += 1
        Graphics.update
        Input.update
        if i >= frame
          break
        end
      end
    end
	
  end
  
#==============================================================================
# ¦ Pokemon_Box
# Pokemon Script Project - Krosk 
# 20/07/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------
# Interface Menu de stockage
#-----------------------------------------------------------------------------
# $data_storage[0] : informations générales et noms
#    id = 1..xx
# $data_storage[0][id] = [ nom, attribut ]
# $data_storage[id] = boite
#-----------------------------------------------------------------------------

  class Pokemon_Box
    def initialize(mode = 0, index = 0)
      #mode: 1 Party , 2 Box
      @z_level = 0
      
      # 2 images de background: gauche, et fond
      @background = Plane.new(Viewport.new(0,0,640,480))
      @background_left = Sprite.new
      @background_left.z = @z_level + 1
      
      # Box status: boite d'affichage info Pokémon
      @box_status = POKEMON_S::Pokemon_Box_Status.new
      # Box party: boite affichage contenu équipe
      @box_party = POKEMON_S::Pokemon_Box_Party.new(0)
      # Box viewer: contenu boite
      @box_viewer = POKEMON_S::Pokemon_Box_Viewer.new(1, -1)
      # Box_name: boite nom
      @box_name = Window_Base.new(332-16, 14-16, 304+32, 66+32)
      @box_name.opacity = 0
      @box_name.contents = Bitmap.new(304, 66)
      @box_name.contents.font.color = Color.new(0,0,0)
      @box_name.contents.font.size = $fontsizebis+5
      @box_name.contents.font.name = $fontfacebis
      @box_name.active = false
      @box_name.z = @z_level + 6
      
      @box_arrow = Sprite.new
      @box_arrow.bitmap = RPG::Cache.picture("boxarrow.png")
      @box_arrow.opacity = 0
      @box_arrow.x = 332
      @box_arrow.y = 80
      @box_arrow.z = @z_level + 6
      
      s1 = "TRANSFERER"
      s2 = "RESUME"
      s3 = "DEPLACER"
      s4 = "RELACHER"
      s5 = "ANNULER"
      @command_window = Window_Command.new(220, [s1,s2,s3,s4,s5])
      @command_window.x = 640 - 220 - 3
      @command_window.y = 280
      @command_window.z = @z_level + 10
      @command_window.visible = false
      @command_window.active = false
      
      @temp_index = index
      @mode = mode # 1: Party 2: Box
      @selected = nil #selected = [box, index]
    end
    
    def main
      @background.bitmap = RPG::Cache.picture("black.png")
      Graphics.transition
      Graphics.freeze
      @background.bitmap = RPG::Cache.picture("boxbackc.png")
      @background_left.bitmap = RPG::Cache.picture("boxleft.png")
      
      @box_party.active = true
      @box_viewer.active = false
      @box_viewer.refresh
      @box_party.refresh
      refresh_box_name
      
      Graphics.transition(20, "Graphics/Transitions/computertr.png")
      loop do
        background_move
        Graphics.update
        Input.update
        update
        if $scene != self
          break
        end
      end
      Graphics.freeze
      @command_window.dispose
      @background.dispose
      @background_left.dispose
      @box_status.dispose
      @box_party.dispose
      @box_viewer.dispose
      @box_name.dispose
      @box_arrow.dispose
      Graphics.transition(20, "Graphics/Transitions/computertrclose.png")
      Graphics.freeze
    end
    
    def background_move
      @background.ox += 1
      @background.oy -= 1
    end
    
    def update
      # Box viewer -> Box Party
      if @box_viewer.active and Input.repeat?(Input::LEFT) and (@box_viewer.index)%4 == 0
        $game_system.se_play($data_system.cursor_se)
        @box_viewer.active = false
        @box_party.active = true
        @box_party.index = @box_viewer.index/4
        until $pokemon_party.actors[@box_party.index] != nil
          @box_party.index -= 1
        end
        # Refresh obligatoire?
        @box_viewer.refresh
        @box_party.refresh
        return
      end
      
      # Box party -> Box viewer
      if @box_party.active and Input.repeat?(Input::RIGHT)
        $game_system.se_play($data_system.cursor_se)
        @box_party.active = false
        @box_viewer.active = true
        @box_viewer.index = @box_party.index * 4
        @box_party.refresh
        @box_viewer.refresh
        return
      end
      
      # Box viewer -> Box name
      if @box_viewer.active and Input.repeat?(Input::UP) and @box_viewer.index < 4
        $game_system.se_play($data_system.cursor_se)
        @box_viewer.active = false
        @box_name.active = true
        @box_viewer.index = -1
        @box_viewer.refresh
        refresh_box_name
        return
      end
      
      # En sélection
      if Input.repeat?(Input::DOWN) and @box_party.active and @selected != nil and @box_party.index >= $pokemon_party.size-1
        if @selected[0] != 0
          $game_system.se_play($data_system.cursor_se)
          @box_party.index = $pokemon_party.size
          @box_party.refresh
        end
        return
      end
      
      # Rafraichissement boite de statut
      if @box_party.active
        @box_party.update
        @box_status.refresh(@box_party.pokemon_pointed)
      end
      if @box_viewer.active
        @box_viewer.update
        @box_status.refresh(@box_viewer.pokemon_pointed)
      end
      
      # Update @box_name
      if @box_name.active
        @box_arrow.opacity = 255
        # box_name -> bow_viewer
        if Input.repeat?(Input::DOWN)
          $game_system.se_play($data_system.cursor_se)
          @box_name.active = false
          @box_viewer.index = 0
          @box_viewer.active = true
          @box_viewer.refresh
          refresh_box_name
          return
        end
        
        # Navigation entre boites
        if Input.repeat?(Input::RIGHT)
          Audio.se_play("Audio/SE/Pokeopenbreak.wav")
          @box = @box_viewer.box
          if $data_storage[@box+1] != nil
            @box_viewer.box += 1
          else
            @box_viewer.box = 1
          end
          refresh_box_name
          @box_viewer.refresh
        end
        if Input.repeat?(Input::LEFT)
          Audio.se_play("Audio/SE/Pokeopenbreak.wav")
          @box = @box_viewer.box
          if @box == 1
            @box_viewer.box = $data_storage.length-1
          else
            @box_viewer.box -= 1
          end
          refresh_box_name
          @box_viewer.refresh
        end
      else
        @box_arrow.opacity = 0
      end
      
      # Selection pokemon dans équipe -> mode = 1
      if @box_party.active and Input.trigger?(Input::C)
        if @selected == nil
          @box_party.active = false
          @mode = 1
          @command_window.index = 0
          @command_window.active = true
          @command_window.visible = true
          return
        else
          if @selected[0] == 0
            $pokemon_party.switch_party(@box_party.index, @selected[1])
          else
            $pokemon_party.switch_storage_party(@box_party.index, @selected[1], @selected[0])
          end
          @selected = nil
          @box_party.selected(@selected)
          @box_viewer.selected(@selected)
          @box_party.refresh
          @box_viewer.refresh
          return
        end
      end
      
      # Selection pokemon dans boite -> mode = 2 
      if @box_viewer.active and Input.trigger?(Input::C)
        if @selected == nil
          @box_viewer.active = false
          @mode = 2
          @command_window.index = 0
          @command_window.active = true
          @command_window.visible = true
          return
        else
          if @selected[0] == 0
            $pokemon_party.switch_storage_party(@selected[1], @box_viewer.index, @box)
            #if @box_party.index == $pokemon_party.size
            #  @box_party.index = $pokemon_party.size - 1
            #end
          else
            $pokemon_party.switch_storage(@box_viewer.index, @selected[1], @box, @selected[0])
          end
          @selected = nil
          @box_party.selected(@selected)
          @box_viewer.selected(@selected)
          @box_viewer.refresh
          @box_party.refresh
          return          
        end
      end
      
      # Selection boite options
      if @box_name.active and Input.trigger?(Input::C)
        return
      end
      
      # Update commande
      if @command_window.active
        update_command_window
      end
      
      # Annulation
      if Input.trigger?(Input::B)
        $game_system.se_play($data_system.decision_se)
        @command_window.active = false
        @command_window.visible = false
        if @selected != nil
          if @selected[0] == 0
            @box_party.index = @selected[1]
            @box_party.active = true
            @box_viewer.active = false
          elsif @selected[0] != 0
            @box_viewer.index = @selected[1]
            @box_viewer.box = @selected[0]
            @box_party.active = false
            @box_viewer.active = true
          end
          @selected = nil
          @box_viewer.selected(@selected)
          @box_party.selected(@selected)
          @box_viewer.refresh
          @box_party.refresh
          refresh_box_name
          return
        end
        case @mode
        when 0 # Quitter le PC
          $game_system.se_play($data_system.decision_se)
          $scene = POKEMON_S::Pokemon_Computer.new(1, 1)
        when 1 # Retour
          @mode = 0
          @box_party.active = true
          @box_party.visible = true
        when 2 # Retour
          @mode = 0
          @box_viewer.active = true
          @box_viewer.visible = true
        end
        return
      end
    end
    
    # Refresh du nom de la boite
    def refresh_box_name
      @box = @box_viewer.box
      name = $data_storage[0][@box][0]
      @box_name.contents.clear
      @box_name.contents.font.color = Color.new(0,0,0)
      @box_name.contents.font.bold = true
      @box_name.contents.draw_text(4,4, 304, 66, name, 1)
      @box_name.contents.font.color = Color.new(255,255,255)
      @box_name.contents.draw_text(2,2, 304, 66, name, 1)
    end
    
    
    def update_command_window
      @command_window.update
      @box = @box_viewer.box
      if @box_viewer.pokemon_pointed == nil and @mode == 2
        @command_window.disable_item(1)
      else
        @command_window.enable_item(1)
      end
      if ($pokemon_party.size == 1 and @mode == 1) or (@mode == 2 and @box_viewer.pokemon_pointed == nil)
        @command_window.disable_item(0)
        @command_window.disable_item(2)
        @command_window.disable_item(3)
      elsif ($pokemon_party.size == 6 and @mode == 2) or ($pokemon_party.box_full?(@box) and @mode == 1)
        @command_window.disable_item(0)
        @command_window.enable_item(2)
        @command_window.enable_item(3)
      else
        @command_window.enable_item(0)
        @command_window.enable_item(2)
        @command_window.enable_item(3)
      end
      
      # Mode pokémon sélectionné dans l'équipe
      if Input.trigger?(Input::C) and @mode == 1
        case @command_window.index
        when 0 # Transférer
          if $pokemon_party.size == 1
            $game_system.se_play($data_system.buzzer_se)
            return
          end
          if $pokemon_party.box_full?(@box)
            $game_system.se_play($data_system.buzzer_se)
            return
          end
          id = @box_party.index #indique la place du pokémon dans l'equipe
          id_store = $pokemon_party.store(id, @box)
          $game_system.se_play($data_system.decision_se)
          @command_window.active = false
          @command_window.visible = false
          @box_viewer.active = true
          @box_viewer.index = id_store
          @mode = 0
          
        when 1 # Résumé
          $game_system.se_play($data_system.decision_se)
          # Données [@mode, @box_party.index]
          scene = POKEMON_S::Pokemon_Status.new(@box_party.pokemon_pointed, -1, @z_level + 100, [@mode, @box_party.index])
          scene.main
          Graphics.transition
          return
        when 2 # Déplacer
          if $pokemon_party.size == 1
            $game_system.se_play($data_system.buzzer_se)
            return
          end          
          @selected = [0, @box_party.index]
          @box_party.selected(@selected)
          $game_system.se_play($data_system.decision_se)
          @command_window.active = false
          @command_window.visible = false
          @box_party.active = true
          @mode = 0
          @box_party.refresh
          return
          
        when 3 # Relacher
          if $pokemon_party.size == 1
            $game_system.se_play($data_system.buzzer_se)
            return
          end
          id = @box_party.index
          $game_system.se_play($data_system.decision_se)
          $pokemon_party.remove_id(id)
          @box_party.active = true
          @command_window.active = false
          @command_window.visible = false
          @mode = 0
          
          if $pokemon_party.actors[@box_party.index] == nil
            @box_party.index = $pokemon_party.size-1
          end
            
        when 4 # Annuler
          $game_system.se_play($data_system.decision_se)
          @command_window.active = false
          @command_window.visible = false
          @mode = 0
          @box_party.active = true
        end
        
        @box_party.refresh
        @box_viewer.refresh
        return
      end
      
      # Mode pokémon sélectionné dans la box
      if Input.trigger?(Input::C) and @mode == 2
        case @command_window.index
        when 0 # Transférer
          id = @box_viewer.index
          if $data_storage[@box][id] == nil or $pokemon_party.size == 6
            $game_system.se_play($data_system.buzzer_se)
            return
          end
          $pokemon_party.retrieve(id, @box)
          $game_system.se_play($data_system.decision_se)
          @command_window.active = false
          @command_window.visible = false
          @box_party.active = true
          @box_party.index = $pokemon_party.size - 1
          @mode = 0
          
        when 1 # Résumé
          id = @box_viewer.index
          if $data_storage[@box][id] == nil
            $game_system.se_play($data_system.buzzer_se)
            return
          end
          $game_system.se_play($data_system.decision_se)
          scene = POKEMON_S::Pokemon_Status.new(@box_viewer.pokemon_pointed, -1, @z_level + 100, [@mode, @box_viewer.index])
          scene.main
          Graphics.transition
          return
        when 2 # Deplacer
          id = @box_viewer.index
          if $data_storage[@box][id] == nil
            $game_system.se_play($data_system.buzzer_se)
            return
          end
          @selected = [@box, @box_viewer.index]
          @box_viewer.selected(@selected)
          $game_system.se_play($data_system.decision_se)
          @command_window.active = false
          @command_window.visible = false
          @mode = 0
          @box_viewer.active = true
          @box_viewer.refresh
          return
          
        when 3 # Relacher
          id = @box_viewer.index
          if $data_storage[@box][id] == nil
            $game_system.se_play($data_system.buzzer_se)
            return
          end
          $game_system.se_play($data_system.decision_se)
          $data_storage[@box][id] = nil
          @box_viewer.active = true
          @mode = 0
          @command_window.active = false
          @command_window.visible = false
          
        when 4 # Annuler
          $game_system.se_play($data_system.decision_se)
          @command_window.active = false
          @command_window.visible = false
          @mode = 0
          @box_viewer.active = true
        end
        @box_party.refresh
        @box_viewer.refresh
        return
      end
    end
  end
  
#==============================================================================
# ¦ Pokemon_Box_Status
# Pokemon Script Project - Krosk 
# 20/07/07
#==============================================================================
# Fenêtre d'affichage info Pokémon
#==============================================================================
  class Pokemon_Box_Status < Window_Base
    def initialize
      super(0 - 16, 0 - 16, 210 + 32, 480 + 32)
      self.contents = Bitmap.new(210, 480)
      self.contents.font.name = $fontface
      self.contents.font.size = $fontsizebig
      #self.contents.font.bold = true
      self.opacity = 0
      self.z = 5
      @pokemon = nil
      @pokemon_sprite = Sprite.new
      @pokemon_sprite.mirror = true
      @pokemon_sprite.x = 30
      @pokemon_sprite.y = 72
      @pokemon_sprite.z = 6
    end
    
    def dispose
      super
      @pokemon_sprite.dispose
    end
    
    def define_pokemon(pokemon)
      @pokemon = pokemon
    end
    
    def pointed_pokemon
      return @pokemon
    end
    
    def refresh(pokemon)
      if pokemon != nil and @pokemon != pokemon
        self.contents.clear
        @pokemon = pokemon
        @pokemon_sprite.bitmap = RPG::Cache.battler(@pokemon.battler_face, 0)
        self.draw_text(9, 278, 192, 39, @pokemon.given_name)
        self.draw_text(3, 278 + 32 + 4, 204, 39, "/" + @pokemon.name)
        draw_gender(18, 278 + 64 + 12, @pokemon.gender)
        self.draw_text(18 + 64, 278 + 64 + 8, 170, 39, "N. " + @pokemon.level.to_s)
      elsif pokemon == nil and @pokemon != pokemon
        self.contents.clear
        @pokemon = pokemon
        @pokemon_sprite.bitmap = RPG::Cache.battler("", 0)
      end
    end
    
    def draw_gender(x, y, gender)
      if gender == 1
        rect = Rect.new(0, 0, 18, 33)
        bitmap = RPG::Cache.picture("Male.png")
        self.contents.blt(x, y, bitmap, rect, 255)
      end
      if gender == 2
        rect = Rect.new(0, 0, 18, 33)
        bitmap = RPG::Cache.picture("Female.png")
        self.contents.blt(x, y, bitmap, rect, 255)        
      end
    end
    
  end

#==============================================================================
# ¦ Pokemon_Box_Party
# Pokemon Script Project - Krosk 
# 20/07/07
#==============================================================================
# Fenêtre affichage équipe
#==============================================================================

  class Pokemon_Box_Party < Window_Base
    def initialize(index)
      super(237 - 16, 104 - 16, 64 + 32, 336 + 16 + 32)
      self.contents = Bitmap.new(64, 336 + 16)
      self.opacity = 0
      self.z = 5
      @index = index
      @pokemon = $pokemon_party.actors[0]
      @selected = nil
    end
    
    def index=(index)
      @index = index
    end
    
    def index
      return @index
    end
    
    def pokemon_pointed
      return @pokemon
    end
    
    def selected(data)
      @selected = data
    end

    def update
      if Input.repeat?(Input::DOWN)
        if @index == $pokemon_party.size-1
          return
        end
        $game_system.se_play($data_system.cursor_se)
        @index += 1
        refresh
        return
      end
      if Input.repeat?(Input::UP)
        if (@index == 0)
          return
        end
        $game_system.se_play($data_system.cursor_se)
        @index -= 1
        refresh
        return
      end
    end
    
    def refresh
      self.contents.clear
      for i in 0..$pokemon_party.size-1
        pokemon = $pokemon_party.actors[i]
        src_rect = Rect.new(0, 0, 64, 64)
        if i == @index
          @pokemon = pokemon
        end
        if i == @index and self.active
          bitmap = RPG::Cache.picture("boxiconl.png")
        else
          bitmap = RPG::Cache.picture("boxicon.png")
        end
        if @selected != nil
          if @selected[0] == 0 and i == @selected[1]
            bitmap = RPG::Cache.picture("boxiconm.png")
          end
        end
        self.contents.blt(2, 16 + 58*i, bitmap, src_rect, 255)
        
        src_rect = Rect.new(0, 0, 64, 64)
        bitmap = RPG::Cache.battler(pokemon.icon, 0)
        self.contents.blt(0, 58*i - 2, bitmap, src_rect, 255)
      end
      if @index == $pokemon_party.size  and self.active
        i = @index
        src_rect = Rect.new(0, 0, 64, 64)
        bitmap = RPG::Cache.picture("boxiconl.png")
        self.contents.blt(2, 16 + 58*i, bitmap, src_rect, 255)
      end
    end
  end  
  
#==============================================================================
# ¦ Pokemon_Box_Viewer
# Pokemon Script Project - Krosk 
# 20/07/07
#==============================================================================  
# Fenêtre affichage contenu boite
#==============================================================================  
  
  class Pokemon_Box_Viewer < Window_Base
    attr_accessor :box
    
    def initialize(box, index)
      super(328 + 22 - 16, 118 - 16 - 16, 268 + 32, 340 + 16 + 32)
      self.contents = Bitmap.new(268, 340 + 16)
      self.opacity = 0
      self.z = 3
      @box = box
      @index = index
      @pokemon = nil
      @box_sprite = Sprite.new
      @box_sprite.x = 328
      @box_sprite.z = 1
      @selected = nil
    end
    
    def dispose
      super
      @box_sprite.dispose
    end
    
    def index=(index)
      @index = index
    end
    
    def index
      return @index
    end
    
    def selected(data)
      @selected = data
    end
    
    def update
      if Input.repeat?(Input::DOWN)
        if @index >= 20
          return
        end
        $game_system.se_play($data_system.cursor_se)
        @index += 4
        refresh
        return
      end
      if Input.repeat?(Input::UP)
        $game_system.se_play($data_system.cursor_se)
        @index -= (@index <= 3) ? 0 : 4
        refresh
        return
      end
      if Input.repeat?(Input::LEFT)
        $game_system.se_play($data_system.cursor_se)
        if @index%4 == 0
          refresh
          return
        else
          @index -= 1
          refresh
          return
        end
      end
      
      if Input.repeat?(Input::RIGHT)
        if (@index-3)%4 == 0
          return
        end
        $game_system.se_play($data_system.cursor_se)
        @index += 1
        refresh
        return
      end
    end
    
    def pokemon_pointed
      return @pokemon
    end
    
    def refresh
      name = "box"+ $data_storage[0][@box][1].to_s + ".png"
      @box_sprite.bitmap = RPG::Cache.picture(name)
      self.contents.clear
      for i in 0..23
        if $data_storage[@box][i] != nil
          pokemon = $data_storage[@box][i]
          src_rect = Rect.new(0, 0, 64, 64)
          bitmap = RPG::Cache.battler(pokemon.icon, 0)
          self.contents.blt((i%4)*68, 58*(i/4), bitmap, src_rect, 255)
        end
        if i == @index and self.active
          src_rect = Rect.new(0, 0, 64, 64)
          bitmap = RPG::Cache.picture("boxselector.png")
          self.contents.blt(2+68*(i%4), 18+58*(i/4), bitmap, src_rect, 255)
          pokemon = $data_storage[@box][i]
          @pokemon = pokemon
        end
        if @selected != nil
          if @box == @selected[0] and i == @selected[1]
            src_rect = Rect.new(0, 0, 64, 64)
            bitmap = RPG::Cache.picture("boxselected.png")
            self.contents.blt(2+68*(i%4), 18+58*(i/4), bitmap, src_rect, 255)
          end
        end
      end
      
    end
  end
  
end