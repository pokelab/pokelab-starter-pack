#==============================================================================
# ■ Scene_Name
#------------------------------------------------------------------------------
# 　名前入力画面の処理を行うクラスです。
#==============================================================================

class Scene_Name
  def initialize(code_exchange = nil)
    @code_exchange = code_exchange
  end
  #--------------------------------------------------------------------------
  # ● メイン処理
  #--------------------------------------------------------------------------
  def main
    Graphics.freeze
    @z_level = 10000
    @background = Sprite.new
    @background.bitmap = RPG::Cache.picture("name.png")
    @background.z = @z_level
    
    @actor = $game_actors[$game_temp.name_actor_id]
    if @code_exchange != nil
      @edit_window = Window_NameEdit.new(@code_exchange, 7)
      @input_window = Window_NameInput.new(true)
    else
      @edit_window = Window_NameEdit.new(@actor, $game_temp.name_max_char)
      @input_window = Window_NameInput.new
    end
    
    @edit_window.z = @z_level + 1
    @input_window.z = @z_level + 2
    
    @done = false
    Graphics.transition
    loop do
      Graphics.update
      Input.update
      update
      if @done
        break
      end
    end
    Graphics.freeze
    @edit_window.dispose
    @input_window.dispose
    @background.dispose
  end
  #--------------------------------------------------------------------------
  # ● フレーム更新
  #--------------------------------------------------------------------------
  def update
    @edit_window.update
    @input_window.update
    # Effacement
    if Input.repeat?(Input::B)
      if @edit_window.index == 0
        return
      end
      $game_system.se_play($data_system.cancel_se)
      @edit_window.back
      return
    end
    
    # Confirmer
    if Input.trigger?(Input::C)
      # Bouton sur Confirmer
      if @input_window.character == nil
        # Pas de nom entrée => Par défaut
        if @edit_window.name == ""
          @edit_window.restore_default
          if @edit_window.name == ""
            $game_system.se_play($data_system.buzzer_se)
            return
          end
          $game_system.se_play($data_system.decision_se)
          return
        end
        
        if @code_exchange != nil and @edit_window.name.length < 7
          $game_system.se_play($data_system.buzzer_se)
          return
        end
        $game_system.se_play($data_system.decision_se)
        
        if @code_exchange == nil
          @actor.name = @edit_window.name
          $scene = Scene_Map.new
        else
          $string[0] = @edit_window.name
        end
        
        @done = true
        return
      end
      
      # Entrée d'un caractère de trop
      if @code_exchange == nil and @edit_window.index == $game_temp.name_max_char
        $game_system.se_play($data_system.buzzer_se)
        return
      end
      if @code_exchange != nil and @edit_window.index == 7
        $game_system.se_play($data_system.buzzer_se)
        return
      end
      
      # Entrée d'un mauvais caractère
      if @input_window.character == ""
        $game_system.se_play($data_system.buzzer_se)
        return
      end
      
      $game_system.se_play($data_system.decision_se)
      
      @edit_window.add(@input_window.character)
      return
    end
  end
end
