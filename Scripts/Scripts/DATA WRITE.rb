=begin
Console.step("Écriture de nouveaux éléments dans la BDD...")

_c = 0

file = File.open("new_data.txt")
while(line = file.gets)
  if line.split("").first == '#'
    line[0] = ''
    Console.debug(line)
    _c = line.to_i
  else
    $data_enemies[line.to_i].element_ranks[_c] = 1
  end
end
file.close

# On écrit dans le fichier de données
file = File.open("Data/Enemies.rxdata", "wb")
Marshal.dump($data_enemies, file)
file.close

Console.done
=end