#==============================================================================
# ■ Interpreter
# Pokemon Script Project - Krosk 
# 18/07/07
#-----------------------------------------------------------------------------
# Scène à modifier à loisir
#-----------------------------------------------------------------------------
# Fonctions du logiciel
#-----------------------------------------------------------------------------

class Interpreter
  #--------------------------------------------------------------------------
  # Fonction communes
  #     parameter : 1 ID、0 Equipe
  #--------------------------------------------------------------------------
  def iterate_actor(parameter)
    if parameter == 0
      for actor in $pokemon_party.actors
        yield actor
      end
    else
      actor = $pokemon_party.actors[$game_variables[4]]
      yield actor if actor != nil
    end
  end
  
  #--------------------------------------------------------------------------
  # Ajouter / Retirer monnaie
  #--------------------------------------------------------------------------
  def command_125
    value = operate_value(@parameters[0], @parameters[1], @parameters[2])
    $pokemon_party.add_money(value)
    return true
  end
  
  #--------------------------------------------------------------------------
  # Ajouter / Retirer objets
  #--------------------------------------------------------------------------
  def command_126
    value = operate_value(@parameters[1], @parameters[2], @parameters[3])
    $pokemon_party.add_item(@parameters[0], value)
    return true
  end
  
  #--------------------------------------------------------------------------
  # Modifier les PV
  #--------------------------------------------------------------------------
  def command_311
    value = operate_value(@parameters[1], @parameters[2], @parameters[3])
    iterate_actor(@parameters[0]) do |actor|
      if actor.hp > 0
        if @parameters[4] == false and actor.hp + value <= 0
          actor.hp = 1
        else
          actor.hp += value
        end
      end
    end
    $game_temp.gameover = $pokemon_party.dead?
    return true
  end
  
  #--------------------------------------------------------------------------
  # Infliger statut
  #--------------------------------------------------------------------------
  def command_313
    iterate_actor(@parameters[0]) do |actor|
      if @parameters[1] == 0
        if @parameters[2] == 9
          actor.hp = 0
        else
          actor.status = @parameters[2]
        end
      elsif actor.status == @parameters[2]
        actor.cure
      elsif actor.dead? and @parameters[2] == 9
        actor.hp = actor.max_hp
      end
    end
    return true
  end

  #--------------------------------------------------------------------------
  # Soigner complètement
  #--------------------------------------------------------------------------  
  def command_314
    iterate_actor(@parameters[0]) do |actor|
      actor.refill
    end
    return true
  end
  
  #--------------------------------------------------------------------------
  # Ajouter de l'exp
  #--------------------------------------------------------------------------   
  def command_315
    # 操作する値を取得
    value = operate_value(@parameters[1], @parameters[2], @parameters[3])
    # イテレータで処理
    iterate_actor(@parameters[0]) do |actor|
      # アクターの EXP を変更
      actor.exp += value
      if actor.level_check
        actor.level_up
        if actor.evolve_check != false
          scenebis = Pokemon_Evolve.new(actor, actor.evolve_check)
          scenebis.main
          Graphics.transition
        end
      end
    end
    # 継続
    return true
  end
  
  #--------------------------------------------------------------------------
  # Régler le niveau
  #--------------------------------------------------------------------------
  def command_316
    value = operate_value(@parameters[1], @parameters[2], @parameters[3])
    iterate_actor(@parameters[0]) do |actor|
      if value > 0
        for i in 1..value
          actor.level_up
          if actor.evolve_check != false
            scenebis = Pokemon_Evolve.new(actor, actor.evolve_check)
            scenebis.main
            Graphics.transition
          end
        end
      else
        actor.level += value
        if actor.level <= 0
          actor.level = 1
        end
      end
      actor.statistic_refresh
    end
    return true
  end
  
  #--------------------------------------------------------------------------
  # Enseigner compétence
  #--------------------------------------------------------------------------
  def command_318
    actor = $pokemon_party.actors[$game_variables[4]]
    if actor != nil
      if @parameters[1] == 0
        enseigner_capacite(actor, @parameters[2])
      else
        actor.forget_skill(@parameters[2])
      end
    end
    # 継続
    return true
  end
  
  #--------------------------------------------------------------------------
  # Modifier nom
  #--------------------------------------------------------------------------
  def command_320
    # アクターを取得
    actor = $pokemon_party.actors[$game_variables[4]]
    # 名前を変更
    if actor != nil
      
      if @parameters[1] != ""
        actor.given_name = @parameters[1]
      else
        name_pokemon(actor)
      end
    end
    # 継続
    return true
  end
  
  #--------------------------------------------------------------------------
  # Démarrer un combat
  #--------------------------------------------------------------------------  
  def command_301
    call_battle_trainer(@parameters[0], true, @parameters[1], @parameters[2])
    current_indent = @list[@index].indent
    $game_temp.battle_proc = Proc.new { |n| @branch[current_indent] = n }
    #@index += 1
    return true
  end
  
  #--------------------------------------------------------------------------
  # Action conditionnelle
  #-------------------------------------------------------------------------- 
  def command_111
    # ローカル変数 result を初期化
    result = false
    # 条件判定
    case @parameters[0]
    when 0  # スイッチ
      result = ($game_switches[@parameters[1]] == (@parameters[2] == 0))
    when 1  # 変数
      value1 = $game_variables[@parameters[1]]
      if @parameters[2] == 0
        value2 = @parameters[3]
      else
        value2 = $game_variables[@parameters[3]]
      end
      case @parameters[4]
      when 0  # と同値
        result = (value1 == value2)
      when 1  # 以上
        result = (value1 >= value2)
      when 2  # 以下
        result = (value1 <= value2)
      when 3  # 超
        result = (value1 > value2)
      when 4  # 未満
        result = (value1 < value2)
      when 5  # 以外
        result = (value1 != value2)
      end
    when 2  # セルフスイッチ
      if @event_id > 0
        key = [$game_map.map_id, @event_id, @parameters[1]]
        if @parameters[2] == 0
          result = ($game_self_switches[key] == true)
        else
          result = ($game_self_switches[key] != true)
        end
      end
    when 3  # タイマー
      if $game_system.timer_working
        sec = $game_system.timer / Graphics.frame_rate
        if @parameters[2] == 0
          result = (sec >= @parameters[1])
        else
          result = (sec <= @parameters[1])
        end
      end
    when 4  # アクター
      if pokemon_numero($game_variables[4]) != nil
        case @parameters[2]
        when 1
          result = (pokemon_numero($game_variables[4]).name == @parameters[3])
        when 2  # スキル
          result = (pokemon_numero($game_variables[4]).skill_learnt?(@parameters[3]))
        when 5
          if @parameters[3] == 9
            result = pokemon_numero($game_variables[4]).dead?
          else
            result = (pokemon_numero($game_variables[4]).status == @parameters[3])
          end
        end
      end
    when 6  # キャラクター
      character = get_character(@parameters[1])
      if character != nil
        result = (character.direction == @parameters[2])
      end
    when 7  # ゴールド
      if @parameters[2] == 0
        result = ($pokemon_party.money >= @parameters[1])
      else
        result = ($pokemon_party.money <= @parameters[1])
      end
    when 8  # アイテム
      result = ($pokemon_party.item_number(@parameters[1]) > 0)
    when 11  # ボタン
      result = (Input.press?(@parameters[1]))
    when 12  # スクリプト
      result = eval(@parameters[1])
    end
    # 判定結果をハッシュに格納
    @branch[@list[@index].indent] = result
    # 判定結果が真だった場合
    if @branch[@list[@index].indent] == true
      # 分岐データを削除
      @branch.delete(@list[@index].indent)
      # 継続
      return true
    end
    # 条件に該当しない場合 : コマンドスキップ
    return command_skip
  end
  
  #--------------------------------------------------------------------------
  # Shop
  #-------------------------------------------------------------------------- 
  def command_302
    shop_list = [@parameters[1]]
    loop do
      @index += 1
      if @list[@index].code == 605
        shop_list.push(@list[@index].parameters[1])
      else
        break
      end
    end
    $scene = Pokemon_Shop.new(shop_list)
    @wait_count = 2
    @index -= 1
    return true
  end
  
  #--------------------------------------------------------------------------
  # Gestion de Variables
  #-------------------------------------------------------------------------- 
  def command_122
    # 値を初期化
    value = 0
    # オペランドで分岐
    case @parameters[3]
    when 0  # 定数
      value = @parameters[4]
    when 1  # 変数
      value = $game_variables[@parameters[4]]
    when 2  # 乱数
      value = @parameters[4] + rand(@parameters[5] - @parameters[4] + 1)
    when 3  # アイテム
      value = $pokemon_party.item_number(@parameters[4])
    when 4  # アクター
      actor = $pokemon_party.actors[$game_variables[4]]
      if actor != nil
        case @parameters[5]
        when 0  # レベル
          value = actor.level
        when 1  # EXP
          value = actor.exp
        when 2  # Type1
          value = actor.type1
        when 3  # type2
          value = actor.type2
        when 4  # HP
          value = actor.hp
        when 5  # max_hp
          value = actor.max_hp
        when 6  # 腕力
          value = actor.atk
        when 7  # 器用さ
          value = actor.dfe
        when 8  # 素早さ
          value = actor.spd
        when 9  # 魔力
          value = actor.ats
        when 10  # 攻撃力
          value = actor.dfs
        when 11  # 物理防御
          value = actor.gender
        when 12  # 魔法防御
          value = actor.id
        when 13  # 回避修正
          value = actor.loyalty
        end
      end
    when 6  # キャラクター
      character = get_character(@parameters[4])
      if character != nil
        case @parameters[5]
        when 0  # X 座標
          value = character.x
        when 1  # Y 座標
          value = character.y
        when 2  # 向き
          value = character.direction
        when 3  # 画面 X 座標
          value = character.screen_x
        when 4  # 画面 Y 座標
          value = character.screen_y
        when 5  # 地形タグ
          value = character.terrain_tag
        end
      end
    when 7  # その他
      case @parameters[4]
      when 0  # マップ ID
        value = $game_map.map_id
      when 1  # パーティ人数
        value = $pokemon_party.actors.size
      when 2  # ゴールド
        value = $pokemon_party.money
      when 3  # 歩数
        value = $pokemon_party.steps
      when 4  # プレイ時間
        value = Graphics.frame_count / Graphics.frame_rate
      when 5  # タイマー
        value = $game_system.timer / Graphics.frame_rate
      when 6  # セーブ回数
        value = $game_system.save_count
      end
    end
    for i in @parameters[0] .. @parameters[1]
      # 操作で分岐
      case @parameters[2]
      when 0  # 代入
        $game_variables[i] = value
      when 1  # 加算
        $game_variables[i] += value
      when 2  # 減算
        $game_variables[i] -= value
      when 3  # 乗算
        $game_variables[i] *= value
      when 4  # 除算
        if value != 0
          $game_variables[i] /= value
        end
      when 5  # 剰余
        if value != 0
          $game_variables[i] %= value
        end
      end
      # 上限チェック
      if $game_variables[i] > 99999999
        $game_variables[i] = 99999999
      end
      # 下限チェック
      if $game_variables[i] < -99999999
        $game_variables[i] = -99999999
      end
    end
    # マップをリフレッシュ
    $game_map.need_refresh = true
    # 継続
    return true
  end
  
  #--------------------------------------------------------------------------
  # Inserer un script
  #--------------------------------------------------------------------------
  def command_355
    # script に 1 行目を設定
    script = @list[@index].parameters[0] + "\n"
    # ループ
    loop do
      # 次のイベントコマンドがスクリプト 2 行目以降の場合
      if @list[@index+1].code == 655
        # script に 2 行目以降を追加
        script += @list[@index+1].parameters[0] + "\n"
      # イベントコマンドがスクリプト 2 行目以降ではない場合
      else
        # ループ中断
        break
      end
      # インデックスを進める
      @index += 1
    end
    $running_script = "MAP #{@map_id} EVENT #{@event_id}\nSCRIPT\n#{script}"
    (eval(script) or true)
  end
  
end