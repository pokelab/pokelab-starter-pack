#==============================================================================
# ■ Pokemon
# Pokemon Script Project - Krosk 
# 20/07/07
# 26/08/08 - révision, support des oeufs
#-----------------------------------------------------------------------------
# Gestion individuelle
#-----------------------------------------------------------------------------

module POKEMON_S
  #------------------------------------------------------------  
  # class Pokemon : génère l'information sur un Pokémon.
  #------------------------------------------------------------
  class Pokemon
    #Archetype
    attr_reader :id             # ID du Pokémon
    attr_reader :id_bis         # ID secondaire du Pokémon
    attr_reader :name           # Nom générique de l'espèce
    attr_reader :battler_face   # Généré automatiquement
    attr_reader :battler_back   # Généré automatiquement
    attr_reader :cry            # Généré automatiquement
    attr_reader :icon           # Généré automatiquement
    attr_reader :base_hp        # Stats de base
    attr_reader :base_atk
    attr_reader :base_dfe
    attr_reader :base_spd
    attr_reader :base_ats
    attr_reader :base_dfs
    attr_reader :skills_table   # Table de skills naturels
    attr_reader :skills_allow   # Table de skills permissibles (CT, CS)
    attr_reader :description    # Liste
    attr_reader :exp_type       # Expérience: Courbe d'évolution, définie la table
    attr_reader :type1
    attr_reader :type2
    #attr_reader :exp_list       # Expérience: Table d'expérience
    attr_reader :evolve_list
    attr_reader :battle_list    # Table des EV
    attr_reader :ability        # Capacité propre du Pokémon
    attr_reader :rareness       # Rareté
    # Caractéristiques
    attr_reader :code           # Code indentification propre au Pokémon
    attr_reader :dv_hp
    attr_reader :dv_atk
    attr_reader :dv_dfe
    attr_reader :dv_spd
    attr_reader :dv_ats
    attr_reader :dv_dfs
    attr_reader :nature         # Code "caractère" du Pokémon
    attr_reader :shiny          # Caractère Shiny
    attr_reader :gender         # 0 sans genre 1 male 2 femelle
    attr_reader :egg           # état oeuf
    attr_accessor :trainer_id
    attr_accessor :trainer_name
    attr_accessor :given_name   # Nom donné au Pokémon
    attr_accessor :level
    attr_accessor :exp
    attr_accessor :skills_set   # Class Skill
    attr_accessor :atk_plus     # Effort Value
    attr_accessor :dfe_plus
    attr_accessor :spd_plus
    attr_accessor :ats_plus
    attr_accessor :dfs_plus
    attr_accessor :hp_plus
    # Statut_Variable
    attr_accessor :hp
    attr_accessor :atk
    attr_accessor :dfe
    attr_accessor :spd
    attr_accessor :ats
    attr_accessor :dfs
    attr_accessor :status
    attr_accessor :status_count
    attr_accessor :confused
    attr_accessor :state_count  # pour confusion
    attr_accessor :flinch
    attr_accessor :effect       # Liste des effets au combat
    attr_accessor :ability_active # true/false
    attr_accessor :ability_token
    attr_accessor :item_hold      # ID objet tenu
    attr_accessor :loyalty
    attr_accessor :battle_stage   # [atk, dfe, spd, ats, dfs, eva, acc]
    attr_accessor :atk_stage      # Stats de combat
    attr_accessor :dfe_stage
    attr_accessor :spd_stage
    attr_accessor :ats_stage
    attr_accessor :dfs_stage
    attr_accessor :eva_stage
    attr_accessor :acc_stage
    attr_accessor :ball_data           # Sprite de la ball utilisée
    attr_accessor :step_remaining      # pas restants avant éclosion
    attr_accessor :form                # Le numéro de la forme alt
    #------------------------------------------------------------  
    # Créer un Pokémon: 
    #         @pokemon = Pokemon.new(id, level, shiny)
    # Appel d'une commande attr_accessor:
    #         @pokemon.accessor
    # Assignation:
    #         @pokemon.accessor = value
    # Commandes_d'appel du type poke = Pokemon.new(id), poke.commande
    # poke.maxhp_basis, poke.sta_basis
    #------------------------------------------------------------
    
    #------------------------------------------------------------
    # Création d'un Pokémon
    #------------------------------------------------------------
    #------------------------------------------------------------
    # Modèle invariant / Archetype
    #------------------------------------------------------------
    def archetype(id)
      @base_hp = Pokemon_Info.base_hp(id)
      @base_atk = Pokemon_Info.base_atk(id)
      @base_dfe = Pokemon_Info.base_dfe(id)
      @base_spd = Pokemon_Info.base_spd(id)
      @base_ats = Pokemon_Info.base_ats(id)
      @base_dfs = Pokemon_Info.base_dfs(id)
      @skills_table = skill_table_building
      @skills_allow = Pokemon_Info.skills_tech(id)
      @exp_type = Pokemon_Info.exp_type(id)
      @type1 = Pokemon_Info.type1(id)
      @type2 = Pokemon_Info.type2(id)
      @rareness = Pokemon_Info.rareness(id)
      @male_rate = 100 - Pokemon_Info.female_rate(id)
      @female_rate = Pokemon_Info.female_rate(id)
      @battle_list = Pokemon_Info.battle_list(id)
      @battler_id = id
      @ability = ability_conversion
    end
    
    def name
      if @egg
        return "OEUF"
      end
      return Pokemon_Info.name(id)
    end
    
    def evolve_list
      if @egg
        return []
      end
      return Pokemon_Info.evolve_table(id)
    end
    
    def description
      return Pokemon_Info.descr(id)
    end
    
    def icon
      if @egg
        return "Icon/Egg000.png"
      end
      ida = sprintf("%03d", id)
      string = "Icon/#{ida}#{battler_form}.png"
      if not( $picture_data["Graphics/Battlers/" + string] )
        string.sub!(battler_form, "")
      end
      return string
    end
        
    def cry
      if @egg
        return ""
      end
      ida = sprintf("%03d", id)
      cry = "Audio/SE/Cries/#{ida}Cry.wav"
      return cry
    end
    
    def battler_form
      if @form == nil
        @form = 0
      end
      if @form > 0
        return sprintf("_%02d", @form)
      end
      return ""
    end
    
    def battler_face
      ida = sprintf("%03d", id)
      
      if @egg
        string = "Eggs/#{ida}.png"
        if not( $picture_data["Graphics/Battlers/#{string}"] )
          string = "Eggs/Egg000.png"
        end
        return string
      end
      
      if @gender == 1 or @gender == 0
        string = "Front_Male/#{ida}#{battler_form}.png"
      elsif @gender == 2
        string = "Front_Female/#{ida}#{battler_form}.png"
        #if not(FileTest.exist?("Graphics/Battlers/" + string))
        if not( $picture_data["Graphics/Battlers/" + string] )
          string = "Front_Male/#{ida}#{battler_form}.png"
        end
      end
            
      if @shiny
        string2 = "Shiny_" + string
        #if FileTest.exist?("Graphics/Battlers/" + string2)
        if $picture_data["Graphics/Battlers/" + string2]
          string = string2
        end
      end
      
      if not( $picture_data["Graphics/Battlers/" + string] )
        string.sub!(battler_form, "")
      end
      
      return string
    end
    
    def battler_back
      ida = sprintf("%03d", id)
      if @gender == 1 or @gender == 0
        string = "Back_Male/#{ida}#{battler_form}.png"
      elsif @gender == 2
        string = "Back_Female/#{ida}#{battler_form}.png"
        #if not(FileTest.exist?("Graphics/Battlers/" + string))
        if not($picture_data["Graphics/Battlers/" + string])
          string = "Back_Male/#{ida}#{battler_form}.png"
        end
      end
      
      if @shiny
        string2 = "Shiny_" + string
        #if FileTest.exist?("Graphics/Battlers/" + string2)
        if $picture_data["Graphics/Battlers/" + string2]
          string = string2
        end
      end
      
      if not( $picture_data["Graphics/Battlers/" + string] )
        string.sub!(battler_form, "")
      end
      
      return string
    end  
    
    def skills_allow
      return Pokemon_Info.skills_tech(id)
    end
    
    def hatch_step
      return Pokemon_Info.hatch_step(id)
    end
    
    def breed_move
      return Pokemon_Info.breed_move(id)
    end
    
    def breed_group
      return Pokemon_Info.breed_group(id)
    end
  
    def initialize(id_data = 1, level = 1, shiny = false)
      if id_data.type == Fixnum
        id = id_data
      elsif id_data.type == String
        id = id_conversion(id_data)
      end
      @id = id
      @id_bis = Pokemon_Info.id_bis(id)
      @trainer_id = Player.id
      @trainer_name = Player.name
      @code = code_generation
      @male_rate = 100 - Pokemon_Info.female_rate(id)
      @female_rate = Pokemon_Info.female_rate(id)
      @gender = gender_generation
      if shiny # Shiny forcé
        @shiny = true
      else
        a = @code ^ Player.code
        b = a & 0xFFFF
        c = (a >> 16) & 0xFFFF
        d = b ^ c
        @shiny = d < 8
      end
      archetype(id)
      @dv_hp = rand(32)
      @dv_atk = rand(32)
      @dv_dfe = rand(32)
      @dv_spd = rand(32)
      @dv_ats = rand(32)
      @dv_dfs = rand(32)
      if @shiny
        @dv_hp = @dv_hp>16 ? 31 : @dv_hp += 15
        @dv_atk = @dv_atk>16 ? 31 : @dv_atk += 15
        @dv_dfe = @dv_dfe>16 ? 31 : @dv_dfe += 15
        @dv_spd = @dv_spd>16 ? 31 : @dv_spd += 15
        @dv_ats = @dv_ats>16 ? 31 : @dv_ats += 15
        @dv_dfs = @dv_dfs>16 ? 31 : @dv_dfs += 15
      end
      @hp_plus = 0
      @atk_plus = 0
      @dfe_plus = 0
      @ats_plus = 0
      @dfs_plus = 0
      @spd_plus = 0
      @given_name = name.clone
      @level = level == 0 ? 1 : level
      @exp = exp_list[@level]
      @skills_set = []
      @status = 0
      @status_count = 0
      @item_hold = 0 #id item
      @loyalty = Pokemon_Info.base_loyalty(id)
      @loyalty += 100
      @nature = nature_generation
      @ability = ability_conversion
      initialize_skill
      reset_stat_stage
      statistic_refresh
      @hp = maxhp_basis
      @ball_data = $data_ball[1]
      @effect = []
      @effect_count = []
      @ability_active = false
      @ability_token = nil
      @egg = false
      @step_remaining = 0
      @form = 0
    end

    #------------------------------------------------------------
    # Création de la table d'expérience / table des skills
    #------------------------------------------------------------    
    def exp_list
      return EXP_TABLE[@exp_type]
    end
        
    def skill_table_building
      list = []
      if Pokemon_Info.skills_list(id).length != 0
        for i in 0..(Pokemon_Info.skills_list(id).length/2-1)
          # [id skill, level skill], ...
          list.push([Pokemon_Info.skills_list(id)[2*i+1], Pokemon_Info.skills_list(id)[2*i]])
        end
      end
      return list
    end
    
    #------------------------------------------------------------
    # Création du code d'identification du Pokémon
    #  code 32 bits
    #------------------------------------------------------------
    def code_generation
      return rand(2**32)
    end
    
    #------------------------------------------------------------
    # Conversion "nom" - > id
    #------------------------------------------------------------
    def id_conversion(name)
      for id in 1..$data_pokemon.length-1
        if name == Pokemon_Info.name(id)
          return id
        end
      end
    end

    #------------------------------------------------------------
    # Désignation du sexe
    #------------------------------------------------------------
    def gender_generation
      if @male_rate == 101
        return 0 #0 = genderless
      else
        low = @code % 256
        if low < (256 * @female_rate / 100)
          return 2
        else
          return 1
        end
      end
    end
    
    def set_gender(gender)
      if gender == "F" or gender == 2
        return @gender = 2
      end
      if gender == "M" or gender == 1
        return @gender = 1
      end
      if gender == "I" or gender == 0
        return @gender = 0
      end
    end
    
         def set_do(new_do)
      @trainer_name = new_do
    end
    
         def set_id(new_id)
      @trainer_id = new_id
    end
    
    def male?
      return @gender == 1
    end
    
    def female?
      return @gender == 2
    end
    alias femelle? female?
    
    def genderless?
      return @gender == 0
    end
    alias sans_genre? genderless?

    #------------------------------------------------------------
    # Désignation du caractère
    #------------------------------------------------------------
    def nature_generation 
      nature_code = @code % 25
      case nature_code #atk, dfe, vit, ats, dfs
      when 0
        return ["Assuré", 90,110,100,100,100]
      when 1
        return ["Bizarre", 100,100,100,100,100]
      when 2
        return ["Brave", 110,100,90,100,100]
      when 3
        return ["Calme", 90,100,100,100,110]        
      when 4
        return ["Discret", 100,100,90,110,100]
      when 5
        return ["Docile", 100,100,100,100,100]
      when 6
        return ["Doux", 100,90,100,110,100]
      when 7
        return ["Foufou", 100,100,100,110,90]
      when 8
        return ["Gentil", 100,90,100,100,110]
      when 9
        return ["Hardi", 100,100,100,100,100]
      when 10
        return ["Jovial", 100,100,110,90,100]
      when 11
        return ["Lâche", 100,110,100,100,90]
      when 12
        return ["Malin", 100,110,100,90,100]
      when 13
        return ["Malpoli", 100,100,90,100,110]
      when 14
        return ["Mauvais", 110,100,100,100,90]
      when 15
        return ["Modeste", 90,100,100,110,100]
      when 16
        return ["Naif", 100,100,110,100,90]
      when 17
        return ["Pressé", 100,90,110,100,100]
      when 18
        return ["Prudent", 100,100,100,90,110]
      when 19
        return ["Pudique", 100,100,100,100,100]
      when 20
        return ["Relax", 100,110,90,100,100]
      when 21
        return ["Rigide", 110,100,100,90,100]
      when 22
        return ["Sérieux", 100,100,100,100,100]
      when 23
        return ["Solo", 110,90,100,100,100]
      when 24
        return ["Timide", 90,100,110,100,100]
      end
    end
    
    #------------------------------------------------------------
    # Désignation de la Capacité spéciale
    #------------------------------------------------------------
    def ability_conversion
      # Une seule capa. spéciale  ou code paire
      if Pokemon_Info.ability_list(id)[1] == nil or @code.even? 
        ability_string = Pokemon_Info.ability_list(id)[0]
      elsif @code.odd? and Pokemon_Info.ability_list(id)[1] != nil
        ability_string = Pokemon_Info.ability_list(id)[1]
      end
      list = []
      for i in 1..$data_ability.length-1
        list.push($data_ability[i][0]) # Noms
      end
      id = list.index(ability_string)
      if id == nil
        ###print ability_string
      end
      return id + 1
    end
    
    def ability_name
      return $data_ability[@ability][0]
    end
    
    def ability_descr
      return $data_ability[@ability][1]
    end
    
    def change_name(name)
      @given_name = name
    end
    
    def ball_sprite
      return @ball_data[2]
    end
    
    def ball_open_sprite
      return @ball_data[3]
    end
    
    def ball_color
      return @ball_data[4]
    end
    
    def item_name
      return Item.name(@item_hold)
    end
    
    def skills # équivalent de skills_set, mais avec les id seulement
      list = []
      if @egg
        return list
      end
      for skill in @skills_set
        list.push(skill.id)
      end
      return list
    end
    
    def ss(index)
      return @skills_set[index]
    end
    
  #------------------------------------------------------------
  # Gestion des skills
  #------------------------------------------------------------
    #------------------------------------------------
    # $data_skills_pokemon[id] = [
    #   "Nom",
    #   dommages de base,
    #   type,
    #   précision %,   
    #   pp,max
    #   special %,
    # ]
    #------------------------------------------------
    #------------------------------------------------------------
    # Apprendre une compétence
    #------------------------------------------------------------
    def learn_skill(id_data)
      if id_data.type == Fixnum
        id = id_data
      elsif id_data.type == String
        id = Skill_Info.id(id_data)
      end
      if not(skills.include?(id)) and @skills_set.length < 4
        @skills_set.push(Skill.new(id))
        return true
      end
      return false
    end
    
    #------------------------------------------------------------
    # Oublier une compétence par son numéro de place
    #------------------------------------------------------------
    def forget_skill_index(index) # De 0 à 3
      if skills[index] != nil
        @skills_set.delete_at(index)
      end
    end
    
    def forget_skill(id_data)
      if id_data.type == Fixnum
        id = id_data
      elsif id_data.type == String
        id = Skill_Info.id(id_data)
      end
      index = skills.index(id)
      if index != nil
        forget_skill_index(index)
      end
    end    
    
    #------------------------------------------------------------
    # convert_skill(learnt, learning)
    #------------------------------------------------------------
    def convert_skill(learnt, learning)
      if learnt.type == String
        learnt_id = Skill_Info.id(learnt)
      elsif learnt.type == Fixnum
        learnt_id = learnt
      end
      if learning.type == String
        learning_id = Skill_Info.id(learning)
      elsif learning.type == Fixnum
        learning_id = learning
      end
      index = skills.index(learnt_id)
      @skills_set[index] = Script.new(learning_id)
    end
    
    #------------------------------------------------------------
    # replace_skill_index(index, id_data)
    #   index = index du skill dans les attaques
    #   id_data = nom/ID du skill à mettre
    #------------------------------------------------------------
    def replace_skill_index(index, id_data)
      if id_data.type == String
        id = Skill_Info.id(id_data)
      elsif id_data.type == Fixnum
        id = id_data
      end
      @skills_set[index] = Skill.new(id)
    end
    
    #------------------------------------------------------------
    # Commandes supplémentaires:
    # @pokemon.skill_learn?(id)
    #       renvoie true/false
    # @pokemon.initialize_skill
    #       Crèe les compétences naturelles
    #       Ecrase les compétences de bas niveau!
    #------------------------------------------------------------      
    def skill_learnt?(id_data)
      if id_data.type == Fixnum
        id = id_data
      elsif id_data.type == String
        id = Skill_Info.id(id_data)
      end
      return skills.include?(id)
    end
    
    def initialize_skill
      for skill in @skills_table
        if skill[1] <= @level and @skills_set.length < 4
          learn_skill(skill[0])
        elsif skill[1] <= @level and @skills_set.length == 4
          forget_skill_index(0)
          learn_skill(skill[0])
        end
      end
    end
    
    def refresh_skill(backscene = nil)
      for skill in @skills_table
        if skill[1] == @level and not(skill_learnt?(skill[0]))
          scene = Pokemon_Skill_Learn.new(self, skill[0], backscene)
          scene.main
        end
      end
    end
    
    def force_skill(list)
      @skills_set = []
      for id in list
        if id.type == Fixnum
          skill = Skill.new(id)
          @skills_set.push(skill)
        end
        if id.type == String
          skill = Skill.new(Skill_Info.id(id))
          @skills_set.push(skill)
        end
      end
    end
    
    # ------------------------------------------------------
    # skill_selection
    #   Permet d'ouvrir une fenêtre de sélection d'un skill.
    #   Renvoie -1, ou l'index du de l'attaque (0 pour le premier,
    #   1 pour le suivant, 2 pour le suisuivant...
    # ------------------------------------------------------
    def skill_selection
      scene = Pokemon_Skill_Selection.new(self)
      scene.main
      data = scene.return_data
      scene = nil
      $game_variables[5] = data
      return data
    end
    
  #------------------------------------------------------------
  # Gestion d'évolution
  #------------------------------------------------------------    
    #------------------------------------------------------------    
    # Génère l'expérience.
    #   battle_list : battle_list de l'adversaire vaincu battle_list[6]: base_exp
    #   level : niveau du vaincu
    #   number : nombre de participants
    #   type : de combat: sauvage ou dresseur, et autres multiplicateurs
    #   exp_share : multi_exp
    #------------------------------------------------------------   
    def exp_calculation(battle_list, level, number = 1, type = 1, exp_share_number = 0, out_battle = 1)
      # Formule de base
      exp_sup = Integer(battle_list[6] * level * type / 7) / number
      # Formule multi_exp
      if exp_share_number > 0
        exp_share_hold = Item.data(item_hold)["expshare"] ? 1 : 0
        value = level*battle_list[6]/14
        exp_sup = (Integer(value/number)*out_battle + Integer(value/exp_share_number)*exp_share_hold)*type
      end
      return exp_sup
    end
    
    def add_exp_battle(amount)
      @exp = (@exp + amount > exp_list[MAX_LEVEL])? exp_list[MAX_LEVEL] : @exp + amount
    end
    
    # Bonus EV pour celui qui a mis KO le pokémon adverse
    def add_bonus(battle_list)
      points = 0
      for i in battle_list
        points += i
      end
      if total_ev + points <= 510
        if @hp_plus + battle_list[0] <= 255
          @hp_plus += battle_list[0]
        end
        if @atk_plus + battle_list[1] <= 255
          @atk_plus += battle_list[1]
        end
        if @dfe_plus + battle_list[2] <= 255
          @dfe_plus += battle_list[2]
        end
        if @spd_plus + battle_list[3] <= 255
          @spd_plus += battle_list[3]
        end
        if @ats_plus + battle_list[4] <= 255
          @ats_plus += battle_list[4]
        end
        if @dfs_plus + battle_list[5] <= 255
          @dfs_plus += battle_list[5]
        end
        return true
      else
        return false
      end
    end
    
    def total_ev
      return @hp_plus + @atk_plus + @dfe_plus + @spd_plus + @ats_plus + @dfs_plus
    end
    
    def drop_loyalty(amount = 1)
      @loyalty -= amount
      if @loyalty < 0
        @loyalty = 0
      end
    end
    
    def raise_loyalty(amount = 0)
      if amount == 0
        if @loyalty < 100
          @loyalty += 5
        elsif @loyalty < 200
          @loyalty += 3
        elsif @loyalty < 255
          @loyalty += 2
        end
        if @loyalty > 255
          @loyalty = 255
        end
      else
        @loyalty += amount
      end
    end
    
    #------------------------------------------------------------    
    # Vérification du niveau
    # Augmente le niveau
    #------------------------------------------------------------    
    def level_check
      if @level >= MAX_LEVEL
        return false
      end
      return @exp >= exp_list[@level+1]
    end
    
    def level_up(scene = nil)
      list = level_up_stat_refresh
      list0 = list[0]
      list1 = list[1]
      
      # Loyauté
      raise_loyalty
      
      Audio.me_play("Audio/ME/PkmRS-LevelUp.mid")
      if scene != nil
        if $battle_var.in_battle 
          if self == scene.actor
            scene.actor_status.refresh
            scene.actor_sprite.animation($data_animations[497], true)
          end
        elsif not($battle_var.in_battle)
          scene.item_refresh
        end
        scene.draw_text(@given_name + " monte au", "niveau " + @level.to_s + "!")
        Graphics.update
        Input.update
        if $battle_var.in_battle 
          until Input.trigger?(Input::C) and not(scene.actor_sprite.effect?)
            scene.actor_sprite.update
            Graphics.update
            Input.update
          end
        end
        level_up_window_call(list0, list1, scene.z_level + 100)
        scene.draw_text("", "")
        refresh_skill(scene)
      else
        new_scene = Pokemon_Window_Help.new
        new_scene.draw_text(@given_name + " monte au", "niveau " + @level.to_s + "!")
        new_scene.update
        Input.update
        until Input.trigger?(Input::C)
          Graphics.update
          Input.update
        end
        new_scene.dispose
        Graphics.update
        level_up_window_call(list0, list1, new_scene.z_level + 1000)
        refresh_skill
      end
      if level_check
        level_up(scene)
      end
    end
    
    def level_up_stat_refresh
      if @exp < exp_list[@level+1]
        @exp = exp_list[@level+1]
      end
      hp_minus = maxhp_basis - @hp
      list0 = [maxhp_basis, atk_basis, dfe_basis, ats_basis, dfs_basis, spd_basis]
      @level += 1
      statistic_refresh
      list1 = [maxhp_basis, atk_basis, dfe_basis, ats_basis, dfs_basis, spd_basis]
      @hp = maxhp_basis - hp_minus
      return [list0, list1]
    end
    
    def silent_level_up
      # Calculs
      level_up_stat_refresh
      # Skills
      for skill in @skills_table
        if skill[1] == @level and not(skill_learnt?(skill[0]))
          @skills_set.push(Skill.new(skill[0]))
          if @skills_set.length  > 4
            @skills_set = @skills_set[-4..-1]
          end
        end
      end
    end
    
    #------------------------------------------------------------    
    # Modification/Evolution du Pokémon
    #------------------------------------------------------------    
    def evolve(id = 0)
      if @egg
        @egg = false
        @trainer_id = Player.id
        @trainer_name = Player.name
        @given_name = name
        scenebis = Pokemon_Name.new(self)
        scenebis.main
        Graphics.transition
        return
      end
      if evolve_list[1] == [] or evolve_list[1] == nil or evolve_list[1][0] == ""
        return
      end
      hp_lost = max_hp - @hp
      if id == 0
        id = evolve_check
        if id == false
          return
        end
        if @given_name == name
          @id = id
          @given_name = name
        else
          @id = id
        end
        archetype(@id)
        @hp = max_hp - hp_lost
        refresh_skill
      else # Force l'évolution (par l'usage de pierres ou de transfert)
        if @given_name == name
          @id = id
          @given_name = name
        else
          @id = id
        end
        @id = id
        archetype(@id)
        @hp = max_hp - hp_lost
        refresh_skill
      end
    end
    
    def evolve_check(mode = "", param = "")
      # Pas d'évolution
      if evolve_list[1] == [] or evolve_list[1] == nil or evolve_list[1][0] == ""
        return false
      end
      
      # [1, ["ONIGLALI", 42], ["MOMARTIK", ["stone", "PIERRE AUBE"], ["genre", "femelle"]]]
      
      for i in 1..evolve_list.length-1
        check = true
        
        for j in 1..evolve_list[i].length-1
          # V0.7 : PSP teste chaque critère.
          #   Si le critère est bon, il passe au critère suivant (next), sinon, 
          #   si aucun des critères na été respecté, l'évolution est rendue
          #   invalide (check = false) 
          #   et on inspecte l'évolution possible suivante (break). 
          #     A la fin, l'évolution est lancée car tous 
          #     les critères ont été validés.
          
          # Evolution par niveau
          if evolve_list[i][j].type == Fixnum and @level >= evolve_list[i][j]
            next
          end
          
          # Evolution par loyauté
          if evolve_list[i][j] == "loyal" and @loyalty > 220
            next
          end
        
          # Evolution par lieu
          if evolve_list[i][j].is_a?(Array) and 
              evolve_list[i][j][0] == "place" and 
              evolve_list[i][j][1].include?($game_map.map_id)
            next
          end
          
          # Evolution par apprentissage d'attaques
          if evolve_list[i][j].is_a?(Array) and 
              evolve_list[i][j][0] == "attaque" and
              skills.include?(Skill_Info.id(evolve_list[i][j][1]))
            next
          end
          
          # Evolution par objet tenu
          if evolve_list[i][j].is_a?(Array) and 
              evolve_list[i][j][0] == "item" and
              @item_hold == Item.id(evolve_list[i][j][1])
            next
          end
          
          # Evolution par genre
          if evolve_list[i][j].is_a?(Array) and evolve_list[i][j][0] == "genre"
            if male? and evolve_list[i][j][1] == "male"
              next
            end
            if female? and evolve_list[i][j][1] == "femelle"
              next
            end
          end
          
          # Evolution par periode
          if evolve_list[i][j].is_a?(Array) and evolve_list[i][j][0] == "periode"
            if POKEMON_S.jour? and evolve_list[i][j][1] == "jour"
              next
            end
            if POKEMON_S.nuit? and evolve_list[i][j][1] == "nuit"
              next
            end
          end
          
          # Evolution aléatoire
          if evolve_list[i][j].is_a?(Array) and evolve_list[i][j][0] == "aleatoire"
            if rate == nil
              rate = rand(100)
            end
            if chance == nil
              chance = 0
            end
            if rate <= evolve_list[i][j][1] + chance
              next
            else
              chance += evolve_list[i][j][1]
            end
          end
          
          # Echange
          if evolve_list[i][j] == "trade" and mode == "trade"
            next
          end
          
          # Par pierre
          if evolve_list[i][j].is_a?(Array) and 
              evolve_list[i][j][0] == "stone" and
              mode == "stone" and
              evolve_list[i][j][1] == param
            next
          end
          
          check = false
          break
        end
        
        if check
          name = evolve_list[i][0]
          id = id_conversion(name)
          return id
        end
        
      end
      
      return false
    end
    
    def next_exp
      if @level >= MAX_LEVEL
        return 0
      end
      return (exp_list[@level+1]-@exp)
    end
    
  #------------------------------------------------------------
  # Gestion des stats
  #------------------------------------------------------------   
    def base_hp
      return @base_hp
    end
    def base_atk
      return @base_atk
    end
    def base_dfe
      return @base_dfe
    end
    def base_spd
      return @base_spd
    end
    def base_ats
      return @base_ats
    end
    def base_dfs
      return @base_dfs
    end
  
    #------------------------------------------------------------    
    # Planchers
    #------------------------------------------------------------    
    def maxhp_basis
      n = Integer((base_hp*2+@dv_hp+@hp_plus/4.0)*@level/100)+@level+10
      return n
    end
    
    alias max_hp maxhp_basis
    
    def atk_basis
      n = Integer((base_atk*2+@dv_atk+@atk_plus/4.0)*@level/100)+5
      n = Integer(n * @nature[1] / 100.0)
      return n
    end
    
    def dfe_basis
      n = Integer((base_dfe*2+@dv_dfe+@dfe_plus/4.0)*@level/100)+5
      n = Integer(n * @nature[2] / 100.0)
      return n
    end
    
    def spd_basis
      n = Integer((base_spd*2+@dv_spd+@spd_plus/4.0)*@level/100)+5
      n = Integer(n * @nature[3] / 100.0)
      return n
    end
    
    def ats_basis
      n = Integer((base_ats*2+@dv_ats+@ats_plus/4.0)*@level/100)+5
      n = Integer(n * @nature[4] / 100.0)
      return n
    end    
    
    def dfs_basis
      n = Integer((base_dfs*2+@dv_dfs+@dfs_plus/4.0)*@level/100)+5
      n = Integer(n * @nature[5] / 100.0)
      return n
    end
    
    #------------------------------------------------------------
    # Modificateurs et valeurs finales - Stat Stage - Combat
    # Fonctions complémentaires
    #------------------------------------------------------------   
    def statistic_refresh
      @atk = Integer(atk_basis * atk_modifier)
      @dfe = Integer(dfe_basis * dfe_modifier)
      @spd = Integer(spd_basis * spd_modifier)
      @ats = Integer(ats_basis * ats_modifier)
      @dfs = Integer(dfs_basis * dfs_modifier)
    end
    
    def reset_stat_stage
      @battle_stage = [0, 0, 0, 0, 0, 0, 0]
      statistic_refresh
    end
    
    def atk_stage
      return @battle_stage[0]
    end
    
    def dfe_stage
      return @battle_stage[1]
    end
    
    def spd_stage
      return @battle_stage[2]
    end
    
    def ats_stage
      return @battle_stage[3]
    end
    
    def dfs_stage
      return @battle_stage[4]
    end
    
    def eva_stage
      return @battle_stage[5]
    end
    
    def acc_stage
      return @battle_stage[6]
    end
    
    def change_stat(stat_id, amount = 0)
      if amount > 0 and @battle_stage[stat_id] == 6
        return 0
      end
      if amount < 0 and @battle_stage[stat_id] == -6
        return 0
      end
      
      @battle_stage[stat_id] += amount
      if @battle_stage[stat_id].abs > 6
        @battle_stage[stat_id] = 6*@battle_stage[stat_id].sgn
      end
      return amount
    end
    
    def change_atk(amount = 0)
      return change_stat(0, amount)
    end
    
    def change_dfe(amount = 0)
      return change_stat(1, amount)
    end
    
    def change_spd(amount = 0)
      return change_stat(2, amount)
    end
    
    def change_ats(amount = 0)
      return change_stat(3, amount)
    end
    
    def change_dfs(amount = 0)
      return change_stat(4, amount)
    end
    
    def change_eva(amount = 0)
      return change_stat(5, amount)
    end
    
    def change_acc(amount = 0)
      return change_stat(6, amount)
    end
    
    # Renvoi du multiplicateur selon le stage
    def modifier_stage(stage)
      if stage >= 0
        return (2+stage)/2.0
      elsif stage < 0
        return 2.0/(2-stage)
      end
    end
    
    def atk_modifier
      n = 1 * modifier_stage(atk_stage)
      # Etat burn
      if burn?
        n *= 0.5
      end
      return n
    end
    
    def dfe_modifier
      n = 1 * modifier_stage(dfe_stage)
      return n
    end    
    
    def spd_modifier
      n = 1 * modifier_stage(spd_stage)
      # Etat paralyze
      if paralyzed?
        n *= 0.25
      end
      return n
    end    

    def ats_modifier
      n = 1 * modifier_stage(ats_stage)
      return n
    end    

    def dfs_modifier
      n = 1 * modifier_stage(dfs_stage)
      return n
    end
    
    # Modification des DV
    def dv_modifier(list)
      @dv_hp = list[0]
      @dv_atk = list[1]
      @dv_dfe = list[2]
      @dv_spd = list[3]
      @dv_ats = list[4]
      @dv_dfs = list[5]
      statistic_refresh
      @hp = max_hp
    end
    
  #------------------------------------------------------------
  # Gestion combat/status
  #------------------------------------------------------------        
    #------------------------------------------------------------    
    # Effets spéciaux des attaques
    #------------------------------------------------------------ 
    # @effect: liste des effets
    def skill_effect(id, duration = -1, data = nil)
      # [ effet, compteur de tours] -1 si temps indéfini
      @effect.push([id, duration, data])
    end
    
    def effect_list
      list = []
      for effect in @effect
        list.push(effect[0])
      end
      return list
    end
    
    # Réduction des compteurs à effets
    def skill_effect_end_turn
      for i in 0..@effect.length-1
        @effect[i][1] -= 1
      end
    end
    
    def skill_effect_clean
      for i in 0..@effect.length-1
        if @effect[i] != nil          
          if @effect[i][1] == 0
            @effect.delete_at(i)
          end
        else
          @effect.delete_at(i)
        end
      end
    end
    
    def skill_effect_reset
      @effect = []
      for skill in @skills_set
        skill.enable
      end
    end
    

    
    def confuse_damage
      power = 40
      base_damage = Integer(((@level*2/5.0+2)*power*@atk/@dfe)/50)
      return base_damage
    end
    
    def remove_hp(amount)
      @hp -= amount
      if @hp < 0
        @hp = 0
      elsif @hp > max_hp
        @hp = max_hp
      end
    end
    
    def add_hp(amount)
      @hp += amount
      if @hp < 0
        @hp = 0
      elsif @hp > max_hp
        @hp = max_hp
      end
    end

    def dead?
      if @hp <= 0
        @hp = 0
        skill_effect_reset
        cure
        cure_state
        @ability_active = false
        @ability_token = nil
        return true
      else
        return (false or @egg)
      end
    end
    
    def party_index
      return $pokemon_party.actors.index(self)
    end
    
  #------------------------------------------------------------
  # Gestion de statut
  #------------------------------------------------------------           
  # @status:
  # 0: Normal, 1: Poison, 2: Paralysé, 3: Brulé, 4:Sommeil, 5:Gelé, 8: Toxic
  # @confuse (6), @flinch (7)
    # Cure
    def cure
      @status = 0
      @status_count = 0
    end
  
    # Poison
    def poisoned?
      if @status == 1
        return true
      else
        return false
      end
    end
    
    def status_poison(forcing = false)
      if @status == 0 or forcing
        @status = 1
        return true # status imposé
      else
        return false # autre statut
      end
    end
    
    def poison_effect
      if @status == 1
        return Integer(maxhp_basis / 8.0)
      end
    end
    
    # Paralysis
    def paralysis_check
      if rand(100) < 25
        return true #lose attack chance
      else
        return false
      end
    end
    
    def status_paralyze(forcing = false)
      if @status == 0 or forcing
        @status = 2
        return true # status imposé
      else
        return false # autre statut
      end
    end
    
    def paralyzed?
      if @status == 2
        return true
      else
        return false
      end
    end
    
    # Burn
    def burn?
      if @status == 3
        return true
      else
        return false
      end
    end
    
    def burn_effect
      if @status == 3
        return @hp - Integer(@hp * 0.875)
      end
    end
    
    def status_burn(forcing = false)
      if @status == 0 or forcing
        @status = 3
        return true # status imposé
      else
        return false # autre statut
      end
    end
    
    # Sleep
    def asleep?
      if @status == 4
        return true
      else
        return false
      end
    end
    
    def status_sleep(forcing = false)
      if @status == 0 or forcing
        @status = 4
        @status_count = rand(7) + 1
        return true
      else
        return false
      end
    end
    
    def sleep_check
      @status_count -= 1
      if @status_count > 0
        return true #Dort
      else
        @status = 0
        return false #réveillé
      end
    end
    
    # Frozen
    def frozen?
      if @status == 5
        return true
      else
        return false
      end
    end
    
    def status_frozen(forcing = false)
      if @status == 0 or (forcing)
        @status = 5
        return true
      else
        return false
      end
    end
    
    def froze_check
      i = rand(100)
      if @status == 5 and i < 10 
        @status = 0
        return false #defrosted
      elsif @status == 5 and i >= 10
        return true #still frozen
      end
    end
        
    # Confuse
    def confused?
      return @confused
    end
    
    def status_confuse
      if not(@confused)
        @confused = true
        @state_count = rand(4) + 2
        return true
      end
      return false
    end
    
    def confuse_check
      if @confused and @state_count > 0
        if rand(2) > 0 
          return true # Auto damage
        end
      elsif @confused and @state_count == 0
        cure_state
        return "cured"
      end
      return false
    end
    
    def confuse_decrement
      if @confused
        @state_count -= 1
      end
    end
    
    def cure_state
      @confused = false
      @state_count = 0
    end
    
    # Flinch (peur?)
    def flinch?
      return @flinch
    end
    
    def status_flinch
      return @flinch = true
    end
    
    def flinch_check
      @flinch = false
    end
    
    # Toxic
    def toxic?
      if @status == 8
        return true
      end
      return false
    end
    
    def status_toxic(forcing = false)
      if @status == 0 or forcing
        @status = 8
        @status_count = 0
        return true
      end
      return false
    end
    
    def toxic_effect
      if @status == 8
        @status_count += 1
        return Integer(maxhp_basis / 16.0 * @status_count)
      end
    end
    
    def reset_toxic_count
      if @status == 8
        @status_count = 0
      end
    end
    
  #------------------------------------------------------------
  # Gestion de soin
  #------------------------------------------------------------       
    def refill_skill
      for skill in @skills_set
        skill.refill
      end
    end
    
    def refill_skill(id, amount = 99)
      if @skills_set[id] != nil
        amount = [amount, @skills_set[id].ppmax-@skills_set[id].pp].min
        @skills_set[id].pp += amount
        return amount
      end
    end
    
    def refill_hp
      @hp = maxhp_basis
    end
    
    def refill
      refill_hp
      for skill in @skills_set
        skill.refill
      end
      cure
      cure_state
    end    
    
    def kill
      @hp = 0
    end
    
  #------------------------------------------------------------
  # Fenêtres externes
  #------------------------------------------------------------
    def level_up_window_call(list0, list1, z_level)
      @window = Window_Base.new(355, 170, 282, 308)
      @window.z = z_level
      @window.contents = Bitmap.new(250, 276)
      @window.contents.font.name = $fontface
      @window.contents.font.size = $fontsizebig
      @window.contents.font.color = @window.normal_color
      @window.contents.draw_text(0,0,189,$fhb, "PV MAX.")
      @window.contents.draw_text(0,48,189,$fhb, "ATTAQUE")
      @window.contents.draw_text(0,96,189,$fhb, "DEFENSE")
      @window.contents.draw_text(0,144,189,$fhb, "ATT.SPE.")
      @window.contents.draw_text(0,192,189,$fhb, "DEF.SPE.")
      @window.contents.draw_text(0,240,189,$fhb, "VITESSE")
      for i in 0..5
        string = (list1[i] - list0[i]).to_s
        if string.length == 1
          string = "+ " + string
        elsif string.length == 2
          string = "+" + string
        end
        @window.contents.draw_text(0, 48*i, 250, $fhb, string, 2)
      end
      loop do
        Graphics.update
        Input.update
        if Input.trigger?(Input::C)
          $game_system.se_play($data_system.decision_se)
          break
        end
      end
      @window.contents.clear
      @window.contents.draw_text(0,0,189,$fhb, "PV MAX.")
      @window.contents.draw_text(0,48,189,$fhb, "ATTAQUE")
      @window.contents.draw_text(0,96,189,$fhb, "DEFENSE")
      @window.contents.draw_text(0,144,189,$fhb, "ATT.SPE.")
      @window.contents.draw_text(0,192,189,$fhb, "DEF.SPE.")
      @window.contents.draw_text(0,240,189,$fhb, "VITESSE")
      for i in 0..5
        string = (list1[i]).to_s
        @window.contents.draw_text(0, 48*i, 250, $fhb, string, 2)
      end
      loop do
        Graphics.update
        Input.update
        if Input.trigger?(Input::C)
          $game_system.se_play($data_system.decision_se)
          break
        end
      end
      @window.dispose
      @window = nil
    end
    
    def type_normal?
      if [@type1, @type2].include?(1)
        return true
      end
      return false
    end
    
    def type_fire?
      if [@type1, @type2].include?(2)
        return true
      end
      return false
    end
    alias type_feu? type_fire?
    
    def type_water?
      if [@type1, @type2].include?(3)
        return true
      end
      return false
    end
    alias type_eau? type_water?
    
    def type_electric?
      if [@type1, @type2].include?(4)
        return true
      end
      return false
    end
    
    def type_grass?
      if [@type1, @type2].include?(5)
        return true
      end
      return false
    end
    alias type_plante? type_grass?
    
    def type_ice?
      if [@type1, @type2].include?(6)
        return true
      end
      return false
    end
    alias type_glace? type_ice?
    
    def type_fighting?
      if [@type1, @type2].include?(7)
        return true
      end
      return false
    end
    alias type_combat? type_fighting?
    
    def type_poison?
      if [@type1, @type2].include?(8)
        return true
      end
      return false
    end
    
    def type_ground?
      if [@type1, @type2].include?(9)
        return true
      end
      return false
    end
    alias type_sol? type_ground?
    
    def type_fly?
      if [@type1, @type2].include?(10)
        return true
      end
      return false
    end
    alias type_vol? type_fly?
    
    def type_psy?
      if [@type1, @type2].include?(11)
        return true
      end
      return false
    end
    
    def type_insect?
      if [@type1, @type2].include?(12)
        return true
      end
      return false
    end
    
    def type_rock?
      if [@type1, @type2].include?(13)
        return true
      end
      return false
    end
    alias type_roche? type_rock?
    
    def type_ghost?
      if [@type1, @type2].include?(14)
        return true
      end
      return false
    end
    alias type_spectre? type_ghost?
    
    def type_dragon?
      if [@type1, @type2].include?(15)
        return true
      end
      return false
    end
    
    def type_steel?
      if [@type1, @type2].include?(16)
        return true
      end
      return false
    end
    alias type_acier? type_steel?
    
    def type_dark?
      if [@type1, @type2].include?(17)
        return true
      end
      return false
    end
    alias type_tenebres? type_dark?
    
    def type_custom?(number)
      if [@type1, @type2].include?(number)
        return true
      end
      return false
    end
    
    
    #--------------------------------------------------------------------------
    # Gestion des Oeufs
    #--------------------------------------------------------------------------
    
    # Initialisation
    def new_egg(mother = 1, father = nil)
      if mother.type == Fixnum or mother.type == String
        initialize(mother, 1)
      elsif mother.is_a?(Pokemon) and father.is_a?(Pokemon)
        # Règles spéciales pour l'ID du bébé
        # ( Voir script DAYCARE )
        
        # ID = Base mère
        baby_id = base_id(mother.id)
        
        # Cf Pokemon_Custom
        baby_id = breeding_custom_rules(baby_id, mother, father)
        
        initialize(baby_id, 1)
        
        # Réglage capa spé
        common_ability = (Pokemon_Info.ability_list(mother.id) & Pokemon_Info.ability_list(father.id))
        if common_ability.include?(mother.ability_name)
          @ability = mother.ability
        end
        
        # Réglages skills : skill connus en commun
        common_skills = mother.skills & father.skills
        for skill in common_skills
          if @skills_table.assoc(skill) != nil
            @skills_set.push(Skill.new(skill))
          end
        end
        
        # Réglages skills : skill en ct/cs connus par le père
        for skill in father.skills
          if @skills_allow.include?(skill)
            @skills_set.push(Skill.new(skill))
          end
        end
        
        # Réglages skills : skill par accouplement connus par le père 
        common_breed_skills = breed_move & father.skills
        for skill in common_breed_skills
          @skills_set.push(Skill.new(skill))
        end
        
        # On garde les 4 derniers
        if @skills_set.length > 4
          @skills_set = @skills_set[-4..-1]
        end
        
        # Heritage des DV
        dv_list = []
        while dv_list.length < 3
          dv_list.push(rand(6))
          dv_list.uniq!
        end
        for dv in dv_list
          parent = rand(2) == 0 ? mother : father
          case dv
          when 0
            @dv_hp = parent.dv_hp
          when 1
            @dv_atk = parent.dv_atk
          when 2
            @dv_dfe = parent.dv_dfe
          when 3
            @dv_spd = parent.dv_spd
          when 4
            @dv_ats = parent.dv_ats
          when 5
            @dv_dfs = parent.dv_dfs
          end
        end
      else
        return self
      end
      @egg = true
      @given_name = name
      @step_remaining = hatch_step
      return self
    end
    
    #--------------------------------------------------------------------------
    # retourne la forme basique d'un Pokémon
    #--------------------------------------------------------------------------
    def base_id(mother_id)
      finish = false
      current_id = mother_id
      while not finish
        back = false
        for i in 1...$data_pokemon.length
          list = Pokemon_Info.evolve_table(i).flatten
          if list.include?(Pokemon_Info.name(current_id))
            current_id = i
            back = true
            break
          end
        end
        if not back
          finish = true
        end
      end
      return current_id
    end
    
    #--------------------------------------------------------------------------
    # réduit les pas avant éclosion
    #--------------------------------------------------------------------------
    def decrease_step
      if not @egg
        return
      end
      @step_remaining -= 1
      if @step_remaining <= 0
        scenebis = Pokemon_Evolve.new(self, @id)
        scenebis.main
        Graphics.transition
      end
    end
    
    
  end
  
end