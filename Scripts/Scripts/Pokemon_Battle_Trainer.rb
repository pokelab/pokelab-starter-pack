#==============================================================================
# ■ Pokemon_Battle_Trainer
# Pokemon Script Project - Krosk 
# 29/09/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------
# Système de Combat - Pokémon Dresseur
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# 0: Normal, 1: Poison, 2: Paralysé, 3: Brulé, 4:Sommeil, 5:Gelé, 8: Toxic
# @confuse (6), @flinch (7)
#-----------------------------------------------------------------------------
# 1 Normal  2 Feu  3 Eau 4 Electrique 5 Plante 6 Glace 7 Combat 8 Poison 9 Sol
# 10 Vol 11 Psy 12 Insecte 13 Roche 14 Spectre 15 Dragon 16 Acier 17 Tenebres
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# $battle_var.action_id
#   0 : Phase de Sélection
#   1 : Sélection Item
#   2 : Switch Pokémon
#   4 : Switch Fin de Tour
#-----------------------------------------------------------------------------

module POKEMON_S
  #------------------------------------------------------------  
  # Pokemon_Battle_Trainer
  #   scene
  #------------------------------------------------------------  
  class Pokemon_Battle_Trainer < Pokemon_Battle_Core
    attr_accessor :z_level
    attr_accessor :actor_status
    attr_accessor :actor
    #------------------------------------------------------------  
    # Fonction d'initialisation
    # Appel: $scene = Pokemon_Battle_Trainer.new(
    #           party, trainer_id, ia)
    #     party: $pokemon_party (classe Pokemon_Party)
    #     trainer_id: L'ID du dresseur dans $data_trainer (groupe de monstres)
    #     ia: Fausse AI
    #     run_able: possible de fuir
    #------------------------------------------------------------
    def initialize(party, trainer_id, ia = false, run_able = false, lose_able = false, capturable = false)
      @z_level = 0
      @ia = ia
      @trainer_id = trainer_id
      @run = run_able
      @lose = lose_able
      @capture = capturable
      
      $battle_var.reset
      
      # Création données des Pokémons adverses
      #$battle_var.enemy_party = Pokemon_Party.new
      for pokemon_data in Trainer_Info.pokemon_list(@trainer_id)
        id = pokemon_data[0]
        level = pokemon_data[1]
        # Shiny
        if pokemon_data[6] != nil
          shiny = pokemon_data[6]
        else
          shiny = false
        end
        pokemon = Pokemon.new(id, level, shiny)
        pokemon.given_name += " ennemi"
        # Paramètres optionnels
        # Objet
        if pokemon_data[2] != nil
          pokemon.item_hold = Item.id(pokemon_data[2])
        end
        # Moveset
        if pokemon_data[4] != nil
          i = 0
          for skill in pokemon_data[4]
            if skill != nil and skill != "AUCUN"
              pokemon.skills_set[i] = Skill.new(Skill_Info.id(skill))
            end
            if skill == "AUCUN"
              pokemon.skills_set[i] = nil
            end
            i += 1
          end
          pokemon.skills_set.compact!
        end
        # Stats avancées
        if pokemon_data[3] != nil and pokemon_data[3].length == 6
          pokemon.dv_modifier(pokemon_data[3])
        end
        # Genre
        if pokemon_data[5] != nil
          pokemon.set_gender(pokemon_data[5])
        end
        # Forme
        if pokemon_data[6] != nil
          pokemon.form = pokemon_data[6]
        end
        $battle_var.enemy_party.actors.push(pokemon)
      end
      @enemy = $battle_var.enemy_party.actors[0]
      $battle_var.enemy_battle_order = [0,1,2,3,4,5]
      
      @party = party
      
      # Mise à jour Pokedex: Pokémon vu
      $data_pokedex[@enemy.id][0] = true
      
      # Génération ordre de combat
      @battle_order = Array.new(@party.size)
      @battle_order.fill {|i| i}
      
      # Désignation 1er Pokémon au combat
      # @actor désigne le (class) Pokémon
      actor_index = 0
      @actor = @party.actors[actor_index]
      if @actor == nil
        print("Attention, vous n'avez pas de Pokémon dans votre équipe! Réglez ce bug.")
      end
      while @actor.dead?
        actor_index += 1
        @actor = @party.actors[actor_index]
      end
      
      # Correction ordre combat (Pokémon vivant en premier)
      @battle_order = switch(@battle_order, 0, actor_index)
      
      # Remise à zéro résultat
      $battle_var.result_flee = false
      $battle_var.result_win = false
      $battle_var.result_defeat = false
      
      # Initialisation des variables de combat
      @phase = 0
      # 0 Non décidé, 1 Attaque, 2 Switch, 3 Item, 4 Fuite
      @actor_action = 0
      @enemy_action = 0
      
      @start_actor_battler = Player.battler #$trainer_battler
      @start_enemy_battler = Trainer_Info.battler(@trainer_id)
      
      $battle_var.have_fought.push(@actor.party_index)
      $battle_var.battle_order = @battle_order
      $battle_var.in_battle = true
      
      @actor.reset_stat_stage
      @enemy.reset_stat_stage
      @actor.skill_effect_reset
      @enemy.skill_effect_reset
      @actor_skill = nil
      @enemy_skill = nil
      @actor.ability_active = false
      @enemy.ability_active = false
      @item_id = 0   # item utilisé
    end
 
        
    #------------------------------------------------------------  
    # Animations pré-combat
    #------------------------------------------------------------  
    def pre_battle_transition
      # Jingle et BGM
      #Audio.bgm_play("Audio/BGM/PkmRS-Battle3.mid")
      $game_system.bgm_play($game_system.battle_bgm)
      Audio.me_play("Audio/ME/battle_jingle.mid")
      Graphics.freeze
      
      # Sélection transition
      s = (rand(BATTLE_TRANS)+1).to_s
      @background.bitmap = RPG::Cache.picture("black.png")
      Graphics.transition(100, "Graphics/Transitions/battle"+ s +".png") 
      Audio.me_stop
      
      # Dessin
      Graphics.freeze
      @background.bitmap = RPG::Cache.battleback(@battleback_name)
      @message_background.bitmap = RPG::Cache.battleback(BATTLE_MSG)
      @enemy_sprite.bitmap = RPG::Cache.battler(@start_enemy_battler, 0)
      @enemy_sprite.ox = @enemy_sprite.bitmap.width / 2
      @enemy_sprite.oy = @enemy_sprite.bitmap.height * 2 / 3
      @enemy_sprite.x -= 782
      @enemy_ground.bitmap = RPG::Cache.battleback(@ground_name)
      @enemy_ground.ox = @enemy_ground.bitmap.width / 2
      @enemy_ground.oy = @enemy_ground.bitmap.height / 2
      @enemy_ground.zoom_x = @enemy_ground.zoom_y = 2.0/3
      @enemy_ground.x -= 782
      @actor_ground.bitmap = RPG::Cache.battleback(@ground_name)
      @actor_ground.ox = @actor_ground.bitmap.width / 2
      @actor_ground.oy = @actor_ground.bitmap.height
      @actor_ground.x += 782
      @actor_sprite.bitmap = RPG::Cache.battler(@start_actor_battler, 0)
      @actor_sprite.ox = @actor_sprite.bitmap.width / 2
      @actor_sprite.oy = @actor_sprite.bitmap.height
      @actor_sprite.x += 782
      Graphics.transition(50, "Graphics/Transitions/battle0.png")
    end
    
    def pre_battle_animation
      # Glissement des sprites
      loop do
        @enemy_sprite.x += 17
        @enemy_ground.x += 17
        @actor_sprite.x -= 17
        @actor_ground.x -= 17
        Graphics.update
        if @enemy_sprite.x == 464
          break
        end
      end
      
      # Confrontation des joueurs avec affichage des balls
      draw_text("Un combat est lancé", "par "+ Trainer_Info.string(@trainer_id) + "!")
      @actor_party_status.reset_position
      @enemy_party_status.reset_position
      @actor_party_status.visible = true
      @enemy_party_status.visible = true
      @actor_party_status.active = true
      @enemy_party_status.active = true
      @actor_party_status.x += 400
      @enemy_party_status.x -= 400
      Graphics.update
      
      loop do
        @actor_party_status.x -= 20
        @enemy_party_status.x += 20
        Graphics.update
        if @actor_party_status.x <= 309
          @actor_party_status.reset_position
          @enemy_party_status.reset_position
          break
        end
      end
      wait_hit
      
      # Dégagement sprite dresseur
      loop do
        @enemy_sprite.x += 10
        @enemy_party_status.x -= 20
        Graphics.update
        if @enemy_sprite.x >= 723
          @enemy_party_status.visible = false
          @enemy_party_status.reset_position
          @enemy_sprite.oy = @enemy_sprite.bitmap.height / 2
          break
        end
      end
      
      @enemy_sprite.bitmap = RPG::Cache.battler(@enemy.battler_face, 0)

      # Envoi sprite
      launch_enemy_pokemon
      
      # Attente appui de touche
      wait(40)
      
      # Dégagement du sprite dresseur
      @actor_party_status.reset_position
      loop do
        @actor_sprite.x -= 10
        @actor_party_status.x += 20
        Graphics.update
        if @actor_sprite.x <= -200
          @actor_sprite.bitmap = RPG::Cache.battler(@actor.battler_back, 0)
          @actor_party_status.reset_position
          @actor_party_status.visible = false
          break
        end
      end
      
      # Envoi du pokémon (animation)
      launch_pokemon
      
      @text_window.contents.clear
      Graphics.update
    end
    
    #------------------------------------------------------------  
    # Déroulement
    #------------------------------------------------------------
    def enemy_skill_decision
      action = true
      # ------- ---------- --------- --------
      #    Saut de phase de sélection (ennemi)
      #   action = false  : saut
      #   action = true   : pas de saut
      # ------- ---------- --------- --------
      action = phase_jump(true)
      
      @enemy_action = 1
      
      # Décision skill ennemi
      if not(@actor.dead?) and action
        list = []
        for i in 0..@enemy.skills_set.size - 1
          list.push(i)
        end
        list.shuffle!
        # Skill sans PP // Lutte
        @enemy_skill = Skill.new(165)
        # ----------
        for i in list
          if @enemy.skills_set[i].usable?
            @enemy_skill = @enemy.skills_set[i]
          end
        end
      end
      
      
      # Trainer IA
      if @ia and not(@actor.dead?) and action
        # Skill sans PP // Lutte
        @enemy_skill = Skill.new(165)
        
        rate_list = {}
        i = 0
        for skill in @enemy.skills_set
          if skill.usable?
            rate_list[i] = ia_rate_calculation(skill, @enemy, @actor)
          else
            rate_list[i] = 0
          end
          i += 1
        end
        
        # Tri dans l'ordre decroissant de valeur
        sorted_list = rate_list.sort {|a,b| b[1]<=>a[1]}
        
        #for element in sorted_list
        #  print(@enemy.skills_set[element[0]].name)
        #  print(element[1])
        #end
        
        # Decision prioritaire: max dégat ou soin
        # Valeur seuil: 200
        # si une attaque/défense dépasse ce seuil, il est choisi
        if sorted_list[0][1] > 200 
          @enemy_skill = @enemy.skills_set[sorted_list[0][0]]
        else
          # Decision par pallier
          i = rand(100)
          # Taux de decision
          taux = [100, 25, 5, 0]
          for a in 0..3
            if i <= taux[a] and sorted_list[a] != nil and sorted_list[a][1] != 0
              @enemy_skill = @enemy.skills_set[sorted_list[a][0]]
            end
          end
        end
      end
      
      if not(action) # Reprise du skill précédent
        @enemy_skill = $battle_var.enemy_last_used
      end
    end
    
    #------------------------------------------------------------  
    # Rounds
    #------------------------------------------------------------                
    def end_battle_check
      @actor_status.refresh
      @enemy_status.refresh
      if $battle_var.enemy_party.dead? and not(@party.dead?)
        end_battle_victory
      elsif @enemy.dead? and not($battle_var.enemy_party.dead?)
        exp_battle
        
        index = $battle_var.enemy_party.actors.index(@enemy)
        @enemy_switch_id = index+1
        name = $battle_var.enemy_party.actors[index+1].name
        draw_text(name + " va être envoyé", "par " + Trainer_Info.string(@trainer_id) + ".")
        
        if @enemy_party_status.active
          @enemy_party_status.reset_position
          @enemy_party_status.visible = true
          @enemy_party_status.x -= 400
          until @enemy_party_status.x == -16
            @enemy_party_status.x += 20
            Graphics.update
          end
        end
        
        alive = 0
        for pokemon in @party.actors
          if not pokemon.dead?
            alive += 1
          end
        end
        
        wait_hit
        if alive > 1
          draw_text("Voulez-vous appeler", "un autre Pokémon?")
          decision = false
          if draw_choice
            $battle_var.window_index = @action_window.index
            scene = Pokemon_Party_Menu.new(0)
            scene.main
            return_data = scene.return_data
            decision = true
            Graphics.transition
          end
        
          # Switch de Pokémon
          if ($battle_var.action_id == 4 or $battle_var.action_id == 6) and decision
            @switch_id = return_data
            actor_pokemon_switch
            reset_have_fought
          end
        end
        
        if @enemy_party_status.active
          until @enemy_party_status.x == -416
            @enemy_party_status.x -= 20
            Graphics.update
          end
          @enemy_party_status.visible = false
          @enemy_party_status.reset_position
        end
        
        # Switch enemy
        enemy_pokemon_switch
      end
      if @actor.dead?
        if @party.dead?
          end_battle_defeat
        else
          # Switch forcé
          $battle_var.window_index = @action_window.index
          scene = Pokemon_Party_Menu.new(0)
          scene.main
          return_data = scene.return_data
          # Switch de Pokémon
          if $battle_var.action_id == 4 or $battle_var.action_id == 6
            @switch_id = return_data
            actor_pokemon_switch
          end
        end
      end
    end
    
    def reset_have_fought
      $battle_var.have_fought.clear
      $battle_var.have_fought.push(@actor.party_index)
    end
    
    
    #------------------------------------------------------------  
    # Items
    #------------------------------------------------------------     
    def actor_item_use # items à utiliser
      # Item déjà utilisé ie remplacé par 0
      if @item_id == 0
        return
      end
      if Item.data(@item_id)["flee"] != nil
        end_battle_flee
        return
      end
      if Item.data(@item_id)["ball"] != nil
        ball_data = Item.data(@item_id)["ball"]
        catch_pokemon(ball_data)
      end
    end

    

    #------------------------------------------------------------  
    # Lancer de pokéball
    #------------------------------------------------------------         
    def catch_pokemon(ball_data)
      # Initialisation des données
      ball_name = ball_data[0]
      ball_rate = ball_data[1]
      ball_sprite = ball_data[2]
      ball_open_sprite = ball_data[3]
      ball_color = ball_data[4]
      
      # Procédure / Animation
      # Lancer
      draw_text(Player.name + " utilise", ball_name + "!")
      @ball_sprite = Sprite.new
      @ball_sprite.bitmap = RPG::Cache.picture(ball_sprite)
      @ball_sprite.x = -25
      @ball_sprite.y = 270
      @ball_sprite.z = @z_level + 16
      t = 0.0
      pi = Math::PI
      loop do
        t += 16
        @ball_sprite.y = (270.0 - 230.0 * Math.sin(2 * pi / 3.0 * t / 445.0)).to_i
        @ball_sprite.x = -15 + t
        Graphics.update
        if @ball_sprite.x >= 445
          break
        end
      end
      
      Graphics.update
      # Rejet
      draw_text(Trainer_Info.string(@trainer_id) + " dévie", "la " + ball_name + "!")
      Audio.se_stop
      Audio.se_play("Audio/SE/Pokeopenbreak.wav")
      
      t = 0
      loop do
        t += 1
        @ball_sprite.x -= -20
        @ball_sprite.y += t
        Graphics.update
        if t == 10
          @ball_sprite.dispose
          break
        end
      end
      
      wait(40)
      
      return false
    end
    
    #------------------------------------------------------------  
    # Fuite
    #------------------------------------------------------------           
    def run_able?(runner, opponent)
      if @run
        return super
      end
      return false
    end
    
    def flee_able(actor, enemy)
      if @run
        return super
      end
      @action_window.visible = true
      return false
    end
    
    def end_battle_flee(expulsion = false)
      if @run
        return super
      end
      draw_text("On ne s'enfuit pas", "d'un combat de dresseurs!")
      wait_hit
    end
    
    def end_battle_flee_enemy(expulsion = false)
      if @run
        return super
      end
      draw_text("On ne s'enfuit pas", "d'un combat de dresseurs!")
      wait_hit
    end
    
    #------------------------------------------------------------  
    # Exp en cours de combat
    #------------------------------------------------------------      
    def exp_battle
      @actor_status.z = @z_level + 15
      
      # Réduction de liste
      $battle_var.have_fought.uniq!
      for index in $battle_var.have_fought
        actor = $pokemon_party.actors[index]
        if actor.dead?
          $battle_var.have_fought.delete(index)
        end
      end
      number = $battle_var.have_fought.length
      
      @enemy.skill_effect_reset
      @enemy.reset_stat_stage
      if @evolve_checklist == nil
        @evolve_checklist = []
      end
      if @money_rate == nil
        @money_rate = 1
      end
      
      # Recherche de paramètres
      type = 1.3
      exp_share_number = 0
      for pokemon in $pokemon_party.actors
        if not(pokemon.dead?) and Item.data(pokemon.item_hold)["expshare"]
          exp_share_number += 1
        end
      end
      
      for actor in $pokemon_party.actors
        if actor.dead?
          next
        end
        
        amount = nil
        if $battle_var.have_fought.include?(actor.party_index)
          amount = actor.exp_calculation(@enemy.battle_list, @enemy.level, number, type, exp_share_number)
          # Tag objet
          if Item.data(actor.item_hold)["amuletcoin"]
            @money_rate = 2
          end
        end
        
        if Item.data(actor.item_hold)["expshare"] and 
            not($battle_var.have_fought.include?(actor.party_index)) and not(actor.dead?)
          amount = actor.exp_calculation(@enemy.battle_list, @enemy.level, number, type, exp_share_number, 0)
        end
        
        if amount != nil
          actor.skill_effect_reset
          actor.reset_stat_stage
          actor.add_bonus(@enemy.battle_list)
          draw_text(actor.given_name + " a gagné", amount.to_s + " points d'expérience!")
          Graphics.update
          wait_hit
        
          for i in 1..amount
            actor.add_exp_battle(1)
            if actor.level_check
              actor.level_up(self)
              if not(@evolve_checklist.include?(actor))
                @evolve_checklist.push(actor)
              end
            end
            if actor == @actor
              if @actor.exp_list[@actor.level+1] != nil and @actor.level < MAX_LEVEL
                divide = (@actor.exp_list[@actor.level+1]-@actor.exp_list[@actor.level])/192
                if divide == 0
                  divide = 1
                end
                if (@actor.exp - @actor.exp_list[@actor.level])%divide == 0
                  @actor_status.exp_refresh
                  Graphics.update
                end
              end
            end
          end
        end
      end
      
      @actor_status.refresh
      Graphics.update
      
      # Reset have_fought
      $battle_var.have_fought = [@actor.party_index]
      
    end
    
    #------------------------------------------------------------  
    # Fin de combat
    #------------------------------------------------------------      
    def end_battle_victory
      #Audio.bgm_fade(800)
      exp_battle
      
      $battle_var.result_win = true
      Audio.me_play("Audio/ME/fainted_jingle.mid")
      #Audio.me_play("Audio/ME/trainer_jingle.mid")
      Audio.bgm_play("Audio/BGM/PkmRS_Victory2.mid")
      
      
      draw_text("Vous avez battu", Trainer_Info.string(@trainer_id) + "!")
      
      @enemy_sprite.opacity = 255
      @enemy_sprite.zoom_x = 1
      @enemy_sprite.zoom_y = 1
      @enemy_sprite.visible = true
      @enemy_sprite.bitmap = RPG::Cache.battler(@start_enemy_battler, 0)
      @enemy_sprite.ox = @enemy_sprite.bitmap.width / 2
      @enemy_sprite.oy = @enemy_sprite.bitmap.height / 2
      @enemy_sprite.x = 723
      @enemy_sprite.y -= @enemy_sprite.bitmap.height / 2 + 34
      loop do
        @enemy_sprite.x -= 10
        Graphics.update
        if @enemy_sprite.x <= 464
          break
        end
      end
      wait_hit
      
      
      list_string = Trainer_Info.string_victory(@trainer_id)
      draw_text(list_string)
      wait_hit
      
      
      $battle_var.money = Trainer_Info.money(@trainer_id)
      draw_text(Player.name + " remporte " + ($battle_var.money).to_s + "$!")
      $pokemon_party.add_money($battle_var.money)
      wait(40)
      
      wait(30)
      $game_system.bgm_play($game_temp.map_bgm)
      for actor in @evolve_checklist
        info = actor.evolve_check
        if info != false
          scene = Pokemon_Evolve.new(actor, info, @z_level + 200)
          scene.main
        end
      end
      
      end_battle
    end
    
    def end_battle_defeat
      $battle_var.result_defeat = true
      list_string = Trainer_Info.string_defeat(@trainer_id)
      draw_text(list_string)
      wait_hit
      
      wait(40)
      $pokemon_party.money /= 2
      if not(@lose)
        if $game_variables[1] == 0
          print("Réglez votre point de retour!")
        else
          $game_map.setup($game_variables[1])
          $game_map.display_x = $game_variables[2]
          $game_map.display_y = $game_variables[3]
          $game_player.moveto($game_variables[2], $game_variables[3])
        end
        $game_temp.common_event_id = 2
      end
      $game_temp.map_bgm = $game_map.bgm
      end_battle(2)
    end
    
  end
  
end