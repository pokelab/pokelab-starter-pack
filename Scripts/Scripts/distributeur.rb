      def distributeur(arg1 = "EAU FRAICHE  200$", arg2 = "SODA COOL     300$", arg3 = "LIMONADE      350$", arg4 = "RETOUR")  
        window = Window_Command.new(1, [arg1, arg2, arg3, arg4], $fontsizebig)  
        width = [window.contents.text_size(arg1).width, window.contents.text_size(arg2).width, window.contents.text_size(arg3).width, window.contents.text_size(arg4).width].max + 16  
        window.dispose  
        @command = Window_Command.new(width + 32, [arg1, arg2, arg3, arg4], $fontsizebig)  
        @command.x = 605 - width  
        @command.y = 2  
        loop do  
          Graphics.update  
          Input.update  
          @command.update  
          if Input.trigger?(Input::C) and @command.index == 0  
            if $pokemon_party.money < 200  
              $game_system.se_play($data_system.buzzer_se)  
              @command.dispose  
              @command = nil  
              return  
            end  
            $game_variables[516] = 1  
            @command.dispose  
            @command = nil  
            #Input.update  
            @wait_count = 2  
            return true  
          end  
          if Input.trigger?(Input::C) and @command.index == 1  
            if $pokemon_party.money < 300  
              $game_system.se_play($data_system.buzzer_se)  
              @command.dispose  
              @command = nil  
              return  
            end  
            $game_variables[516] = 2  
            @command.dispose  
            @command = nil  
            #Input.update  
            @wait_count = 2  
            return true  
          end  
          if Input.trigger?(Input::C) and @command.index == 2  
            if $pokemon_party.money < 350  
              $game_system.se_play($data_system.buzzer_se)  
              @command.dispose  
              @command = nil  
              return  
            end  
            $game_variables[516] = 3  
            @command.dispose  
            @command = nil  
            #Input.update  
            @wait_count = 2  
            return true  
          end  
          if Input.trigger?(Input::C) and @command.index == 3  
            @command.dispose  
            @command = nil  
            #Input.update  
            @wait_count = 2  
            return false  
          end  
          if Input.trigger?(Input::B)  
            $game_system.se_play($data_system.cancel_se)  
            @command.dispose  
            @command = nil  
            #Input.update  
            @wait_count = 2  
            return false  
          end  
        end  
      end  