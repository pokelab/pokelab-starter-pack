#==============================================================================
# ● Base de données - Dresseurs
# Pokemon Script Project - Krosk 
# 29/07/07
#==============================================================================

module POKEMON_S
  $data_trainer = []
  $data_trainer[0] = ["", "", "", [], [], 0, ["",""], ["",""]]
  
  #$data_trainer[id] = [
  #"battler.png", 
  #"TYPE",
  #"Nom"
  #[
  #  [Pokémon 1, niveau, //objet, //[stats avancées], //[skill 1, 2, 3, 4]], 
  #  [Pokémon 2, niveau, //objet, //[stats avancées], //[skill 1, 2, 3, 4]], 
  #  [Pokémon 3, niveau, //objet, //[stats avancées], //[skill 1, 2, 3, 4]], 
  #  [Pokémon 4, niveau, //objet, //[stats avancées], //[skill 1, 2, 3, 4]], 
  #  [Pokémon 5, niveau, //objet, //[stats avancées], //[skill 1, 2, 3, 4]], 
  #  [Pokémon 6, niveau, //objet, //[stats avancées], //[skill 1, 2, 3, 4]], 
  #],
  #[ "Objets" ]
  #Argent,
  #["String Victoire Ligne 1", "String Victoire Ligne 2"] (Joueur gagnant)
  #["String Défaite Ligne 1", "String Défaite Ligne 2"] (Joueur perdant)
  #]
  
  $data_troops       = load_data("Data/Troops.rxdata")
  for i in 1..$data_troops.length-1
    $data_trainer[i] = []
    name = $data_troops[i].name
    
    # Vérification Tag
    tag = (name.split('/'))[0]
    if tag != "T"
      $data_trainer[i] = $data_trainer[0]
      next
    end
    
    # Nom et Type
    data_name = (name.split('/'))[1]
    data_name = data_name.split('_')
    
    if data_name[1] != nil
      $data_trainer[i][1] = data_name[0].to_s
      $data_trainer[i][2] = data_name[1].to_s
    else
      $data_trainer[i][1] = ""
      $data_trainer[i][2] = data_name[0].to_s
    end
    
    # Pokémons
    list = []
    for member in $data_troops[i].members
      list.push([member.enemy_id, 1, nil, nil, nil])
    end
    
    # Vérification page event
    if $data_troops[i].pages == []
      # Effacage car invalide
      $data_trainer[i] = $data_trainer[0]
      next
    end
    
    # Event Page1
    event_list = $data_troops[i].pages[0].list
    
    # Effacage si aucun event
    if event_list == []
      $data_trainer[i] = $data_trainer[0]
      next
    end
    
    for event in event_list
      case event.code
      when 322 # Changer Apparence => Définition battler
        battler = event.parameters[3]
        $data_trainer[i][0] = battler
      when 332 # Ajout de MP => Définition Level
        target = event.parameters[0]
        operation = event.parameters[1]
        operand_type = event.parameters[2]
        operand = event.parameters[3]
        if operand_type == 0
          value = operand
        else
          if $game_variables == nil or $game_variables[operand] == nil
            value = 5
          else
            value = $game_variables[operand]
          end
        end
        
        value = value.abs
        
        case target
        when -1 # Tous les ennemis => ecart // Rien 
          for element in list
            element[1] = value
          end
        else # Ennemi défini
          if list[target+1] != nil
            list[target+1][1] = value
          end
        end
      when 355 # Script
        index_script = 0
        script = event.parameters[0]
        if script == "Pokemon"
          pokemon_tag = true
        else
          pokemon_tag = false
        end
        if script == "Dresseur"
          trainer_tag = true
        else
          trainer_tag = false
        end
      when 655 # Script suite
        if pokemon_tag
          script = event.parameters[0]
          # type Hash personnalisé
          if eval(script).type == Hash and list[index_script] != nil
            # Niveau
            list[index_script][1] = eval(script)["NV"]
            # Objet
            list[index_script][2] = eval(script)["OBJ"]
            # Stats avancées
            list[index_script][3] = eval(script)["STAT"]
            # Moveset
            list[index_script][4] = eval(script)["MOVE"]
            # Genre
            list[index_script][5] = eval(script)["GR"]
            # Forme
            list[index_script][6] = eval(script)["FORM"]
            # shiny
            list[index_script][7] = eval(script)["SHINY"]
          end
          # type Fixnum réglage de niveau seulement
          if eval(script).type == Fixnum and list[index_script] != nil
            list[index_script][1] = eval(script)
          end
          index_script += 1
        end
        if trainer_tag
          index_script += 1
          script = event.parameters[0]
          case index_script
          when 1 # Argent
            $data_trainer[i][5] = script.to_i
          when 2 # String Victoire / Défaite
            $data_trainer[i][6] = []
            $data_trainer[i][6][0] = script != nil ? script : ""
          when 3
            $data_trainer[i][6][1] = script != nil ? script : ""
          when 4
            $data_trainer[i][7] = []
            $data_trainer[i][7][0] = script != nil ? script : ""
          when 5
            $data_trainer[i][7][1] = script != nil ? script : ""
          end
        end
      end
    end
        
    # Vérification: expulsion des Pokémons sans niveau
    for pokemon in list
      if pokemon[1] == nil or pokemon[0] == nil
        index = list.index(pokemon)
        list[index] = nil
      end
    end
    
    list.compact!
    
    $data_trainer[i][3] = list
  end
  
  class Trainer_Info
    def self.battler(id)
      return $data_trainer[id][0]
    end
    
    def self.type(id)
      return $data_trainer[id][1]
    end
    
    def self.name(id)
      name_string = $data_trainer[id][2].gsub(/\\[Nn]\[([0-9]+)\]/) {$game_actors[$1.to_i] != nil ? $game_actors[$1.to_i].name : ""}
      return name_string
    end
    
    def self.string(id)
      return (type(id) + " " + name(id))
    end
    
    def self.money(id)
      return $data_trainer[id][5]
    end
    
    def self.pokemon(id, index)
      return $data_trainer[id][3][index]
    end
    
    def self.pokemon_list(id)
      return $data_trainer[id][3]
    end
    
    def self.string_victory(id)
      return $data_trainer[id][6]
    end
    
    def self.string_defeat(id)
      return $data_trainer[id][7]
    end
  end
  
  
  # -----------------------------------------------------------------
  #   Classe d'information du joueur
  # -----------------------------------------------------------------
  class Player
    def self.id
      return sprintf("%05d", $game_variables[9]%(2**16))
    end
    
    def self.code
      return $game_variables[9]
    end
    
    def self.name
      return $game_party.actors[0].name
    end
    
    def self.battler
      return $game_party.actors[0].battler_name
    end
    
    def self.set_trainer_code(value)
      $game_variables[9] = value
    end
    
    def self.trade_list
      if not $game_variables[10].is_a?(Array)
        $game_variables[10] = []
      end
      return $game_variables[10]
    end
    
    def self.register_code(value)
      if not $game_variables[10].is_a?(Array)
        $game_variables[10] = []
      end
      $game_variables[10].push(value)
      $game_variables[10].uniq!
    end
    
    def self.erase_code(value)
      if not $game_variables[10].is_a?(Array)
        $game_variables[10] = []
      end
      $game_variables[10].delete(value)
    end
    
    def self.pokedex_update
      for i in 1..$data_pokemon.length-1
        if $data_pokedex[i] == nil
          $data_pokedex[i] = [false, false]
        end
      end
    end
  end
  
end