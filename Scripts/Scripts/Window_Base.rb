#==============================================================================
# ■ Window_Base
# Pokemon Script Project - Krosk 
# 18/07/07
#-----------------------------------------------------------------------------
# Scène modifiable
#-----------------------------------------------------------------------------
class Window_Base < Window

  def normal_color
    return Color.new(60, 60, 60, 255)
  end
  
  def disabled_color
    return Color.new(60, 60, 60, 128)
  end
  
  def white
    return Color.new(255, 255, 255, 255)
  end
  
  def draw_text(x, y, w, h, string, align = 0, color = white)
    self.contents.font.color = Color.new(96,96,96,175)
    self.contents.draw_text(x + 3, y + 3, w, h, string, align)
    self.contents.font.color = color
    self.contents.draw_text(x, y, w, h, string, align)
  end
  
  def text_color(n)
    case n
    when 0
      return normal_color
    when 1
      return Color.new(128, 128, 255, 255)
    when 2
      return Color.new(255, 128, 128, 255)
    when 3
      return Color.new(128, 255, 128, 255)
    when 4
      return Color.new(128, 255, 255, 255)
    when 5
      return Color.new(255, 128, 255, 255)
    when 6
      return Color.new(255, 255, 128, 255)
    when 7
      return Color.new(192, 192, 192, 255)
    else
      normal_color
    end
  end

  
end
