#==============================================================================
# ■ Pokemon_Window_Help
# Pokemon Script Project - Krosk 
# 18/07/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------
# Window_Help modifié
#-----------------------------------------------------------------------------

module POKEMON_S
  class Pokemon_Window_Help < Window_Base
    attr_reader :z_level
    #--------------------------------------------------------------------------
    # 
    #--------------------------------------------------------------------------
    def initialize
      super(21, 342, 597, 126)
      self.contents = Bitmap.new(width - 32, height - 32)
      self.contents.font.name = $fontface
      self.contents.font.size = $fontsize
      self.z = 9998
      self.opacity = 0
      @dummy = Sprite.new
      @dummy.bitmap = RPG::Cache.picture(POKEMON_S::MSG)
      @dummy.visible = false
      @dummy.y = 336
      @dummy.z = 9997
      @z_level = 9998
    end
    
    #--------------------------------------------------------------------------
    # 
    #--------------------------------------------------------------------------
    def draw_text(text, text_or_align = 0, is_align = 0)
      if text_or_align != 0
        align = is_align
      else
        align = text_or_align
      end
      
      # テキストとアラインメントの少なくとも一方が前回と違っている場合
      if text != @text or align != @align
        # テキストを再描画
        self.contents.clear
        self.contents.font.color = normal_color
        @text = text
        if text_or_align == 0
          string = string_builder(text, 58)
          self.contents.draw_text(4, 0, self.width - 40, 32, string[0], align)
          self.contents.draw_text(4, 32, self.width - 40, 32, string[1], align)
          self.contents.draw_text(4, 64, self.width - 40, 32, string[2], align)
        else
          @text = text
          self.contents.draw_text(4, 0, self.width - 40, 32, text, align)
          self.contents.draw_text(4, 32, self.width - 40, 32, text_or_align, align)
        end
        @align = align
      end
      self.visible = true
      @dummy.visible = true
      if text == "" and text_or_align == ""
        @dummy.visible = false
        self.visible = false
      end
    end
    
    def dispose
      @dummy.dispose
      @dummy = nil
      super
    end
    
    #--------------------------------------------------------------------------
    # string_builder
    # Permet le retour automatique à la ligne du texte
    #   text: chaine de caractère
    #   limit: le nombre maximum de caractères sur une ligne
    #--------------------------------------------------------------------------    
    def string_builder(text, limit)
      length = text.length
      full1 = false
      full2 = false
      full3 = false
      string1 = ""
      string2 = ""
      string3 = ""
      word = ""
      for i in 0..length
        letter = text[i..i]
        if letter != " " and i != length
          word += letter.to_s
        else
          word = word + " "
          if (string1 + word).length < limit and not(full1)
            string1 += word
            word = ""
          else
            full1 = true
          end
          
          if (string2 + word).length < limit and not(full2)
            string2 += word
            word = ""
          else
            full2 = true
          end
          
          if (string3 + word).length < limit and not(full3)
            string3 += word
            word = ""
          else
            full3 = true
          end
        end
      end
      return [string1, string2, string3]
    end
  end
end
