#==============================================================================
# ■ Window_NameEdit
# Pokemon Script Project - Krosk 
# 19/08/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier
#-----------------------------------------------------------------------------

class Window_NameEdit < Window_Base
  attr_reader   :name                     # 名前
  attr_reader   :index                    # カーソル位置
  #--------------------------------------------------------------------------
  # ● オブジェクト初期化
  #--------------------------------------------------------------------------
  def initialize(actor, max_char)
    super(99, 27, 442, 114)
    self.contents = Bitmap.new(width - 32, height - 32)
    self.contents.font.name = $fontface
    self.contents.font.size = $fontsizebig
    self.contents.font.color = normal_color
    self.opacity = 0
    if actor.type == String
      @actor = nil
      @name = actor
    else
      @actor = actor
      @name = actor.name
    end
    @max_char = max_char
    
    name_array = @name.split(//)[0...@max_char]
    @name = ""
    for i in 0...name_array.size
      @name += name_array[i]
    end
    @default_name = @name
    @index = name_array.size
    refresh
    update_cursor_rect
  end
  #--------------------------------------------------------------------------
  # ● リフレッシュ
  #--------------------------------------------------------------------------
  def refresh
    self.contents.clear
    
    name_array = @name.split(//)
    for i in 0...@max_char
      c = name_array[i]
      if c == nil
        c = "_"
      end
      x = 120 + i * 21
      draw_text(x, 33, 24, 44, c, 1, normal_color)
    end
    
    self.contents.font.size = $fontsize
    if @actor != nil
      self.contents.draw_text(69, 6, 321, 39, "Quel nom ?", 1)
    else
      self.contents.draw_text(69, 6, 321, 39, "CODE ECHANGE ?", 1)
    end
    self.contents.font.size = $fontsizebig
    
    if @actor != nil
      bitmap = RPG::Cache.character(@actor.character_name, @actor.character_hue)
      cw = bitmap.width / 4
      ch = bitmap.height / 4
      src_rect = Rect.new(0, 0, cw, ch)
      self.contents.blt(20, 0, bitmap, src_rect, 255)
    end
  end
  #--------------------------------------------------------------------------
  # ● カーソルの矩形更新
  #--------------------------------------------------------------------------
  def update_cursor_rect
    x = 115 + @index * 21
    self.cursor_rect.set(x, 40, 32, 32)
  end
end
