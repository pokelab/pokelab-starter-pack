#==============================================================================
# ■ Pokemon_Battle_Wild
# Pokemon Script Project - Krosk 
# 20/07/07
# Pokemon_Battle_Core_Safari
# Therand - 01/2015
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------
# Système de Combat - Pokémon Sauvage du parc safari
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# Ici se trouve le calcul de capture et l'animation de la safari ball
# ainsi que la pré-décision de l'enemy
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# Ordre de déroulement du combat (update)
#-----------------------------------------------------------------------------

module POKEMON_S
  #------------------------------------------------------------  
  # Pokemon_Battle_Safari
  #   scene
  #------------------------------------------------------------  
  class Pokemon_Battle_Safari < Pokemon_Battle_Core_Safari
    attr_accessor :z_level
    attr_accessor :actor_status
    attr_accessor :actor
    #------------------------------------------------------------  
    # Fonction d'initialisation
    # Appel: $scene = POKEMON_S::Pokemon_Battle_Safari.new(
    #           party, pokemon, ia)
    #     party: $pokemon_party (classe Pokemon_Party)
    #     pokemon: class Pokemon
    #     ia: Fausse AI
    #------------------------------------------------------------
    def initialize(party, pokemon, ia = false, lose_able = false)
      @z_level = 0
      @ia = ia
      @lose = lose_able
      $battle_var.reset
      # Assignations données des Pokémons
      @enemy = pokemon
      if not(@enemy.given_name.include?("sauvage"))
        @enemy.given_name += " sauvage"
      end
      $battle_var.enemy_party.actors[0] = @enemy
      @party = party
      
      # Mise à jour Pokedex: Pokémon vu
      $data_pokedex[@enemy.id][0] = true
      
      # Génération ordre de combat
      @battle_order = Array.new(@party.size)
      @battle_order.fill {|i| i}
      
      # Désignation 1er Pokémon au combat
      # @actor désigne le (class) Pokémon
      actor_index = 0
      @actor = @party.actors[actor_index]
      if @actor == nil
        print("Attention, vous n'avez pas de Pokémon dans votre équipe! Réglez ce bug.")
      end
      while @actor.dead?
        actor_index += 1
        @actor = @party.actors[actor_index]
      end
    
      
      # Correction ordre combat (Pokémon vivant en premier)
      @battle_order = switch(@battle_order, 0, actor_index)
      
      # Remise à zéro résultat
      $battle_var.result_flee = false
      $battle_var.result_win = false
      $battle_var.result_defeat = false
      
      # Initialisation des variables de combat
      @phase = 0
      # 0 Non décidé, 1 Attaque, 2 Switch, 3 Item, 4 Fuite
      @actor_action = 0
      @enemy_action = 0
      
      @start_actor_battler = Player.battler # $trainer_battler
      @start_enemy_battler = @enemy.battler_face
      
      $battle_var.have_fought.push(@actor.party_index)
      $battle_var.battle_order = @battle_order
      $battle_var.in_battle = true
      
      @actor.reset_stat_stage
      @enemy.reset_stat_stage
      @actor.skill_effect_reset
      @enemy.skill_effect_reset
      @actor_skill = nil
      @enemy_skill = nil
      @actor.ability_active = false
      @enemy.ability_active = false
      @item_id = 0 # item utilisé
      #Initialisation de toutes les variables du safari
      #colère
      @angry = 0
      #Faim
      @eating = 0
      #Multiplicateur de drop rate
      @chance_safari = 1
      #Nombre aléatoire obtenu après tour (1 à 255)
      @decision_safari = 0
      #verificateur si ball lancé et si pkmn capturé
      @ball = 0
    end
    
    #------------------------------------------------------------  
    # Animations pré-combat
    #------------------------------------------------------------  
    def pre_battle_transition
      # Jingle et BGM
      $game_system.bgm_play($game_system.battle_bgm)
      Audio.me_play("Audio/ME/battle_jingle.mid")
      Graphics.freeze
      
      # Sélection transition
      s = (rand(BATTLE_TRANS)+1).to_s
      @background.bitmap = RPG::Cache.picture("black.png")
      Graphics.transition(100, "Graphics/Transitions/battle"+ s +".png") 
      Audio.me_stop
      
      # Dessin
      Graphics.freeze
      @background.bitmap = RPG::Cache.battleback(@battleback_name)
      @message_background.bitmap = RPG::Cache.battleback(BATTLE_MSG)
      @enemy_sprite.bitmap = RPG::Cache.battler(@start_enemy_battler, 0)
      @enemy_sprite.ox = @enemy_sprite.bitmap.width / 2
      @enemy_sprite.oy = @enemy_sprite.bitmap.height / 2
      @enemy_sprite.x -= 782
      @enemy_sprite.color = Color.new(60,60,60,128)
      @enemy_ground.bitmap = RPG::Cache.battleback(@ground_name)
      @enemy_ground.ox = @enemy_ground.bitmap.width / 2
      @enemy_ground.oy = @enemy_ground.bitmap.height / 2
      @enemy_ground.zoom_x = @enemy_ground.zoom_y = 2.0/3
      @enemy_ground.x -= 782
      @actor_ground.bitmap = RPG::Cache.battleback(@ground_name)
      @actor_ground.ox = @actor_ground.bitmap.width / 2
      @actor_ground.oy = @actor_ground.bitmap.height
      @actor_ground.x += 782
      @actor_sprite.bitmap = RPG::Cache.battler(@start_actor_battler, 0)
      @actor_sprite.ox = @actor_sprite.bitmap.width / 2
      @actor_sprite.oy = @actor_sprite.bitmap.height
      @actor_sprite.x += 782
      Graphics.transition(50, "Graphics/Transitions/battle0.png")
    end
    
    def pre_battle_animation
      # Glissement des sprites
      loop do
        @enemy_sprite.x += 17
        @enemy_ground.x += 17
        @actor_sprite.x -= 17
        @actor_ground.x -= 17
        Graphics.update
        if @enemy_sprite.x == 464
          until @enemy_sprite.color.alpha <= 0
            @enemy_sprite.color.alpha -= 20
            Graphics.update
          end
          break
        end
      end
      
      # Texte
      draw_text("Un " + @enemy.given_name, "apparait!")
      if FileTest.exist?(@enemy.cry)
        Audio.se_play(@enemy.cry)
      end
      
      # Arrivé du panel de l'adversaire
      @enemy_status.x -= 300
      @enemy_status.visible = true
      if @enemy.shiny
        animation = $data_animations[496]
        @enemy_sprite.animation(animation, true)
      end
      loop do
        @enemy_sprite.x -= 3*(-1)**(@enemy_sprite.x)
        @enemy_status.x += 20
        @enemy_sprite.update
        Graphics.update
        if @enemy_status.x == 23
          until not(@enemy_sprite.effect?)
            @enemy_sprite.update
            Graphics.update
          end
          @enemy_sprite.x = 464
          break
        end
      end
      
      # Attente appui de touche
      loop do
        Graphics.update
        Input.update
        if Input.trigger?(Input::C)
          $game_system.se_play($data_system.decision_se)
          break
        end
      end   
      @text_window.contents.clear
      Graphics.update
    end
    #------------------------------------------------------------  
    # Déroulement
    #------------------------------------------------------------
    #La pré-décision enemy de l'action à utiliser en réponse à la votre
def enemy_skill_decision
  action = true
      # ------- ---------- --------- --------
      #    Saut de phase de sélection (ennemi)
      #   action = false  : saut
      #   action = true   : pas de saut
      # ------- ---------- --------- --------
      action = phase_jump(true)
      
      @enemy_action = 1
      #Réinitialisation entre chaque tour
      @decision_safari = 0
      #Nombre aléatoire entre 1 et 255 inclus
      @decision_safari += 1 + rand(256)

      # Le skill paisible (ce n'est pas réelement un skill en faite, il ne
      # demande pas de réaction de la part du pokémon, juste un calcul)
      # Si ball nous indique que le joueur à utilisé une ball et raté la capture
      if @ball == 1
        wait(40)
        #On vérifie si l'enemy fuit
        if @decision_safari <= (@enemy.spd * 2)
          scene = end_battle_flee_enemy
        end
      end
      # Le skill furieux
      if @angry != 0
        @angry -= 1
        #On prépare le skill à utiliser
        @enemy_skill = Skill.new(566)
      end
      # Le skill manger
      if @eating != 0
        @eating -= 1
        #On prépare le skill à utiliser
        @enemy_skill = Skill.new(564)
      end
end

    #------------------------------------------------------------  
    # Rounds
    #------------------------------------------------------------                
    def end_battle_check
      @actor_status.refresh
      @enemy_status.refresh
      if @enemy.dead? and not(@party.dead?)
        end_battle_victory
      elsif @actor.dead?
        if @party.dead?
          end_battle_defeat
        else
          draw_text("Voulez-vous appeler", "un autre Pokémon?")
          if draw_choice
            $battle_var.window_index = @action_window.index
            scene = POKEMON_S::Pokemon_Party_Menu.new(0)
            scene.main
            return_data = scene.return_data
            # Switch de Pokémon
            if $battle_var.action_id == 4 or $battle_var.action_id == 6
              @switch_id = return_data
              actor_pokemon_switch
            end
          elsif run_able?(@actor, @enemy)
            run
          else
            fail_flee
            $battle_var.window_index = @action_window.index
            scene = POKEMON_S::Pokemon_Party_Menu.new(0)
            scene.main
            return_data = scene.return_data
            # Switch de Pokémon
            if $battle_var.action_id == 4 or $battle_var.action_id == 6
              @switch_id = return_data
              actor_pokemon_switch
            end
          end
        end
      end
    end
    
    #------------------------------------------------------------  
    # Items
    #------------------------------------------------------------     
    def actor_item_use # items à utiliser
      # Item déjà utilisé ie remplacé par 0
      if @item_id == 0
        return
      end
      if Item.data(@item_id)["flee"]
        end_battle_flee
        return
      end
      if Item.data(@item_id)["ball"]
        ball_data = Item.data(@item_id)["ball"]
        if catch_pokemon(ball_data)
          @enemy.given_name = @enemy.name
          @enemy.ball_data = ball_data
          # Changement de surnom
          string1 = "Voulez-vous changer le"
          string2 = "surnom de " + @enemy.given_name + "?"
          draw_text(string1, string2)
          if draw_choice
            draw_text("")
            scene = POKEMON_S::Pokemon_Name.new(@enemy, @z_level + 50)
            scene.main
          end
          # Intégration au PC
          if $pokemon_party.size < 6
            $pokemon_party.add(@enemy)
          else
            $pokemon_party.store_captured(@enemy)
            string1 = @enemy.given_name
            string2 = "est envoyé au PC."
            draw_text(string1, string2)
            wait(40)
          end
          $battle_var.result_win = true
          end_battle
        end
      end
    end

    

    #------------------------------------------------------------  
    # Lancer de pokéball
    #------------------------------------------------------------         
    def catch_pokemon(ball_data)
      # Initialisation des données
      ball_name = ball_data[0]
      ball_rate = ball_data[1]
      ball_sprite = ball_data[2]
      ball_open_sprite = ball_data[3]
      ball_color = ball_data[4]
      if @enemy.asleep? or @enemy.frozen?
        status_multiplier = 2
      elsif @enemy.burn? or @enemy.paralyzed? or 
          @enemy.poisoned? or @enemy.toxic?
        status_multiplier = 1.5
      else
        status_multiplier = 1
      end

      if ball_rate.type == Array
        case ball_rate[0]
        when "type"
          if ball_rate[1].include?(@enemy.type1)
            ball_rate = ball_rate[2]
          elsif ball_rate[1].include?(@enemy.type2)
            ball_rate = ball_rate[2]
          elsif ball_rate[3] != nil
            ball_rate = ball_rate[3]
          else
            ball_rate = 1
          end
        when "id"
          if ball_rate[1].include?(@enemy.id)
            ball_rate = ball_rate[2]
          elsif ball_rate[3] != nil
            ball_rate = ball_rate[3]
          else
            ball_rate = 1
          end
        when "level"
          max = 99
          if ball_rate[2] != nil
            
            max = ball_rate[2]
          end
          ball_rate = eval( sprintf( ball_rate[1], @enemy.level ) )
          if ball_rate <= 1
            ball_rate = 1
          end
          if ball_rate >= max
            ball_rate = max
          end
        when "time"
          max = 99
          if ball_rate[2] != nil
            max = ball_rate[2]
          end
          ball_rate = eval( sprintf( ball_rate[1], $battle_var.round ) )
          if ball_rate <= 1
            ball_rate = 1
          end
          if ball_rate >= max
            ball_rate = max
          end
        else
          ball_rate = 1
        end
      end
      
# Calcul des la possibilité de capture.    
# Source : Poképedia (http://www.pokepedia.fr/index.php/Capture_des_Pok%C3%A9mon)    
# Conversion des valeurs utilisé en flottant    
maxhp             = @enemy.maxhp_basis.to_f    
hp                = @enemy.hp.to_f    
rareness          = @enemy.rareness.to_f    
ball_rate         = 1.5#Le taux de la ball (Safari ball = X1.5)  
status_multiplier = status_multiplier.to_f   
chance_safari     = @chance_safari#On convertis notre variable "multiplicatrice"
          
# Calcul du premier flottant  
# ball_rate est multiplié par un multiple de 2 en fonction du nombre de caillou/appat
a = Float(((maxhp * 3 - 2 * hp) * rareness * (ball_rate * chance_safari) * status_multiplier)/(maxhp * 3))    
          
if a >= 255 # Le Pokémon est capturé    
  j = 4    
else           # Deuxième chance de capture    
  b = 65535.0 * Math.sqrt(Math.sqrt(a/255.0))    
  j = 0        # Initialement, la ball ne bouge pas.    
  for i in 0...4    
    if rand(65535) <= b   # Ajoute un mouvement de la ball pour chaque nombre aléatoire en dessous de b    
      j += 1                    # Il y a une chance pour qu'ils soient tous en dessous de b et que le Pokémon soit capturé au rattrapage (comme au bac)    
    end    
  end    
end  

      # Procédure / Animation
      # Lancer
      # Animation forcé de la safari ball
      draw_text(Player.name + " utilise", "SAFARI BALL!")
      @ball_sprite = Sprite.new
      @ball_sprite.bitmap = RPG::Cache.picture("ball12") 
      @ball_sprite.x = -25
      @ball_sprite.y = 270
      @ball_sprite.z = @z_level + 16
      t = 0.0
      pi = Math::PI
      loop do
        t += 16
        @ball_sprite.y = (270.0 - 230.0 * Math.sin(2 * pi / 3.0 * t / 445.0)).to_i
        @ball_sprite.x = -15 + t
        Graphics.update
        if @ball_sprite.x >= 445
          @ball_sprite.bitmap = RPG::Cache.picture("ballopen12")
          break
        end
      end
      
      count = j
      
      # "Aspiration"
      @enemy_sprite.color = Color.new(0, 0, 0, 255)
      @enemy_sprite.color.alpha = 0
      @ball_sprite.z -= 2
      until @enemy_sprite.color.alpha >= 255
        @flash_sprite.opacity += 50
        @enemy_sprite.color.alpha += 50
        Graphics.update
      end
      Audio.se_play("Audio/SE/Pokeopen.wav")
      loop do
        @enemy_sprite.zoom_x -= 0.1
        @enemy_sprite.zoom_y -= 0.1
        @enemy_sprite.opacity -= 25
        @flash_sprite.opacity -= 25
        Graphics.update
        if @enemy_sprite.zoom_x <= 0.1
          @flash_sprite.opacity = 0
          @enemy_sprite.opacity = 0
          break
        end
      end
      @ball_sprite.bitmap = RPG::Cache.picture("ball12")
      
      # Oscillement
      t = 0
      r = 0
      loop do
        t += 1
        @ball_sprite.y = 81 + 50*(1-Math.exp(-t/20.0)*(Math.cos(t*2*pi/30.0)).abs)
        if @ball_sprite.y >= 81+45 and r < 6
          r += 1
          Audio.se_play("Audio/SE/Pokerebond.wav")
        end
        Graphics.update
        if t >= 60
          break
        end
      end
      
      while count > 0
        count -= 1
        t = 0
        Audio.se_play("Audio/SE/Pokemove.wav")
        loop do
          t += 4
          @ball_sprite.angle = 40*Math.sin(2*pi*t/100.0)
          @ball_sprite.x = 449 - 12*Math.sin(2*pi*t/100.0)
          @ball_sprite.y = 131 + 12*Math.sin(2*pi*t/100.0)
          Graphics.update
          if t == 100
            @ball_sprite.angle = 0
            break
          end
        end
      end
      
      if j != 4
        # Echappé
        @ball_sprite.bitmap = RPG::Cache.picture("ball12")
        @ball_sprite.z -= 1
        Audio.se_stop
        Audio.se_play("Audio/SE/Pokeopenbreak.wav")
        @enemy_sprite.oy = @enemy_sprite.bitmap.height
        @enemy_sprite.y += @enemy_sprite.bitmap.height / 2
        loop do
          @enemy_sprite.opacity += 25
          @enemy_sprite.zoom_x += 0.1
          @enemy_sprite.zoom_y += 0.1
          @ball_sprite.opacity -= 25
          @flash_sprite.opacity += 25
          Graphics.update
          if @enemy_sprite.zoom_x >= 1
            @enemy_sprite.oy = @enemy_sprite.bitmap.height / 2
            @enemy_sprite.y -= @enemy_sprite.bitmap.height / 2
            Graphics.update
            @ball_sprite.dispose
            break
          end
        end
        until @enemy_sprite.color.alpha <= 0
          @enemy_sprite.color.alpha -= 25
          @flash_sprite.opacity -= 25
          Graphics.update
        end
        @enemy_sprite.color.alpha = 0
        @enemy_sprite.opacity = 255
        @flash_sprite.opacity = 0
        Graphics.update
        
        string1 = j == 3 ? "Mince!" : j == 2 ? "Aaaah!" : j == 1 ? "Raah!" : "Oh non!"
        string2 = j == 3 ? "Ca y était presque!" : j == 2 ? "Presque!" : j == 1 ? "Ca y était presque!" : "Le POKéMON s'est libéré!"
        draw_text(string1, string2)
        wait(40)
      else
        # Attrapé
        $battle_var.action_id = 2
        Audio.me_play("Audio/ME/PkmRS-Caught.mid")
        @enemy_caught = true
        draw_text("Et hop! " + @enemy.given_name , "est attrapé!")
        wait(90)
        wait_hit
      if not($data_pokedex[@enemy.id][1])  
        draw_text(@enemy.name + " est ajouté", "au pokédex.")  
        wait(50)  
        wait_hit  
        $data_pokedex[@enemy.id][0] = true  
        $data_pokedex[@enemy.id][1] = true  
        scene = POKEMON_S::Pokemon_Detail.new(@enemy.id, false, 0, "combat", 9999)  
        scene.main  
        wait(10)  
        Graphics.transition  
        wait(10)  
      end  
        until @ball_sprite.opacity <= 0
          @ball_sprite.opacity -= 25
          Graphics.update
        end
      end
      if j != 4
        return false
      elsif j == 4
        return true
      end
    end
    
    
    
    #------------------------------------------------------------  
    # Fin de combat
    #------------------------------------------------------------      
    def end_battle_victory
      #Audio.bgm_fade(800)
      $battle_var.result_win = true
      @actor_status.z = @z_level + 15
      
      #$game_system.me_play($game_system.battle_end_me)
      Audio.me_play("Audio/ME/fainted_jingle.mid")
      Audio.bgm_play("Audio/BGM/PkmRS-Victory.mid")
      
      # Réduction du nombre de participants
      $battle_var.have_fought.uniq!
      for index in $battle_var.have_fought
        actor = $pokemon_party.actors[index]
        if actor.dead?
          $battle_var.have_fought.delete(index)
        end
      end
      number = $battle_var.have_fought.length
      @enemy.skill_effect_reset
      @enemy.reset_stat_stage
      evolve_checklist = []
      type = 1
      
      # PIECE RUNE/AMULET COIN
      money_rate = 1
      # EXP SHARE/MULTI EXP
      exp_share_number = 0
      for pokemon in $pokemon_party.actors
        if not(pokemon.dead?) and Item.data(pokemon.item_hold)["expshare"]
          exp_share_number += 1
        end
      end
      
      # Exp de bataille
      for actor in $pokemon_party.actors
        if actor.dead?
          next
        end
        
        amount = nil
        if $battle_var.have_fought.include?(actor.party_index)
          amount = actor.exp_calculation(@enemy.battle_list, @enemy.level, number, type, exp_share_number)
          # Tag objet
          if Item.data(actor.item_hold)["amuletcoin"]
            money_rate = 2
          end
        end
        if Item.data(actor.item_hold)["expshare"] and 
            not($battle_var.have_fought.include?(actor.party_index)) and not(actor.dead?)
          amount = actor.exp_calculation(@enemy.battle_list, @enemy.level, number, type, exp_share_number, 0)
        end
        if amount != nil
          actor.skill_effect_reset
          actor.reset_stat_stage
          actor.add_bonus(@enemy.battle_list)
          draw_text(actor.given_name + " a gagné", amount.to_s + " points d'expérience!")
          Graphics.update
          wait_hit
        
          for i in 1..amount
            actor.add_exp_battle(1)
            if actor.level_check
              actor.level_up(self)
              evolve_checklist.push(actor)
            end
            if actor == @actor
              if @actor.exp_list[@actor.level+1] != nil and @actor.level < MAX_LEVEL
                divide = (@actor.exp_list[@actor.level+1]-@actor.exp_list[@actor.level])/192
                if divide == 0
                  divide = 1
                end
                if (@actor.exp - @actor.exp_list[@actor.level])%divide == 0
                  @actor_status.exp_refresh
                  Graphics.update
                end
              end
            end
          end
        end
      end
      
      
      @actor_status.refresh
      Graphics.update
      if $battle_var.money > 0
        $battle_var.money *= money_rate
        draw_text(Player.name + " gagne " + ($battle_var.money).to_s + "$!")
        $pokemon_party.add_money($battle_var.money)
        wait(40)
      end
      
      wait(30)
      $game_system.bgm_play($game_temp.map_bgm)
      for actor in evolve_checklist
        info = actor.evolve_check
        if info != false
          scene = POKEMON_S::Pokemon_Evolve.new(actor, info, @z_level + 200)
          scene.main
        end
      end
      end_battle
    end
    
    
  end
  
end