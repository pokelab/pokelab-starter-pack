#==============================================================================
# ■ Pokemon_Custom
# Pokemon Script Project - Krosk 
# 05/09/08
#-----------------------------------------------------------------------------
# Méthodes spéciales pour Pokémons et capacités spécifiques
#-----------------------------------------------------------------------------
# Ce script sert à placer les modifications de méthodes propres à certains 
# Pokémons ou capacités spéciales, pour éviter de modifier les scripts clés
#-----------------------------------------------------------------------------
# La méthode à suivre doit être comprise avant d'essayer d'ajouter des règles...
#-----------------------------------------------------------------------------


module POKEMON_S
  class Pokemon
    
    # -----------------------------------------------------------------
    #  Stat spéciale Munja
    # -----------------------------------------------------------------
    alias temp_maxhp_basis maxhp_basis
    def maxhp_basis
      if name == "MUNJA"
        return 1
      end
      temp_maxhp_basis
    end
    
    # -----------------------------------------------------------------
    #  Zarbi à l'état sauvage
    # -----------------------------------------------------------------
    alias temp_initialize initialize
    def initialize(id_data = 1, level = 1, shiny = false)
      temp_initialize(id_data, level, shiny)
      if name == "ZARBI"
        @form = rand(28) + 1
      end
    end  
    
    # -----------------------------------------------------------------
    #  Objet tenu, changement de forme
    # -----------------------------------------------------------------
    def item_hold=(item_id)
      @item_hold = item_id
      if name == "DEOXYS"
        @form = 5 if item_id == 1
        @form = 0 if item_id != 1
      end
    end
    
    # -----------------------------------------------------------------
    #  Stat spéciale Deoxys
    # -----------------------------------------------------------------
    # 1 : Attaque
    # 2 : Defense
    # 3 : Normal
    # 4 : Vitesse
    alias temp_base_atk base_atk
    def base_atk
      if name == "DEOXYS"
        print 180 if form == 1
        return 180 if form == 1
        return 70 if form == 2
        return 95 if form == 4
      end
      temp_base_atk
    end
    
    alias temp_base_dfe base_dfe
    def base_dfe
      if name == "DEOXYS"
        return 20 if form == 1
        return 160 if form == 2
        return 90 if form == 4
      end
      temp_base_dfe
    end
    
    alias temp_base_spd base_spd
    def base_spd
      if name == "DEOXYS"
        return 150 if form == 1
        return 90 if form == 2
        return 180 if form == 4
      end
      temp_base_spd
    end
    
    alias temp_base_ats base_ats
    def base_ats
      if name == "DEOXYS"
        return 180 if form == 1
        return 70 if form == 2
        return 95 if form == 4
      end
      temp_base_ats
    end
    
    alias temp_base_dfs base_dfs
    def base_dfs
      if name == "DEOXYS"
        return 20 if form == 1
        return 160 if form == 2
        return 90 if form == 4
      end
      temp_base_dfs
    end
    
    # -----------------------------------------------------------------
    #  Effect
    # -----------------------------------------------------------------
    #  La suite n'est à modifier que si vous savez ce que vous faites
    # -----------------------------------------------------------------
    alias temp_skill_effect_reset skill_effect_reset
    def skill_effect_reset
      # Transform / Morphing cleaning
      if effect_list.include?(0x39)
        index = effect_list.index(0x39)
        transform_effect(@effect[index][2], true)
        @effect.delete_at(index)
      end
      # Copie / Mimic cleaning
      if effect_list.include?(0x52)
        index = effect_list.index(0x52)
        skill_index = @skills_set.index(@effect[index][2][1])
        @skills_set[skill_index] = @effect[index][2][0]
        @effect.delete(effect)
      end
      temp_skill_effect_reset
    end
    
    # --------- ----------
    # Morphing / Transform
    # --------- ----------
    def transform_effect(target, recover = false)
      @type1 = target.type1
      @type2 = target.type2
      @atk = target.atk
      @dfe = target.dfe
      @ats = target.ats
      @dfs = target.dfs
      @spd = target.spd
      @base_atk = target.base_atk
      @base_ats = target.base_ats
      @base_dfe = target.base_dfe
      @base_dfs = target.base_dfs
      @base_spd = target.base_spd
      @dv_atk = target.dv_atk
      @dv_ats = target.dv_ats
      @dv_dfe = target.dv_dfe
      @dv_dfs = target.dv_dfs
      @dv_spd = target.dv_spd
      @battle_stage = target.battle_stage
      @atk_plus = target.atk_plus
      @dfe_plus = target.dfe_plus
      @ats_plus = target.ats_plus
      @dfs_plus = target.dfs_plus
      @spd_plus = target.spd_plus
      @ability = target.ability
      @battler_id = target.id
      if recover
        @skills_set = target.skills_set
      end
      statistic_refresh
    end
    
    # -----------------------------------------------------------------
    #  Capacités spéciales
    # -----------------------------------------------------------------
    #  La suite n'est à modifier que si vous savez ce que vous faites
    # -----------------------------------------------------------------
    
    alias temp_change_stat change_stat
    def change_stat(stat_id, amount = 0)
      # Corps sain / Clear Body (ab) // Mist / Brume
      if amount < 0 and (effect_list.include?(0x2E) or @ability == 29) 
        return 0
      end
      # Regard Vif / Keen Eye (ab)
      if amount < 0 and stat_id == 6 and @ability == 51
        return 0
      end
      # Hyper Cutter (ab)
      if amount < 0 and stat_id == 0 and @ability == 52
        return 0
      end
      temp_change_stat(stat_id, amount)
    end
    
    alias temp_spd_modifier spd_modifier
    def spd_modifier
      n = 1
      # Swift Swim / Glissade (ab)
      if @ability == 33 and $battle_var.rain?
        n *= 2
      end
      # Chlorophyle (ab)
      if @ability == 34 and $battle_var.sunny?
        n *= 2
      end
      return n*temp_spd_modifier
    end
    
    alias temp_sleep_check sleep_check
    def sleep_check
      if @ability == 48 # Matinal / Early Bird (ab)
        @status_count -= 1
      end
      temp_sleep_check
    end
    
    # -----------------------------------------------------------------
    #  Pension et Oeufs
    # -----------------------------------------------------------------
    #  La suite n'est à modifier que si vous savez ce que vous faites
    # -----------------------------------------------------------------
    
    def breeding_custom_rules(baby, father, mother)
      baby_id = baby
      # Base mère = Metamorph
      if Pokemon_Info.name(baby_id) == "METAMORPH"
        baby_id = base_id(father.id)
      end
      # Base mère = Nidoran femelle
      if Pokemon_Info.name(baby_id) == "NIDORAN♀"
        baby_id = Pokemon_Info.id(["NIDORAN♀", "NIDORAN♂"][rand(2)])
      end
      # Base mère = Lumivole
      if Pokemon_Info.name(baby_id) == "LUMIVOLE"
        baby_id = Pokemon_Info.id(["LUMIVOLE", "MUCIOLE"][rand(2)])
      end
      # Base mère = Manaphy
      if Pokemon_Info.name(baby_id) == "MANAPHY"
        baby_id = Pokemon_Info.id("PHIONE")
      end
      # Base mère = Oke oke avec Encens doux
      if Pokemon_Info.name(baby_id) == "OKEOKE"
        if Item.name(mother.item_hold) == "ENCENS DOUX" and 
            Pokemon_Info.id(base_id(mother.id)) == "OKEOKE"
        
        elsif Item.name(father.item_hold) == "ENCENS DOUX" and
            Pokemon_Info.id(base_id(mother.id)) == "OKEOKE"
            
        else
          baby_id = Pokemon_Info.id("QULBUTOKE")
        end
      end
      # Base mère = Azurill avec Encens mer
      if Pokemon_Info.name(baby_id) == "AZURILL"
        if Item.name(mother.item_hold) == "ENCENS MER" and 
            Pokemon_Info.id(base_id(mother.id)) == "AZURILL"
          
        elsif Item.name(father.item_hold) == "ENCENS MER" and 
            Pokemon_Info.id(base_id(father.id)) == "AZURILL"
          
        else
          baby_id = Pokemon_Info.id("MARILL")
        end
      end
      return baby_id
    end
  end
end


# Les modules suivants sont complémentaires et ne sont pas activés par défaut.
if false

module POKEMON_S
  # Météo automatique au combat
  class Pokemon_Battle_Variable
    def reset
      reset_weather
      @actor_last_used = nil
      @enemy_last_used = nil
      @battle_order = (0..5).to_a
      @enemy_battle_order = (0..5).to_a
      @in_battle = false
      @actor_last_taken_damage = 0
      @enemy_last_taken_damage = 0
      @have_fought = []
      @enemy_party = Pokemon_Party.new
      @action_id = 0
      @window_index = 0
      @last_index = 0
      @round = 0
      @run_count = 0
      @money = 0
    end
    
    def reset_weather
      @weather = [0, 0]
      case $game_screen.weather_type
      when 1
        set_rain
      when 2
        set_rain
      when 3
        set_hail
      end
    end
  end
end


module POKEMON_S
  # Capacités évolutives (idée de Mister-K)
  class Skill
    PAS_UTIL = 20
    PAS_PUIS = 10
   
    alias initialize_temp initialize
    def initialize(id)
      initialize_temp(id)
      @use_number = 0
    end
   
    def power
      @use_number = 0 if @use_number == nil # Protection indéfinition
      return Skill_Info.base_damage(id) + PAS_PUIS * @use_number / PAS_UTIL
    end
   
    alias use_temp use
    def use
      use_temp
      @use_number = 0 if @use_number == nil # Protection indéfinition
      @use_number += 1 if @usable
    end
  end
end

end
    