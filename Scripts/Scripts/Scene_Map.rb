#==============================================================================
# ■ Scene_Map
# Pokemon Script Project - Krosk 
# 18/07/07
#-----------------------------------------------------------------------------
# Scène à ne pas modifier de préférence
#-----------------------------------------------------------------------------
# Variables appelées:
# $random_encounter
#   Défini les monstres rencontrés sur la map et la fréquence d'apparition
#   A définir de manière automatique par le logiciel, ou de manière manuelle
#   en processus parallèle sur chaque map comme ci: 
#     set_encounter(rate, list1, list2, list3, list4)
#     'rate' correspond à la fréquence d'apparition
#     'listx' correspond à la liste de Pokémons, défini comme:
#         x = tag terrain
#         listx = [écart, [id, level, rareté locale] , [id, level, rl] , ... ]
#
#     $random_encounter[0] = rate
#     $random_encounter[1] = list1
#     $random_encounter[2] = list2
#     $random_encounter[3] = list3
#     $random_encounter[4] = list4
#-----------------------------------------------------------------------------

class Scene_Map
  attr_reader :spriteset

  def call_menu
    # Redéfinition
    $game_temp.menu_calling = false
    if $game_temp.menu_beep
      $game_system.se_play($data_system.decision_se)
      $game_temp.menu_beep = false
    end
    $game_player.straighten
    $scene = POKEMON_S::Pokemon_Menu.new
  end
  
  def call_save
    $game_player.straighten
    $scene = POKEMON_S::Pokemon_Save.new
  end
  
  def update
    loop do
      $game_map.update
      $game_system.map_interpreter.update
      $game_player.update
      $game_system.update
      $game_screen.update
      unless $game_temp.player_transferring
        break
      end
      transfer_player
      if $game_temp.transition_processing
        break
      end
    end
    
    @spriteset.update
    @message_window.update
    
    if $game_temp.gameover
      $scene = Scene_Gameover.new
      return
    end
    
    if $game_temp.to_title
      $scene = Scene_Title.new
      return
    end
    
    if $game_temp.transition_processing
      $game_temp.transition_processing = false
      if $game_temp.transition_name == ""
        Graphics.transition(20)
      else
        Graphics.transition(40, "Graphics/Transitions/" +
          $game_temp.transition_name)
      end
    end
    
    if $game_temp.message_window_showing
      return
    end
    
    # Comptage Rencontre aléatoire
    if $random_encounter != [0] and $game_player.encounter_count == 0
      tag = $game_player.terrain_tag
      unless $game_system.map_interpreter.running? or
              $game_system.encounter_disabled or 
              tag == 0
        
        enemy_list = $random_encounter[tag]
        if enemy_list != nil and enemy_list.length > 1
          $game_temp.battle_calling = true
        end
      end
    end
    
    if Input.trigger?(Input::B)
      unless $game_system.map_interpreter.running? or
             $game_system.menu_disabled
        $game_temp.menu_calling = true
        $game_temp.menu_beep = true
      end
    end
    
    if $DEBUG and Input.press?(Input::F9)
      $game_temp.debug_calling = true
    end
    
    # Map monde
    if $game_map.map_id == POKEMON_S::_WMAPID
      $game_temp.menu_calling = false
      $game_temp.menu_beep = false
      $game_temp.debug_calling = false
      if Input.trigger?(Input::B)
        $game_temp.back_calling = true
      end
      if Input.dir4 == 2 or Input.dir4 == 4 or Input.dir4 == 6 or
          Input.dir4 == 8
        $game_temp.world_map_event_checking = true
      end
      unless $game_player.moving?
        $game_player.check_world_map
      end
    end
    
    unless $game_player.moving?
      if $game_temp.battle_calling
        call_battle
      elsif $game_temp.shop_calling
        call_shop
      elsif $game_temp.name_calling
        call_name
      elsif $game_temp.menu_calling
        call_menu
      elsif $game_temp.save_calling
        call_save
      elsif $game_temp.debug_calling
        call_debug
      elsif $game_temp.back_calling
        call_back_world_map
      end
    end
  end

  #---------------------------------------------------
  # Reset de la variable $random_encounter
  # au changement de map
  #---------------------------------------------------
  def transfer_player
    # Raz Random Encounter
    $random_encounter = [0]
    # Indicateur Force
    $on_strength = false
    
    $game_temp.player_transferring = false

    if $game_map.map_id != $game_temp.player_new_map_id
      $game_map.setup($game_temp.player_new_map_id)
    end
    
    # Random Encounter
    if $game_map.encounter_list != []
      for i in 1..7
        if $random_encounter[i] == nil
          $random_encounter[i] = []
        end
      end
      update_encounter
    end
    
    $game_player.moveto($game_temp.player_new_x, $game_temp.player_new_y)

    case $game_temp.player_new_direction
    when 2  # 下
      $game_player.turn_down
    when 4  # 左
      $game_player.turn_left
    when 6  # 右
      $game_player.turn_right
    when 8  # 上
      $game_player.turn_up
    end

    $game_player.straighten
    $game_map.update
    @spriteset.dispose
    @spriteset = Spriteset_Map.new
    if $game_temp.transition_processing
      $game_temp.transition_processing = false
      Graphics.transition(20)
    end
    
    $game_map.autoplay
    Graphics.frame_reset
    Input.update
  end
  
  def update_encounter
    # Fréquence
    $random_encounter[0] = (2880 / (16.0 * $game_map.encounter_step)).to_i
    # Liste
    for list_id in $game_map.encounter_list
      tag = $data_encounter[list_id][0]
      switch_id = $data_encounter[list_id][3]
      # Vérification de switch
      if (switch_id == 0 or $game_switches[switch_id]) and tag > 0  
        if $random_encounter[tag]  
          $random_encounter[tag] += $data_encounter[list_id][2]  
        else  
          $random_encounter[tag] = $data_encounter[list_id][2]  
        end  
      end  
    end
  end
  
  def call_battle
    tag = $game_player.terrain_tag
    
    $game_temp.battle_calling = false
    $game_temp.menu_calling = false
    $game_temp.menu_beep = false
    
    $game_player.make_encounter_count
    
    x = rand(100)
    undist_rate = 100
    call = false
    pokemon = nil
    number = 0
    
    for i in 1..$random_encounter[tag].length-1
      if $random_encounter[tag][i][2] == nil
        number += 1
      else
        undist_rate -= $random_encounter[tag][i][2]
      end
    end
    
    until pokemon != nil
      for i in 1..$random_encounter[tag].length-1
        if call
          next
        end
        if $random_encounter[tag][i][2] != nil
          x -= $random_encounter[tag][i][2]
        elsif number != 0
          x -= undist_rate/number
        end
        if x < 0
          level = $random_encounter[tag][i][1]
          id = $random_encounter[tag][i][0]
          level -= rand($random_encounter[tag][0])
          if level < 1
            level = 1
          end
          pokemon = POKEMON_S::Pokemon.new(id, level)
          call = true
        end
      end
    end
    
    if $pokemon_party.repel_count > 0 and level < $pokemon_party.max_level
      $game_player.make_encounter_count
      return
    end
    
    $game_temp.map_bgm = $game_system.playing_bgm
    $game_system.bgm_stop
    $game_system.se_play($data_system.battle_start_se)
    $game_player.straighten
    
if $game_variables[39] == 0
      $scene = POKEMON_S::Pokemon_Battle_Wild.new($pokemon_party, pokemon)
    else
      $scene = POKEMON_S::Pokemon_Battle_Safari.new($pokemon_party, pokemon)
    end
  end
end