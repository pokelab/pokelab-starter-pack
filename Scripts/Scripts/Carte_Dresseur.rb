#==============================================================================
# ■ Carde Dresseur
#    Script Communauté PSP - Louro
#    Remis en forme par Krosk
#-----------------------------------------------------------------------------
# Support de carte dresseur
#-----------------------------------------------------------------------------
# Pour changer l'image de vos badges, réglez les noms juste en dessous
# Pour afficher un badge, Insérez un script: badgex où x est le num du badge
# Pour afficher un objectif, Insérez un script: objectifx où x est le num de 
#   l'objectif
#-----------------------------------------------------------------------------
# Variables occupées : 200, 1000, 1500
# Interrupteurs occupés : 1001-1008, 1500-1505
#-----------------------------------------------------------------------------

module POKEMON_S_TCARD
  # Script crée par Louro pour les membres de la communautée de PSP 
  # qui veulent s'en servir
  
  # Les noms d'icones pour chaque badge 
  BADGE1 = "baie01.png" 
  BADGE2 = "baie02.png"
  BADGE3 = "baie03.png"   
  BADGE4 = "baie04.png"   
  BADGE5 = "baie05.png"   
  BADGE6 = "baie06.png"   
  BADGE7 = "baie07.png"   
  BADGE8 = "baie08.png"
  
  # Réglez les coordonnées horizontales de vos badges ici, 
  # si vous avez un décalage !
  BADGE1_X = 59
  BADGE2_X = 128
  BADGE3_X = 197  
  BADGE4_X = 266   
  BADGE5_X = 335   
  BADGE6_X = 404   
  BADGE7_X = 473   
  BADGE8_X = 542   
  
  # Numéro de la variable stockant le nombre de badge acquit   
  VAR = 200
  
  # Nom des objectifs
  OBJECTIF1 = "PREMIER OBJECTIF"
  OBJECTIF2 = "DEUXIEME OBJECTIF"
  OBJECTIF3 = "TROISIEME OBJECTIF"
  OBJECTIF4 = "QUATRIEME OBJECTIF"
  OBJECTIF5 = "CINQUIEME OBJECTIF"
end

class TrainerCardWindow < Window_Base   
  attr_accessor :devant
  include POKEMON_S_TCARD
  include POKEMON_S
  
  def initialize(devant)
    super(0, 0, 640, 480)   
    self.contents = Bitmap.new(width - 32, height - 32)   
    self.contents.font.name = $fontface   
    self.contents.font.size = $fontsizebig   
    self.contents.font.color = Color.new(60,60,60)   
    @captured = 0
    @devant = devant
    for i in 1..$data_pokedex.length-1   
      if $data_pokedex[i][1]   
        @captured += 1   
      end   
    end   
    refresh   
  end   
  
  def refresh   
    self.contents.clear 
    if @devant == true
      @total_sec = Graphics.frame_count / Graphics.frame_rate   
      hour = @total_sec / 60 / 60   
      min = @total_sec / 60 % 60   
      temps = sprintf("%02d:%02d", hour, min)   
      wd1 = self.contents.text_size($pokemon_party.money.to_s+"$").width   
      wd2 = self.contents.text_size(@captured.to_s).width   
      wd3 = self.contents.text_size(Player.id.to_s).width   
      self.contents.font.color = Color.new(110,110,110, 85)  
      self.contents.draw_text(48+2, 99+2+12, 160, 64, "NOM ")       
      self.contents.draw_text(48+3, 99+2+12, 160, 64, "NOM ") 
      self.contents.draw_text(48+3, 99-1+12, 160, 64, "NOM ")   
      self.contents.draw_text(48+2, 99-1+12, 160, 64, "NOM ")     
      self.contents.font.color = Color.new(60,60,60)  
      self.contents.draw_text(48, 99+12, 160, 64, "NOM ")   
      self.contents.draw_text(48+1, 99+12, 160, 64, "NOM ") 
      self.contents.draw_text(48, 99-1+12, 160, 64, "NOM ")   
      self.contents.font.color = Color.new(110,110,110, 85)  
      self.contents.draw_text(130+2, 99+2+12, 160, 64, Player.name)   
      self.contents.draw_text(130+2, 99-1+12, 160, 64, Player.name)   
      self.contents.font.color = Color.new(110,110,110)  
      self.contents.draw_text(130, 99+12, 160, 64, Player.name) 
      self.contents.draw_text(130, 99-1+12, 160, 64, Player.name)   
      self.contents.font.color = Color.new(60,60,60)  
      self.contents.font.color = Color.new(110,110,110, 85)  
      self.contents.draw_text(48+2, 171+2+12, 160, 64, "ARGENT ")   
      self.contents.draw_text(48+3, 171+2+12, 160, 64, "ARGENT ")   
      self.contents.draw_text(48+3, 171-1+12, 160, 64, "ARGENT ")   
      self.contents.draw_text(48+2, 171-1+12, 160, 64, "ARGENT ")   
      self.contents.font.color = Color.new(60,60,60)  
      self.contents.draw_text(48, 171+12, 160, 64, "ARGENT ")   
      self.contents.draw_text(48+1, 171+12, 160, 64, "ARGENT ")   
      self.contents.draw_text(48, 171-1+12, 160, 64, "ARGENT ")   
      self.contents.font.color = Color.new(110,110,110, 85)  
      self.contents.draw_text(340 - wd1+2, 171+2+12, 160, 64, $pokemon_party.money.to_s+"$")   
      self.contents.draw_text(340 - wd1+2, 171-1+12, 160, 64, $pokemon_party.money.to_s+"$")   
      self.contents.font.color = Color.new(110,110,110)  
      self.contents.draw_text(340 - wd1, 171+12, 160, 64, $pokemon_party.money.to_s+"$")   
      self.contents.draw_text(340 - wd1, 171-1+12, 160, 64, $pokemon_party.money.to_s+"$")   
      if $data_pokedex[0] == true   
        self.contents.font.color = Color.new(110,110,110, 85)  
        self.contents.draw_text(48+2, 220+2+12, 160, 64, "POKéDEX ")   
        self.contents.draw_text(48+3, 220+2+12, 160, 64, "POKéDEX ")   
        self.contents.draw_text(48+3, 220-1+12, 160, 64, "POKéDEX ")   
        self.contents.draw_text(48+2, 220-1+12, 160, 64, "POKéDEX ")   
        self.contents.font.color = Color.new(60,60,60)  
        self.contents.draw_text(48, 220+12, 160, 64, "POKéDEX ")   
        self.contents.draw_text(48+1, 220+12, 160, 64, "POKéDEX ")   
        self.contents.draw_text(48, 220-1+12, 160, 64, "POKéDEX ")   
        self.contents.font.color = Color.new(110,110,110, 85)  
        self.contents.draw_text(340 - wd2+2, 220+2+12, 160, 64, @captured.to_s)   
        self.contents.draw_text(340 - wd2+2, 220-1+12, 160, 64, @captured.to_s)   
        self.contents.font.color = Color.new(110,110,110)      
        self.contents.draw_text(340 - wd2, 220+12, 160, 64, @captured.to_s)   
        self.contents.draw_text(340 - wd2, 220-1+12, 160, 64, @captured.to_s)   
      end
      self.contents.font.color = Color.new(110,110,110, 85)  
      self.contents.draw_text(48+2, 268+2+12, 160, 64, "DUREE JEU ")   
      self.contents.draw_text(48+3, 268+2+12, 160, 64, "DUREE JEU ")   
      self.contents.draw_text(48+3, 268-1+12, 160, 64, "DUREE JEU ")   
      self.contents.draw_text(48+2, 268-1+12, 160, 64, "DUREE JEU ")   
      self.contents.font.color = Color.new(60,60,60)  
      self.contents.draw_text(48, 268+12, 160, 64, "DUREE JEU ")   
      self.contents.draw_text(48+1, 268+12, 160, 64, "DUREE JEU ")   
      self.contents.draw_text(48, 268-1+12, 160, 64, "DUREE JEU ")   
      self.contents.font.color = Color.new(110,110,110, 85)  
      self.contents.draw_text(257+2, 268+2+12, 160, 64, temps)   
      self.contents.draw_text(257+2, 268-1+12, 160, 64, temps)   
      self.contents.font.color = Color.new(110,110,110)      
      self.contents.draw_text(257, 268+12, 160, 64, temps)   
      self.contents.draw_text(257, 268-1+12, 160, 64, temps)   
      self.contents.font.color = Color.new(230,230,230, 225)  
      self.contents.draw_text(370, 25+1+12, 160, 64, "N°ID")   
      self.contents.draw_text(370, 25+2+12, 160, 64, "N°ID ")   
      self.contents.draw_text(370+1, 25+1+12, 160, 64, "N°ID")   
      self.contents.draw_text(370+1, 25+2+12, 160, 64, "N°ID ")   
      self.contents.draw_text(370+2, 25+12, 160, 64, "N°ID ")   
      self.contents.draw_text(370+3, 25+12, 160, 64, "N°ID ")   
      self.contents.font.color = Color.new(60,60,60)  
      self.contents.draw_text(370, 25+12, 160, 64, "N°ID ")   
      self.contents.draw_text(370+1, 25+12, 160, 64, "N°ID ")   
      self.contents.draw_text(370, 25-1+12, 160, 64, "N°ID")   
      self.contents.font.color = Color.new(230,230,230, 225)  
      self.contents.draw_text(370 + wd3+2, 25+2+12, 160, 64, Player.id.to_s)   
      self.contents.draw_text(370 + wd3+2, 25-1+12, 160, 64, Player.id.to_s)   
      self.contents.font.color = Color.new(110,110,110)  
      self.contents.draw_text(370 + wd3, 25+12, 160, 64, Player.id.to_s)   
      self.contents.draw_text(370 + wd3, 25-1+12, 160, 64, Player.id.to_s)   
    elsif @devant == false
      wd1 = self.contents.text_size($pokemon_party.money.to_s+"$").width   
      wd2 = self.contents.text_size(@captured.to_s).width   
      wd3 = self.contents.text_size(Player.id.to_s).width   
      self.contents.font.color = Color.new(110,110,110, 85)  
      self.contents.draw_text(30+2, 25+2+12, 550, 64, "CARTE DRESSEUR de "+Player.name, 2)       
      self.contents.draw_text(30+3, 25+2+12, 550, 64, "CARTE DRESSEUR de "+Player.name, 2) 
      self.contents.draw_text(30+3, 25-1+12, 550, 64, "CARTE DRESSEUR de "+Player.name, 2)   
      self.contents.draw_text(30+2, 25-1+12, 550, 64, "CARTE DRESSEUR de "+Player.name, 2)     
      self.contents.font.color = Color.new(60,60,60)  
      self.contents.draw_text(30, 25+12, 550, 64, "CARTE DRESSEUR de "+Player.name, 2)
      self.contents.draw_text(30+1, 25+12, 550, 64, "CARTE DRESSEUR de "+Player.name, 2)
      self.contents.draw_text(30, 25-1+12, 550, 64, "CARTE DRESSEUR de "+Player.name, 2)
      if $game_switches[1500] == true
        wd4 = self.contents.text_size($game_variables[1500].to_s).width   
        self.contents.font.color = Color.new(110,110,110, 85)  
        self.contents.draw_text(48+2, 99+15+2, 160, 64, "PANTHEON")       
        self.contents.draw_text(48+3, 99+15+2, 160, 64, "PANTHEON") 
        self.contents.draw_text(48+3, 99+15-1, 160, 64, "PANTHEON")   
        self.contents.draw_text(48+2, 99+15-1, 160, 64, "PANTHEON")     
        self.contents.font.color = Color.new(60,60,60)  
        self.contents.draw_text(48, 99+15, 160, 64, "PANTHEON ")   
        self.contents.draw_text(48+1, 99+15, 160, 64, "PANTHEON") 
        self.contents.draw_text(48, 99+15-1, 160, 64, "PANTHEON")
        self.contents.font.size += 2
        self.contents.font.color = Color.new(242,118,214)  
        self.contents.draw_text(570-wd4+3, 99+15, 160, 64, $game_variables[1500])  
        self.contents.draw_text(570-wd4, 99+15+3, 160, 64, $game_variables[1500])  
        self.contents.draw_text(570-wd4+1, 99+15+3, 160, 64, $game_variables[1500])  
        self.contents.draw_text(570-wd4+2, 99+15+3, 160, 64, $game_variables[1500])  
        self.contents.draw_text(570-wd4+3, 99+15+3, 160, 64, $game_variables[1500])  
        self.contents.font.color = Color.new(190,0,0)  
        self.contents.draw_text(570-wd4, 99+15, 160, 64, $game_variables[1500])  
        self.contents.font.size -= 2
      end
      if $game_switches[1501] == true
        wd4 = self.contents.text_size($game_variables[1501].to_s).width   
        self.contents.font.color = Color.new(110,110,110, 85)  
        self.contents.draw_text(48+2, 147+15+2, 380, 64, OBJECTIF1)       
        self.contents.draw_text(48+3, 147+15+2, 380, 64, OBJECTIF1) 
        self.contents.draw_text(48+3,147+15-1, 380, 64, OBJECTIF1)   
        self.contents.draw_text(48+2, 147+15-1, 380, 64, OBJECTIF1)     
        self.contents.font.color = Color.new(60,60,60)  
        self.contents.draw_text(48, 147+15, 380, 64, OBJECTIF1)   
        self.contents.draw_text(48+1, 147+15, 380, 64, OBJECTIF1) 
        self.contents.draw_text(48, 147+15-1, 380, 64, OBJECTIF1)
        self.contents.font.size += 2
        self.contents.font.color = Color.new(242,118,214)  
        self.contents.draw_text(570-wd4+3, 147+15, 160, 64, $game_variables[1501].to_s)  
        self.contents.draw_text(570-wd4, 147+15+3, 160, 64, $game_variables[1501].to_s)  
        self.contents.draw_text(570-wd4+1, 147+15+3, 160, 64, $game_variables[1501].to_s)  
        self.contents.draw_text(570-wd4+2, 147+15+3, 160, 64, $game_variables[1501].to_s)  
        self.contents.draw_text(570-wd4+3, 147+15+3, 160, 64, $game_variables[1501].to_s)  
        self.contents.font.color = Color.new(190,0,0)  
        self.contents.draw_text(570-wd4, 147+15, 160, 64, $game_variables[1501].to_s)   
        self.contents.font.size -= 2
      end
      if $game_switches[1502] == true
        wd4 = self.contents.text_size($game_variables[1502].to_s).width   
        self.contents.font.color = Color.new(110,110,110, 85)  
        self.contents.draw_text(48+2, 195+15+2, 380, 64, OBJECTIF2)       
        self.contents.draw_text(48+3, 195+15+2, 380, 64, OBJECTIF2) 
        self.contents.draw_text(48+3,195+15-1, 380, 64, OBJECTIF2)   
        self.contents.draw_text(48+2, 195+15-1, 380, 64, OBJECTIF2)     
        self.contents.font.color = Color.new(60,60,60)  
        self.contents.draw_text(48, 195+15, 380, 64, OBJECTIF2)   
        self.contents.draw_text(48+1, 195+15, 380, 64, OBJECTIF2) 
        self.contents.draw_text(48, 195+15-1, 380, 64, OBJECTIF2)
        self.contents.font.size += 2
        self.contents.font.color = Color.new(242,118,214)  
        self.contents.draw_text(570-wd4+3, 195+15, 160, 64, $game_variables[1502].to_s)  
        self.contents.draw_text(570-wd4, 195+15+3, 160, 64, $game_variables[1502].to_s)  
        self.contents.draw_text(570-wd4+1, 195+15+3, 160, 64, $game_variables[1502].to_s)  
        self.contents.draw_text(570-wd4+2, 195+15+3, 160, 64, $game_variables[1502].to_s)  
        self.contents.draw_text(570-wd4+3, 195+15+3, 160, 64, $game_variables[1502].to_s)  
        self.contents.font.color = Color.new(190,0,0)  
        self.contents.draw_text(570-wd4, 195+15, 160, 64, $game_variables[1502].to_s)   
        self.contents.font.size -= 2
      end
      if $game_switches[1503] == true
        wd4 = self.contents.text_size($game_variables[1503].to_s).width   
        self.contents.font.color = Color.new(110,110,110, 85)  
        self.contents.draw_text(48+2, 243+15+2, 380, 64, OBJECTIF3)       
        self.contents.draw_text(48+3, 243+15+2, 380, 64, OBJECTIF3) 
        self.contents.draw_text(48+3,243+15-1, 380, 64, OBJECTIF3)   
        self.contents.draw_text(48+2, 243+15-1, 380, 64, OBJECTIF3)     
        self.contents.font.color = Color.new(60,60,60)  
        self.contents.draw_text(48, 243+15, 380, 64, OBJECTIF3)   
        self.contents.draw_text(48+1, 243+15, 380, 64, OBJECTIF3)
        self.contents.draw_text(48, 243+15-1, 380, 64, OBJECTIF3)
        self.contents.font.size += 2
        self.contents.font.color = Color.new(242,118,214)  
        self.contents.draw_text(570-wd4+3, 243+15, 160, 64, $game_variables[1503].to_s)  
        self.contents.draw_text(570-wd4, 243+15+3, 160, 64, $game_variables[1503].to_s)  
        self.contents.draw_text(570-wd4+1, 243+15+3, 160, 64, $game_variables[1503].to_s)  
        self.contents.draw_text(570-wd4+2, 243+15+3, 160, 64, $game_variables[1503].to_s)  
        self.contents.draw_text(570-wd4+3, 243+15+3, 160, 64, $game_variables[1503].to_s)  
        self.contents.font.color = Color.new(190,0,0)  
        self.contents.draw_text(570-wd4, 243+15, 160, 64, $game_variables[1503].to_s)   
        self.contents.font.size -= 2
      end
      if $game_switches[1504] == true
        wd4 = self.contents.text_size($game_variables[1504].to_s).width   
        self.contents.font.color = Color.new(110,110,110, 85)  
        self.contents.draw_text(48+2, 291+15+2, 380, 64, OBJECTIF4)       
        self.contents.draw_text(48+3, 291+15+2, 380, 64, OBJECTIF4) 
        self.contents.draw_text(48+3,291+15-1, 380, 64, OBJECTIF4)   
        self.contents.draw_text(48+2, 291+15-1, 380, 64, OBJECTIF4)     
        self.contents.font.color = Color.new(60,60,60)  
        self.contents.draw_text(48, 291+15, 380, 64, OBJECTIF4)   
        self.contents.draw_text(48+1, 291+15, 380, 64, OBJECTIF4)
        self.contents.draw_text(48, 291+15-1, 380, 64, OBJECTIF4)
        self.contents.font.size += 2
        self.contents.font.color = Color.new(242,118,214)  
        self.contents.draw_text(570-wd4+3, 291+15, 160, 64, $game_variables[1504].to_s)  
        self.contents.draw_text(570-wd4, 291+15+3, 160, 64, $game_variables[1504].to_s)  
        self.contents.draw_text(570-wd4+1, 291+15+3, 160, 64, $game_variables[1504].to_s)  
        self.contents.draw_text(570-wd4+2, 291+15+3, 160, 64, $game_variables[1504].to_s)  
        self.contents.draw_text(570-wd4+3, 291+15+3, 160, 64, $game_variables[1504].to_s)  
        self.contents.font.color = Color.new(190,0,0)  
        self.contents.draw_text(570-wd4, 291+15, 160, 64, $game_variables[1504].to_s)   
        self.contents.font.size -= 2
      end
      if $game_switches[1505] == true
        wd4 = self.contents.text_size($game_variables[1505].to_s).width   
        self.contents.font.color = Color.new(110,110,110, 85)  
        self.contents.draw_text(48+2, 339+15+2, 380, 64, OBJECTIF5)       
        self.contents.draw_text(48+3, 339+15+2, 380, 64, OBJECTIF5) 
        self.contents.draw_text(48+3,339+15-1, 380, 64, OBJECTIF5)   
        self.contents.draw_text(48+2, 339+15-1, 380, 64, OBJECTIF5)     
        self.contents.font.color = Color.new(60,60,60)  
        self.contents.draw_text(48, 339+15, 380, 64, OBJECTIF5)   
        self.contents.draw_text(48+1, 339+15, 380, 64, OBJECTIF5)
        self.contents.draw_text(48, 339+15-1, 380, 64, OBJECTIF5)
        self.contents.font.size += 2
        self.contents.font.color = Color.new(242,118,214)  
        self.contents.draw_text(570-wd4+3, 339+15, 160, 64, $game_variables[1505].to_s)  
        self.contents.draw_text(570-wd4, 339+15+3, 160, 64, $game_variables[1505].to_s)  
        self.contents.draw_text(570-wd4+1, 339+15+3, 160, 64, $game_variables[1505].to_s)  
        self.contents.draw_text(570-wd4+2, 339+15+3, 160, 64, $game_variables[1505].to_s)  
        self.contents.draw_text(570-wd4+3, 339+15+3, 160, 64, $game_variables[1505].to_s)  
        self.contents.font.color = Color.new(190,0,0)  
        self.contents.draw_text(570-wd4, 339+15, 160, 64, $game_variables[1505].to_s)   
        self.contents.font.size -= 2
      end
    end
  end   
end   

class TCARD
  include POKEMON_S_TCARD
  
  def main   
    @tcard = TrainerCardWindow.new(true)
    @tcard.opacity = 0   
    @background = Plane.new(Viewport.new(0,0,640,480))   
    @background.bitmap = RPG::Cache.picture("tcard.png")   
    @badge1 = Sprite.new   
    @badge2 = Sprite.new   
    @badge3 = Sprite.new   
    @badge4 = Sprite.new   
    @badge5 = Sprite.new   
    @badge6 = Sprite.new   
    @badge7 = Sprite.new   
    @badge8 = Sprite.new   
    $game_variables[VAR] = 0   
    unless $game_switches[1001] == true then @badge1.visible = false else @badge1.visible = true and $game_variables[VAR] += 1 end   
    unless $game_switches[1002] == true then @badge2.visible = false else @badge2.visible = true and $game_variables[VAR] += 1 end   
    unless $game_switches[1003] == true then @badge3.visible = false else @badge3.visible = true and $game_variables[VAR] += 1 end   
    unless $game_switches[1004] == true then @badge4.visible = false else @badge4.visible = true and $game_variables[VAR] += 1 end   
    unless $game_switches[1005] == true then @badge5.visible = false else @badge5.visible = true and $game_variables[VAR] += 1 end   
    unless $game_switches[1006] == true then @badge6.visible = false else @badge6.visible = true and $game_variables[VAR] += 1 end   
    unless $game_switches[1007] == true then @badge7.visible = false else @badge7.visible = true and $game_variables[VAR] += 1 end   
    unless $game_switches[1008] == true then @badge8.visible = false else @badge8.visible = true and $game_variables[VAR] += 1 end   
    @badge1.bitmap = RPG::Cache.icon(BADGE1)   
    @badge2.bitmap = RPG::Cache.icon(BADGE2)   
    @badge3.bitmap = RPG::Cache.icon(BADGE3)   
    @badge4.bitmap = RPG::Cache.icon(BADGE4)   
    @badge5.bitmap = RPG::Cache.icon(BADGE5)   
    @badge6.bitmap = RPG::Cache.icon(BADGE6)   
    @badge7.bitmap = RPG::Cache.icon(BADGE7)   
    @badge8.bitmap = RPG::Cache.icon(BADGE8)   
    @badge1.x = BADGE1_X
    @badge1.y = 363 + 12
    @badge2.x = BADGE2_X
    @badge2.y = 363 + 12
    @badge3.x = BADGE3_X
    @badge3.y = 363 + 12
    @badge4.x = BADGE4_X
    @badge4.y = 363 + 12
    @badge5.x = BADGE5_X
    @badge5.y = 363 + 12
    @badge6.x = BADGE6_X
    @badge6.y = 363 + 12
    @badge7.x = BADGE7_X
    @badge7.y = 363 + 12
    @badge8.x = BADGE8_X
    @badge8.y = 363 + 12
    @perso = Sprite.new   
    if $game_variables[1000] == nil or $game_variables[1000] == 0
      @perso.bitmap = RPG::Cache.picture("persogars.png")
    end 
    if $game_variables[1000] == 1   
      @perso.bitmap = RPG::Cache.picture("persogars.png")   
    end 
    if $game_variables[1000] == 2   
      @perso.bitmap = RPG::Cache.picture("persofille.png")   
    end   
    @perso.x = 434
    @perso.y = 150 + 12   
    Graphics.transition   
    loop do   
      Graphics.update   
      Input.update   
      update   
      if $scene != self   
        break   
      end   
    end   
    Graphics.freeze   
    unless @tcard == nil then @tcard.dispose end
    unless @background == nil then @background.dispose end
    unless @badge1 == nil then @badge1.dispose end
    unless @badge2 == nil then @badge2.dispose end
    unless @badge3 == nil then @badge3.dispose end
    unless @badge4 == nil then @badge4.dispose end
    unless @badge5 == nil then @badge5.dispose end
    unless @badge6 == nil then @badge6.dispose end
    unless @badge7 == nil then @badge7.dispose end
    unless @badge8 == nil then @badge8.dispose end
    unless @perso == nil then @perso.dispose end
  end   
    
  def update   
    if Input.trigger?(Input::C)   
      if @tcard.devant == false
        $game_system.se_play($data_system.cancel_se)   
        $scene = Scene_Map.new   
        return
      end
      if @tcard.devant == true
        @tcard.devant = false
        $game_system.se_play($data_system.decision_se)   
        @badge1.dispose   
        @badge2.dispose   
        @badge3.dispose   
        @badge4.dispose   
        @badge5.dispose   
        @badge6.dispose   
        @badge7.dispose   
        @badge8.dispose   
        @perso.dispose   
        @tcard.dispose
        @tcard = TrainerCardWindow.new(false)
        @tcard.opacity = 0   
        @background.bitmap = RPG::Cache.picture("backcard.png")   
      end
    end
    
    if Input.trigger?(Input::B)   
      $game_system.se_play($data_system.cancel_se)   
      $scene = Scene_Map.new   
      return   
    end   
  end   
end  


class Interpreter
  def ligue_finie
    @total_sec = Graphics.frame_count / Graphics.frame_rate   
    hour = @total_sec / 60 / 60   
    min = @total_sec / 60 % 60   
    sec = @total_sec % 60
    temps = sprintf("%02d:%02d:%02d", hour, min, sec)
    $game_variables[1500] = temps
    $game_switches[1500] = true
  end

  def objectif1
    $game_switches[1501] = true
  end

  def objectif2
    $game_switches[1502] = true
  end

  def objectif3
    $game_switches[1503] = true
  end

  def objectif4
    $game_switches[1504] = true
  end

  def objectif5
    $game_switches[1505] = true
  end

  def fille
    $game_variables[1000] = 2
  end

  def gars
    $game_variables[1000] = 1
  end

  def badge1
    $game_switches[1001] = true
  end

  def badge2
    $game_switches[1002] = true
  end

  def badge3
    $game_switches[1003] = true
  end

  def badge4
    $game_switches[1004] = true
  end

  def badge5
    $game_switches[1005] = true
  end

  def badge6
    $game_switches[1006] = true
  end

  def badge7
    $game_switches[1007] = true
  end

  def badge8
    $game_switches[1008] = true
  end

  def carte_dresseur
    $scene = TCARD.new
  end

end

# Merci à Zellyunie qui a changé les couleurs de l'ancienne version pour 
# arranger la mise en page, ce qui m'a donné envi de l'améliorer entièrement. 