#========================================================
# Extension de la class Pokemon - Ŧž.A 
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code sans crédits tant que vous ne vous l'attribuez
# pas entièrement.
#========================================================

module POKEMON_S
  class Pokemon
  
    def generate(hash)
      id = (hash.has_key?("ID")) ? hash["ID"] : 1 
      shiny = (hash.has_key?("SHINY")) ? hash["SHINY"] : nil 
      if id.type == Fixnum
        id = id
      elsif id.type == String
        id = id_conversion(id)
      end
      @id = id
      @id_bis = Pokemon_Info.id_bis(@id)
      @trainer_id = Player.id
      @trainer_name = Player.name
      @code = code_generation
      @male_rate = 100 - Pokemon_Info.female_rate(@id)
      @female_rate = Pokemon_Info.female_rate(@id)
      @gender = (hash.has_key?("GR")) ? set_gender(hash["GR"]) : gender_generation
      if shiny # Shiny forcé
        @shiny = true
      elsif shiny == false
        @shiny = false
      else
        a = @code ^ Player.code
        b = a & 0xFFFF
        c = (a >> 16) & 0xFFFF
        d = b ^ c
        @shiny = d < 8
      end
      archetype(id)
      @dv_hp = (hash.has_key?("STAT")) ? hash["STAT"][0] : rand(32)
      @dv_atk = (hash.has_key?("STAT")) ? hash["STAT"][1] : rand(32)
      @dv_dfe = (hash.has_key?("STAT")) ? hash["STAT"][2] : rand(32)
      @dv_spd = (hash.has_key?("STAT")) ? hash["STAT"][3] : rand(32)
      @dv_ats = (hash.has_key?("STAT")) ? hash["STAT"][4] : rand(32)
      @dv_dfs = (hash.has_key?("STAT")) ? hash["STAT"][5] : rand(32)
      if @shiny
        @dv_hp = @dv_hp>16 ? 31 : @dv_hp += 15
        @dv_atk = @dv_atk>16 ? 31 : @dv_atk += 15
        @dv_dfe = @dv_dfe>16 ? 31 : @dv_dfe += 15
        @dv_spd = @dv_spd>16 ? 31 : @dv_spd += 15
        @dv_ats = @dv_ats>16 ? 31 : @dv_ats += 15
        @dv_dfs = @dv_dfs>16 ? 31 : @dv_dfs += 15
      end
      @hp_plus = 0
      @atk_plus = 0
      @dfe_plus = 0
      @ats_plus = 0
      @dfs_plus = 0
      @spd_plus = 0
      @given_name = name.clone
      @level = (hash.has_key?("NV")) ? hash["NV"] : 1
      @exp = exp_list[@level]
      @skills_set = []
      @status = 0
      @status_count = 0
      @item_hold = 0 #id item
      @loyalty = Pokemon_Info.base_loyalty(id)
      @loyalty += 100
      @nature = (hash.has_key?("NATURE")) ? nature_conversion(hash["NATURE"]) : nature_generation
      @ability = (hash.has_key?("TALENT")) ? hash["TALENT"] : ability_conversion
      initialize_skill
      reset_stat_stage
      statistic_refresh
      @hp = maxhp_basis
      @ball_data = $data_ball[1]
      @effect = []
      @effect_count = []
      @ability_active = false
      @ability_token = nil
      @egg = false
      @step_remaining = 0
      @form = 0
    end
    
    #------------------------------------------------------------
    # Désignation du caractère
    #------------------------------------------------------------
    def nature_generation 
      nature_code = @code % 25
      case nature_code #atk, dfe, vit, ats, dfs
      when 0
        return ["Assuré", 90,110,100,100,100]
      when 1
        return ["Bizarre", 100,100,100,100,100]
      when 2
        return ["Brave", 110,100,90,100,100]
      when 3
        return ["Calme", 90,100,100,100,110]        
      when 4
        return ["Discret", 100,100,90,110,100]
      when 5
        return ["Docile", 100,100,100,100,100]
      when 6
        return ["Doux", 100,90,100,110,100]
      when 7
        return ["Foufou", 100,100,100,110,90]
      when 8
        return ["Gentil", 100,90,100,100,110]
      when 9
        return ["Hardi", 100,100,100,100,100]
      when 10
        return ["Jovial", 100,100,110,90,100]
      when 11
        return ["Lâche", 100,110,100,100,90]
      when 12
        return ["Malin", 100,110,100,90,100]
      when 13
        return ["Malpoli", 100,100,90,100,110]
      when 14
        return ["Mauvais", 110,100,100,100,90]
      when 15
        return ["Modeste", 90,100,100,110,100]
      when 16
        return ["Naif", 100,100,110,100,90]
      when 17
        return ["Pressé", 100,90,110,100,100]
      when 18
        return ["Prudent", 100,100,100,90,110]
      when 19
        return ["Pudique", 100,100,100,100,100]
      when 20
        return ["Relax", 100,110,90,100,100]
      when 21
        return ["Rigide", 110,100,100,90,100]
      when 22
        return ["Sérieux", 100,100,100,100,100]
      when 23
        return ["Solo", 110,90,100,100,100]
      when 24
        return ["Timide", 90,100,110,100,100]
      end
    end
    
    #------------------------------------------------------------
    # Conversion de la nature str -> array
    #------------------------------------------------------------
    def nature_conversion(str) #atk def vit atts defs
      case str.downcase
      when "assuré"
        return ["Assuré", 90,110,100,100,100]
      when "bizarre"
        return ["Bizarre", 100,100,100,100,100]
      when "brave"
        return ["Brave", 110,100,90,100,100]
      when "calme"
        return ["Calme", 90,100,100,100,110]        
      when "discret"
        return ["Discret", 100,100,90,110,100]
      when "docile"
        return ["Docile", 100,100,100,100,100]
      when "doux"
        return ["Doux", 100,90,100,110,100]
      when "foufou"
        return ["Foufou", 100,100,100,110,90]
      when "gentil"
        return ["Gentil", 100,90,100,100,110]
      when "hardi"
        return ["Hardi", 100,100,100,100,100]
      when "jovial"
        return ["Jovial", 100,100,110,90,100]
      when "lâche"
        return ["Lâche", 100,110,100,100,90]
      when "malin"
        return ["Malin", 100,110,100,90,100]
      when "malpoli"
        return ["Malpoli", 100,100,90,100,110]
      when "mauvais"
        return ["Mauvais", 110,100,100,100,90]
      when "modeste"
        return ["Modeste", 90,100,100,110,100]
      when "naif"
        return ["Naif", 100,100,110,100,90]
      when "pressé"
        return ["Pressé", 100,90,110,100,100]
      when "prudent"
        return ["Prudent", 100,100,100,90,110]
      when "pudique"
        return ["Pudique", 100,100,100,100,100]
      when "relax"
        return ["Relax", 100,110,90,100,100]
      when "rigide"
        return ["Rigide", 110,100,100,90,100]
      when "sérieux"
        return ["Sérieux", 100,100,100,100,100]
      when "solo"
        return ["Solo", 110,90,100,100,100]
      when "timide"
        return ["Timide", 90,100,110,100,100]
      end

    end
  end
end
  
class Interpreter
    
  include POKEMON_S
    
  #-----------------------------------------------------------------------------
  # call_battle_wild
  #   Appel d'un combat contre un Pokémon sauvage quelconque (hash)
  #   hash: voir fonction d'ajout de Pokémon
  #-----------------------------------------------------------------------------
  def call_battle_wild_param(hash, ia = false)
    $game_temp.map_bgm = $game_system.playing_bgm
    $game_system.bgm_stop
    id_data = hash["ID"]
    if id_data.type == Fixnum
      id = id_data
    elsif id_data.type == String
      id = id_conversion(id_data)
    end
    pokemon = Pokemon.new(1, 1, false)
    pokemon.generate(hash)
    $scene = Pokemon_Battle_Wild.new($pokemon_party, pokemon, ia)
    @wait_count = 2
    return true
  end
  alias demarrer_combat_param call_battle_wild_param
  
end