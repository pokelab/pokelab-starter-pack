module POKEMON_S
  # --------------------------------------------------------
  # SAVE_CONVERT
  #    Pour convertir vos sauvegardes pré-v0.7 en compatible 0.7
  #    Mettez true, chargez vos parties et sauvegardez vos parties à nouveau
  #    Elles seront converties et vous ne pourrez plus les charger
  #    tant que SAVE_CONVERT = true
  # --------------------------------------------------------
  SAVE_CONVERT = false
  
  # --------------------------------------------------------
  # XXX_CONVERSION
  #   - Indique l'utilisation de la database scriptée (false)
  #     ou celle du logiciel (true)
  #   - A partir du moment où vous souhaitez modifier les attaques/objets/pokémons
  #     par le logiciel réglez ceci en true.
  # --------------------------------------------------------
  SKILL_CONVERSION = true
  ITEM_CONVERSION = true
  PKMN_CONVERSION = true
  
  # --------------------------------------------------------
  # SPEED_MSG
  #   - Vitesse de défilement du texte, en caractère par images.
  #     Minimum 1 (lent), Maximum 30 (instantanné)
  #     Réglez à 0 pour avoir un affichage instantanné
  # --------------------------------------------------------
  SPEED_MSG = 2
  
  # --------------------------------------------------------
  # MSG
  #   - Fichier image de la boîte de dialogue du jeu
  #     dans le dossier Graphics/picture
  # --------------------------------------------------------
  MSG = "messagedummy.png"
  
  # --------------------------------------------------------
  # BATTLE_MSG
  #   - Fond de la fenêtre de message au combat
  #     dans le dossier Graphics/Pictures
  # --------------------------------------------------------
  BATTLE_MSG = "dummy1.png"
  
  # --------------------------------------------------------
  # BATTLE_TRANS
  #   - Nombre de transitions vers les combats
  #       Les transitions doivent être contenues dans Graphics/Transition/
  #       et doivent avoir pour nom "battlex.png" où 
  #       x vaut de 1 à BATTLE_TRANS
  #     Important: BATTLE_TRANS est égal au nombre de transitions dans le dossier
  # --------------------------------------------------------
  BATTLE_TRANS = 4 
  
  # --------------------------------------------------------
  # MAX_LEVEL
  #   - Niveau maximal des Pokémons
  # --------------------------------------------------------
  MAX_LEVEL = 100
  
  # --------------------------------------------------------
  # MAPLINK
  #   - Permet d'activer la jonction des maps (plus de lag, mais meilleur
  #     effet visuel)
  #     Inclure sur chaque map la fonction jonction_map décrite dans Interpreter
  # --------------------------------------------------------
  MAPLINK = true
  
  # --------------------------------------------------------
  # _MAPLINK
  #   - $game_switches[4]
  # --------------------------------------------------------
  # Ne pas modifier
  def self._MAPLINK
    if $game_switches[4] == nil
      return false
    end
    return $game_switches[4]
  end
  
  def self._MAPLINK=(value)
    $game_switches[4] = value
  end
  
  # --------------------------------------------------------
  # MAPPANEL
  #   - Permet d'activer les messages de localisation qui donnent le nom
  #     de la map dans laquelle on entre
  # --------------------------------------------------------
  MAPPANEL = true
  
  # --------------------------------------------------------
  # _WMAPID
  #   - Vous pouvez y régler une valeur par défaut ici
  # --------------------------------------------------------
  def self._WMAPID
    if $game_variables[6] == 0
      return 10 #Valeur par défaut
    end
    return $game_variables[6]
  end

  
  # --------------------------------------------------------
  # TRADEGROUP
  #   - Nombre entre 1 et 1023 compris, qui détermine un groupe de jeux compatibles
  #     Si TRADEGROUP est réglé à 0, tout Pokémon peut être reçu de n'importe
  #     quel jeu (et donc attentions aux bugs)
  # --------------------------------------------------------
  TRADEGROUP = 667
  
  
  # --------------------------------------------------------
  # SAVEBOUNDSLOT
  #   - Mettez true si vous voulez qu'une sauvegarde soit liée
  #     à un slot
  # --------------------------------------------------------
  SAVEBOUNDSLOT = true
  
  # --------------------------------------------------------
  # ATTACKKIND
  #   - Mettez true pour que les attaques utilisent leur caractère
  #     Physique / Spécial au sens D/P du terme.
  #     false si vous retournez au système de R/S/E
  # --------------------------------------------------------
  ATTACKKIND = true
  
  
  # --------------------------------------------------------
  # MAPINTRO
  #   - Ecrivez [numéro de map, position x, position y] 
  #     si vous voulez spécifiez une intro faite sur une map de votre choix.
  #     Ecrivez false sinon : l'intro en script sera utilisée
  #     Exemple : [5, 12, 13] => commence une intro sur la map 5, x=12, y=13
  #   - Quand votre intro est terminée, inscrivez
  # --------------------------------------------------------
  MAPINTRO = false
  
  
  # --------------------------------------------------------
  # EXP_TABLE
  #   - Table précalculée de l'expérience
  #     A modifier si vous savez ce que vous faites
  # --------------------------------------------------------
  EXP_TABLE = []
  for j in 0..5
    EXP_TABLE[j] = []
    EXP_TABLE[j][0] = nil
    EXP_TABLE[j][1] = 0
    case j
    when 0 # Rapide
      for i in 2..MAX_LEVEL
        EXP_TABLE[j][i] = Integer(0.8*(i**3))
      end
    when 1 # Normal
      for i in 2..MAX_LEVEL
        EXP_TABLE[j][i] = Integer(i**3)
      end
    when 2 # Lent
      for i in 2..MAX_LEVEL
        EXP_TABLE[j][i] = Integer(1.25*(i**3))
      end
    when 3 # Parabolique
      for i in 2..MAX_LEVEL
        EXP_TABLE[j][i] = Integer((1.2*(i**3) - 15*(i**2) + 100*i - 140))
      end
    when 4 # Erratic
      for i in 2..50
        EXP_TABLE[j][i] = Integer( i**3*(100-i)/50.0 )
      end
      for i in 51..68
        EXP_TABLE[j][i] = Integer( i**3*(150-i)/100.0 )
      end
      for i in 69..98
        case i%3
        when 0
          EXP_TABLE[j][i] = Integer( i**3 * (1.274 - 1/50 * (i/3) - 0) )
        when 1
          EXP_TABLE[j][i] = Integer( i**3 * (1.274 - 1/50 * (i/3) - 0.008) )
        when 2
          EXP_TABLE[j][i] = Integer( i**3 * (1.274 - 1/50 * (i/3) - 0.014) )
        end
      end
      for i in 99..MAX_LEVEL
        EXP_TABLE[j][i] = Integer( i**3*(160-i)/100.0 )
      end
    when 5 # Fluctuant
      for i in 2..15
        EXP_TABLE[j][i] = Integer( i**3* (24 + (i+1)/3) / 50  )
      end
      for i in 16..35
        EXP_TABLE[j][i] = Integer( i**3* ( 14 + i) / 50 )
      end
      for i in 36..MAX_LEVEL
        EXP_TABLE[j][i] = Integer( i**3 * ( 32 + (i/2) ) / 50 )
      end
    end
  end
  

end

