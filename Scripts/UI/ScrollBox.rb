module UI
  
  class ScrollBox < TextBloc
    
    attr_reader :scroll
    attr_reader :handle
    attr_reader :active
    attr_reader :view_rect
    
    def initialize(x,y,width,height,lineh=26,margin=26,windowskin=nil,scroll=nil,handle=nil,text="",v=nil)
      
      @window = Window.new(Rect.new(x,y,width+2*margin,height+2*margin),windowskin)
      
      @view_rect = Rect.new(x,y,width,height)
      
      super(x+margin,y+margin,width,lineh,text,v)
      @scroll = Window.new(Rect.new(x+width+margin,y+margin,8,height),scroll)
      @handle = Window.new(Rect.new(x+width+margin,y+margin,8,(height*height).to_f/@sprite.bitmap.height.to_f),handle)
      
      @rect = Rect.new(x,y,width+2*margin,height+2*margin)
      @sprite.src_rect = Rect.new(0,0,@view_rect.width,@view_rect.height)
      @margin = margin
      
    end
    
    def update
      
            # On règle les options d'alignement
      align = (@align == :left) ? 0 : (@align == :center) ? 1 : 2
      
      # Variables de fonctionnement
      lines = []          # Tableau des lignes de texte
      line = ""           # Ligne courante
      current_width = 0   # Largeur du texte de la ligne courante
      
      # On distribue le texte dans les lignes
      @text.split(" ").each do |word|   # Pour chaque mot
        
        # On rajoute l'espace enlevé par le split
        word += " " 
        # On rajoute la largeur du texte pour le mot
        current_width += @sprite.bitmap.text_size(word).width 
        
        # Si ça ne dépasse pas, on ajoute à la ligne actuelle
        if(current_width < @view_rect.width)
          line += word
        # Sinon, on crée une nouvelle ligne
        else
          current_width = @sprite.bitmap.text_size(word).width
          lines.push(line)
          line = word
        end
        
      end
      
      # On ajoute la dernière ligne si elle est différente de la dernière enregistrée
      lines.push(line) if line != lines.last
      
      # Suppression du bitmap
      @sprite.bitmap.dispose
      
      # Création du nouveau bitmap
      @sprite.bitmap = Bitmap.new(@view_rect.width, lines.length*@lineh)
      @sprite.bitmap.font = @font
      
      # On écrit chaque ligne
      lines.each_with_index do |line,i|
        @sprite.bitmap.draw_text(0,lineh*i,@view_rect.width,@lineh,line,align)
      end
      
      @sprite.src_rect = Rect.new(0,0,@view_rect.width,@view_rect.height)
      
    end      
    
    def activate
      
      @active = true
      
      @sprite.src_rect = Rect.new(0,0,@view_rect.width,@view_rect.height)
      
      loop do
        
        Graphics.update
        Input.update([0x26,0x28,0x1B])
        @sprite.src_rect.y -= 4 if Input.press?(0x26)
        @sprite.src_rect.y += 4 if Input.press?(0x28)
        
        @sprite.src_rect.y = 0 if @sprite.src_rect.y < 0
        @sprite.src_rect.y = @sprite.bitmap.height - @view_rect.height if @sprite.src_rect.y > @sprite.bitmap.height - @view_rect.height
        
        break if Input.trigger?(0x1B)
        
        @handle.y = (((@view_rect.y+@margin+@scroll.height-@handle.height) - (@view_rect.y+@margin)).to_f/((@sprite.bitmap.height - @view_rect.height)).to_f) * @sprite.src_rect.y + @view_rect.y + @margin
        
      end
      
      @active = false
      
    end
    
    def dispose
      
      @window.dispose
      @scroll.dispose
      @sprite.dispose
      @handle.dispose
    
    end
    
  end
  
end

begin
  scroll = UI::WindowSkin.new("scrollbar",[4,4,4,4])
  e = UI::ScrollBox.new(0,0,400,200,26,26,nil,scroll,scroll,$lorem)
  e.color = Color.new(0,0,0)
  e.activate
  e.dispose
end