module UI
  
  class Interface
    
    attr_accessor :children           # Liste de tous les contrôles interactifs de l'interface
    attr_accessor :children_binding   # Binding des contrôles
    attr_reader :current              # Symbol du contrôle courant
    attr_reader :current_selector     # Symbol sur lequel est le sélecteur
    attr_accessor :selector           # Sprite du sélecteur
    attr_accessor :selector_skin      # WindowSkin du sélecteur
    attr_reader :current_active       # Contrôle actif actuellement, nil si aucun
    attr_accessor :active             # Si l'interface est active
    attr_reader :mouse_enabled        # Si la souris est utilisée (par défaut, oui)
    attr_reader :old_mouse_pos        # Précédente position de la souris
    
    def initialize
      
      @children = {}
      @children_binding = {}
      @current = nil
      @current_selector = nil
      @selector = Scaling_Sprite.new
      @selector.z = 1_000_000
      @selector_skin = WindowSkin.new(CONFIG::SKINS[:selector][0],CONFIG::SKINS[:selector][1])
      @active = false
      @current_active = nil
      @mouse_enabled = true
      @old_mouse_pos = Point.new(-1,-1)
      
    end
    
    def [](sym)
      
      @children[sym]
      
    end
    
    def []=(sym,val)
      
      @children[sym] = val
      @children_binding[sym] = {} if @children_binding[sym] == nil
      
    end
    
    def current=(i)
      
      if(!@children.keys.include? i)
        Console.warning("Aucune clé correspondant à #{i.to_s} n'a été trouvée parmi les enfants de cette interface.")
        return false
      end
      
      @current = i
      
      update
      
    end
    
    def update
      
      Input.mouse_update if @mouse_enabled
      
      if (@current_selector != @current)
        @current_selector = @current
        update_selector
      end
      
      @current_active = nil
      
      @children.each do |key,child|
        
        if child.active == true
          @current_active = key
          break
        end
        
      end
      
      mouse_detection if @mouse_enabled
      
      Input.update([0x25,0x26,0x27,0x28])
      Input.update(CONFIG::ACTION_KEYS[:confirm])
      Input.update([0x01])
      
      if(Input.trigger?(0x25))
        @current = @children_binding[@current][:left] if @children_binding[@current][:left] != nil
      elsif(Input.trigger?(0x26))
        @current = @children_binding[@current][:up] if @children_binding[@current][:up] != nil
      elsif(Input.trigger?(0x27))
        @current = @children_binding[@current][:right] if @children_binding[@current][:right] != nil
      elsif(Input.trigger?(0x28))
        @current = @children_binding[@current][:down] if @children_binding[@current][:down] != nil
      elsif(Input.trigger_m?(CONFIG::ACTION_KEYS[:confirm]) || (Input.trigger?(0x01) && @mouse_enabled && @children[@current].rect.contains?(Point.new(Input.mouse_x,Input.mouse_y))))
        @children[@current].activate()
      end
      
      Input.update(CONFIG::ACTION_KEYS[:cancel])
      @active = false if Input.trigger_m?(CONFIG::ACTION_KEYS[:cancel])
      
    end
    
    def mouse_detection
      
      pos = Point.new(Input.mouse_x,Input.mouse_y)
      
      return if pos.x == @old_mouse_pos.x && pos.y == @old_mouse_pos.y
      
      @children.each do |key,child|
        
        if child.rect.contains?(pos)
          @current = key 
          break
        end
        
      end
      
      @old_mouse_pos = pos
      
    end
    
    def update_selector
      
      r = @children[@current_selector].rect
      @selector.bitmap = @selector_skin.get(r.width,r.height)
      @selector.x, @selector.y = r.x, r.y
      
    end
    
    def bind_control_unidirectional(sym_from,sym_to,key)
      
      return -1 unless [:left,:right,:up,:down].include? key
      return -1 unless @children.keys.include? sym_from
      return -1 unless @children.keys.include? sym_to
      
      @children_binding[sym_from][key] = sym_to
      
    end
    
    def bind_control(sym_1,sym_2,key)
      
      return -1 unless [:left,:right,:up,:down].include? key
      return -1 unless @children.keys.include? sym_1
      return -1 unless @children.keys.include? sym_2
      
      key2 = :left
      
      case(key)
      when :left:
        key2 = :right
      when :up:
        key2 = :down
      when :down:
        key2 = :up
      end
      
      @children_binding[sym_1][key]  = sym_2
      @children_binding[sym_2][key2] = sym_1
      
    end
    
    def dispose
    
      @children.each {|key,child| child.dispose }
      @selector.dispose
      
    end
    
  end
  
end

begin 
  white = Scaling_Sprite.new
  white.bitmap = RPG::Cache.title("white.png")
  white.z = -1
  title_win = UI::Window.new(Rect.new(-20,10,200,72),UI::WindowSkin.new("title.png",[16,16,16,16]))
  title_text = UI::Text.new(20,10,140,72,"Mon interface")
  title_text.color = Color.new(0,0,0)
  title_text.align = :center
  e = UI::Interface.new
  e[:valider] = UI::Button.new(Graphics.width/$ZOOM-328,Graphics.height/$ZOOM-142,100,20,26,UI::WindowSkin.new("button_quit.png",[16,16,16,16]))
  scroll = UI::WindowSkin.new("scrollbar",[4,4,4,4])
  e[:scroll] = UI::ScrollBox.new(20,112,400,200,26,26,nil,scroll,scroll,$lorem)
  e[:quitter] = UI::Button.new(Graphics.width/$ZOOM-172,Graphics.height/$ZOOM-142,100)
  e[:valider].text = "Valider"
  e[:valider].color = Color.new(0,0,0)
  e[:scroll].color = Color.new(0,0,0)
  e[:quitter].text = "Quitter"
  e.bind_control(:scroll,:valider,:down)
  e.bind_control(:valider,:quitter,:right)
  e.current = :scroll
  e.active = true
  loop do
    e.update
    Graphics.update
    if e[:valider].active
      Console.log("VALIDATION")
      e[:valider].active = false
    elsif e[:quitter].active
      Console.log("QUITTER")
      e.active = false
    end      
    break unless e.active
  end
  e.dispose
end