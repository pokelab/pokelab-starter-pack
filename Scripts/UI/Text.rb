#========================================================
# Composant Texte - Ŧž.A
# Affiche un texte
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module UI
  
  class Text
    
    attr_reader :font       # Police d'écriture
    attr_reader :text       # Texte affiché
    attr_reader :rect       # Rectangle d'affichage
    attr_reader :align      # Alignement du texte [:left, :right, :center]
    attr_reader :sprite     # Sprite du texte
    
    #----------------------------------------------
    # Constructeur
    #   - x: position x
    #   - y: position y
    #   - width: largeur du texte
    #   - height: hauteur du texte
    #   - text: texte de départ
    #   - v: viewport
    #----------------------------------------------
    def initialize(x,y,width,height,text="",v=nil)
      
      @sprite = Scaling_Sprite.new(v)
      @sprite.x = x
      @sprite.y = y
      @sprite.bitmap = Bitmap.new(width, height)
      @rect = Rect.new(0,0,width,height)
      @align = :left
      @font = @sprite.bitmap.font
      self.text = text
      
    end #END def
    
    #----------------------------------------------
    # Modifier le texte
    #----------------------------------------------
    def text=(t)
      
      @text = t
      _update
      
    end #END def
    
    #----------------------------------------------
    # Met à jour le sprite
    #----------------------------------------------
    def _update
      
      # On vérifie l'alignement
      a = 0
      if @align == :center
        a = 1
      elsif @align == :right
        a = 2
      end
      
      # On efface puis on écrit
      @sprite.bitmap.clear()
      @sprite.bitmap.draw_text(@rect, @text, a)
      
    end #END def
    
    #----------------------------------------------
    # Modifier la police
    #----------------------------------------------
    def font=(f)
      
      @sprite.bitmap.font = f
      @font = f
      _update
      
    end #END def
    
    #----------------------------------------------
    # Modifier les dimensions du sprite
    #----------------------------------------------
    def rect=(r)
      
      @rect = r
      @sprite.bitmap = Bitmap.new(r)
      _update
      
    end #END def
    
    #----------------------------------------------
    # Modifier l'alignement
    #----------------------------------------------
    def align=(a)
      
      @align = a
      _update
      
    end #END def
    
    #----------------------------------------------
    # Modifier la couleur
    #   - c: couleur
    #----------------------------------------------
    def color=(c)
      
      @font.color = c
      _update
      
    end #END def
    
    #----------------------------------------------
    # Modifier la taille
    #   - s: taille
    #----------------------------------------------
    def size=(s)
      
      @font.size = s
      _update
      
    end #END def
    
    #----------------------------------------------
    # On récupère les méthodes qui ne sont pas 
    # disponibles et on tente de les renvoyer
    # au sprite attaché à la classe.
    #----------------------------------------------
    def method_missing(method_symbol, *args, &block)
      
      return @sprite.send(method_symbol, *args, &block) if @sprite.respond_to?(method_symbol)
      super(method_symbol, *args, &block)
      
    end #END def
    
    
    #----------------------------------------------
    # Animer la couleur
    #   - _c: couleur
    #   - i: itérateur
    #   - d: durée en frames
    #   - easing: fonction d'easing
    #----------------------------------------------
    def animate_color(_c,i,d=60,easing=:linear)
      return if i > d # Si l'animation est finie
      b = @font.color.red
      c = _c.red - @font.color.red
      @font.color.red = UI::Easing.apply(i,b,c,d,easing)
      b = @font.color.green
      c = _c.green - @font.color.green
      @font.color.green = UI::Easing.apply(i,b,c,d,easing)
      b = @font.color.blue
      c = _c.blue - @font.color.blue
      @font.color.blue = UI::Easing.apply(i,b,c,d,easing)
      b = @font.color.alpha
      c = _c.alpha - @font.color.alpha
      @font.color.alpha = UI::Easing.apply(i,b,c,d,easing)
      # On met à jour le sprite
      _update
    end #END def
    
    #----------------------------------------------
    # Libère le texte
    #----------------------------------------------
    def dispose
      @sprite.dispose
    end #END def
    
    #----------------------------------------------
    # Renvoie l'index-z du sprite
    #----------------------------------------------
    def z
      return @sprite.z
    end #END def
    
    #----------------------------------------------
    # Modifie l'index-z du sprite
    #   - e: index
    #----------------------------------------------
    def z=(e)
      @sprite.viewport.z = e
    end #END def
    
    #----------------------------------------------
    # Modifie la position ox du sprite
    #----------------------------------------------
    def ox=(i)
      @sprite.ox = i
    end #END def
    
    #----------------------------------------------
    # Modifie la position oy du sprite
    #----------------------------------------------
    def oy=(i)
      @sprite.oy = i
    end #END def
    
    def opacity
      @sprite.opacity
    end
    
    def opacity=(i)
      @sprite.opacity = i
    end
    
    def move_to(x,y,i,d=60,easing=:linear)
      @sprite.move_to(x,y,i,d,easing)
    end
    
  end #END class
  
end #END module UI