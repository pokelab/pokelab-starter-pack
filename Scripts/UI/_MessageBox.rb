module UI
  
  class MessageBox
    
    attr_accessor :sprite
    attr_accessor :contents
    
    def initialize(rect,margin=14,text="",lineh=24,window_skin=nil,cursor=nil)
      
      @sprite = Window.new(rect,window_skin)
      @sprite.sprite.x = rect.x
      @sprite.sprite.y = rect.y
      @contents = TextBox.new(rect.x+margin,rect.y+margin,rect.width-margin*2,rect.height-margin*2,text,lineh)
      @margin = margin
      @cursor = Cursor.new(CONFIG::BASE_CURSOR)
      @cursor.x = rect.x + rect.width - (margin.to_f*2.5).round
      @cursor.y = rect.y + rect.height - margin*2
      
    end
    
    def draw_message()
      @contents.text.length.times do |i|
        @contents.draw_char(i)
        Graphics.update
      end
      @cursor.visible = true
      loop do
        Graphics.update
        Input.update
        @cursor.update
        break if(Input.trigger?(Input::C))
      end
      @cursor.visible = false
    end
    
    def text=(i)
      @contents.text = i
    end
    
    def text
      return @contents.text
    end
    
    def color=(i)
      @contents.color = i
    end
    
    def color
      return @contents.color
    end
    
    def z=(i)
      @sprite.sprite.z = i
      @contents.z = i+1
      @cursor.z = i+2
    end
    
    def x=(i)
      @sprite.sprite.x = i
      @contents.sprite.y = i + @margin
    end
    
    def y=(i)
      @sprite.sprite.y = i
      @contents.sprite.y = i + @margin
    end
    
    def dispose
      @sprite.dispose
      @contents.dispose
      @cursor.dispose
    end
    
  end
  
end