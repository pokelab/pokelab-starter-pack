#========================================================
# Composant Window - Ŧž.A
# Affiche une fenêtre
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module UI
  
  class Window
    
    attr_accessor :sprite         # Sprite principal
    
    #----------------------------------------------
    # Constructeur
    #   - rect: dimensions de la fenêtre
    #   - window_skin: window_skin de la fenêtre
    #----------------------------------------------
    def initialize(rect,window_skin=nil)
      @sprite = Scaling_Sprite.new
      window_skin = WindowSkin.new(CONFIG::BASE_WINDOW_SKIN[0],CONFIG::BASE_WINDOW_SKIN[1]) if window_skin == nil
      @sprite.bitmap = window_skin.get(rect.width,rect.height)
      @sprite.x = rect.x
      @sprite.y = rect.y
    end #END def
    
    #----------------------------------------------
    # Libère la fenêtre
    #----------------------------------------------
    def dispose;@sprite.dispose;end
    #----------------------------------------------
    # Modifie la position x de la fenêtre
    #----------------------------------------------
    def x=(i);@sprite.x = i;end
    #----------------------------------------------
    # Renvoie la position x de la fenêtre
    #----------------------------------------------
    def x;@sprite.x;end
    #----------------------------------------------
    # Modifie la position y de la fenêtre
    #----------------------------------------------
    def y=(i);@sprite.y = i;end
    #----------------------------------------------
    # Renvoie la position y de la fenêtre
    #----------------------------------------------
    def y;@sprite.y;end
    #----------------------------------------------
    # Modifie la position z de la fenêtre
    #----------------------------------------------
    def z=(i);@sprite.z = i;end
    #----------------------------------------------
    # Renvoie la hauteur de la fenêtre
    #----------------------------------------------
    def height;@sprite.bitmap.height;end
    #----------------------------------------------
    # Renvoie la largeur de la fenêtre
    #----------------------------------------------
    def width;@sprite.bitmap.width;end
      
    def opacity;@sprite.opacity;end
    def opacity=(i);@sprite.opacity=i;end
      
    def move_to(x,y,i,d=60,easing=:linear)
      @sprite.move_to(x,y,i,d,easing)
    end
    
  end #END class
  
end #END module UI