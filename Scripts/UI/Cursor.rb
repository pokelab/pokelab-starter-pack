#========================================================
# Composant Cursor - Ŧž.A
# Affiche un curseur
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module UI
  
  class Cursor < Scaling_Sprite
    
    attr_accessor :size             # Taille du curseur
    attr_accessor :current_frame    # Frame courante de l'animation
    
    #----------------------------------------------
    # Constructeur
    #   - file: nom du fichier
    #   - v: viewport (par défaut, l'écran de jeu)
    #----------------------------------------------
    def initialize(file=nil,v=nil)
      super(v)
      file = CONFIG::BASE_CURSOR if file == nil
      self.bitmap = RPG::Cache.windowskin(file)
      @size = self.bitmap.height
      self.visible = false
      self.src_rect = Rect.new(0,0,@size,@size)
      @current_frame = 0
      @speed = 6
      @i = 0
    end #END def
    
    #----------------------------------------------
    # Met à jour l'animation du sprite
    #----------------------------------------------
    def update
     @i += 1
     if @i == @speed
       @i = 0
       @current_frame += 1
       @current_frame = 0 if @current_frame * @size >= self.bitmap.width
       self.src_rect = Rect.new(@size*@current_frame,0,@size,@size)
     end
   end #END def
   
   #----------------------------------------------
   # Modifie la vitesse de l'animation
   #----------------------------------------------
   def speed=(i);@speed=i;end
   #----------------------------------------------
   # Renvoie la vitesse de l'animation
   #----------------------------------------------
   def speed;@speed;end
    
  end #END class
  
end #END module UI