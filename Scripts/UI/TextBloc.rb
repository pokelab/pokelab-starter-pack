#========================================================
# Composant TextBloc - Ŧž.A
# Affiche un bloc de texte de plusieurs lignes
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module UI
  
  class TextBloc
    
    attr_reader :font       # Police d'écriture
    attr_reader :text       # Texte affiché
    attr_reader :lineh      # Hauteur d'une ligne
    attr_reader :rect       # Rectangle d'affichage
    attr_reader :align      # Alignement du texte [:left, :right, :center]
    attr_reader :sprite     # Sprite du texte
    
    #----------------------------------------------
    # Constructeur
    #   - x: position x
    #   - y: position y
    #   - width: largeur du bloc de texte
    #   - lineh: hauteur d'une ligne
    #   - text: texte affiché
    #   - v: viewport (par défaut, l'écran de jeu)
    #----------------------------------------------
    def initialize(x,y,width,lineh=26,text="",v=nil)
      
      @sprite = Scaling_Sprite.new(v)
      @sprite.x = x
      @sprite.y = y
      @sprite.bitmap = Bitmap.new(1, 1)
      @rect = Rect.new(0,0,width,lineh)
      @align = :left
      @font = @sprite.bitmap.font
      @text = text
      @lineh = lineh
      
      update      
      
    end #END def
    
    #----------------------------------------------
    # Mise à jour
    #----------------------------------------------
    def update
      
      # On règle les options d'alignement
      align = (@align == :left) ? 0 : (@align == :center) ? 1 : 2
      
      # Variables de fonctionnement
      lines = []          # Tableau des lignes de texte
      line = ""           # Ligne courante
      current_width = 0   # Largeur du texte de la ligne courante
      
      # On distribue le texte dans les lignes
      @text.split(" ").each do |word|   # Pour chaque mot
        
        # On rajoute l'espace enlevé par le split
        word += " " 
        # On rajoute la largeur du texte pour le mot
        current_width += @sprite.bitmap.text_size(word).width 
        
        # Si ça ne dépasse pas, on ajoute à la ligne actuelle
        if(current_width < @rect.width)
          line += word
        # Sinon, on crée une nouvelle ligne
        else
          current_width = @sprite.bitmap.text_size(word).width
          lines.push(line)
          line = word
        end
        
      end
      
      # On ajoute la dernière ligne si elle est différente de la dernière enregistrée
      lines.push(line) if line != lines.last
      
      # Suppression du bitmap
      @sprite.bitmap.dispose
      
      # Création du nouveau bitmap
      @sprite.bitmap = Bitmap.new(@rect.width, lines.length*@lineh)
      @sprite.bitmap.font = @font
      
      # On écrit chaque ligne
      lines.each_with_index do |line,i|
        @sprite.bitmap.draw_text(0,lineh*i,@rect.width,@lineh,line,align)
      end
      
    end #END def
    
    #----------------------------------------------
    # On modifie le texte
    #----------------------------------------------
    def text=(t);@text = t;update;end
      
    #----------------------------------------------
    # On retourne le texte
    #----------------------------------------------
    def text;@text;end
    
    #----------------------------------------------
    # On modifie la couleur
    #----------------------------------------------
    def color=(c);@font.color = c;update;end
      
    #----------------------------------------------
    # On retourne la couleur
    #----------------------------------------------
    def color;@font.color;end
    
    #----------------------------------------------
    # On libère le bloc de texte
    #----------------------------------------------
    def dispose;@sprite.dispose;end
    
    #----------------------------------------------
    # Animer la couleur
    #   - _c: couleur
    #   - i: itérateur
    #   - d: durée en frames
    #   - easing: fonction d'easing
    #----------------------------------------------
    def animate_color(_c,i,d=60,easing=:linear)
      return if i > d # Si l'animation est finie
      b = @font.color.red
      c = _c.red - @font.color.red
      @font.color.red = UI::Easing.apply(i,b,c,d,easing)
      b = @font.color.green
      c = _c.green - @font.color.green
      @font.color.green = UI::Easing.apply(i,b,c,d,easing)
      b = @font.color.blue
      c = _c.blue - @font.color.blue
      @font.color.blue = UI::Easing.apply(i,b,c,d,easing)
      b = @font.color.alpha
      c = _c.alpha - @font.color.alpha
      @font.color.alpha = UI::Easing.apply(i,b,c,d,easing)
      # On met à jour le sprite
      update
    end #END def
    
    #----------------------------------------------
    # On retourne la position x du sprite
    #----------------------------------------------
    def x;@sprite.x;end
    #----------------------------------------------
    # On retourne la position y du sprite
    #----------------------------------------------
    def y;@sprite.y;end
    #----------------------------------------------
    # On modifie la position x du sprite
    #----------------------------------------------
    def x=(i);@sprite.x=i;end
    #----------------------------------------------
    # On modifie la position y du sprite
    #----------------------------------------------
    def y=(i);@sprite.y=i;end
    
  end #END class TextBloc
  
end #END module UI