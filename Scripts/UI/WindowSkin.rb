#========================================================
# Composant WindowSkin - Ŧž.A
# Retourne un bitmap rempli avec un skin spécifique
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module UI
  
  class WindowSkin
    
    #----------------------------------------------
    # Constructeur
    #   - file: nom du fichier du skin
    #   - coord: bordures [left, top, right, bottom]
    #----------------------------------------------
    def initialize(file, coord=[48,48,48,48])
      
      @base_window = RPG::Cache.windowskin(file)
      @coord = coord
      
    end #END def
    
    #----------------------------------------------
    # Génère le bitmap pour une fenêtre de taille donnée
    #   - width: largeur de la fenêtre
    #   - height: hauteur de la fenêtre
    #----------------------------------------------
    def get(width, height)
      bmp = Bitmap.new(width, height)
      # Haut gauche
      bmp.blt(0,0,@base_window,Rect.new(0,0,@coord[0], @coord[1]))
      # Haut droite
      bmp.blt(width - @coord[2],0,@base_window,Rect.new(@base_window.width - @coord[2],0,@coord[2], @coord[1]))
      # Bas gauche
      bmp.blt(0,height - @coord[3],@base_window,Rect.new(0,@base_window.height - @coord[3],@coord[0], @coord[3]))
      # Bas droite
      bmp.blt(width - @coord[2],height - @coord[3],@base_window,Rect.new(@base_window.width - @coord[2],@base_window.height - @coord[3],@coord[2], @coord[3]))
      # Haut
      bmp.stretch_blt(Rect.new(@coord[0],0,width - @coord[0] - @coord[2], @coord[1]), @base_window, Rect.new(@coord[0],0,@base_window.width-@coord[0]-@coord[2],@coord[1]))
      # Bas
      bmp.stretch_blt(Rect.new(@coord[0],height-@coord[3],width - @coord[0] - @coord[2], @coord[3]), @base_window, Rect.new(@coord[0],@base_window.height-@coord[3],@base_window.width-@coord[0]-@coord[2],@coord[3]))
      # Gauche
      bmp.stretch_blt(Rect.new(0,@coord[1],@coord[0],height-@coord[1]-@coord[3]),@base_window,Rect.new(0,@coord[1],@coord[0],@base_window.height-@coord[1]-@coord[3]))
      # Droite
      bmp.stretch_blt(Rect.new(width-@coord[2],@coord[1],@coord[0],height-@coord[1]-@coord[3]),@base_window,Rect.new(@base_window.width-@coord[2],@coord[1],@coord[0],@base_window.height-@coord[1]-@coord[3]))
      # Centre
      bmp.stretch_blt(Rect.new(@coord[0],@coord[1],width-@coord[0]-@coord[2],height-@coord[1]-@coord[3]),@base_window,Rect.new(@coord[0],@coord[1],@base_window.width-@coord[0]-@coord[2],@base_window.height-@coord[1]-@coord[3]))
      return bmp
    end #END def
    
  end #END class
  
end #END module UI