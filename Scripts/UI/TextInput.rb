#========================================================
# Composant TextBox - Ŧž.A
# Affiche un bloc de texte à défilement dans un style
# Pokémon
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module UI
  
  class TextInput
    
    attr_reader :prefix
    attr_reader :active
    attr_reader :text
    attr_reader :cursor
    attr_reader :cursor_anim
    attr_reader :cursor_visible
    attr_reader :sprite
    attr_reader :rect
    attr_reader :max_length
    
    def initialize(x,y,width,height,max_length,prefix="")
      
      @sprite = Text.new(x,y,width,height)
      @sprite_cursor = Scaling_Sprite.new
      @sprite_cursor.bitmap = Bitmap.new(2,height)
      @sprite_cursor.bitmap.fill_rect(Rect.new(0,0,@sprite_cursor.bitmap.width,@sprite_cursor.bitmap.height),Color.new(255,255,255))
      @sprite_cursor.x = x
      @sprite_cursor.y = y
      @rect = Rect.new(x,y,width,height)
      @prefix = prefix
      @max_length = max_length
      @text = ""
      @cursor = 0
      @cursor_anim = 0
      @cusor_visible = true
      
      @sprite.text = @prefix
      @sprite_cursor.x = @sprite.sprite.bitmap.text_size(@prefix).width + @rect.x
      Graphics.update
      
    end
    
    def activate
      
      @active = true
      
      loop do
        Graphics.update
        Input.update
        key = Input.get_input
        @cursor_anim += 1
        if @cursor_anim == 60
          @sprite_cursor.visible = (@cusor_visible) ? false : true
          @cusor_visible = @sprite_cursor.visible
          @cursor_anim = 0
        end
        draw(key) if key != nil
        break if not @active
      end
      
    end
    
    def draw(key)
      
      # Si le message est terminé
      if(key == 0x0D || key == 0x1B)
        @active = false
        return
      end
      
      if key == 0x08 #Backspace
        return unless @cursor > 0
        e = @text.split(//)
        @cursor -= 1
        e.delete_at(@cursor)
        @text = e.join('')
      elsif key == 0x2E #Delete
        return unless @cursor > 0
        e = @text.split(//)
        e.delete_at(@cursor)
        @text = e.join('')
      elsif key == 0x25 #Left
        @cursor -= 1 if @cursor > 1
      elsif key == 0x27 #Right
        @cursor += 1 if @cursor < @text.length
      elsif @cursor < @max_length
        c = Input.get_key(key)
        return if c.length == 0
        e = @text.split(//)
        e.insert(@cursor,c)
        @text = e.join('')
        @cursor += 1
      end
      
      @sprite.text = "#{@prefix}#{@text}"
      @sprite_cursor.x = @sprite.sprite.bitmap.text_size("#{@prefix}#{@text.split(//)[0..@cursor-1]}").width + @rect.x
      @sprite_cursor.visible = true
      Graphics.update
      
    end
    
  end
  
end

=begin
  Input.mouse_visible = false
  e = UI::TextInput.new(10,454,300,26,16,"> ")
  e.sprite.color = Color.new(255,255,255)
  e.activate
=end