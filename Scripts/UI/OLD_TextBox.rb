#========================================================
# Composant TextBox - Ŧž.A
# Affiche un bloc de texte à défilement dans un style
# Pokémon
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module UI
  
  class TextBoxe
    
    attr_reader :font       # Police d'écriture
    attr_reader :text       # Texte affiché
    attr_reader :lineh      # Hauteur d'une ligne
    attr_reader :rect       # Rectangle d'affichage
    attr_reader :align      # Alignement du texte [:left, :right, :center]
    attr_reader :sprite     # Sprite du texte
    attr_reader :draw_x     # Position x du curseur de dessin de texte
    attr_reader :draw_y     # Position y du curseur de dessin de texte
    attr_reader :line_id    # Id de la ligne actuelle
    attr_reader :char_id    # Id du caractère actuel
    attr_reader :num_lines  # Nombre de lignes qui peuvent être affichées
    attr_reader :lines      # Tableau de toutes les lignes
    attr_reader :active     # Si le composant est actif
    
    def initialize(x,y,width,num_lines=3,lineh=26,text="",v=nil)
      
      @sprite = Scaling_Sprite.new(v)
      @sprite.x = x
      @sprite.y = y
      @sprite.bitmap = Bitmap.new(width,lineh*num_lines)
      @rect = Rect.new(0,0,width,lineh*num_lines)
      @align = :left
      @font = @sprite.bitmap.font
      @lineh = lineh
      @text = text
      @draw_x = 0
      @draw_y = 0
      @line_id = 0
      @char_id = 0
      @num_lines = num_lines
      @lines = []
      
      update
      
    end #END def
    
    #----------------------------------------------
    # Mise à jour du texte
    #----------------------------------------------
    def update
      
      # Variables de fonctionnement
      @lines = []
      line = ""           # Ligne courante
      current_width = 0   # Largeur du texte de la ligne courante
      
      # On distribue le texte dans les lignes
      @text.split(" ").each do |word|   # Pour chaque mot
        
        # On rajoute l'espace enlevé par le split
        word += " " 
        # On rajoute la largeur du texte pour le mot
        current_width += @sprite.bitmap.text_size(word).width 
        
        # Si ça ne dépasse pas, on ajoute à la ligne actuelle
        if(current_width < @rect.width)
          line += word
        # Sinon, on crée une nouvelle ligne
        else
          current_width = @sprite.bitmap.text_size(word).width
          @lines.push(line.split(//))
          line = word
        end
        
      end
      
      # On ajoute la dernière ligne si elle est différente de la dernière enregistrée
      @lines.push(line.split(//)) if line.split(//) != @lines.last
      
    end #END def
    
    #----------------------------------------------
    # Dessine un nouveau caractère
    #----------------------------------------------
    def draw_char

      # Variables de fonctionnement      
      c = @lines[@line_id][@char_id]          # Caractère à écrire
      w = @sprite.bitmap.text_size(c).width   # Largeur du caractère

      # On dessine
      @sprite.bitmap.draw_text(@draw_x, @draw_y, w, @lineh, c)

      # On modifie la position de l'écriture
      @draw_x += w
      @char_id += 1

      # S'il ne reste plus de caractères sur la ligne
      if not @char_id < @lines[@line_id].length
        @draw_x = 0
        @line_id += 1
        # Si on est pas arrivé au maximum de lignes affichables
        if @line_id < @num_lines
          @draw_y += @lineh
        # Sinon, on affiche une nouvelle ligne
        elsif @line_id < @lines.length
          new_line
        end
        @char_id = 0        
      end #END if
      
    end #END def
    
    #----------------------------------------------
    # Affiche une nouvelle ligne à la Pokémon
    #----------------------------------------------
    def new_line
      
      # Attend l'appui d'une touche de confirmation
      Input.wait_for_keys(CONFIG::ACTION_KEYS[:confirm])

      # Effet de scroll
      (@lineh/4).times do |i|
        Graphics.update
        @sprite.oy += 4
        @sprite.bitmap.fill_rect(0,i*4,@sprite.bitmap.width,4,Color.new(255,255,255,0))
      end

      # On rétablit le bitmap
      bmp = Bitmap.new(@sprite.bitmap.width,@sprite.bitmap.height)
      bmp.blt(0,0,@sprite.bitmap,Rect.new(0,0,@sprite.bitmap.width,@sprite.bitmap.height))
      @sprite.bitmap.clear
      @sprite.bitmap.blt(0,0,bmp,Rect.new(0,@lineh,bmp.width,@lineh*(@num_lines-1)))
      bmp.dispose
      @sprite.oy = 0

      # On reprend
      Graphics.update
      
    end
    
    #----------------------------------------------
    # Lance l'affichage du message
    #----------------------------------------------
    def active
      
      @active = true
      
      loop do
        
        draw_char
        Graphics.update
        break if @line_id >= @lines.length # Si le message est terminé, on casse la boucle
        
      end
      # On attend le dernier appui
      Input.wait_for_keys(CONFIG::ACTION_KEYS[:confirm])
      
      @active = false
      
    end #END def
    
    #----------------------------------------------
    # Modifie la couleur
    #----------------------------------------------
    def color=(c);@font.color=c;@sprite.bitmap.font=@font;end
    #----------------------------------------------
    # Modifie la police
    #----------------------------------------------
    def font=(f);@font=f;@sprite.bitmap.font=@font;end
    #----------------------------------------------
    # Modifie le texte
    #----------------------------------------------
    def text=(i);@text=i;update;end
    #----------------------------------------------
    # Libère le sprite
    #----------------------------------------------
    def dispose;@sprite.dispose;end
    
  end #END class TextBox
  
end #END module UI

=begin
  e = UI::TextBox.new(20,10,600)
  e.color = Color.new(255,255,255)
  e.text = $lorem
  e.active
  e.dispose
=end
