module UI
  
  class ScrollBox
    
    attr_accessor :sprite
    attr_accessor :contents
    attr_reader :contents_height
    
    def initialize(rect,height,margin=14,text="",lineh=24,scrollbar=nil,window_skin=nil)
      
      @sprite = Window.new(rect,window_skin)
      @sprite.sprite.x = rect.x
      @sprite.sprite.y = rect.y
      @scrollbar = Window.new(Rect.new(rect.width+margin, margin,4,rect.height-margin*2),scrollbar)
      @scrollbar.sprite.x = rect.x+rect.width-margin*1.5
      @scrollbar.sprite.y = rect.y+margin
      @min = ((height + 2*margin).to_f / 2.0).floor
      @max = @min + height - rect.height + 2*margin
      @scroll = Window.new(Rect.new(rect.width+margin, margin,4,(((rect.height-margin*2)*(rect.height-margin*2)).to_f/(height).to_f).floor),scrollbar)
      @scroll.sprite.x = rect.x+rect.width-margin*1.5
      @scroll.sprite.y = rect.y+margin
      @height = rect.height
      @scroll_base_y = rect.y+margin
      rect.height -= 2*margin
      rect.y += margin
      @contents = TextBloc.new(rect.x,rect.y,rect.width-margin*4,height,text,lineh,Viewport.new(rect))
      @margin = margin
      @contents_height = @contents.sprite.bitmap.height
      @contents.sprite.oy = @min
      
    end
    
    def update
      #@contents.sprite.bitmap.fill_rect(0, 0, @contents.sprite.bitmap.width, @contents.sprite.bitmap.height, Color.new(255,0,0))
      #@contents.sprite.bitmap.fill_rect(0, 0, @contents.sprite.bitmap.width, 4, Color.new(0,0,0))
      #@contents.sprite.bitmap.fill_rect(0, @contents.sprite.bitmap.height-4, @contents.sprite.bitmap.width, 4, Color.new(0,0,0))
      if(Input.press?(0x041)) && @contents.sprite.oy > @min
        @contents.sprite.oy -= 4
        Console.log(@contents.sprite.oy)
      elsif(Input.press?(0x042)) && @contents.sprite.oy < @max
        @contents.sprite.oy += 4
        Console.log(@contents.sprite.oy)
      end
      
      
    end
    
    def text=(i)
      @contents.text = i
    end
    
    def text
      return @contents.text
    end
    
    def color=(i)
      @contents.color = i
    end
    
    def color
      return @contents.color
    end
    
    def z=(i)
      @sprite.sprite.z = i
      @contents.z = i+1
    end
    
  end
  
end