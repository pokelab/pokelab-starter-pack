#========================================================
# Fonctions d'easing - Ŧž.A
# Gestion avancée des animations de sprites
# -------------------------------------------------------
# Il s'agit d'une implémentation partielle des fonctions
# d'easing de Robert Penner
# http://robertpenner.com/easing/
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module UI
  
  class Easing
    
    def self.apply(t, b, c, d, e)
      case e
      when :linear
        return linear(t,b,c,d)
      when :inQuad
        return inQuad(t,b,c,d)
      when :outQuad
        return outQuad(t,b,c,d)
      when :inOutQuad
        return inOutQuad(t,b,c,d)
      when :outBounce
        return outBounce(t,b,c,d)
      end
    end
    
    def self.linear(t, b, c, d)
      return (c.to_f * t.to_f / d.to_f + b.to_f).round
    end
    
    def self.inQuad(t, b, c, d)
      t = t.to_f / d.to_f
      return (c * t ** 2 + b).round
    end
    
    def self.outQuad(t, b, c, d)
      t = t.to_f / d.to_f
      return (-c * t * (t - 2) + b).round
    end
    
    def self.inOutQuad(t, b, c, d)
      t = t.to_f / d.to_f * 2
      if t < 1.0 then
        return (c.to_f / 2.0 * t ** 2 + b).round
      else
        return (-c.to_f / 2.0 * ((t - 1.0) * (t - 3.0) - 1.0) + b).round
      end
    end
    
    def self.outBounce(t, b, c, d)
      t = t.to_f / d.to_f
      if t < 1.0 / 2.75 then
        Console.log("e")
        return (c * (7.5625 * t * t) + b).round
      elsif t < 2.0 / 2.75 then
        Console.log("e2")
        t = t - (1.5 / 2.75)
        return (c * (7.5625 * t * t + 0.75) + b).round
      elsif t < 2.5 / 2.75 then
        Console.log("e3")
        t = t - (2.25 / 2.75)
        return (c * (7.5625 * t * t + 0.9375) + b).round
      else
        Console.log("e4")
        t = t - (2.625 / 2.75)
        return (c * (7.5625 * t * t + 0.984375) + b).round
      end
    end
    
  end
  
end