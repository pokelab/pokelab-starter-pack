#========================================================
# Composant MessageBox - Ŧž.A
# Affiche un message à la Pokémon
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module UI
  
  class MessageBox < TextBox
    
    attr_reader :window       # La fenêtre du message
    attr_reader :cursor       # Le curseur du message
    
    def initialize(x,y,width,num_lines=3,lineh=26,margin=26,windowskin=nil,cursor=nil,text="",v=nil)
      
      _rect = Rect.new(x,y,width+margin*2,num_lines*lineh+margin*2)
      @window = Window.new(_rect,windowskin)
      
      super(x+margin,y+margin,width,num_lines,lineh,text,v)
      
      @cursor = Cursor.new(cursor,v)
      @cursor.x = x+width+margin/2.0
      @cursor.y = y+num_lines*lineh+margin/2.0
      @cursor.visible = false
      
    end
    
    def z=(i)
      
      @window.z = i
      @sprite.z = i+1
      @cursor.z = i+2
      
    end
    
    #----------------------------------------------
    # Affiche une nouvelle ligne à la Pokémon
    #----------------------------------------------
    def new_line
      
      @cursor.visible = true
      
      loop do
        Graphics.update
        Input.update(CONFIG::ACTION_KEYS[:confirm])
        @cursor.update
        break if Input.trigger_m?(CONFIG::ACTION_KEYS[:confirm])
      end
      
      @cursor.visible = false

      # Effet de scroll
      @lineh.times do |i|
        Graphics.update if i % 4 == 0
        @sprite.src_rect.y += 1
      end

      # On reprend
      Graphics.update
      
    end
    
    #----------------------------------------------
    # Lance l'affichage du message
    #----------------------------------------------
    def active
      
      @active = true
      
      loop do
        
        draw_char
        Graphics.update
        break if @line_id >= @lines.length # Si le message est terminé, on casse la boucle
        
      end
      
      @cursor.visible = true
      
      loop do
        Graphics.update
        Input.update(CONFIG::ACTION_KEYS[:confirm])
        @cursor.update
        break if Input.trigger_m?(CONFIG::ACTION_KEYS[:confirm])
      end
      
      @cursor.visible = false
      
      Graphics.update
      
      @active = false
      
    end #END def
    
    def dispose
      
      @sprite.dispose
      @cursor.dispose
      @window.dispose
      
    end
    
  end
  
end