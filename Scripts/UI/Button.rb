module UI
  
  class Button
    
    attr_reader :sprite
    attr_reader :window
    attr_reader :rect
    attr_accessor :active
    attr_reader :margin
    attr_reader :text
    
    def initialize(x,y,width,height=20,margin=26,windowskin=WindowSkin.new(CONFIG::SKINS[:button][0],CONFIG::SKINS[:button][1]),text="")
      
      @window = Window.new(Rect.new(x,y,width+2*margin,height+2*margin),windowskin)
      @sprite = Text.new(x+margin,y+margin,width,height,text)
      
      @rect = Rect.new(x,y,width+margin*2,height+margin*2)
      @margin = margin
      @text = text
      @sprite.align = :center
      
    end
    
    def z=(i)
      @window.z = i
      @sprite.z = i + 1
    end
    
    def z;@window.z;end
    
    def dispose
      @window.dispose
      @sprite.dispose
    end
    
    def text=(i)
      @text = i
      @sprite.text = i
    end
    
    def color=(c);@sprite.color = c;end
    
    def color;@sprite.color;end
      
    def align=(a);@sprite.align = a;end
    
    def align;@sprite.align;end
      
    def x=(i);@window.x = i;@sprite.x = i + @margin;@rect.x = i;end
      
    def x;@rect.x;end
      
    def y=(i);@window.y = i;@sprite.y = i + @margin;@rect.y = i;end
      
    def y;@rect.y;end
      
    def opacity=(i);@sprite.opacity = i;@window.opacity = i;end
    
    def opacity;@sprite.opacity;end
      
    def move_to(x,y,i,d=60,easing=:linear)
      @sprite.move_to(x+@margin,y+@margin,i,d,easing)
      @window.move_to(x,y,i,d,easing)
      @rect.x = @window.x
      @rect.y = @window.y
    end
    
    def activate
      
      @active = true
      
    end
    
  end
  
end

begin
  btn = UI::Button.new(20,30,100)
  btn.text = "Annuler"
  btn.color = Color.new(181,62,69)
  btn.opacity = 0
  20.times do |i|
    Graphics.update
    Input.update([0x1B])
    btn.opacity += 13
    btn.move_to(20,20,i,20)
    break if Input.trigger?(0x1B)
  end
  Graphics.wait(200)
  btn.dispose
end