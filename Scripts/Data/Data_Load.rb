module POKEMON_S
  
  def self.load
    
    Console.step("Chargement des données de la BDD...")
    
    $data_actors        = load_data("Data/Actors.rxdata")
    $data_classes       = load_data("Data/Classes.rxdata")
    $data_skills        = load_data("Data/Skills.rxdata")
    $data_items         = load_data("Data/Items.rxdata")
    $data_weapons       = load_data("Data/Weapons.rxdata")
    $data_armors        = load_data("Data/Armors.rxdata")
    $data_enemies       = load_data("Data/Enemies.rxdata")
    $data_troops        = load_data("Data/Troops.rxdata")
    $data_states        = load_data("Data/States.rxdata")
    $data_animations    = load_data("Data/Animations.rxdata")
    $data_tilesets      = load_data("Data/Tilesets.rxdata")
    $data_common_events = load_data("Data/CommonEvents.rxdata")
    $data_system        = load_data("Data/System.rxdata")
    
    Console.done
    
  end
  
  def self.load_pokemon
    
    Console.step("Chargement des données Pokémon de la BDD...")
    
    $data_pokemon       = load_data("Data/Pokemon.rxdata")
    
    Console.done
    Console.var("$data_pokemon", binding)
    
  end
  
end

POKEMON_S::load
#POKEMON_S::load_pokemon