module POKEMON_S
  
  # Si la compilation est activée
  if (::CONFIG::BDD_COMPILATION == :always || (::CONFIG::BDD_COMPILATION == :auto && $DEBUG))
    
    Console.title("Démarrage de la compilation")
    
    Console.step("Initialisation de la BDD Pokémon...")
    
    # On supprime le premier élément qui est vide
    $data_enemies.shift 
    
    # On crée un nouveau tableau qui est une copie de $data_enemies
    _pokemon_arr = Array.new($data_enemies)
    _pokemon_arr.delete_if{|i| /^\\/.match(i.name)}
    
    Console.done
    
    # On range le tableau temporaire en fonction des id (maxsp)
    _pokemon_arr.sort! { |a,b| a.maxsp <=> b.maxsp}
    
    # On élimine les doublons
    _id = []
    _pokemon_arr.each_with_index do |pkmn, i|
      if pkmn.maxsp == 0
        Console.warning("#{pkmn.name.capitalize} a été mis de côté car il possède un id de 0.", false)
        _pokemon_arr.delete_at(i)
      elsif !_id.include? pkmn.maxsp
        _id.push(pkmn.maxsp)
      else
        Console.error("#{pkmn.name.capitalize} possède le même id qu'un autre Pokémon. Il n'a pas été copié.", false)
        _pokemon_arr.delete_at(i)
      end
    end
    
    # On vérifie que les id se suivent sans aucun trou
    _prev = 0
    _pokemon_arr.each do |i|
        if i.maxsp != _prev + 1
          Console.error("Les id dans la BDD ne se suivent pas: il manque l'id #{_prev+1}", false)
          raise
        end
        _prev += 1
    end
    
    Console.step("Génération de la BDD Pokémon...")
    Console.done
    
    Console.step("Vérification des types...")
    Console.done
    
    # On vérifie l'intégrité des types    
    TYPES.each do |t,v|
      Console.error("L'id du type #{t} est invalide.", false) unless v[:id].is_a? Fixnum
      Console.error("Le nom du type #{t} est invalide.", false) unless v[:name].is_a? String
      Console.error("Les données d'attaque du type #{t} sont invalides/incomplètes.", false) unless v[:attacking].size == TYPES.size
      v[:attacking].each do |a,b|
        Console.error("Des erreurs sont présentes dans les données d'attaque du type #{t} (par rapport au type #{a})", false) unless (b.is_a?(Fixnum) || b.is_a?(Float))
        Console.error("Les données d'attaque du type #{t} sont invalides: le type #{a} est inconnu", false) unless TYPES.keys.include? a
      end
    end
      
    Console.step("Vérification des formes...")
    Console.done
    
    # On vérifie l'intégrité des formes de Pokémon    
    CONFIG::SHAPES.each do |t,v|
      Console.error("L'id de la forme #{t} est invalide.", false) unless v[:id].is_a? Fixnum
      Console.error("L'icône de la forme #{t} est invalide.", false) unless v[:icon].is_a? String
    end
    
    Console.step("Vérification des couleurs...")
    Console.done
    
    # On vérifie l'intégrité des couleurs de Pokémon    
    CONFIG::COLORS.each do |t,v|
      Console.error("L'id de la couleur #{t} est invalide.", false) unless v[:id].is_a? Fixnum
      Console.error("Le nom de la couleur #{t} est invalide.", false) unless v[:name].is_a? String
    end
    
    Console.step("Génération du log de la compilation...")
    
    # On écrit un fichier de log
    file = File.open("CompilationLog.txt", "w")
    file.write("Liste des Pokémon exportés (Enemies)\n\n")
    _pokemon_arr.each do |i|
      file.write("#{i.maxsp}:#{i.name.capitalize}\n")
    end
    file.close
    
    Console.done
    
    Console.step("Écriture de la BDD Pokémon...")
    
    # On crée le tableau final
    $data_pokemon = [nil]
    
    # On remplit le tableau final
    _pokemon_arr.each do |i|
      # Création du Pokémon de base
      _pokemon = Pokemon.new(i.maxsp, i.name.capitalize)
      # Ajout des statistiques de base
      _pokemon.base_stats = {:pv => i.maxhp, 
                               :at => i.str, 
                               :df => i.dex, 
                               :s_at => i.int, 
                               :s_df => i.atk, 
                               :vt => i.agi}
      # Base d'expérience
      _pokemon.base_xp = i.exp
      # Rareté
      _pokemon.rareness = i.gold
      # Ajout des types
      u = 1
      while u < i.element_ranks.xsize
        if i.element_ranks[u] == 1
          if type_from_id(u) != false # Si le type existe
            _pokemon.type1 = type_from_id(u)
          end
        elsif i.element_ranks[u] == 2
          if type_from_id(u) != false # Si le type existe
            _pokemon.type2 = type_from_id(u)
          end
        end
        u += 1
      end #END while
      # Ajout des formes de corps
      u = 1
      while u < i.element_ranks.xsize
        if i.element_ranks[u] == 1
          if CONFIG::shape_from_id(u) != false # Si la forme existe
            _pokemon.shape = CONFIG::shape_from_id(u)
          end
        end
        u += 1
      end #END while
      # Ajout des couleurs
      u = 1
      while u < i.element_ranks.xsize
        if i.element_ranks[u] == 1
          if CONFIG::color_from_id(u) != false # Si la couleur existe
            _pokemon.color = CONFIG::color_from_id(u)
          end
        end
        u += 1
      end #END while
      u = {}
      # Ajout des dex régionaux
      CONFIG::DEX.each do |d,e|
        u[d] = e.index(_pokemon.id)
        u[d] += 1 if u[d] != nil
      end
      _pokemon.dex = u
      # On ajoute le Pokémon une fois terminé
      $data_pokemon.push(_pokemon)
    end #END each
    
    Console.var("$data_pokemon[1]", binding)
    
    # On écrit dans le fichier de données
    file = File.open("Data/Pokemon.rxdata", "wb")
    Marshal.dump($data_pokemon, file)
    file.close
    
    Console.done   
    
    Console.debug("#{$data_pokemon.size - 1} Pokémon ont été copiés.", false)
    
    Console.title("Fin de la compilation")
      
  end #END if

end #END module POKEMON_S