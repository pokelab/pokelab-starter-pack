def export_script
  current_dir = ""
  Dir.mkdir "MunchPack" unless FileTest.exist?("MunchPack")
  Dir.mkdir "MunchPack/pokelab" unless FileTest.exist?("MunchPack/pokelab")
  list = File.open("MunchPack/pokelab/!index.list","wb")
  _list = 0
  scripts = load_data("Data/Scripts.rxdata")
  scripts.each do |script|
    e = script[1]
    e.tr!('\\/?!.:*"<>|','')
    if e.split(//).first == '='
      e.tr!('= ','')
      current_dir = "#{e}/"
      list.write("#{e}\n")
      Console.log("'MunchPack/pokelab/#{e}'")
      _list.close if _list.is_a? File
      Dir.mkdir "MunchPack/pokelab/#{e}" unless FileTest.exist?("MunchPack/pokelab/#{e}")
      _list = File.open("MunchPack/pokelab/#{e}/!index.list","wb")
    elsif e != '' && e.split(//).first != '$'
      _list.write("#{e}\n")
      file = File.open("MunchPack/pokelab/#{current_dir}#{e}.rb","wb")
      file.write(Zlib::Inflate.inflate(script[2]))
      file.close
    end
  end
  list.close
  _list.close
end

begin
  export_script
end