#========================================================
# Errors - Krosk | Ŧž.A 
# -------------------------------------------------------
# Script de gestion des erreurs du type "No such file"
# Ces actions sont rendues non bloquantes et s'affichent
# dans la console dans l'éditeur
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code sans crédits tant que vous ne vous l'attribuez
# pas entièrement.
#========================================================

class << Bitmap
  
  alias_method :_new, :new unless method_defined?(:_new)
  def new(*a)
    _new(*a)
  rescue
    Console.warning("No such file or directory: \"#{a[0]}\"", true, caller.first.split(":")) if a.size == 1
    _new(32, 32)
  end
  
end
 
module Audio
  
  class << self
    alias_method :_bgm_play, :bgm_play unless method_defined?(:_bgm_play)
    alias_method :_bgs_play, :bgs_play unless method_defined?(:_bgs_play)
    alias_method :_me_play, :me_play unless method_defined?(:_me_play)
    alias_method :_se_play, :se_play unless method_defined?(:_se_play)
  end
  
  def self.bgm_play(filename, volume = 100, pitch = 100)
    self._bgm_play(filename, volume, pitch)
  rescue
    Console.warning("No such file or directory: \"#{filename}\"", true, caller.first.split(":"))
  end
  
  def self.bgs_play(filename, volume = 100, pitch = 100)
    self._bgs_play(filename, volume, pitch)
  rescue
    Console.warning("No such file or directory: \"#{filename}\"", true, caller.first.split(":"))
  end
  
  def self.me_play(filename, volume = 100, pitch = 100)
    self._me_play(filename, volume, pitch)
  rescue
    Console.warning("No such file or directory: \"#{filename}\"", true, caller.first.split(":"))
  end
  
  def self.se_play(filename, volume = 100, pitch = 100)
    self._se_play(filename, volume, pitch)
  rescue
    Console.warning("No such file or directory: \"#{filename}\"", true, caller.first.split(":"))
  end
  
end