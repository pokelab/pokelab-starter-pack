#========================================================
# Console - Ŧž.A
# -------------------------------------------------------
# Utilisation libre sous crédits.
# Modifications autorisées, partage à l'identique,
# pas d'utilisation commerciale.
# -------------------------------------------------------
# IL EST FORTEMENT DÉCONSEILLÉ DE TOUCHER À CE SCRIPT À
# MOINS QUE VOUS NE SACHIEZ EXACTEMENT CE QUE VOUS FAÎTES!
# -------------------------------------------------------
# Utilisation:
# Console.init -> Démarre la console
# Console.debug(msg) -> Affiche un message en mode debug
# Console.log(msg) -> Affiche un message simple
# Console.warning(msg) -> Affiche un avertissement
# Console.error(msg) -> Affiche une erreur
# Console.step(msg) -> Affiche une tâche en cours
# Console.done(msg) -> Termine la tâche en cours
#========================================================

# Lorem ipsum de test
$lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec aliquet et sem eu mattis. Fusce bibendum nisl risus, quis malesuada ligula sagittis a. Nam finibus placerat fermentum. In sed tortor a lectus luctus varius a nec magna. Integer porta, nibh in imperdiet interdum, nunc nisi maximus massa, ut pellentesque urna ante sed massa. Quisque malesuada orci nec neque tempor, sed."

class Console
  
  #----------------------------------------------
  # CONFIGURATION
  #----------------------------------------------  
  
  # Tenter de modifier la police de la console pour Lucida Console
  # au démarrage de celle-ci.
  # Il est conseillé de laisser à true sauf si la commande ne
  # fonctionne pas.
  # En cas de problème, passer à false et faire un clic-droit sur l'icône
  # de la console, Propriétés > Police > Choisir Lucida Console dans la
  # liste disponible.
  CHANGE_FONT = true
  
  # Si true, l'encodage seront Unicode. Sinon, ce sera ASCII.
  # Il est recommandé de laisser à true, sinon vous perdrez les accents et
  # les caractères spéciaux.
  # Il peut être utile de passer à false sur de vieilles versions de Windows
  # mais n'y touchez pas sans bonne raison.
  USE_UNICODE = true
  
  # Désactive l'affichage en popup des Warnings en dehors de l'éditeur.
  # Déconseillé durant le développement. Éventuellement à activer
  # lors de la sortie du jeu, mais certaines informations pour des 
  # corrections de bugs ne seront plus disponibles.
  NO_WARNINGS = false
  
  # Si true, la console sera également affichée en dehors de l'éditeur.
  # Cette option est déconseillée si vous distribuez votre jeu, mais
  # peut être utile si vous organisez une phase de test (type alpha).
  FORCE_DEBUG = false
  
  #----------------------------------------------
  # CONSTRUCTEUR
  # Lance la console si le jeu est lancé depuis 
  # l'éditeur
  #----------------------------------------------
  def self.init
    
    # Si on est dans l'éditeur ou que la console doit se lancer
    if $DEBUG || FORCE_DEBUG
      
      # On crée la console
      Win32API.new('kernel32', 'AllocConsole', 'V', 'L').call
      $stdout.reopen('CONOUT$')
      # On affiche le titre de la console
      Win32API.new('kernel32','SetConsoleTitle','P','S').call("RGSS Console")
      # On récupère l'handle
      ghs = Win32API.new('kernel32','GetStdHandle','l','l')
      @@out = ghs.call(-11)
      # On récupère le WinAPI pour la couleur du texte
      @@scta = Win32API.new('kernel32','SetConsoleTextAttribute','ll','l')
      # On fixe l'encodage en UTF-8
      Win32API.new('kernel32', 'SetConsoleOutputCP', 'I','I').call(65001)
      # On récupère le WinAPI pour l'affichage du texte unicode
      @@write = Win32API.new('kernel32','WriteConsoleA', 'IPIPI','I')
      # On tente de changer la police pour mettre Lucida Console
      Win32API.new('kernel32','SetConsoleFont','PI','').call(@out, 12) if CHANGE_FONT
      # On affiche le bandeau
      puts("")
      color(248)
      unicode('-'*80)
      unicode(' '*80)
      unicode("                         Console - Pokélab Starter Pack                         ")
      color(247)
      unicode("                                   Mode Debug                                   ")
      color(248)
      unicode(' '*80)
      unicode('-'*80)
      color(0x000f)
      unicode("                                                                  Ŧž.A - v 0.0.1")
      puts("")
      # On affiche la note pour l'encodage
      color(0x0008)
      puts('NOTE: Si des ? ou des [] sont visibles, il faut changer la police de la console',
           '      Pour ce faire, clic droit sur l\'icône de la console',
           "      Puis Propriétés > Police > Choisir Lucida Console","")
      puts('Si à chaque démarrage la police est incorrecte, allez dans le script Console',
           'et réglez CHANGE_FONT pour false','')
           
         end #END if $DEBUG || FORCE_DEBUG
         
  end #END def
  
  #----------------------------------------------
  # Liste de caractères
  #----------------------------------------------
  
  # Liste des mots-clés disponibles en Ruby
  KEYWORDS = ["BEGIN", "END", "__ENCODING__", "__END__", "__FILE__", "__LINE__", "alias", "and", "begin", "break", "case", "class", "def", "defined?", "do", "else", "elsif", "end", "ensure", "false", "for", "if", "in", "module", "next", "nil", "not", "or", "redo", "rescue", "retry", "return", "self", "super", "then", "true", "undef", "unless", "until", "when", "while", "yield"]
  # Liste des nombres
  NUMBERS = ["0","1","2","3","4","5","6","7","8","9"]
  # Liste des opérateurs disponibles en Ruby
  OPERATORS = ["[","]","{","}","+","-","*","/","?",":","%","(",")","=",";","|", "&","<",">","!"]
  
  # Liste des caractères ANSI
  ANSI = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
          "0","1","2","3","4","5","6","7","8","9",
          "!",'"',"#","$","%","&","'","(",")","*","+",",","-",".","/",
          ":",";","<","=",">","?","@",
          "[","]","^","_","`","{","|","}","~"," "]
  # Liste des caractères spéciaux qui ne valent qu'une unité
  NO = ["\n"]
  
  #----------------------------------------------
  # Affiche un texte unicode
  #   - msg: message à afficher
  #----------------------------------------------
  def self.unicode(msg)
    return if !$DEBUG && !FORCE_DEBUG
    i = 0 # Compteur pour la taille du texte
    msg.split("").each do |c| # Pour chaque caractère
      if (ANSI.include?(c.downcase) || NO.include?(c))
        i += 1 # Si c'est un caractère ASCII ou échappé...
      else
        i += 2 # Si c'est un caractère Unicode
      end
    end #END each
    # On appelle le WinAPI pour afficher
    @@write.call(@@out, msg, i, 0, 0)
  end #END def
  
  #----------------------------------------------
  # Affiche un caractère unicode
  #   - c: caractère à afficher
  #----------------------------------------------
  def self.unicode_c(c)
    return if !$DEBUG && !FORCE_DEBUG
    if (ANSI.include?(c.downcase) || NO.include?(c))
      @@write.call(@@out, c, 1, 0, 0) # Si c'est un caractère ASCII ou échappé...
    else
      @@write.call(@@out, c, 2, 0, 0) # Si c'est un caractère Unicode
    end
  end #END def
  
  #----------------------------------------------
  # Modifie la couleur du texte et du fond
  #   - c: couleur du texte
  #   - b: couleur du fond
  #----------------------------------------------
  def self.color(c=0x0000,b=0x0000)
    @@scta.call(@@out,(c)|(b)) if $DEBUG || FORCE_DEBUG
  end #END def
  
  #----------------------------------------------
  # Affiche un message basique dans la console 
  # à partir de l'éditeur ou dans une popup en
  # dehors.
  #   - msg: Message à afficher
  #----------------------------------------------
  def self.log(msg)
    return if NO_WARNINGS
    ($DEBUG || FORCE_DEBUG) ? puts(msg) : p(msg)
  end
  
  #----------------------------------------------
  # Affiche un message d'erreur dans la console 
  # à partir de l'éditeur ou dans une popup en
  # dehors.
  #   - msg: Message à afficher
  #   - line: Affiche la ligne où se situe l'erreur
  #   - custom_caller: remplace le caller par défaut par
  #                    celui d'un autre contexte
  #                    (utile lorsque l'erreur est lancée
  #                    depuis une classe et où l'important
  #                    est de retrouver l'instance)
  #----------------------------------------------
  def self.error(msg, line = true, custom_caller = nil)
    color(0x000c)
    if $DEBUG || FORCE_DEBUG # Si nous sommes dans l'éditeur
      printf("ERREUR: #{msg}")
      
      # Affichage de la ligne
      color(0x000d)
      _caller = caller.first.split(":")
      if _caller[0].sub('Section', '').to_i != 0 && line # Si le log ne provient pas d'un event
        script = load_data("Data/Scripts.rxdata")
        if custom_caller == nil
          printf(" (#{script[_caller[0].sub('Section', '').to_i][1]} : ligne #{_caller[1]})")
        else
          printf(" (#{script[custom_caller[0].sub('Section', '').to_i][1]} : ligne #{custom_caller[1]})")
        end
      end
      
      # On termine l'affichage par le saut de ligne et la couleur blanche
      printf("\n")
      color(0x000f)
    else
      p("ERREUR: #{msg}")
    end
  end
  
  #----------------------------------------------
  # Affiche un message d'avertissement dans la console 
  # à partir de l'éditeur ou dans une popup en
  # dehors.
  #   - msg: Message à afficher
  #   - line: Affiche la ligne où se situe le warning
  #   - custom_caller: remplace le caller par défaut par
  #                    celui d'un autre contexte
  #                    (utile lorsque l'erreur est lancée
  #                    depuis une classe et où l'important
  #                    est de retrouver l'instance)
  #----------------------------------------------
  def self.warning(msg, line = true, custom_caller = nil)
    
    return if NO_WARNINGS
    
    color(0x000e)
    
    if $DEBUG || FORCE_DEBUG # Si nous sommes dans l'éditeur
      
      printf("WARNING: #{msg}")
      
      # Affichage de la ligne
      color(0x000d)
      _caller = caller.first.split(":")
      if _caller[0].sub('Section', '').to_i != 0 && line # Si le log ne provient pas d'un event
        script = load_data("Data/Scripts.rxdata")
        if custom_caller == nil
          printf(" (#{script[_caller[0].sub('Section', '').to_i][1]} : ligne #{_caller[1]})")
        else
          printf(" (#{script[custom_caller[0].sub('Section', '').to_i][1]} : ligne #{custom_caller[1]})")
        end
      end
      
      # On termine l'affichage par le saut de ligne et la couleur blanche
      printf("\n")
      color(0x000f)
      
    else # En jeu, on affiche simplement une popup
      
      p("WARNING: #{msg}")
      
    end
  end
  
  #----------------------------------------------
  # Affiche un message seulement en phase de test
  # Coloration syntaxique VERSION AUTOMATIQUE
  #   - msg: Message à afficher
  #   - line: affiche la ligne et le script
  #   - custom_caller: remplace le caller par défaut par
  #                    celui d'un autre contexte
  #                    (utile lorsque l'erreur est lancée
  #                    depuis une classe et où l'important
  #                    est de retrouver l'instance)
  #----------------------------------------------
  # UTILISER au lieu de debug_a et debug_u
  #----------------------------------------------
  def self.debug(msg, line = true, custom_caller = nil)
    return if !$DEBUG && !FORCE_DEBUG
    # Correction du caller
    custom_caller = caller.first.split(":") if custom_caller == nil
    # On redirige vers la bonne fonction suivant USE_UNICODE
    (USE_UNICODE) ? debug_u(msg.to_s, line, custom_caller) : debug_a(msg.to_s, line, custom_caller)
  end #END def
  
  #----------------------------------------------
  # Affiche un message seulement en phase de test
  # Coloration syntaxique VERSION ANSI
  #   - msg: Message à afficher
  #   - line: affiche la ligne et le script
  #   - custom_caller: remplace le caller par défaut par
  #                    celui d'un autre contexte
  #                    (utile lorsque l'erreur est lancée
  #                    depuis une classe et où l'important
  #                    est de retrouver l'instance)
  #----------------------------------------------
  # NE PAS UTILISER -> Préférer self.debug
  #----------------------------------------------
  def self.debug_a(msg,line = true,custom_caller = nil)
    
    # Variables de fonctionnement pour la coloration syntaxique
    guillemets = nil
    numbers = false
    
    # On s'arrête si jamais on n'est pas dans l'éditeur
    return if !$DEBUG && !FORCE_DEBUG
    
    # On découpe le message par mots
    msg.split(" ").each do |w|
      
      # Si c'est un mot clé, on ajoute la coloration syntaxique
      if KEYWORDS.include? w
        color(0x000b) # Bleu
        printf("#{w} ")
      else
        # On découpe chaque caractère
        w.split("").each do |c|
          color(0x000f) # Blanc
          # Si nous ne sommes pas déjà dans une chaîne de caractères
          if guillemets == nil
            if c == "'" || c == '"' || c == '`'
              guillemets = c
              color(0x000c) # Rouge
              printf(c)
            else
              # Si c'est un chiffre
              if NUMBERS.include? c
                color(0x000e) # Jaune
                numbers = true
              elsif c == "." && numbers
                color(0x000e) # Jaune
              else
                numbers = false
              end
              # Si c'est un opération
              if OPERATORS.include? c
                color(0x000a) # Vert
              end
              printf(c)
            end #END if c == "'" || c == '"' || c == '`'
          else
            color(0x000c) # Rouge
            # Si on ferme les guillemets, on désactive la coloration
            if c == guillemets || (c == "'" && guillemets == "`")
              guillemets = nil
            end
            printf(c)
          end #END if guillemets == false
        end
        printf(" ")
      end
    end
    # Affichage de la ligne
    color(0x000d)
    _caller = (custom_caller == nil) ? caller.first.split(":") : custom_caller
    if _caller[0].sub('Section', '').to_i != 0 && line # Si le log ne provient pas d'un event
      script = load_data("Data/Scripts.rxdata")
      printf(" (#{script[_caller[0].sub('Section', '').to_i][1]} : ligne #{_caller[1]})")
    end
      
    # On termine l'affichage par le saut de ligne et la couleur blanche
    printf("\n")
    color(0x000f)
  end #END def
  
  #----------------------------------------------
  # Affiche un message seulement en phase de test
  # Coloration syntaxique VERSION UNICODE
  #   - msg: Message à afficher
  #   - line: affiche la ligne et le script
  #   - custom_caller: remplace le caller par défaut par
  #                    celui d'un autre contexte
  #                    (utile lorsque l'erreur est lancée
  #                    depuis une classe et où l'important
  #                    est de retrouver l'instance)
  #----------------------------------------------
  # NE PAS UTILISER -> Préférer self.debug
  #----------------------------------------------
  def self.debug_u(msg,line = true,custom_caller = nil)
    
    # Variables de fonctionnement pour la coloration syntaxique
    guillemets = nil
    numbers = false
    
    # On s'arrête si jamais on n'est pas dans l'éditeur
    return if !$DEBUG && !FORCE_DEBUG
    
    # On découpe le message par mots
    msg.split(" ").each do |w|
      
      # Si c'est un mot clé, on ajoute la coloration syntaxique
      if KEYWORDS.include? w
        color(0x000b) # Bleu
        unicode("#{w} ")
      else
        # On découpe chaque caractère
        w.split("").each do |c|
          color(0x000f) # Blanc
          # Si nous ne sommes pas déjà dans une chaîne de caractères
          if guillemets == nil
            if c == "'" || c == '"' || c == '`'
              guillemets = c
              color(0x000c) # Rouge
              unicode_c(c)
            else
              # Si c'est un chiffre
              if NUMBERS.include? c
                color(0x000e) # Jaune
                numbers = true
              elsif c == "." && numbers
                color(0x000e) # Jaune
              else
                numbers = false
              end
              # Si c'est un opération
              if OPERATORS.include? c
                color(0x000a) # Vert
              end
              unicode_c(c)
            end #END if c == "'" || c == '"' || c == '`'
          else
            color(0x000c) # Rouge
            # Si on ferme les guillemets, on désactive la coloration
            if c == guillemets || (c == "'" && guillemets == "`")
              guillemets = nil
            end
            unicode_c(c)
          end #END if guillemets == false
        end
        unicode_c(" ")
      end
    end
    # Affichage de la ligne
    color(0x000d)
    _caller = (custom_caller == nil) ? caller.first.split(":") : custom_caller
    if _caller[0].sub('Section', '').to_i != 0 && line # Si le log ne provient pas d'un event
      script = load_data("Data/Scripts.rxdata")
      unicode(" (#{script[_caller[0].sub('Section', '').to_i][1]} : ligne #{_caller[1]})")
    end
      
    # On termine l'affichage par le saut de ligne et la couleur blanche
    unicode_c("\n")
    color(0x000f)
  end #END def
  
  #----------------------------------------------
  # Affichage d'une tâche en cours
  #   - msg: Le message à afficher
  #----------------------------------------------
  def self.step(msg)
    return if !$DEBUG && !FORCE_DEBUG
    color(0x000f)
    unicode(msg)
  end #END def
  
  #----------------------------------------------
  # Marque la tâche en cours comme terminée
  #----------------------------------------------
  def self.done
    return if !$DEBUG && !FORCE_DEBUG
    color(0x000a)
    printf("Done\n")
    color(0x000f)
  end #END def
  
  #----------------------------------------------
  # Affiche le détail d'une variable particulière
  #   - v: le nom de la variable (str)
  #   - b: binding
  # EXEMPLE: Console.var("variable", binding)
  #----------------------------------------------
  def self.var(v,b)
    
    # On s'arrête si jamais on n'est pas dans l'éditeur
    return if !$DEBUG && !FORCE_DEBUG
    
    if !v.is_a? String
      error("#{v} n'est pas une chaîne de caractères", true, caller.first.split(":"))
      return
    elsif !b.is_a? Binding
      error("#{v} n'est pas un binding", true, caller.first.split(":"))
      return
    end
    
    #On extrait le nom de la classe
    type = eval(v, b).class.name
    
    # On teste en fonction de la classe
    if type == "String"
      debug("Variable #{ v } (#{type}): #{v} = \"#{ eval(v, b)}\"", true, caller.first.split(":"))
    elsif type == "Float" || type == "Fixnum" || type == "TrueClass" || type == "FalseClass" || type == "NilClass"
      debug("Variable #{ v } (#{type}): #{v} = #{ eval(v, b)}", true, caller.first.split(":"))
    elsif type == "Array"
      debug("Variable #{ v } (#{type}): #{v} = #{ eval(v, b).inspect}", true, caller.first.split(":"))
    elsif type == "Hash"
      debug("Variable #{ v } (#{type}): #{v} = #{ eval(v, b).inspect}", true, caller.first.split(":"))
    elsif type == "Symbol"
      debug("Variable #{ v } (#{type}): #{v} = #{ eval(v, b).inspect}", true, caller.first.split(":"))
    else
      # Si la classe est inconnue, on génére un hash contenant toutes les variables d'instance
      r = Hash.new
      # On remplit le hash
      eval(v, b).instance_variables.each do |var|      # Pour chaque variable d'instance
        r[var] = eval(v, b).instance_variable_get(var) # On set la valeur
      end
      puts("")
      debug("Variable #{ v } (#{type}):", true, caller.first.split(":"))
      puts("")
      debug("BEGIN #{v}", false)
      debug("#{r.inspect}", false)
      debug("END #{v}", false)
      puts("")
    end
    
  end #END def
  
  #----------------------------------------------
  # Affiche un titre unicode
  #   - msg: Titre à afficher
  #----------------------------------------------
  def self.title(msg)
    
    # On s'arrête si jamais on n'est pas dans l'éditeur
    return if !$DEBUG && !FORCE_DEBUG
    
    e = "... @@@ #{msg} @@@ ..."
    unicode("\n#{e}#{'.'*(80-e.length)}\n\n")
  end #END def
  
end #END class Console

# Initialisation de la console.
Console.init
Console.step("Initialisation de la console...");Console.done