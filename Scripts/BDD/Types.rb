module POKEMON_S
  
  TYPES = {
    :normal => {
      :name => "Normal",
      :attacking => {
        :normal => 1, :fight => 1, :flying => 1, :poison => 1, :ground => 1, :rock => 0.5, :bug => 1, :ghost => 0, :steel => 0.5, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 1, :ice => 1, :dragon => 1, :dark => 1, :fairy => 1
      },
      :id => 1
    },
    :fight => {
      :name => "Combat",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 7
    },
    :flying => {
      :name => "Vol",
      :attacking => {
        :normal => 1, :fight => 2, :flying => 1, :poison => 1, :ground => 1, :rock => 0.5, :bug => 2, :ghost => 1, :steel => 0.5, :fire => 1, :water => 1, :grass => 2, :electric => 0.5, :psychic => 1, :ice => 1, :dragon => 1, :dark => 1, :fairy => 1
      },
      :id => 10
    },
    :poison => { #Pas fait
      :name => "Poison",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 8
    },
    :ground => { #Pas fait
      :name => "Sol",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 9
    },
    :rock => { #Pas fait
      :name => "Roche",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 13
    },
    :bug => { #Pas fait
      :name => "Insecte",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 12
    },
    :ghost => { #Pas fait
      :name => "Spectre",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 14
    },
    :steel => { #Pas fait
      :name => "Acier",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 16
    },
    :fire => { #Pas fait
      :name => "Feu",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 2
    },
    :water => { #Pas fait
      :name => "Eau",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 3
    },
    :grass => { #Pas fait
      :name => "Plante",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 5
    },
    :electric => { #Pas fait
      :name => "Électrique",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 4
    },
    :psychic => { #Pas fait
      :name => "Psy",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 11
    },
    :ice => { #Pas fait
      :name => "Glace",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 6
    },
    :dragon => { #Pas fait
      :name => "Dragon",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 15
    },
    :dark => { #Pas fait
      :name => "Ténèbres",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 17
    },
    :fairy => { #Pas fait
      :name => "Fée",
      :attacking => {
        :normal => 2, :fight => 1, :flying => 0.5, :poison => 0.5, :ground => 1, :rock => 2, :bug => 0.5, :ghost => 0, :steel => 2, :fire => 1, :water => 1, :grass => 1, :electric => 1, :psychic => 0.5, :ice => 2, :dragon => 1, :dark => 2, :fairy => 0.5
      },
      :id => 18
    }
  }
  
  def self.type_from_id(id)
    TYPES.each do |t,v|
      return t if v[:id] == id
    end
    return false
  end
  
end