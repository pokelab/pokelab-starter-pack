module POKEMON_S
  
  module CONFIG
    
    SHAPES = {
      :head_only => { # Tête seulement
        :icon => "shape_01.png",
        :id => 51
      },
      :serpentine_body => { # Corps serpentin FAIT
        :icon => "shape_02.png",
        :id => 52
      },
      :fins => { # Nageoires FAIT
        :icon => "shape_03.png",
        :id => 53
      },
      :head_arms => { # Tête et bras FAIT
        :icon => "shape_04.png",
        :id => 54
      },
      :head_base => { # Tête et corps FAIT
        :icon => "shape_05.png",
        :id => 55
      },
      :bipedal_tailed => { # Bipède avec queue FAIT
        :icon => "shape_06.png",
        :id => 56
      },
      :head_legs => { # Tête avec jambes FAIT
        :icon => "shape_07.png",
        :id => 57
      },
      :quadruped => { # Quadrupèdes FAIT
        :icon => "shape_08.png",
        :id => 58
      },
      :single_wings => { # Une paire d'ailes FAIT
        :icon => "shape_09.png",
        :id => 59
      },
      :tentacles => { # Tentacules et pattes multiples FAIT
        :icon => "shape_10.png",
        :id => 60
      },
      :multiple_bodies => { # Plusieurs corps FAIT
        :icon => "shape_11.png",
        :id => 61
      },
      :bipedal_tailless => { # Bipède sans queue FAIT
        :icon => "shape_12.png",
        :id => 62
      },
      :more_wings => { # Plusieurs paires d'ailes FAIT
        :icon => "shape_13.png",
        :id => 63
      },
      :insectoid_body => { # Corps insectoïde
        :icon => "shape_14.png",
        :id => 64
      }
    }
    
    def self.shape_from_id(id)
      CONFIG::SHAPES.each do |t,v|
        return t if v[:id] == id
      end
      return false
    end
    
  end
  
end