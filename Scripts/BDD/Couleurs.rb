module POKEMON_S
  
  module CONFIG
    
    COLORS = {
      :white => { # FAIT
        :name => "Blanc",
        :id => 76
      },
      :blue => { # FAIT
        :name => "Bleu",
        :id => 77
      },
      :gray => { # FAIT
        :name => "Gris",
        :id => 78
      },
      :yellow => { # FAIT
        :name => "Jaune",
        :id => 79
      },
      :brown => { # FAIT
        :name => "Brun",
        :id => 80
      },
      :black => { # FAIT
        :name => "Noir",
        :id => 81
      },
      :rose => { # FAIT
        :name => "Rose",
        :id => 82
      },
      :red => { # FAIT
        :name => "Rouge",
        :id => 83
      },
      :green => { # FAIT
        :name => "Vert",
        :id => 84
      },
      :purple => { # FAIT
        :name => "Violet",
        :id => 85
      }
    }
    
    def self.color_from_id(id)
      CONFIG::COLORS.each do |t,v|
        return t if v[:id] == id
      end
      return false
    end
    
  end
  
end