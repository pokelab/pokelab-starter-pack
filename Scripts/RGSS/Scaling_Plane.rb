#========================================================
# Plane adaptatif - Ŧž.A
# Modification automatique en fonction du zoom
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

class Scaling_Plane
  
  attr_reader :bitmap
  
  #----------------------------------------------
  # Constructeur
  # Scaling_Plane.new(v)
  #   - v: viewport (écran du jeu si non spécifié)
  #----------------------------------------------
  def initialize(v=nil)
    @sprite = Scaling_Sprite.new(v)
    @bitmap = nil
  end #END def
    
  #----------------------------------------------
  # Libère le sprite
  #----------------------------------------------
  def dispose
    @sprite.bitmap.dispose if !bitmap_disposed?
    @sprite.dispose if !disposed?
    return nil
  end #END def

  #----------------------------------------------
  # Le bitmap est-il libéré ?
  #----------------------------------------------
  def bitmap_disposed?
    disposed? || @sprite.bitmap.nil? || @sprite.bitmap.disposed?
  end #END def
  
  #----------------------------------------------
  # Le plane est-il libéré ?
  #----------------------------------------------
  def disposed?
    @sprite.nil? || @sprite.disposed?
  end #END def
  
  #----------------------------------------------
  # Modifie la valeur ox
  #----------------------------------------------
  def ox=(v)
    if disposed?
      Console.error("Le Plane a déjà été libéré.");return
    elsif bitmap_disposed?
      Console.error("Le bitmap du Plane a déjà été libéré.");return
    end       
    @sprite.ox = (v % (@bitmap.nil? ? 1 : @bitmap.width))
  end #END def
  
  #----------------------------------------------
  # Modifie la valeur oy
  #----------------------------------------------
  def oy=(v)
    if disposed?
      Console.error("Le Plane a déjà été libéré.");return
    elsif bitmap_disposed?
      Console.error("Le bitmap du Plane a déjà été libéré.");return
    end       
    @sprite.oy = (v % (@bitmap.nil? ? 1 : @bitmap.height))
  end #END def
  
  def ox
    return @sprite.ox
  end #END def
  
  def oy
    return @sprite.oy
  end #END def
  
  def z
    return @sprite.z
  end #END def
  
  def z=(v)
    @sprite.z = v
  end #END def
  
  def tone
    return @sprite.tone
  end #END def
  
  def tone=(v)
    @sprite.tone = v
  end #END def
  
  #----------------------------------------------
  # Modifie le viewport
  #   - v: viewport
  #----------------------------------------------
  def viewport=(v)
    if disposed?
      Console.error("Le Plane a déjà été libéré.");return
    end
    v.rect = Rect.new(v.rect.x*$ZOOM,v.rect.y*$ZOOM,v.rect.width*$ZOOM,v.rect.height*$ZOOM) if v!=nil
    rect = (v == nil) ? Rect.new(0, 0, Graphics.width, Graphics.height) : v.rect
    b = rect != vrect
    ret = @sprite.viewport = v
    self.bitmap = @bitmap if b
    return ret
  end #END def

  #----------------------------------------------
  # Modifie le bitmap
  #   - bmp: bitmap à appliquer
  #----------------------------------------------
  def bitmap=(bmp)
    if disposed?
      Console.error("Le Plane a déjà été libéré.");return
    end
    if bmp.nil?
      @sprite.bitmap = nil
      return @bitmap = nil
    end
       
    w, h = vrect.width, vrect.height
       
    nw = bmp.width <= 100 ? 2 : 3
    nh = bmp.height <= 100 ? 2 : 3
       
    dx = [(w / bmp.width).ceil, 1].max * nw
    dy = [(h / bmp.height).ceil, 1].max * nh
     
    bw = dx * bmp.width
    bh = dy * bmp.height
     
    @bitmap = bmp
    @sprite.bitmap = Bitmap.new(bw, bh)
         
    dx.times do |x|
      dy.times do |y|
        @sprite.bitmap.blt(x * bmp.width, y * bmp.height, @bitmap, @bitmap.rect)
      end
    end #END times
    
  end #END def
  
  #----------------------------------------------
  # Fonctions privées
  #----------------------------------------------
  private
  def vrect
    @sprite.viewport.nil? ? Rect.new(0, 0, Graphics.width, Graphics.height) : @sprite.viewport.rect
  end #END def
  
end #END class