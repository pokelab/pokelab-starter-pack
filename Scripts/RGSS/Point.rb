class Point
  
  attr_accessor :x
  attr_accessor :y
  
  def initialize(x,y)
    @x = x
    @y = y
  end
  
  def set(x,y)
    @x = x
    @y = y
    return self
  end
  
  def translate(x,y)
    @x += x
    @y += y
  end
  
end