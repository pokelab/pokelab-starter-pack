#========================================================
# Module Input - Ŧž.A
# Gestion du clavier et de la souris
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module Input
  
  #----------------------------------------------
  # Ne pas toucher, initialisation du module
  #----------------------------------------------
  
  # On récupère la fenêtre
  @@handle = Win32API.new("user32", "FindWindow", "PP", "I") .call("RGSS Player", 0) 
  # État des touches et de la souris
  @@getKey = Win32API.new("user32", "GetKeyboardState", "P", "I")
  # Langue du clavier
  @@keyLang = Win32API.new('user32', 'GetKeyboardLayout','L', 'L').call(0)
  # Map des Keycodes
  @@mapKey = Win32API.new('user32', 'MapVirtualKeyEx', 'IIL', 'I')
  # Conversion Unicode
  @@unicode = Win32API.new('user32', 'ToUnicodeEx', 'LLPPILL', 'L')
  # Position de la souris
  @@mousePos = Win32API.new("user32", "GetCursorPos", "P", "I") 
  # Conversion par rapport à la fenêtre
  @@clientPos = Win32API.new("user32", "ScreenToClient", "IP", "I") 
  
  #----------------------------------------------
  # Affichage du curseur personnalisé
  #----------------------------------------------
  
  # Init
  @@cursor = Sprite.new
  @@cursor.bitmap = RPG::Cache.windowskin("mouse.png")
  @@cursor.z = 1_000_000_000
  # On cache le curseur de Windows
  Win32API.new("user32", "ShowCursor", "I", "I").call(0)
  # Zoom
  @@cursor.zoom_x = $ZOOM
  @@cursor.zoom_y = $ZOOM
  # Position
  @@cursor_point = Point.new(0,0)
  
  #----------------------------------------------
  # Variables de fonctionnement
  #----------------------------------------------
  
  # Maximum de touches à tester
  NUM_KEYS = 256
  
  # Récupération clavier
  @@cur = "\0"*NUM_KEYS
  # Récupération souris
  @@mse = "\0"*8
  # État du clavier
  @@arr = Array.new(NUM_KEYS,0)
  
  #----------------------------------------------
  # Met à jour l'entrée clavier/souris
  #----------------------------------------------
  # !!! Il est conseillé d'en limiter au !!!
  # !!! maximum l'usage                  !!!
  #----------------------------------------------
  #   - list: tableau contenant les touches à tester
  #           y mettre le moins de touche possible
  #           afin de gagner en performance
  #----------------------------------------------
  def self.update(list=0..(NUM_KEYS-1))
    
    @@getKey.call(@@cur)
    list.each do |key|
      @@arr[key] = (@@cur[key][7]==1) ? @@arr[key] + 1 : (@@arr[key]>0) ? -1 : 0
    end
    
  end #END def
  
  #----------------------------------------------
  # Met à jour la position de la souris
  #----------------------------------------------
  def self.mouse_update
    @@mousePos.call(@@mse);@@clientPos.call(@@handle,@@mse)
    @@cursor.x, @@cursor.y = @@mse.unpack("ii")
  end #END def
  
  #----------------------------------------------
  # Met à jour le zoom de la souris
  #----------------------------------------------
  def self.mouse_zoom_update
    @@cursor.zoom_x = $ZOOM
    @@cursor.zoom_y = $ZOOM
  end #END def
  
  #----------------------------------------------
  # Affiche ou masque le curseur de la souris
  #----------------------------------------------
  def self.mouse_visible=(i)
    @@cursor.visible = i
  end #END def
  
  # Si la touche est maintenue enfoncée
  def self.press?(key); @@arr[key] >= 1; end
  # Si la touche est juste pressée à l'instant
  def self.trigger?(key); @@arr[key] == 1; end
  # Si les touches sont juste pressées à l'instant
  def self.trigger_m?(keys); i=0;keys.each { |key| i += (@@arr[key] == 1) ? 1 : 0};return (i > 0) ? true : false; end
  # Si la touche est relâchée
  def self.release?(key); @@arr[key] == -1; end
  # Position x souris
  def self.mouse_x; @@cursor.x/$ZOOM; end
  # Position y souris
  def self.mouse_y; @@cursor.y/$ZOOM; end
  
  #----------------------------------------------
  # Récupère la touche pressée
  #   - keys: touches à tester
  #----------------------------------------------
  def self.get_input(keys=0..255)
    keys.each do |key|
      return key if (@@arr[key] == 1 || @@arr[key] > 20) && key != 16 && key != 18
    end
    return nil
  end #END def
  
  #----------------------------------------------
  # Récupère le caractère allant avec la touche indiquée
  #   - i: touche indiquée
  #----------------------------------------------
  def self.get_key(i)
    char = @@mapKey.call(i, 2, @@keyLang)
    return '' if char < 32 #Caractères non-imprimables
    scan = @@mapKey.call(i, 0, @@keyLang)
    result = "\0\0"
    length = @@unicode.call(i, scan, @@cur, result, 2, 0, @@keyLang)
    return (length == 0) ? '' : unicode_to_utf8(result)
  end #END def
  
  #----------------------------------------------
  # Conversion vers l'UTF-8
  #   - string: chaîne de caractères à convertir
  #----------------------------------------------
  def self.unicode_to_utf8(string)
    result = ''
    string.unpack('S*').each {|c|
        # Tous les caractères en dessous de l'index 0x0080 (128) valent 1
        if c < 0x0080
          result += c.chr
        # Tous ceux en dessous de 0x0800 (2048) valent 2
        elsif c < 0x0800
          result += (0xC0 | (c >> 6)).chr
          result += (0x80 | (c & 0x3F)).chr
        # Tous ceux au-dessus valent 3
        else
          result += (0xE0 | (c >> 12)).chr
          result += (0x80 | ((c >> 12) & 0x3F)).chr
          result += (0x80 | (c & 0x3F)).chr
        end}
    return result
  end #END def
  
  #----------------------------------------------
  # Attendre l'appui d'un touche
  #    - i: touche attendue
  #----------------------------------------------
  def self.wait_for_key(i)
    loop do 
      Graphics.update
      Input.update([i])
      break if Input.trigger?(i)
    end
  end #END def
  
  #----------------------------------------------
  # Attendre l'appui d'une des touches
  #   - i: liste des touches possibles
  #----------------------------------------------
  def self.wait_for_keys(i)
    loop do
      Graphics.update
      Input.update(i)
      break if Input.trigger_m?(i)
    end
  end #END def
  
end #END module Input

begin
  loop do
    Graphics.update
    Input.update([0x01,0x42])
    Input.mouse_update
    Console.log(Input.mouse_x) if(Input.trigger?(0x01))
    break if(Input.trigger?(0x42))
  end    
  
end
    