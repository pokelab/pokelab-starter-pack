#========================================================
# Sprite adaptatif - Ŧž.A
# Modification automatique en fonction du zoom
# -------------------------------------------------------
# Utilisation libre sous crédits.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

class Scaling_Sprite
  
  # Attributs d'animation
  attr_accessor :_a
  attr_accessor :sprite
  
  #----------------------------------------------
  # Constructeur
  # Scaling_Sprite.new(v)
  #   - v: viewport (écran du jeu si non spécifié)
  #----------------------------------------------
  def initialize(v = nil)
    
    # On applique les effets du zoom sur le viewport
    v.rect = Rect.new(v.rect.x*$ZOOM,v.rect.y*$ZOOM,v.rect.width*$ZOOM,v.rect.height*$ZOOM) if v!=nil
    
    # Création du sprite et application du zoom sur celui-ci
    @sprite = Sprite.new(v)
    @sprite.zoom_x = $ZOOM
    @sprite.zoom_y = $ZOOM
    
    # On initialise les données d'animation
    @_a = {}
    
  end #END def
  
  #----------------------------------------------
  # On force la mise à jour du zoom.
  # Peu utile en temps normal
  #----------------------------------------------
  def update_zoom
    @sprite.zoom_x = $ZOOM
    @sprite.zoom_y = $ZOOM
  end #END def
  
  #----------------------------------------------
  # On force un zoom particulier
  #   - z: zoom (1 = 100%)
  #----------------------------------------------
  def force_zoom(z)
    @sprite.zoom_x = z
    @sprite.zoom_y = z
  end #END def
  
  #----------------------------------------------
  # On déplace le sprite sur l'axe x
  #   - x: déplacement en px pour $ZOOM = 1
  #----------------------------------------------
  def move_x(x=1)
    @sprite.x += x
    Graphics.update
  end #END def
  
  #----------------------------------------------
  # On déplace le sprite sur l'axe y
  #   - y: déplacement en px pour $ZOOM = 1
  #----------------------------------------------
  def move_y(y=1)
    @sprite.y += y
    Graphics.update
  end #END def
  
  #----------------------------------------------
  # On applique le zoom automatiquement lorsque 
  # la position x est modifiée
  #----------------------------------------------
  def x=(i)
    @sprite.x = i * $ZOOM
  end #END def
  
  #----------------------------------------------
  # On applique le zoom automatiquement lorsque 
  # la position y est modifiée
  #----------------------------------------------
  def y=(i)
    @sprite.y = i * $ZOOM
  end #END def
  
  def z=(e)
    @sprite.z = e
  end
  
  def src_rect=(e)
    @sprite.src_rect = e
  end
  
  def src_rect
    @sprite.src_rect
  end
  
  #----------------------------------------------
  # On récupère les méthodes qui ne sont pas 
  # disponibles et on tente de les renvoyer
  # au sprite attaché à la classe.
  #----------------------------------------------
  def method_missing(method_symbol, *args, &block)
    
    return @sprite.send(method_symbol, *args, &block) if @sprite.respond_to?(method_symbol)
    super(method_symbol, *args, &block)
    
  end #END def
  
  # time -> _i
  # begin -> self.x (bx)
  # change -> x - self.x (cx)
  # duration -> d
  #----------------------------------------------
  # Bouger un sprite jusqu'à un point
  #   - x: position x finale
  #   - y: position y finale
  #   - i: itérateur
  #   - d: durée en frames
  #   - easing: symbol de la fonction d'easing
  #----------------------------------------------
  def move_to(x,y,i,d=60,easing=:linear)
    return if i > d
    @_a[:bx] = @sprite.x                # Position initiale
    @_a[:cx] = x * $ZOOM - @sprite.x    # Changement (final - initial)
    @_a[:by] = @sprite.y
    @_a[:cy] = y * $ZOOM - @sprite.y
    b = @_a[:bx]
    c = @_a[:cx]
    @sprite.x = UI::Easing.apply(i,b,c,d,easing)
    b = @_a[:by]
    c = @_a[:cy]
    @sprite.y = UI::Easing.apply(i,b,c,d,easing)
  end #END def
  
end #END class