#========================================================
# Module Graphics - Ŧž.A
# Gestion du zoom, de la redimension de l'écran et autres
# fonctions graphiques.
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module Graphics
  
  # Ne pas toucher, initialisation du module
  find=Win32API.new("user32","FindWindow","pp","l")
  @@pos=Win32API.new("user32","SetWindowPos","lllllll","l")
  @@stext=Win32API.new("user32","SetWindowText","pp","")
  @@gtext=Win32API.new("user32","GetWindowText","ppi","i")
  @@metrics=Win32API.new("user32","GetSystemMetrics","l","l")
  @@handle=find.call("RGSS Player",0)
  
  @@rect=[640,480]  
  
  #----------------------------------------------
  # Redimensionne l'écran
  #   - width: largeur de l'écran
  #   - height: hauteur de l'écran
  #----------------------------------------------
  def self.resize_screen(width,height)
    # Correction pour que la zone de jeu soit à la base de la redimension
    width *= $ZOOM
    width += 6
    height *= $ZOOM
    height += 29
    # On centre
    x = (@@metrics.call(0)/2) - (width/2)
    y = (@@metrics.call(1)/2) - (height/2)
    # On redimensionne
    @@pos.call(@@handle,0,x,y,width,height,0)
    # On met à jour les dimensions stockées dans la classe
    @@rect=[width,height]
  end #END def
  
  #----------------------------------------------
  # Retourne la largeur de l'écran
  #----------------------------------------------
  def self.width
    return @@rect[0]
  end #END def
  
  #----------------------------------------------
  # Retourne la hauteur de l'écran
  #----------------------------------------------
  def self.height
    return @@rect[1]
  end #END def
  
  #----------------------------------------------
  # Retourne le zoom actuel
  #----------------------------------------------
  def self.zoom
    return $ZOOM
  end #END def
  
  #----------------------------------------------
  # Modifie le zoom actuel
  #   - z: zoom (1.0 = 2x2px)
  #----------------------------------------------
  def self.zoom=(z)
    $ZOOM = z
    resize_screen(CONFIG::RESOLUTION[0],CONFIG::RESOLUTION[1])
  end #END def
  
  #----------------------------------------------
  # Modifie le titre de la fenêtre
  #----------------------------------------------
  def self.title=(t)
    @@stext.call(WindowHandle,t)
  end #END def
  
  #----------------------------------------------
  # Renvoie le titre de la fenêtre
  #----------------------------------------------
  def self.title
    @@gtext.call(WindowHandle,t="",50)
    return t
  end #END def
  
  #----------------------------------------------
  # Attendre durant un certain nombre de frames
  #   - frame: nombre de frames à attendre
  #----------------------------------------------
  def self.wait(frame)
    frame.times do 
      Graphics.update
    end
  end #END def
  
  def self.load(folder,filename,script=nil)
    
    if (script == nil)
      return Bitmap.new("Graphics/#{folder}/#{filename}")
    else
      if(FileTest.exist?("Graphics/#{folder}/#{script}/#{filename}"))
        return Bitmap.new("Graphics/#{folder}/#{script}/#{filename}")
      elsif(FileTest.exist?("Modules/#{script}/Graphics/#{folder}/#{filename}"))
        return Bitmap.new("Graphics/#{folder}/#{script}/#{filename}")
      else
        return Bitmap.new("Graphics/#{folder}/#{filename}")
      end        
    end
    
  end
  
end #END module Graphics

begin
  # Initialisation du module, ne pas toucher
  Graphics.zoom = $ZOOM
  Graphics.frame_rate = CONFIG::FRAME_RATE
end