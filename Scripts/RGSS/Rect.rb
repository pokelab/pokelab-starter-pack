class Rect
  
  def contains?(x,y=0)
    if x.is_a? Integer
      return (x >= self.x && x <= self.x + self.width && y >= self.y && y <= self.y + self.height)
    elsif x.is_a? Point
      return (x.x >= self.x && x.x <= self.x + self.width && x.y >= self.y && x.y <= self.y + self.height)
    end
    return false
  end

end