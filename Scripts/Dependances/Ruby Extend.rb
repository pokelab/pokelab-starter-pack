=begin
#========================================================
# Extension des classes de Ruby - Ŧž.A & Krosk
# Ajout de fonctions: 
#   - Vérification rapide des valeurs d'un array
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code sans crédits tant que vous ne vous l'attribuez
# pas entièrement.
# -------------------------------------------------------
# Les contributions de Krosk sont signalées par KROSK
# dans la description de la fonction.
# Le reste des fonctions est de Ŧž.A.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

class Array
  
  #----------------------------------------------
  # Renvoie true si toutes les valeurs du tableau
  # sont égales à true
  #----------------------------------------------
  def is_true?
    self.each do |e|
      return false if e != true
    end
    return true
  end #END def
  
  #----------------------------------------------
  # Renvoie false si toutes les valeurs du tableau
  # sont égales à false
  #----------------------------------------------
  def is_false?
    self.each do |e|
      return false if e != false
    end
    return true
  end #END def
  
  #----------------------------------------------
  # Renvoie la somme de toutes les valeurs de
  # l'array
  #   - i: valeur de départ
  #----------------------------------------------
  def sum(i=0)
    self.each { |n| i += n.to_f }
    return i.to_f
  end #END def
   
  #----------------------------------------------
  # Renvoie la somme de toutes les valeurs 
  # arrondies de l'array
  #   - i: valeur de départ
  #----------------------------------------------
  def sum_i(i=0)
    self.each { |n| i += n.to_f } 
    return (i.round)
  end #END def
  
  #----------------------------------------------
  # Mélange les valeurs de l'array 
  # KROSK
  #----------------------------------------------
  def shuffle
    sort_by { rand }
  end #END def

  #----------------------------------------------
  # Mélange les valeurs de l'array (destructive)
  # KROSK
  #----------------------------------------------
  def shuffle!
    self.replace shuffle
  end #END def
  
  #----------------------------------------------
  # Inverse les clés et valeurs du tableau
  # KROSK
  #----------------------------------------------
  def switch(liste)
    tab = self.clone
    liste.each {  |key, value|  self[value] = tab[key]  }
  end
  
end #END class Array

class Fixnum
  
  #----------------------------------------------
  # Retourne le nombre sous forme de String 
  # agrémenté de "0" devant si besoin est
  #   - z: nombre minimal de chiffres voulus
  #----------------------------------------------
  def min_digits(z)
    return (self.to_i < 10 ** z) ? "0" * ((10 ** z).to_s.size - self.to_s.size - 1) + self.to_s : self.to_s
  end#END def
  
  #----------------------------------------------
  # Retourne la marque de pluriel si le nombre est
  # différent de 1 ou -1.
  #   - l: marque du pluriel
  #----------------------------------------------
  def plurial(l='s')
    return l unless self.abs == 1
  end #END def
  
  #----------------------------------------------
  # Fait un produit en croix: 
  #     retourne valeur / p * n.
  #----------------------------------------------
  def percent(p=100,n=100)
    return self.to_f / p.to_f * n.to_f
  end #END def
  alias cross percent
   
end #END class Fixnum
 
class Float
  
  #----------------------------------------------
  # Retourne le nombre de chiffres après la virgule
  #----------------------------------------------
  def float_digits
    return self.to_s.gsub(/[0-9]+.([0-9]+)/, '\1').size
  end #END def
  alias d_dig float_digits
   
  #----------------------------------------------
  # Renvoie une chaîne avec un nombre spécifié de 
  # chiffres après la virgule
  #   - d: nombre de chiffres après la virgule
  #----------------------------------------------
  def set_float_digits(d=2)
    return (float_digits < d) ? self.to_s + "0" * (d - float_digits) : self.round_digits(d).to_s
  end #END def
  
  #----------------------------------------------
  # Renvoie la partie décimale
  #   - i: si true, renvoie la partie décimale
  #        sous forme d'entier
  #----------------------------------------------
  def decimal_part(i=false)
    return (i) ? self.to_s.gsub(/[0-9]+.([0-9]+)/, '\1').to_i : self.to_s.gsub(/[0-9]+.([0-9]+)/, '\1').to_f / (10**float_digits)
  end #END def
  alias d decimal_part
   
  def d_as_i
    return decimal_part(true)
  end #END def
   
  #----------------------------------------------
  # Renvoie l'arrondi à un nombre de chiffres
  # défini
  #   - d: nombre de chiffres
  #----------------------------------------------
  def round_digits(d=2)
    (self*(10**d)).round / (10**d).to_f
  end #END def
  alias round_d round_digits
   
  #----------------------------------------------
  # Renvoie la marque du pluriel s'il y a lieu
  #   - s: marque du pluriel
  #----------------------------------------------
  def plurial(l='s')
    return l unless self.abs <= 1 && self.abs > 0
  end #END def
     
  #----------------------------------------------
  # Fait un produit en croix: 
  #     retourne valeur / p * n.
  #----------------------------------------------
  def percent(p=100,n=100)
    return self / p.to_f * n.to_f
  end #END def
  alias cross percent
   
end #END class Float
 
class String
  
  #----------------------------------------------
  # Retourne le mot agrémenté d'une marque de 
  # pluriel si besoin est.
  #   - n: nombre entier
  #   - s: marque du pluriel
  #----------------------------------------------
  def plurialize(n,l="s")
    return (n.abs != 1) ? self + l : self
  end #END def
  
  #----------------------------------------------
  # Retourne le mot agrémenté d'une marque de 
  # pluriel si besoin est. (destructive)
  #   - n: nombre entier
  #   - s: marque du pluriel
  #----------------------------------------------
  def plurialize!(n,l="s")
    replace(plurialize(n,l="s"))
  end #END def
   
  #----------------------------------------------
  # Remplace les espaces.
  #   - r: chaîne de remplacement
  #----------------------------------------------
  def slugify(r="-")
    self.gsub(" ",r)
  end #END def
   
  #----------------------------------------------
  # Remplace les espaces. (destructive)
  #   - r: chaîne de remplacement
  #----------------------------------------------
  def slugify!(r="-")
    replace(slugify(r))
  end #END def
   
  #----------------------------------------------
  # Tronque la chaîne après n caractères
  #   - s: chaîne finale
  #----------------------------------------------
  def truncate(n,s="")
    self.slice(0,n) + s
  end #END def
   
  #----------------------------------------------
  # Tronque la chaîne après n caractères (destructive)
  #   - s: chaîne finale
  #----------------------------------------------
  def truncate!(n,s="")
    replace(truncate(n,s))
  end #END def
   
  #----------------------------------------------
  # Entoure la chaîne de deux chaînes identiques
  #   - s: chaîne à ajouter
  #----------------------------------------------
  def wrap(s)
    s+self+s
  end #END def
   
  #----------------------------------------------
  # Entoure la chaîne de deux chaînes identiques 
  # (destructive)
  #   - s: chaîne à ajouter
  #----------------------------------------------
  def wrap!(s)
    replace(wrap(s))
  end #END def
   
  #----------------------------------------------
  # Supprime les caractères superflus (par défaut,
  # les espaces)
  #   - s: chaîne à supprimer lorsque nécessaire
  #----------------------------------------------
  def trim(s=" ")
    self.gsub(/^#{s}*/, "").gsub(/#{s}*$/,"").gsub(/#{s}{2,}/, s)
  end #END def

  #----------------------------------------------
  # Supprime les caractères superflus (par défaut,
  # les espaces) (destructive)
  #   - s: chaîne à supprimer lorsque nécessaire
  #----------------------------------------------
  def trim!(s=" ")
    replace(trim(s))
  end #END def

end #END class String

class Integer
  
  #----------------------------------------------
  # Renvoie le signe du nombre
  #----------------------------------------------
  def sgn
    return (self >= 0) ? 1 : -1;
  end #END def
  
  #----------------------------------------------
  # Renvoie true si le nombre est pair
  # KROSK
  #----------------------------------------------
  def even?
    return self % 2 == 0
  end #END def
  
  #----------------------------------------------
  # Renvoie true si le nombre est impair
  # KROSK
  #----------------------------------------------
  def odd?
    return self % 2 == 1
  end #END def
end
=end