  # --------------------------------------------------------
  # error_handler
  # --------------------------------------------------------

module EXC
  def self.error_handler(exception, file_arg = nil)
    if exception.type == SystemExit
      return
    end
    
    if exception.message == "" # Reset
      raise
    end
    
    # Sauvegarde de secours
    if $game_player != nil
      file = File.open("SaveAuto.rxdata", "wb")
      characters = []
      for i in 0...$game_party.actors.size
        actor = $game_party.actors[i]
        characters.push([actor.character_name, actor.character_hue])
      end
      Marshal.dump(characters, file)
      Marshal.dump(Graphics.frame_count, file)
      $game_system.save_count += 1
      $game_system.magic_number = $data_system.magic_number
      $game_system.reset_interpreter
      Marshal.dump($game_system, file)
      Marshal.dump($game_switches, file)
      Marshal.dump($game_variables, file)
      Marshal.dump($game_self_switches, file)
      Marshal.dump($game_screen, file)
      Marshal.dump($game_actors, file)
      Marshal.dump($game_party, file)
      Marshal.dump($game_troop, file)
      Marshal.dump($game_map, file)
      Marshal.dump($game_player, file)
      Marshal.dump($read_data, file)
      Marshal.dump($pokemon_party, file)
      Marshal.dump($random_encounter, file)
      Marshal.dump($data_pokedex, file)
      Marshal.dump($data_storage, file)
      Marshal.dump($battle_var, file)
      Marshal.dump($existing_pokemon, file)
      Marshal.dump($string, file)
      file.close
    end
    
    script = load_data("Data/Scripts.rxdata")
    
    source = script[exception.backtrace[0].split(":")[0].sub("Section", "").to_i][1]
    source_line = exception.backtrace[0].split(":")[1]
    
    if file_arg != nil
      file = file_arg
      source = file.path
    end
    if source == "Interpreter Bis" and source_line == "444"
      source = "évènement"
    end
    if exception.backtrace[0].split(":")[0].sub("Section", "").to_i == 0 and source_line == 1
      source = "évènement"
    end
      
    Console.debug("\n", false)
    Console.error("Erreur dans #{(source != 'évènement') ? 'le script' + source : 'un event'}, inspectez le rapport Log.txt.")
    
    logfile = File.open("Log.txt", "w")
    
    # Entete
    logfile.write("---------- Erreur de script : #{source} ----------\n")
    Console.debug("\n", false)
    Console.debug("---------- Erreur de script : #{source} ----------", false)
    
    # Type
    logfile.write("----- Type\n")
    Console.debug("----- Type",false)
    logfile.write(exception.type.to_s + "\n\n")
    Console.debug(exception.type.to_s + "\n",false)
    
    # Message
    logfile.write("----- Message\n")
    Console.debug("----- Message", false)
    if exception.type == NoMethodError
      logfile.write("- ARGS - #{exception.args.inspect}\n")
      Console.debug("- ARGS - #{exception.args.inspect}", false)
    end
    logfile.write(exception.message + "\n\n")
    Console.debug(exception.message + "\n", false)
    
    # Position en fichier
    if file_arg != nil
      logfile.write("----- Position dans #{file.path}\n")
      Console.debug("----- Position dans #{file.path}\n", false)
      logfile.write("Ligne #{file.lineno}\n")
      Console.debug("Ligne #{file.lineno}\n", false)
      logfile.write(IO.readlines(file.path)[file.lineno-1] + "\n")
      Console.debug(IO.readlines(file.path)[file.lineno-1] + "\n", false)
    elsif source == "évènement"
      logfile.write("----- Position de l'évènement\n")
      Console.debug("----- Position de l'event\n", false)
      logfile.write($running_script + "\n\n")
      Console.debug($running_script + "\n\n", false)
    else
      logfile.write("----- Position dans #{source}\n")
      Console.debug("----- Position dans #{source}\n", false)
      logfile.write("Ligne #{source_line}\n\n")
      Console.debug("Ligne #{source_line}\n\n", false)
    end
    
    # Backtrace
    logfile.write("----- Backtrace\n")    
    Console.debug("----- Backtrace", false)   
    for trace in exception.backtrace
      location = trace.split(":")
      script_name = script[location[0].sub("Section", "").to_i][1]
      script_name = "event" if location[0].sub("Section", "").to_i == 0
      logfile.write("Script : #{script_name} | Ligne : #{location[1]}")
      Console.debug("Script : #{script_name} | Ligne : #{location[1]}", false)
      if location[2] != nil
        logfile.write(" | Méthode : #{location[2]}")
        Console.debug(" | Method : #{location[2]}", false)
      end
      logfile.write("\n")
    end
    logfile.close
    
    raise
  end
end