#========================================================
# Extension des classes de Ruby - Ŧž.A & Krosk
# Ajout de fonctions: 
#   - Vérification rapide des valeurs d'un array
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code sans crédits tant que vous ne vous l'attribuez
# pas entièrement.
# -------------------------------------------------------
# Les contributions de Krosk sont signalées par KROSK
# dans la description de la fonction.
# Le reste des fonctions est de Ŧž.A.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

class Integer
  
  def min_digits(z)
    return (self.to_i < 10 ** z) ? "0" * ((10 ** z).to_s.size - self.to_s.size - 1) + self.to_s : self.to_s
  end
  
end