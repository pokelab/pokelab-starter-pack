module POKEMON_S
  
  module CAPTURE
    
    def self.capture_calculation(pkmn, ball)
      unless pkmn.is_a? POKEMON_S::Pokemon_Actor
        Console.error("Le pokémon spécifié est incorrect.", true, caller.first.split(":"))
        return
      end
      return (((3*pkmn.stats[:hp] - 2*pkmn.battle_stats[:hp]) * get_grass_modifier * pkmn.rareness * ball.bonus) / (3*pkmn.stats[:hp])) * get_status_modifier * get_oaura_modifier
    end
    
    
    def self.critical_capture(id, ball)
      e
    end
    
    def self.get_grass_modifier
      return 0.3
      GRASS_MODIFIER.each_with_index do |g,i|
        return GRASS_MODIFIER[i][:value] if GRASS_MODIFIER[i][:max] > $pokemon_party.dex[:national].caught
      end
      return GRASS_MODIFIER[0][:value]
    end
    
    def self.get_status_modifier(status)
      return 1
    end
    
    def self.get_oaura_modifier
      return 1
    end
    
  end
  
end