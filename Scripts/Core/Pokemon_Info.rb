#========================================================
# Définition d'un Pokémon - Ŧž.A
# @@@ D'après le travail de Krosk @@@
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code sans crédits tant que vous ne vous l'attribuez
# pas entièrement.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module POKEMON_S
  
  class Pokemon_Info
    
    def self.name(id)
      return $data_enemies[id].name.capitalize
    end
    
    def self.id(id)
      return $data_enemies[id].maxsp
    end
    
  end
end