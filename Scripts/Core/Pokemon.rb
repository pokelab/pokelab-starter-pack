#========================================================
# Définition d'un Pokémon - Ŧž.A
# @@@ D'après le travail de Krosk @@@
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code sans crédits tant que vous ne vous l'attribuez
# pas entièrement.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module POKEMON_S
  
  #========================================================
  # Classe Pokemon
  # -------------------------------------------------------
  # Fonctionnement général
  # Il s'agit principalement d'une classe de stockage.
  # Elle contient les informations de base d'un Pokémon
  # qui sont tirées de la BDD.
  # -------------------------------------------------------
  # Pour accéder aux informations des Pokémon:
  # $pokemon[id] -> renvoie un objet Pokemon correspondant
  # -------------------------------------------------------
  # Classes qui héritent de Pokemon:
  #   - Pokemon_Actor: représente un Pokémon en jeu
  #                    (équipe, adversaire, sauvage)
  #========================================================
  class Pokemon
    
    #----------------------------------------------
    # Attributs de base d'un Pokémon
    #----------------------------------------------
    
    # Informations basiques
    attr_reader :id             # Id national du Pokémon          (int)
    attr_reader :dex            # Id des dex régionaux            (hash)
                                # {:dex1 => 123, :dex2 => 678}
    attr_reader :name           # Nom générique de l'espèce       (str)
    
    # Fichiers
    attr_reader :cry            # Fichier du cri                  (str)
                                # nil si inexistant
    attr_reader :icon           # Fichier de l'icône              (str)
    
    # Statistiques et attaques
    attr_reader :base_stats     # Stats de base                   (hash)
                                # {:pv, :at, :df, :s_at, :s_df, :vt}
    attr_reader :base_xp        # Expérience de base              (int)
    attr_reader :moveset        # Liste des attaques possibles    (arr)
    attr_reader :capsules       # Liste des CT/CS compatibles     (arr)
    attr_reader :moves_tutor    # Liste des attaques donneur capa (arr)
                                # [id, id, id]
    attr_reader :breed_moves    # Liste des attaque par reproduct (arr)
                                # [id, id, id]
    attr_reader :description    # Description de l'espèce         (str)
    attr_reader :exp_type       # Type expérience                 (sym)
    attr_reader :type1          # Premier type                    (sym) 
    attr_reader :type2          # Second type                     (sym)
                                # nil si aucun type
    attr_reader :evolve_list    # Liste des évolutions            (arr)
    attr_reader :ev_table       # EVs donnés par le Pokémon       (arr)
    attr_reader :abilities      # Listes des capacités possibles  (arr)
    attr_reader :rareness       # Rareté
    attr_reader :forms          # Liste des formes du Pokémon     (arr)
                                # [id, id, id]
    attr_reader :color          # Couleur du Pokémon              (sym)
    attr_reader :footprint      # Empreinte du Pokémon            (str)
    attr_reader :shape          # Forme (aspect) du Pokémon       (sym)
    attr_reader :egg_group      # Groupes d'oeuf                  (arr)
    attr_reader :mega_id        # Id des Méga dans la BDD         (arr)
                                # [{:id => 123, :obj => 123}]
    attr_reader :primo_id       # Id de la Primo dans la BDD      (int)
    
    #----------------------------------------------
    # Constructeur
    # Pokemon.new(id, name)
    #   - id: id de l'espèce
    #   - name: nom de l'espèce
    #----------------------------------------------
    def initialize(id, name)
      @id = id
      @name = name
      @dex = {}
      @cry = "SE/Cries/#{id.min_digits(3)}Cry.wav"
      @icon = "Battlers/Icons/#{id.min_digits(3)}"
      @base_stats = {:pv => 0, :at => 0, :df => 0, :s_at => 0, :s_df => 0, :vt => 0}
      @moveset = []
      @description = ""
      @exp_type = :linear
      @type1 = :normal
      @type2 = nil
      @evolve_list = []
      @ev_table = [0,0,0,0,0,0]
      @abilities = []
      @rareness = 255
      @forms = []
      @color = :black
      @footprint = "Battlers/Footprints/#{id.min_digits(3)}"
      @shape = nil
      @egg_groupe = nil
      @mega_id = []
      @primo_id = 0
    end #END def
    
    #----------------------------------------------
    # Régler les numéros dans les dex régionaux
    #   - hash: hash contenant les numéros
    #           {:dex1 => 123, :dex2 => 678}
    #           (nil et 0 font disparaître le Pokémon
    #           du dex correspondant)
    #----------------------------------------------
    def dex=(hash)
      @dex = hash
    end #END def
    
    #----------------------------------------------
    # Régler le fichier du cri
    #   - str: chemin du fichier
    #          (laisser à nil pour une génération
    #          automatique)
    #----------------------------------------------
    def cry=(str = nil)
      @cry = (str == nil) ? "SE/Cries/#{id.min_digits(3)}Cry.wav" : str
    end #END def
    
    #----------------------------------------------
    # Régler le fichier de l'icône
    #   - str: chemin du fichier
    #          (laisser à nil pour une génération
    #          automatique)
    #----------------------------------------------
    def icon=(str = nil)
      @icon = (str == nil) ? "Battlers/Icons/#{id.min_digits(3)}" : str
    end #END def
    
    
    #----------------------------------------------
    # Régler les base stats
    #   - hash: hash contenant les statistiques
    #          de base
    #          {:pv, :at, :df, :s_at, :s_df, :vt}
    #----------------------------------------------
    def base_stats=(hash)
      
      # On vérifie que le tableau est correct
      if hash.all? {|s| @base_stats.key? s}
        Console.warning("Le tableau de base stats est invalide", true, caller.first.split(":")) 
        return
      end #END if arr.length != 6
      
      # On ajoute les valeurs
      hash.each do |stat, val|
        @base_stats[stat] = val.to_i.abs # On fait bien attention de ne mettre que des entiers positifs
      end #END each
      
    end #END def
    
    #----------------------------------------------
    # Régler le moveset
    # NON UTILISABLE
    #----------------------------------------------
    def moveset=(arr)
      @moveset = arr
    end #END def
    
    def rareness=(int)
      if(!int.is_a? Fixnum)
        Console.warning("La rareté est invalide: #{int} n'est pas un nombre entier", true, caller.first.split(":")) 
        return
      elsif int < 0
        Console.warning("La rareté est invalide: elle ne peut pas être négative.", true, caller.first.split(":")) 
        return
      end
      @rareness = int
    end
    
    def base_xp=(int)
      if(!int.is_a? Fixnum)
        Console.warning("La base xp est invalide: #{int} n'est pas un nombre entier", true, caller.first.split(":")) 
        return
      elsif int < 0
        Console.warning("La base xp est invalide: elle ne peut pas être négative.", true, caller.first.split(":")) 
        return
      end
      @base_xp = int
    end
    
    def type1=(sym)
      unless TYPES.keys.include? sym
        Console.error("Le type spécifié (#{sym}) n'existe pas.", true, caller.first.split(":"))
        return
      end
      @type1 = sym
    end
    
    def type2=(sym)
      unless TYPES.keys.include? sym
        Console.error("Le type spécifié (#{sym}) n'existe pas.", true, caller.first.split(":"))
        return
      end
      @type2 = sym
    end
    
    def shape=(sym)
      unless CONFIG::SHAPES.keys.include? sym
        Console.error("La forme du corps spécifiée (#{sym}) n'existe pas.", true, caller.first.split(":"))
        return
      end
      @shape = sym
    end
    
    def color=(sym)
      unless CONFIG::COLORS.keys.include? sym
        Console.error("La couleur spécifiée (#{sym}) n'existe pas.", true, caller.first.split(":"))
        return
      end
      @color = sym
    end
    
  end #END class Pokemon
  
end #END module POKEMON_S