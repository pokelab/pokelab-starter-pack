#========================================================
# Définition d'un Pokémon Jouable - Ŧž.A
# @@@ D'après le travail de Krosk @@@
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code sans crédits tant que vous ne vous l'attribuez
# pas entièrement.
#========================================================
# Il est fortement conseillé de ne pas toucher à ces 
# fonctions étant donné que la plupart des scripts du kit
# en dépendent.
#========================================================

module POKEMON_S
  
  class Pokemon_Actor < Pokemon
    
    #----------------------------------------------
    # Attributs
    #----------------------------------------------
    
    # Définition basique
    attr_accessor :trainer_id   # ID dresseur             (str)
    attr_accessor :trainer_name # Nom du dresseur         (str)
    attr_accessor :given_name   # Nom donné au Pokémon    (str) 
    attr_accessor :level        # Niveau                  (int)
    attr_accessor :exp          # Expérience              (int)
    attr_accessor :moves        # Attaques connues        (arr)
    attr_accessor :ev           # Effort Value            (arr)
    attr_accessor :ability      # Talent                  (int)
    
    # Caractéristiques
    attr_reader :code           # Code unique             (str)
    attr_reader :iv             # IV                      (arr)
    attr_reader :nature         # Nature                  (sym)
    attr_reader :shiny          # Est shiny ?             (bool)
    attr_reader :gender         # Genre                   (sym)
                                # :male/:female/:asexual
                                
    # Fichiers
    attr_reader :battler_face   # Fichier du battler face (str)
    attr_reader :battler_back   # Fichier du battler face (str)
    
    # Statistiques et statut
    attr_accessor :stats        # Statistiques            (hash)
                                # [:pv, :att, :def, :s_att, :s_def, :vit]
    attr_accessor :status       # Statut                  (sym)
                                # si aucun, :none
    attr_accessor :status_count # Tours du statut         (int)
    attr_accessor :confused     # Est confus ?            (bool)
    attr_accessor :state_count  # Tours de confusion      (int)
    attr_accessor :flinch       # Est appeuré ?           (bool)
    attr_accessor :effects      # Effets en combat        (arr)
                                # [[:effet, data(int)]]
    attr_accessor :ability_active # Talent activé ?       (bool)
    attr_accessor :ability_token # Donnée talent          (int)
    attr_accessor :item_hold    # Id de l'objet tenu      (int)
    attr_accessor :loyalty      # Bonheur                 (int)
    attr_accessor :stats_battle # Stats modifiées combat  (hash)
    attr_accessor :ball         # Id de la ball utiliée   (int)
    attr_accessor :steps        # Pas avant éclosion      (int)
                                # Si != 0, c'est un oeuf
    attr_accessor :form         # Id forme alternative    (int)
    attr_accessor :mega         # Méga évolution          (bool)
    attr_accessor :primo        # Primo résurgence        (bool)
    attr_accessor :cap_z        # Capacité z              (bool)
    
    def initialize(e)
      
      @id = e
      
    end
    
  end #END class Pokemon_Actor

end #END module POKEMON_S