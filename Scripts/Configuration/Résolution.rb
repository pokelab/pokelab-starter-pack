#========================================================
# Module Config - Ŧž.A
# Gestion du zoom initial, de la résolution et du framerate
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Ces paramètres sont librement modifiables.
#========================================================

module CONFIG
    
  # Zoom initial du jeu
  # 0.5 - 1x1px
  # 1.0 - 2x2px
  # Tous zooms possibles
  $ZOOM = 0.5
  # Résolution du jeu pour $ZOOM = 1
  RESOLUTION = [640, 480]
  # Framerate de base
  FRAME_RATE = 60
    
end