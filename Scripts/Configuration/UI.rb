#========================================================
# Module Config - Ŧž.A
# Gestion des skins par défaut
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Ces paramètres sont librement modifiables.
#========================================================

module CONFIG
    
  #----------------------------------------------
  # Skin des fenêtres par défaut
  # ["fichier.png", [bordures (gauche, haut, droite, bas)]]
  # Les fichiers sont dans le dossier Windowskin
  #----------------------------------------------
  BASE_WINDOW_SKIN = ["message.png",[16,16,16,16]]
  
  SKINS = {
    :box => ["message.png",[16,16,16,16]],
    :button => ["button.png",[16,16,16,16]],
    :selector => ["selector.png",[16,16,16,16]]
  }
  
  #----------------------------------------------
  # Skin de curseur par défaut
  # Les fichiers sont dans le dossier Windowskin
  #----------------------------------------------
  BASE_CURSOR = "message_cursor.png"
  
  #----------------------------------------------
  # Liste des touches par défaut pour l'UI
  #----------------------------------------------
  ACTION_KEYS = {
    :confirm => [0x0D,0x20,0x41], # Entrer/Espace/A
    :cancel => [0x1B] # Echap
  }

end