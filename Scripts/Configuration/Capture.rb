#========================================================
# Module Config - Ŧž.A
# Gestion des paramètres de capture des Pokémon
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Ces paramètres sont librement modifiables.
#========================================================

module CONFIG
    
  ENABLE_CRITICAL_CAPTURE = true
  
  POKEDEX_MODIFIER = [
    {:min => 0,     :max => 30,     :value => 1229/4096},
    {:min => 31,    :max => 150,    :value => 2048/4096},
    {:min => 151,   :max => 300,    :value => 2867/4096},
    {:min => 301,   :max => 450,    :value => 3277/4096},
    {:min => 451,   :max => 600,    :value => 3686/4096},
    {:min => 601,   :max => nil,    :value => 4096/4096},
  ]
  
  ENABLE_GRASS_MODIFIER = true
  
  GRASS_MODIFIER = [
    {:min => 0,     :max => 30,     :value => 0},
    {:min => 31,    :max => 150,    :value => 0.5},
    {:min => 151,   :max => 300,    :value => 1},
    {:min => 301,   :max => 450,    :value => 1.5},
    {:min => 451,   :max => 600,    :value => 2},
    {:min => 601,   :max => 9999,   :value => 2.5},
  ]
  
end #END module CONFIG