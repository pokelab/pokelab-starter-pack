#========================================================
# Module Config - Ŧž.A
# Gestion des polices par défaut
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Ces paramètres sont librement modifiables.
#========================================================

module CONFIG
    
  #----------------------------------------------
  # Liste des polices utilisées dans le jeu
  #----------------------------------------------
  # :nom_de_la_police => Font.new("Police", taille)
  # Séparer les polices par des virgules
  #----------------------------------------------
  FONTS = {
    :normal => Font.new("Pokemon BW", 24),
    :big => Font.new("Pokemon BW", 48)
  }
  
  #----------------------------------------------
  # Police par défaut
  #----------------------------------------------
  DEFAUT_FONT = :normal
  
  # Ne pas toucher
  Font.default_name = FONTS[:normal].name
  Font.default_size = FONTS[:normal].size

end