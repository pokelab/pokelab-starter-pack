#========================================================
# Module Config - Ŧž.A
# Gestion des paramètres de debug
# -------------------------------------------------------
# Vous êtes libres de distribuer, modifier et partager 
# ce code avec crédits en dehors de toute utilisation
# commerciale.
#========================================================
# Ces paramètres sont librement modifiables.
#========================================================

module CONFIG
    
  #----------------------------------------------
  # Décide de la manière dont la BDD est compilée
  #   - :auto -> Compile à chaque début de jeu si
  #              le jeu est lancé depuis l'éditeur
  #              (recommandé)
  #   - :manual -> Compile seulement depuis le 
  #                menu Debug (F9)
  #   - :always -> Compile à chaque début de jeu
  #                (fortement déconseillé)
  #----------------------------------------------
  # Le processus de compilation consiste à fusionner
  # les données de la BDD de RPG Maker XP et les 
  # données entrées dans la partie 'Données' des
  # scripts.
  #----------------------------------------------
  BDD_COMPILATION = :auto
  
end