# PokéLab Starter Pack
Avancement en cours. Pas de sortie stable prévue actuellement.

Veuillez noter que le code est sous GPL (voir Licence), mais que l'utilisation commerciale du code et des ressources du kit n'est pas autorisée étant donné que Pokémon ne nous appartient aucunement.

__En utilisant ce starter pack, vous vous engagez à ne nuire en aucune manière, que ce soit commercialement ou idéologiquement, à Game Freak, Nintendo ou The Pokémon Company ainsi qu'à soutenir les sorties officielles de jeux et de produits dérivés.__

Notez que les builds preview sont disponibles seulement dans les branches versionnées. Ne cherchez pas dans le master. Sachez également que le master contient principalement des scripts non-fonctionnels.